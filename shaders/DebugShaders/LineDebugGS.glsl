#version 440

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec4 v_endPos[];

void main()
{
	gl_Position = gl_in[0].gl_Position;
	EmitVertex();
	
	gl_Position = v_endPos[0];
	EmitVertex();
		
	EndPrimitive();
}