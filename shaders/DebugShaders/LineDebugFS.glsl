#version 440

uniform vec4 gColor;

out vec4 color;

void main()
{
	color = gColor;
}