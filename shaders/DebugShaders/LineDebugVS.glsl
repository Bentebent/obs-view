#version 440

layout ( location = 0 ) in vec3 startPos;
layout ( location = 1 ) in vec3 endPos;

uniform mat4 gViewMatrix;
uniform mat4 gProjectionMatrix;

out vec4 v_endPos;

void main()
{
	mat4 viewProj = gProjectionMatrix * gViewMatrix;
	v_endPos = viewProj * vec4(endPos, 1.0f);
	gl_Position = viewProj * vec4(startPos, 1.0);
}