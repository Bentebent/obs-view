#version 440
#extension GL_ARB_shader_draw_parameters : require
#extension GL_ARB_shader_storage_buffer_object : require

struct InstanceData
{
	mat4 transform;
	mat4 prevTransform;
	vec4 albedo_specular_acr_normal;
	vec4 disp_emi_pad_pad;
};

layout (std140, binding = 0) buffer CB0
{
    InstanceData Transforms[];
};

uniform mat4 gView;
uniform mat4 gProjection;
uniform mat4 gModel;
uniform uint drawID;

layout ( location = 0 ) in vec4 positionIN;
layout ( location = 1 ) in vec4 normalIN;
layout ( location = 2 ) in vec4 tangentIN;
layout ( location = 3 ) in vec2 uvIN;

out vec4 normalOut;
out vec4 tangenOut;
out vec2 uvOut;
out flat int did;
void main()
{
	mat4 viewProj = gProjection * gView;
	gl_Position = viewProj * Transforms[gl_DrawIDARB].transform * positionIN;
	normalOut = Transforms[gl_DrawIDARB].transform * normalIN;
	uvOut = uvIN;
	did = gl_DrawIDARB;
	//gl_Position = viewProj * Transforms[drawID] * positionIN;
	//gl_Position = viewProj * positionIN;
}