#version 440
#extension GL_NV_bindless_texture : require
#extension GL_NV_gpu_shader5 : require 

//uniform sampler2D t;

in vec4 normalOut;
in vec4 tangenOut;out vec4 color;
in vec2 uvOut;
in flat int did;
uniform sampler2D t;
uniform sampler2D handle;

struct InstanceData
{
	mat4 transform;
	mat4 prevTransform;
	vec4 albedo_specular_acr_normal;
	vec4 disp_emi_pad_pad;
};

layout (std140, binding = 0) buffer CB0
{
    InstanceData Transforms[];
};



layout (std430, binding = 1) buffer CB1
{
    sampler2D handles[];
};

//uniform uvec2 handle;
//value = texture(sampler2D(handle), texCoord);

void main()
{
	//color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//color = normalOut;//vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//color = texture(t, uvOut);
	//color = vec4(uvOut, 0.0f, 1.0f);
	color = texture((handles[int(Transforms[did].albedo_specular_acr_normal.z)]), uvOut);
	//color = texture((handles[1]), uvOut);
}