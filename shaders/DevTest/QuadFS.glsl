#version 430

uniform sampler2D gTexture;

out vec4 color;

in vec2 uv;

void main()
{
	#ifdef DRAW_UV
		color = vec4(uv, 0.0f, 1.0f);
	#endif

	#ifdef DRAW_RED
		color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	#endif
	color = texture(gTexture, uv);
}