#version 430

uniform sampler2D gTexture;
uniform float gNearZ;
uniform float gFarZ;
out vec4 color;

in vec2 uv;

float LinearizeDepth(float zoverw)
{
		float n = gNearZ; // camera z near
		float f = gFarZ; // camera z far
		return (2.0 * n) / (f + n - zoverw * (f - n));
}

void main()
{
	float d = texture(gTexture, uv).r;
	d = LinearizeDepth(d) * 5;
	color = vec4(d, d, d, 1.0f);
}