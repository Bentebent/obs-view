//#version 440
//
//void main()
//{
//	gl_Position = vec4( 1.0 );
//}

#version 420 core

//out VS_OUT
//{
//	vec3 tc;
//} vs_out;

out vec3 uv;

uniform mat4 gView;
uniform mat4 gProjection;

void main(void)
{
	float z = 1.0f;
	float x = 1.0f;
	float y = 1.0f;
	vec3[4] vertices = vec3[4](	vec3(-x, -y, z),
								vec3(x, -y, z),
								vec3(-x, y, z),
								vec3(x, y, z));

	mat4 inverseProjection = inverse(gProjection);
    mat3 inverseModelview = transpose(mat3(gView));
    vec3 unprojected = (inverseProjection * vec4(vertices[gl_VertexID], 1.0f)).xyz;
    vec3 eyeDirection = inverseModelview * unprojected;

	uv = eyeDirection;
	gl_Position = vec4(vertices[gl_VertexID], 1.0);
}