#version 440
#extension GL_NV_bindless_texture : require
#extension GL_NV_gpu_shader5 : require 

layout (std430, binding = 2) buffer CB2
{
    samplerCube cubeHandles[];
};

in vec3 uv;

layout ( location = 1 ) out vec4 gWSNormal_empty;
layout ( location = 2 ) out vec4 gAlbedo_decal;
layout ( location = 3 ) out vec4 gSpecular;
layout ( location = 4 ) out vec4 gRoughness_AO_velocity;
layout ( location = 5 ) out vec4 gEmissive;

uniform float gGamma;

void main(void)
{
	vec4 skyboxSample = texture(cubeHandles[2], -normalize(uv));
	skyboxSample.xyz = pow(skyboxSample.xyz, vec3(gGamma));
	gEmissive = skyboxSample;
}