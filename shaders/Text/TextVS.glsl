#version 440

layout ( location = 0 ) in vec4 pos_dim;
layout ( location = 1 ) in ivec4 uv;

uniform sampler2D textureAtlas;

out vec4 v_uv;
out vec2 v_dim;

void main()
{
	ivec2 texDim = textureSize(textureAtlas, 0);
	vec2 pos = pos_dim.xy;
	vec2 dim = pos_dim.zw;
	vec2 uv0 = uv.xy / vec2(texDim);
	vec2 uv1 = uv.zw / vec2(texDim);
	
	v_uv = vec4(uv0, uv1);
	v_dim = dim;
	gl_Position = vec4(pos, 0.0, 1.0);
}