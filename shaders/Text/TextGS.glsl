#version 440

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec4 v_uv[];
in vec2 v_dim[];

out vec2 g_uv;

void main()
{
	vec4 pos = gl_in[0].gl_Position;
	
	g_uv = vec2(v_uv[0].x, v_uv[0].w);
	gl_Position = pos + vec4(0.0f, v_dim[0].y, 0.0f, 0.0f);
	EmitVertex();
	
	g_uv = vec2(v_uv[0].x, v_uv[0].y);
	gl_Position = pos;
	EmitVertex();
	
	g_uv = vec2(v_uv[0].z, v_uv[0].w);
	gl_Position = pos + vec4(v_dim[0].x, v_dim[0].y, 0.0f, 0.0f);
	EmitVertex();	
	
	g_uv = vec2(v_uv[0].z, v_uv[0].y);
	gl_Position = pos + vec4(v_dim[0].x, 0.0f, 0.0f, 0.0f);
	EmitVertex();
		
	EndPrimitive();
}