#version 440

uniform sampler2D textureAtlas;
uniform vec4 fontColor;

in vec2 g_uv;

out vec4 color;

void main()
{
	color = vec4(fontColor.xyz, fontColor.a * texture(textureAtlas, g_uv).r);
}