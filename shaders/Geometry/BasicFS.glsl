#version 440
#extension GL_NV_bindless_texture : require
#extension GL_NV_gpu_shader5 : require 

//#define IBL_SPEC_OFF

in vec4 wsOut;
in vec4 positionOut;
in vec4 prevPositionOut;
in vec4 normalOut;
in vec4 tangenOut;
in vec2 uvOut;
in flat int drawID;


struct InstanceData
{
	mat4 transform;
	mat4 prevTransform;
	vec4 albedo_specular_acr_normal;
	vec4 disp_emi_pad_pad;
};

layout (std140, binding = 0) readonly buffer CB0
{
    InstanceData Transforms[];
};

layout (std430, binding = 1) buffer CB1
{
    sampler2D handles[];
};

layout (std430, binding = 2) buffer CB2
{
    samplerCube cubeHandles[];
};

layout ( location = 1 ) out vec4 gWSNormal_empty;
layout ( location = 2 ) out vec4 gAlbedo_decal;
layout ( location = 3 ) out vec4 gSpecular;
layout ( location = 4 ) out vec4 gRoughness_AO_velocity;
layout ( location = 5 ) out vec4 gEmissive;

uniform vec3 gEyePosition;
uniform float gGamma;

vec3 FresnelSchlick(vec3 specColour, vec3 l, vec3 m)
{
	return specColour + (1.0f - specColour) * pow(1.0f - clamp(dot(l, m), 0.0f, 1.0f), 5);
}

void main()
{
	//vec4 albedo = vec4(0.0f, 0.0f, 0.0f, 0.0f);//texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.x)]), uvOut);
	//vec4 specular = pow(vec4(255.0f / 255.0f, 219.0f / 255.0f, 147.0f / 255.0f, 1.0f), vec4(2.2f));//texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.y)]), uvOut);
	////vec4 specular = vec4(0.04, 0.04, 0.04, 1.0f);
	//float roughness = 0.8f;//acr.z;

	vec4 albedo = texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.x)]), uvOut);
	vec4 specular = texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.y)]), uvOut);
	float roughness = texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.z)]), uvOut).x;
	gAlbedo_decal = albedo;
	gWSNormal_empty = vec4(normalOut.xyz, 1.0f);//texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.w)]), uvOut);
	gSpecular = specular;

	albedo.xyz = pow(albedo.xyz, vec3(gGamma));
	specular.xyz = pow(specular.xyz, vec3(gGamma));
	//gEmissive = vec4(1.0f);//texture((handles[int(Transforms[drawID].disp_emi_pad_pad.y)]), uvOut);
	vec4 acr = texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.z)]), uvOut);
	//acr.z = roughness;

	// Calculate velocity vector
	vec2 a = (positionOut.xy		/ positionOut.w		) * 0.5 + 0.5;
	vec2 b = (prevPositionOut.xy	/ prevPositionOut.w	) * 0.5 + 0.5;
	vec2 velocityVector = a - b;
	//float velocityPacked = uintBitsToFloat(packHalf2x16(velocityVector));

	//gRoughness_AO_velocity = vec4(acr.z, acr.x, 0.0f, 1.0f);
	gRoughness_AO_velocity = vec4(acr.xy, velocityVector);
	vec3 N = normalize(normalOut.xyz);
	vec3 eyeDir = normalize(gEyePosition.xyz - wsOut.xyz);
	vec3 reflectVector = normalize(reflect(eyeDir, N.xyz));

	ivec2 textureDim = textureSize(cubeHandles[2], 0);
	float numMipMaps = ceil(log2(max(textureDim.x,textureDim.y)))-1.0f;
	float mipLevel = numMipMaps * (1.0f - roughness);
	vec4 iblSpec = pow(textureLod(cubeHandles[2], reflectVector, mipLevel), vec4(gGamma));// * vec4(0.1f, 0.1f, 0.1f, 1.0f) * 1.05f;

	vec3 H = normalize( normalOut.xyz + eyeDir );
	
	//iblSpec.xyz *= FresnelSchlick(specular.xyz, N.xyz, eyeDir);
	
	#ifdef IBL_SPEC_OFF
	iblSpec.xyz = vec3(0.0f);
	#endif
	
//<<<<<< HEAD
//	vec4 iblDiffuse =  pow(texture(cubeHandles[3], N.xyz), vec4(2.2f));
//	iblDiffuse.xyz *= FresnelSchlick(specular.xyz, N.xyz, H);
//======
	vec4 iblDiffuse =  pow(texture(cubeHandles[3], -N.xyz), vec4(gGamma)); // NORMAL IS NEGATIVE, THIS IS TEMP! FIX CUBEMAP PL0X! THEN UN-NEGATIVIZE THE NORMAL!
	//iblDiffuse.xyz *= FresnelSchlick(specular.xyz, H, eyeDir);
//>>>>>>> 803db449b3b16e85deaedbf27ae635db58d7208f

	#ifdef IBL_DIFFUSE_OFF
	iblDiffuse.xyz = vec3(0.0f);
	#endif
	//gEmissive = (iblSpec * specular * roughness) + (iblDiffuse * albedo * (1.0f - roughness));
	gEmissive = (iblSpec * vec4(FresnelSchlick(specular.xyz , N.xyz, eyeDir), 1.0f) * roughness) + (iblDiffuse * albedo * (1.0f - roughness));
	gEmissive.w = 1.0f;
	//gEmissive.xyz *= 0.14f;
	//color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//color = normalOut;//vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//color = texture(t, uvOut);
	//color = vec4(uvOut, 0.0f, 1.0f);
	//color = texture((handles[int(Transforms[drawID].albedo_specular_acr_normal.x)]), uvOut);
	//color = texture((handles[1]), uvOut);
}