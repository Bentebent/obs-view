#version 440

#define WORK_GROUP_SIZE 16
#define MAX_LIGHT_COUNT 1024
#define MAX_LIGHTS_PER_TILE 256
#define M_PI 3.14159265358979323846

#pragma optionNV(fastmath on) 
#pragma optionNV(ifcvt none) 
#pragma optionNV(inline all) 
#pragma optionNV(strict on) 
#pragma optionNV(unroll all)

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE) in;

struct LightData
{
	vec3 position;
	float radius_length;
	vec3 color;
	float intensity;
	vec3 spec_color;
	float spot_penumbra;
	vec3 orientation;
	float spot_angle;
};

struct SurfaceData
{
	vec4 WSNormal;
	vec4 albedo;
	vec4 specular;
	float roughness;
};


//Light buffer
layout (std430, binding = 3) restrict readonly buffer LightBuffer
{
    LightData gLightSources[];
};

//Light data uniforms
uniform uint gTotalPointLights;
uniform uint gTotalSpotLights;
uniform uint gTotalDirectionalLights;

//Output texture
layout (binding = 0, rgba32f) uniform image2D outTexture;

//Texture uniforms
uniform sampler2D gDepthStencil;
uniform sampler2D gWSNormal;
uniform sampler2D gAlbedo_decal;
uniform sampler2D gSpecular;
uniform sampler2D gRoughness_AO_velocity;

//Camera uniforms
uniform mat4 gInvProjView;
uniform mat4 gProjection;
uniform mat4 gView;
uniform vec3 gEyePosition;

//Misc uniforms
uniform ivec2 gFramebufferDimensions;

//HDR uniforms
uniform float gGamma;

shared uint gMinDepth;
shared uint gMaxDepth;

shared uint gPointLightCount;
shared uint gPointLightIndex[MAX_LIGHT_COUNT];

shared uint gSpotLightCount;
shared uint gSpotLightIndex[MAX_LIGHT_COUNT];

vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

float sqr(float x) { return x*x; }


vec3 FresnelSchlick(vec3 specColour, vec3 lightDir, vec3 H)
{
	return specColour + (1.0f - specColour) * pow(1.0f - clamp(dot(lightDir, H), 0.0f, 1.0f), 5);
}

float LinearizeDepth(float zoverw, float nearZ, float farZ)
{
	return (2.0 * nearZ) / (farZ + nearZ - zoverw * (farZ - nearZ));
}

vec4 ReconstructWSPosition(float z, vec2 uv_f)
{
    vec4 sPos = vec4(uv_f * 2.0f - 1.0f, z, 1.0f);
    sPos = gInvProjView * sPos;
    return vec4((sPos.xyz / sPos.w ), sPos.w);
}

vec3 LambertBRDF(vec3 lightDir, vec3 eyeDir, SurfaceData surface)
{
	const float PI = 3.14159f;
	vec3 diffuse = vec3(0.0f);
	vec3 L = lightDir;
	vec3 H = normalize(eyeDir + L);
	diffuse = surface.albedo.xyz / PI * FresnelSchlick(surface.specular.xyz, L, H);
	return diffuse;
}

vec3 OrenNayar(vec3 lightDir, vec3 eyeDir, SurfaceData surface)
{
	const float PI = 3.14159f;
	vec3 diffuse = vec3(0.0f);

	vec3 N = surface.WSNormal.xyz;
	vec3 L = lightDir;
	vec3 V = eyeDir;
	float roughness_sq = surface.roughness * surface.roughness;

	float alpha = clamp(max( acos( max(dot( V, N ), 0.0f) ), acos( dot( L, N ) ) ), 0.0f, 1.0f);
	float beta	= clamp(min( acos( max(dot( V, N ), 0.0f) ), acos( dot( L, N ) ) ), 0.0f, 1.0f);
	float gamma = clamp(dot( V - N * max(dot ( V, N), 0.0f), (L - N) * dot( L, N ) ), 0.0f, 1.0f);
	
	float C1 = 1.0f - 0.5f * (roughness_sq / (roughness_sq + 0.33f));
	float C2 = 0.45f * (roughness_sq / (roughness_sq + 0.09f));
	
	if (gamma >= 0.0f)
	{
		C2 *= sin(alpha);
	}
	else
	{
		C2 *= ( sin( alpha ) - pow( ( 2.0f * beta ) / PI, 3.0f ) );
	}
	
	float C3 = (1.0f / 8.0f) ;
    C3	*= ( roughness_sq / ( roughness_sq + 0.09f ) );
    C3	*= pow( ( 4.0f * alpha * beta ) / (PI * PI), 2.0f );
	
	float A = gamma * C2 * tan( beta );
    float B = (1.0f - abs( gamma )) * C3 * tan( (alpha + beta) / 2.0f );
 	
    diffuse = surface.albedo.xyz * max( 0.0f, dot( N, L ) ) * ( C1 + A + B );
	
	return diffuse;
}

float BlinnPhongDistribution(vec3 H,  SurfaceData surface)
{
		const float PI = 3.14159f;
        float gloss = surface.roughness;
        float specularPower = exp2(gloss * 10.0f + 1.0f);
        float normalisation = (specularPower + 2.0f) / (2.0f * PI);
        return max(0.0f, normalisation * pow( clamp(dot(H, surface.WSNormal.xyz), 0.0f, 1.0f), specularPower));
}

float GGX(float alpha, float cosThetaM)
{
	const float PI = 3.14159f;
    float CosSquared = cosThetaM*cosThetaM;
    float TanSquared = (1-CosSquared)/CosSquared;
    return (1.0/PI) * sqr(alpha/(CosSquared * (alpha*alpha + TanSquared)));
}

float TrowbridgeReitz(float c, float cosAlpha)
{
	const float PI = 3.14159f;
    float cSquared = c * c;
    return (sqr (cSquared / (cosAlpha * cosAlpha * (cSquared - 1) + 1))) * ( 1 / (c * c * PI));
}

// Geometry term
float CookTorranceGeometry(vec3 H, vec3 view, vec3 lightDir,  SurfaceData surface)
{
        float vdoth = clamp(dot(view, H), 0.0f, 1.0f);
        float ndoth = clamp(dot(surface.WSNormal.xyz, H), 0.0f, 1.0f);
        float ndotv = clamp(dot(surface.WSNormal.xyz, view), 0.0f, 1.0f);
        float ndotl = clamp(dot(surface.WSNormal.xyz, lightDir), 0.0f, 1.0f);
        float t = 2.0f * ndoth / vdoth;
        return min(1.0f, min(t * ndotv, t * ndotl));
}

float SchlickGeometry(float v, float m)
{
	const float PI = 3.14159f;
    float k = sqrt(2*m*m/PI);
    return v > 0 ? v/(v-k*v + k) : 0.0;
}


vec3 MicrofacetSpecularBRDF(vec3 lightDir, vec3 eyeDir, SurfaceData surface)
{
	vec3 H = normalize(eyeDir + lightDir);
	vec3 F = FresnelSchlick(surface.specular.xyz, eyeDir, H);

	float vdoth = clamp(dot(eyeDir, H), 0.0f, 1.0f);
    float ndoth = clamp(dot(surface.WSNormal.xyz, H), 0.0f, 1.0f);
    float ndotv = clamp(dot(surface.WSNormal.xyz, eyeDir), 0.0f, 1.0f);
    float ndotl = clamp(dot(surface.WSNormal.xyz, lightDir), 0.0f, 1.0f);

	vec3 D = vec3(0.0f);

	//D = vec3(BlinnPhongDistribution(H, surface));
	D = vec3(GGX(max(1.0f - surface.roughness, 0.001f), dot( surface.WSNormal.xyz,H)));
	//D = vec3(TrowbridgeReitz(max(1.0f - surface.roughness, 0.001f), clamp(dot(surface.WSNormal.xyz, H), 0.0f, 1.0f)));

	vec3 G = vec3(0.0f);

	G = vec3(CookTorranceGeometry(H, eyeDir, lightDir, surface));
	//G = vec3(SchlickGeometry(ndotl, surface.roughness) * SchlickGeometry(ndotv, surface.roughness));
	//vec3 nom = FresnelSchlick(surface.specular.xyz, eyeDir, H) * vec3(BlinnPhongDistribution(H, surface)) * vec3(CookTorranceGeometry(H, eyeDir, lightDir, surface));
	//vec3 nom = FresnelSchlick(surface.specular.xyz, eyeDir, H) * vec3(GGX(1.0f - surface.roughness, dot( surface.WSNormal.xyz,H))) * vec3(CookTorranceGeometry(H, eyeDir, lightDir, surface));
	vec3 nom = F * D * G;

	float denom = 4.0f * dot(surface.WSNormal.xyz, lightDir) * dot(surface.WSNormal.xyz, eyeDir);

	return nom / denom;
}

vec4 CalculatePointLight(LightData lightSource, SurfaceData surface, vec3 WSPosition, vec3 eyePosition)
{
	vec3 lightDir = lightSource.position - WSPosition;

	if(length(lightDir) > lightSource.radius_length) 
		return vec4(0.0f, 0.0f, 0.0f, 0.0f);

	// Calculate attenuation
	float dist = length( lightDir );
	float att = (pow(clamp( 1 - pow(dist / lightSource.radius_length, 4.0f), 0.0f, 1.0f), 2.0f)) / (pow(dist, 2.0f) + 1);

	vec3 eyeDir = normalize(eyePosition - WSPosition);
	lightDir = normalize(lightDir);
	float df =  max( 0.0f, dot(surface.WSNormal.xyz, lightDir));
	float cosine = clamp(dot(surface.WSNormal.xyz, lightDir), 0.0f, 1.0f);
	vec3 lightContribution = lightSource.color * lightSource.intensity * att * cosine;
	vec3 diffuse = OrenNayar(lightDir, eyeDir, surface);
	vec3 specular = MicrofacetSpecularBRDF(lightDir, eyeDir, surface);
	//vec3 diffuse = LambertBRDF(lightDir, eyeDir, surface) * df;
	
	return vec4((specular + diffuse) * lightContribution, 1.0f);

}

vec4 CalculateSpotlight( LightData lightSource, SurfaceData surface, vec3 WSPosition, vec3 eyePosition)
{
	vec3 lightDir = lightSource.position - WSPosition;
	
	if(length(lightDir) > lightSource.radius_length) 
		return vec4(0.0f, 0.0f, 0.0f, 0.0f);
	
	float cosAngle = dot(normalize(lightSource.orientation.xyz), normalize(-lightDir));
	float cosOuterAngle = cos(lightSource.spot_angle);
	float cosInnerAngle = cosOuterAngle + lightSource.spot_penumbra;
	float cosDelta = cosInnerAngle - cosOuterAngle;
	
	float spot = clamp((cosAngle - cosOuterAngle) / cosDelta,0.0f, 1.0f);
	
	if((cosAngle > cos(lightSource.spot_angle)) && (length(lightDir) <= lightSource.radius_length))
	{
		// Calculate attenuation
		float dist = length( lightDir );
		float att = spot * ((pow(clamp( 1 - pow(dist / lightSource.radius_length, 4.0f), 0.0f, 1.0f), 2.0f)) / (pow(dist, 2.0f) + 1));
		
		vec3 eyeDir = normalize(eyePosition - WSPosition);
		lightDir = normalize(lightDir);

		float cosine = clamp(dot(surface.WSNormal.xyz, lightDir), 0.0f, 1.0f);
		vec3 lightContribution = lightSource.color * lightSource.intensity * att * cosine;

		vec3 diffuse = OrenNayar(lightDir, eyeDir, surface);
		vec3 specular = MicrofacetSpecularBRDF(lightDir, eyeDir, surface);


		return vec4((specular + diffuse) * lightContribution, 1.0f);
	}
	else
		return vec4(0.0f, 0.0f, 0.0f, 0.0f);
	
}

vec4 CalculateDirectionalLight(LightData lightSource, SurfaceData surface, vec3 WSPosition, vec3 eyePosition)
{
	vec3 lightDir = normalize(-lightSource.orientation);
	vec3 eyeDir = normalize(eyePosition - WSPosition);

	float cosine = clamp(dot(surface.WSNormal.xyz, lightDir), 0.0f, 1.0f);
	vec3 lightContribution = lightSource.color * lightSource.intensity * cosine;

	vec3 diffuse = OrenNayar(lightDir, eyeDir, surface);
	vec3 specular = MicrofacetSpecularBRDF(lightDir, eyeDir, surface);

	return vec4((specular + diffuse) * lightContribution, 1.0f);
}

void main()
{
	
	if (gl_LocalInvocationIndex == 0)
	{
		gMinDepth = 0xFFFFFFFF;
		gMaxDepth = 0;

		gPointLightCount = 0;
		gSpotLightCount = 0;

	}

	barrier();
	
	ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
	vec2 uv = vec2(pixel) / vec2(gFramebufferDimensions - 1);

	float depth = texelFetch(gDepthStencil, pixel, 0).r * 2.0f - 1.0f;

	float zNear = gProjection[3][2] / (gProjection[2][2] - 1.0);
	float zFar	= gProjection[3][2] / (gProjection[2][2] + 1.0);
	float clipDelta = zFar - zNear;

	vec4 wPosition = ReconstructWSPosition(depth, uv);
	float ldepth = (1.0 / wPosition.w - zNear) / (zFar - zNear);
	uint unsignedDepth = uint(ldepth * uint(0xFFFFFFFF));

	atomicMin(gMinDepth, unsignedDepth);
	atomicMax(gMaxDepth, unsignedDepth);

	barrier();
	
	float minDepthZ =  (float(gMinDepth) / float(uint(0xFFFFFFFF)));
	float maxDepthZ =  (float(gMaxDepth) / float(uint(0xFFFFFFFF)));

	minDepthZ = minDepthZ * clipDelta + zNear;
	maxDepthZ = maxDepthZ * clipDelta + zNear;

	vec2 tileScale = (gFramebufferDimensions / float( 2 * WORK_GROUP_SIZE));
	vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);

	vec4 col1 = vec4(-gProjection[0][0]  * tileScale.x	,0.0									,tileBias.x			,0.0);
		
	vec4 col2 = vec4(0.0								,-gProjection[1][1] * tileScale.y		,tileBias.y			,0.0);

	vec4 col4 = vec4(0.0								,0.0									,-1.0f				,0.0);

	vec4 frustumPlanes[6];

	//Left plane
	frustumPlanes[0] = col4 + col1;

	//right plane
	frustumPlanes[1] = col4 - col1;

	//top plane
	frustumPlanes[2] = col4 - col2;

	//bottom plane
	frustumPlanes[3] = col4 + col2;

	//far
	frustumPlanes[4] = vec4(0.0f, 0.0f, 1.0f, maxDepthZ);

	//near
	frustumPlanes[5] = vec4(0.0f, 0.0f, -1.0f, -minDepthZ);

	for(int i = 0; i < 4; i++)
	{
		frustumPlanes[i].xyz *= 1.0f / length(frustumPlanes[i].xyz);
	}
	
	LightData currentLight;
		
	float dist;
	uint id;
	vec4 pos;
	float rad;
	bool inFrustum = false;

	uint threadCount = WORK_GROUP_SIZE * WORK_GROUP_SIZE;
	uint passCount;

	passCount = (gTotalPointLights + threadCount - 1) / threadCount;
	for (uint passIt = 0; passIt < passCount; ++passIt)
	{
		uint lightIndex =  passIt * threadCount + gl_LocalInvocationIndex;
		if(lightIndex >= gTotalPointLights)
			break;
		
		currentLight = gLightSources[lightIndex];
		pos = gView * vec4(currentLight.position, 1.0f);
		rad = currentLight.radius_length;
		
		if (gPointLightCount < MAX_LIGHTS_PER_TILE)
		{
			inFrustum = true;
			for (uint i = 5; i >= 0 && inFrustum; i--)
			{
				dist = dot(frustumPlanes[i], pos);
				inFrustum = (dist > -rad);
			}
			
			if (inFrustum)
			{
				id = atomicAdd(gPointLightCount, 1);
				gPointLightIndex[id] = lightIndex;
			}
		}
	}

	inFrustum = false;

	passCount = (gTotalSpotLights + threadCount - 1) / threadCount;
		for (uint passIt = 0; passIt < passCount; ++passIt)
	{
		uint lightIndex =  passIt * threadCount + gl_LocalInvocationIndex;
		if(lightIndex >= gTotalSpotLights)
			break;
		
		lightIndex += gTotalPointLights;
		currentLight = gLightSources[lightIndex];

		pos = gView * vec4(currentLight.position, 1.0f);
		rad = currentLight.radius_length;
		
		if (gPointLightCount < MAX_LIGHTS_PER_TILE)
		{
			inFrustum = true;
			for (uint i = 5; i >= 0 && inFrustum; i--)
			{
				dist = dot(frustumPlanes[i], pos);
				inFrustum = (dist > -rad);
			}
			
			if (inFrustum)
			{
				id = atomicAdd(gSpotLightCount, 1);
				gSpotLightIndex[id] = lightIndex;
			}
		}
	}

	barrier();

	vec4 outputColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);

	SurfaceData SD;
	
	SD.WSNormal = normalize(vec4(texelFetch(gWSNormal, pixel, 0).xyz, 0.0f));
	SD.albedo = vec4(texelFetch(gAlbedo_decal, pixel, 0).xyz, 1.0f);
	SD.specular = vec4(texelFetch(gSpecular, pixel, 0).xyz, 1.0f);
	SD.roughness = max(texelFetch(gRoughness_AO_velocity, pixel, 0).x, 0.001f);

	SD.albedo.xyz = pow(SD.albedo.xyz, vec3(gGamma));
	SD.specular.xyz = pow(SD.specular.xyz, vec3(gGamma));

	for(int i = 0; i < gPointLightCount; i++)
	{
		outputColor += CalculatePointLight(gLightSources[gPointLightIndex[i]], SD, wPosition.xyz, gEyePosition);
	}

	for(int i = 0; i < gSpotLightCount; i++)
	{
		outputColor += CalculateSpotlight(gLightSources[gSpotLightIndex[i]], SD, wPosition.xyz, gEyePosition);
	}

	uint ofst = gTotalPointLights + gTotalSpotLights;
	for(uint i = ofst; i < gTotalDirectionalLights + ofst; i++)
	{
		outputColor += max(CalculateDirectionalLight(gLightSources[i],  SD, wPosition.xyz, gEyePosition), vec4(0.0f));
	}
	
	outputColor += imageLoad(outTexture, pixel);
	barrier();

	//outputColor.xyz = Uncharted2Tonemap(outputColor.xyz * 1.0f) / Uncharted2Tonemap(vec3(2.0f));
	//outputColor.xyz = pow(outputColor.xyz, vec3(1.0f / 2.2f));

	//if (gl_LocalInvocationID.x == 0 || gl_LocalInvocationID.y == 0)
	//{
	//	imageStore(outTexture, pixel, vec4(.2f, .2f, .2f, 1.0f));
	//}
	//else
	//{
		//imageStore(outTexture, pixel, vec4(linearDepth, linearDepth, linearDepth, 1.0f));
		//imageStore(outTexture, pixel, texture(gWSNormal, uv));
		//imageStore(outTexture, pixel, vec4(wPosition.xyz, 1.0f));
		//imageStore(outTexture, pixel, vec4(minDepthZ, minDepthZ, minDepthZ, 1.0f));
		//imageStore(outTexture, pixel, vec4(maxDepthZ, maxDepthZ, maxDepthZ, 1.0f));
		//imageStore(outTexture, pixel, texture(gWSNormal, uv) + vec4(gPointLightCount / float(5.0f), 0.0f, 0.0f, 1.0f));
		//imageStore(outTexture, pixel, outputColor + vec4(gPointLightCount / float(75.0f)));
		imageStore(outTexture, pixel, vec4(outputColor.xyz, 1.0f));
		//imageStore(outTexture, pixel, vec4(SD.WSNormal.xyz, 1.0f));
		//imageStore(outTexture, pixel, vec4(SD.albedo.xyz, 1.0f));
	//}
	
}