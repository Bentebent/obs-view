#version 440

#define WORK_GROUP_SIZE 16
#define MAX_LIGHT_COUNT 1024
#define MAX_LIGHTS_PER_TILE 256

#pragma optionNV(fastmath on) 
#pragma optionNV(ifcvt none) 
#pragma optionNV(inline all) 
#pragma optionNV(strict on) 
#pragma optionNV(unroll all)

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE) in;

struct LightData
{
	vec3 position;
	float radius_length;
	vec3 color;
	float intensity;
	vec3 spec_color;
	float spot_penumbra;
	vec3 orientation;
	float spot_angle;
};

struct SurfaceData
{
	vec4 WSNormal;
	vec4 albedo;
	vec4 specular;
};


//Light buffer
layout (std430, binding = 3) restrict readonly buffer LightBuffer
{
    LightData gLightSources[];
};

//Light data uniforms
uniform uint gTotalPointLights;

//Output texture
layout (binding = 0, rgba32f) uniform image2D outTexture;

//Texture uniforms
uniform sampler2D gDepthStencil;
uniform sampler2D gWSNormal;
uniform sampler2D gAlbedo_decal;
uniform sampler2D gSpecular;
uniform sampler2D gRoughness_AO_velocity;

//Camera uniforms
uniform mat4 gInvProjView;
uniform mat4 gProjection;
uniform mat4 gView;
uniform vec3 gEyePosition;

//Misc uniforms
uniform vec2 gFramebufferDimensions;

//HDR uniforms
uniform float gGamma;
uniform float gExposure;
uniform vec3 gWhitePoint;

shared uint gMinDepth;
shared uint gMaxDepth;

shared uint gPointLightCount;
shared uint gPointLightIndex[MAX_LIGHT_COUNT];

vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

float LinearizeDepth(float zoverw, float nearZ, float farZ)
{
	return (2.0 * nearZ) / (farZ + nearZ - zoverw * (farZ - nearZ));
}

vec4 ReconstructWSPosition(float z, vec2 uv_f)
{
    vec4 sPos = vec4(uv_f * 2.0 - 1.0, z, 1.0);
    sPos = gInvProjView * sPos;
    return vec4((sPos.xyz / sPos.w ), sPos.w);
}

vec3 BlinnPhong(LightData lightSource, SurfaceData surface, vec3 eyeDirection, vec3 lightDirection, float attenuation)
{
	lightDirection = normalize(lightDirection);

	float intensity = 0.0f;

	// Diffuse
	vec3 diffuseColor = vec3(0.0f, 0.0f, 0.0f);
	float df =  max( 0.0f, dot(surface.WSNormal.xyz, lightDirection));
	intensity = df;

	diffuseColor = surface.albedo.xyz * intensity * lightSource.color * lightSource.intensity * attenuation;

	// Specular
	vec3 specColor = vec3(0.0f, 0.0f, 0.0f);
	if(df > 0.0f)
	{
		vec3 H = normalize( lightDirection + eyeDirection );
		float NdotH = dot( surface.WSNormal.xyz, H );
		intensity = pow( clamp( NdotH, 0.0f, 1.0f ), max(1.0f, surface.specular.w  * 256)) * df;
	
		// Temp vars, need materials with these channels

		specColor = surface.specular.xyz * intensity * lightSource.spec_color * lightSource.intensity * attenuation;
	}


	return diffuseColor + specColor;
}

vec4 CalculatePointLight(LightData lightSource, SurfaceData surface, vec3 WSPosition, vec3 eyePosition)
{
	vec3 lightDir = lightSource.position - WSPosition;

	if(length(lightDir) > lightSource.radius_length) 
		return vec4(0.0f, 0.0f, 0.0f, 0.0f);

	// Calculate attenuation
	float dist = length( lightDir );
	float att = (pow(clamp( 1 - pow(dist / lightSource.radius_length, 4.0f), 0.0f, 1.0f), 2.0f)) / (pow(dist, 2.0f) + 1);// More attenuations to chose from at the bottom of this file
	//float att = 1.0f / dist - 1.0f / lightSource.radius_length;

	vec3 eyeDir = normalize(eyePosition - WSPosition);
	
	return vec4(BlinnPhong(lightSource, surface, eyeDir, lightDir, att), 0.0f);
}

void main()
{
	
	if (gl_LocalInvocationIndex == 0)
	{
		gMinDepth = 0xFFFFFFFF;
		gMaxDepth = 0;

		gPointLightCount = 0;
	}

	barrier();

	ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
	vec2 uv = vec2(pixel.x / gFramebufferDimensions.x, pixel.y / gFramebufferDimensions.y);

	float depth = texture(gDepthStencil, uv).r * 2.0f - 1.0f;

	float zNear = gProjection[3][2] / (gProjection[2][2] - 1.0);
	float zFar	= gProjection[3][2] / (gProjection[2][2] + 1.0);
	float clipDelta = zFar - zNear;

	vec4 wPosition = ReconstructWSPosition(depth, uv);
	float ldepth = (1.0 / wPosition.w - zNear) / (zFar - zNear);
	uint unsignedDepth = uint(ldepth * uint(0xFFFFFFFF));

	atomicMin(gMinDepth, unsignedDepth);
	atomicMax(gMaxDepth, unsignedDepth);

	barrier();
	
	float minDepthZ =  (float(gMinDepth) / float(uint(0xFFFFFFFF)));
	float maxDepthZ =  (float(gMaxDepth) / float(uint(0xFFFFFFFF)));

	minDepthZ = minDepthZ * clipDelta + zNear;
	maxDepthZ = maxDepthZ * clipDelta + zNear;

	vec2 tileScale = gFramebufferDimensions * (1.0f / float( 2 * WORK_GROUP_SIZE));
	vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);

	vec4 col1 = vec4(-gProjection[0][0]  * tileScale.x	,0.0									,tileBias.x			,0.0);
		
	vec4 col2 = vec4(0.0								,-gProjection[1][1] * tileScale.y		,tileBias.y			,0.0);

	vec4 col4 = vec4(0.0								,0.0									,-1.0f				,0.0);

	vec4 frustumPlanes[6];

	//Left plane
	frustumPlanes[0] = col4 + col1;

	//right plane
	frustumPlanes[1] = col4 - col1;

	//top plane
	frustumPlanes[2] = col4 - col2;

	//bottom plane
	frustumPlanes[3] = col4 + col2;

	//far
	frustumPlanes[4] = vec4(0.0f, 0.0f, 1.0f, maxDepthZ);

	//near
	frustumPlanes[5] = vec4(0.0f, 0.0f, -1.0f, -minDepthZ);

	for(int i = 0; i < 4; i++)
	{
		frustumPlanes[i].xyz *= 1.0f / length(frustumPlanes[i].xyz);
	}
	
	LightData currentLight;
		
	float dist;
	uint id;
	vec4 pos;
	float rad;
	bool inFrustum = false;

	uint threadCount = WORK_GROUP_SIZE * WORK_GROUP_SIZE;
	uint passCount;

	passCount = (gTotalPointLights + threadCount - 1) / threadCount;
	for (uint passIt = 0; passIt < passCount; ++passIt)
	{
		uint lightIndex =  passIt * threadCount + gl_LocalInvocationIndex;
		if(lightIndex >= gTotalPointLights)
			break;
		
		currentLight = gLightSources[lightIndex];
		pos = gView * vec4(currentLight.position, 1.0f);
		rad = currentLight.radius_length;
		
		if (gPointLightCount < MAX_LIGHTS_PER_TILE)
		{
			inFrustum = true;
			for (uint i = 5; i >= 0 && inFrustum; i--)
			{
				dist = dot(frustumPlanes[i], pos);
				inFrustum = (dist > -rad);
			}
			
			if (inFrustum)
			{
				id = atomicAdd(gPointLightCount, 1);
				gPointLightIndex[id] = lightIndex;
			}
		}
	}
	
	barrier();

	vec4 outputColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);

	SurfaceData SD;
	
	//SD.WSNormal = vec4(texture(gWSNormal, uv).xyz, 0.0f) * 2.0f - 1.0f;
	SD.WSNormal = vec4(texelFetch(gWSNormal, pixel, 0).xyz, 0.0f) * 2.0f - 1.0f;
	SD.albedo = vec4(texelFetch(gAlbedo_decal, pixel, 0).xyz, 1.0f);
	SD.albedo.xyz = pow(SD.albedo.xyz, vec3(2.2f));
	//	SD.albedo = vec4(texture(gAlbedo_decal, uv).xyz, 1.0f);
	SD.specular = vec4(texture(gSpecular, uv).xyz, 1.0f);
	SD.specular.xyz = pow(SD.specular.xyz, vec3(2.2f));
	for(int i = 0; i < gPointLightCount; i++)
	{
		outputColor += CalculatePointLight(gLightSources[gPointLightIndex[i]], SD, wPosition.xyz, gEyePosition);
	}
	//outputColor += imageLoad(outTexture, pixel);
	barrier();

	//outputColor.xyz = Uncharted2Tonemap(outputColor.xyz * 1.0f) / Uncharted2Tonemap(vec3(2.0f));
	//outputColor.xyz = pow(outputColor.xyz, vec3(1.0f / 2.2f));

	//if (gl_LocalInvocationID.x == 0 || gl_LocalInvocationID.y == 0)
	//{
	//	imageStore(outTexture, pixel, vec4(.2f, .2f, .2f, 1.0f));
	//}
	//else
	//{
		//imageStore(outTexture, pixel, vec4(linearDepth, linearDepth, linearDepth, 1.0f));
		//imageStore(outTexture, pixel, texture(gWSNormal, uv));
		//imageStore(outTexture, pixel, vec4(wPosition.xyz, 1.0f));
		//imageStore(outTexture, pixel, vec4(minDepthZ, minDepthZ, minDepthZ, 1.0f));
		//imageStore(outTexture, pixel, vec4(maxDepthZ, maxDepthZ, maxDepthZ, 1.0f));
		//imageStore(outTexture, pixel, texture(gWSNormal, uv) + vec4(gPointLightCount / float(5.0f), 0.0f, 0.0f, 1.0f));
		//imageStore(outTexture, pixel, outputColor + vec4(gPointLightCount / float(75.0f)));
		imageStore(outTexture, pixel, vec4(outputColor.xyz, 1.0f));
		//imageStore(outTexture, pixel, vec4(SD.WSNormal.xyz, 1.0f));
	//}
	
}