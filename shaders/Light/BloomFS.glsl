#version 440

//#define MOTION_BLUR
#define MAX_MBLUR_SAMPLES 32
#define TARGET_FRAMERATE 24.0

in vec2 uv;

uniform sampler2D gSourceTexture;
uniform sampler2D gVelocityTexture;
uniform sampler2D gBloomTextures[8];
uniform vec3 gWhitePoint;
uniform float gExposure;
uniform float gGamma;
uniform float gDeltaTime;

out vec4 result;

vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

void main()
{
	vec4 sourceColor = texture(gSourceTexture, uv);

#ifdef MOTION_BLUR
	float velocityScale = 1.0 / (gDeltaTime * TARGET_FRAMERATE);
	vec2 velocity = texture(gVelocityTexture, uv).zw * velocityScale;
	vec2 texelSize = 1.0 / vec2(textureSize(gSourceTexture, 0));
	
	float speed = length(velocity / texelSize);
	int nSamples = clamp(int(speed), 1, MAX_MBLUR_SAMPLES);
	
	for(int i = 1; i < nSamples; i++)
	{
		vec2 offs = velocity * (float(i) / float(nSamples-1) - 0.5);
		sourceColor += texture(gSourceTexture, clamp(uv + offs, vec2(0.0), vec2(0.999)));
	}
	sourceColor /= float(nSamples);
#endif
	vec4 finalBloom = vec4(0);

	for (int i = 0; i < 8; i++)
	{
		finalBloom += texture(gBloomTextures[i], uv);// * 0.05f;
	}

	sourceColor += finalBloom;

	sourceColor.xyz = Uncharted2Tonemap(sourceColor.xyz * gExposure) / Uncharted2Tonemap(gWhitePoint);

	//Gamma correct
	sourceColor.xyz = pow(sourceColor.xyz, vec3(1.0f / gGamma));
		
		
	result = vec4(sourceColor.xyz, 1);
}