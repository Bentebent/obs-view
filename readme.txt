F�r allt
	-r�kna minne
	-m�t perf

Contentmanager
	*async load
	*hard reload
	*dynamisk in/urladdning
	*st�d f�r culling/streaming
	*samma interface som Kravall
	
ASM
	*st�d f�r hierarkier

Linj�rallokator
	*som Kravall
	
Levelhantering
	*tagga entiteter per heap
	*LOD
	*Culling
	*dynamisk in/urladdning
	
Event system
	*event culling
	
Lua
	*fullt dynamiskt
	*load/destroy f�r levels
	*tick OCH update f�r entiteter

Logger
	*som Kravall
	*once-funktion
	*b�ttre kanalhantering
	
Bullet
	*mattelib

Console
	
Content
	*object editor
	*filformat f�r entiteter (typ ASM)
		-beskriver objekt
		-koppla script till ett objekt
	*filformat f�r mesh
		-measurements
	*filformat f�r material (textur, shader)
	*filformat f�r KDA
	*filformat f�r skelett
		*attachment points
	*filformat f�r levels
	
Rendering
	*h�rd spec interface
	*text
		-cachad
	*central shader container
		-reload
		-defines
	*central FBO/RT container
	*settings
	*buffer manager
	*best fit normals
	*bindless textures
	*blending framework
	*gl state change framework
	*texture validation
	*area lights
	*alla ljus ska kasta skuggor
	*speglar
	*Gbuffer
		-Geometri
			�L�s p� IBL
			�Object motion blur
		-Partiklar
	
	