#ifndef SRC_SHARED_UTILITY_STRINGHELPER_HPP
#define SRC_SHARED_UTILITY_STRINGHELPER_HPP

#include <glm/glm.hpp>

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <vector>
#include <sstream>

static inline std::string& TrimLeftToRight(std::string& myString)
{
	myString.erase(myString.begin(), std::find_if(myString.begin(), myString.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return myString;
}

// trim from end
static inline std::string& TrimRightToLeft(std::string &myString) 
{
	myString.erase(std::find_if(myString.rbegin(), myString.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), myString.end());
	return myString;
}

// trim from both ends
static inline std::string& Trim(std::string &myString) 
{
	return TrimLeftToRight(TrimRightToLeft(myString));
}

static inline std::string& RemoveChar(std::string& myString, char c)
{
	myString.erase(std::remove(myString.begin(), myString.end(), c), myString.end());
	return myString;
}

static inline std::vector<std::string>& Split(const std::string &myString, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(myString);
	std::string item;

	while (std::getline(ss, item, delim)) 
	{
		elems.push_back(item);
	}
	return elems;
}


static inline std::vector<std::string> Split(const std::string &myString, char delim) 
{
	std::vector<std::string> elems;
	Split(myString, delim, elems);
	return elems;
}

static inline glm::vec3& StringToVec3(std::string& myString)
{
	RemoveChar(myString, '{');
	RemoveChar(myString, '}');
	RemoveChar(myString, ',');
	Trim(myString);

	std::vector<std::string> values = Split(myString, ' ');

	return glm::vec3(std::stof(values[0]), std::stof(values[1]), std::stof(values[2]));
}

static inline glm::vec4& StringToVec4(std::string& myString)
{
	RemoveChar(myString, '{');
	RemoveChar(myString, '}');
	RemoveChar(myString, ',');
	Trim(myString);

	std::vector<std::string> values = Split(myString, ' ');

	return glm::vec4(std::stof(values[0]), std::stof(values[1]), std::stof(values[2]), std::stof(values[4]));
}

static inline glm::mat4 StringToMat4(std::string& myString)
{
	RemoveChar(myString, '{');
	RemoveChar(myString, '}');
	RemoveChar(myString, ',');
	Trim(myString);

	std::vector<std::string> values = Split(myString, ' ');

	int i = 0;
	while (i < values.size())
	{
		if (values[i] == "")
		{
			values.erase(values.begin() + i);
			continue;
		}
		i++;
	}
	glm::mat4 output = glm::mat4(
		std::stof(values[0]), std::stof(values[1]), std::stof(values[2]), std::stof(values[3]),
		std::stof(values[4]), std::stof(values[5]), std::stof(values[6]), std::stof(values[7]),
		std::stof(values[8]), std::stof(values[9]), std::stof(values[10]), std::stof(values[11]),
		std::stof(values[12]), std::stof(values[13]), std::stof(values[14]), std::stof(values[15]));
	return output;
}

static inline glm::mat3 StringToMat3(std::string& myString)
{
	RemoveChar(myString, '{');
	RemoveChar(myString, '}');
	RemoveChar(myString, ',');
	Trim(myString);

	std::vector<std::string> values = Split(myString, ' ');

	int i = 0;
	while (i < values.size())
	{
		if (values[i] == "")
		{
			values.erase(values.begin() + i);
			continue;
		}
		i++;
	}

	glm::mat3 output = glm::mat3(
		std::stof(values[0]), std::stof(values[1]), std::stof(values[2]),
		std::stof(values[3]), std::stof(values[4]), std::stof(values[5]),
		std::stof(values[6]), std::stof(values[7]), std::stof(values[8]));
	return output;
}

#endif