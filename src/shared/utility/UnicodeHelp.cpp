#include "UnicodeHelp.hpp"

obs_ushort UnicodeHelper::UTF8DecodeChar(const char* utf8char)
{
	obs_ushort result;
	utf8DecodeChar(utf8char, result);
	return result;
}

obs_ustring UnicodeHelper::UTF8DecodeStr(const char* utf8str)
{
	int offset = 0;
	obs_ustring resultString;
	while (*(utf8str + offset) != 0)
	{
		obs_ushort resultChar;
		offset += utf8DecodeChar(utf8str + offset, resultChar);
		resultString.push_back(resultChar);
	}
	return resultString;
}

obs_ustring UnicodeHelper::UTF8DecodeStr(const std::string& utf8str)
{
	return UTF8DecodeStr(utf8str.c_str());
}

int UnicodeHelper::utf8DecodeChar(const char* utf8char, obs_ushort& out_unicode)
{
	unsigned char b1 = utf8char[0];
	if (b1 < 128)
	{
		out_unicode = b1;
		return 1;
	}
	obs_ushort numBytes = 1;
	obs_ushort numBits = 0;
	obs_ushort result = 0;
	unsigned char bitmask = (1 << 6) - 1; // 0011 1111
	unsigned char bitshift = 0;

	while ((b1 & 0xC0) == 0xC0)
	{
		b1 <<= 1;
		numBits += 6;
		bitmask >>= 1;
		result <<= 6;
		bitshift++;
		result |= (unsigned char)utf8char[bitshift] & ((1 << 6) - 1);
		numBytes++;
	}
	result |= ((b1 >> bitshift) & bitmask) << numBits;

	out_unicode = result;
	return numBytes;
}
