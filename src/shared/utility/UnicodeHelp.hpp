#ifndef SRC_SHARED_UTILITY_UNICODEHELP
#define SRC_SHARED_UTILITY_UNICODEHELP
#include <string>
#include <vector>
#include <gfx/GFXDefines.hpp>

//typedef std::u16string obs_ustring;
typedef std::vector<char16_t> obs_ustring;
typedef char16_t obs_ushort;
class UnicodeHelper
{
public:
	static obs_ushort UTF8DecodeChar(const char* utf8char);
	static obs_ustring UTF8DecodeStr(const char* utf8str);
	static obs_ustring UTF8DecodeStr(const std::string& utf8str);
private:
	/* Returns the number of bytes in the character */
	static int utf8DecodeChar(const char* utf8char, obs_ushort& out_unicode);
};
#endif
/*
http://www.endmemo.com/unicode/unicodeconverter.php
UTF - 8 (char*)						 | Unicode(21 bits)
------------------------------------ + --------------------------
0xxxxxxx							 | 00000000000000000xxxxxxx
------------------------------------ + --------------------------
110yyyyy 10xxxxxx					 | 0000000000000yyyyyxxxxxx
------------------------------------ + --------------------------
1110zzzz 10yyyyyy 10xxxxxx			 | 00000000zzzzyyyyyyxxxxxx
------------------------------------ + --------------------------
11110www 10zzzzzz 10yyyyyy 10xxxxxx  | 000wwwzzzzzzyyyyyyxxxxxx

*/