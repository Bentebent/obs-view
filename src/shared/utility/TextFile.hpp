#ifndef SRC_SHARED_UTILITY_TEXTFILE_HPP
#define SRC_SHARED_UTILITY_TEXTFILE_HPP

#include <fstream>
#include <string>
#include <exception>
#include <cstring>

class TextFile
{
private:

	std::string m_text;
	char*		m_cText;
	bool		m_nonConstantChar;
	bool		m_loadedFile;

public:

	TextFile();
	TextFile(bool enableNonConstantChar);

	~TextFile();

	void LoadFile(const char* filepath);

	std::string GetString();
	const char* GetCString();
	//char* GetNonConstantChar(); See body

	void Clear();
};

#endif