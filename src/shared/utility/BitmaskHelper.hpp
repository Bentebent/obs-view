#ifndef SRC_SHARED_UTILITY_BITMASKHELPER_HPP
#define SRC_SHARED_UTILITY_BITMASKHELPER_HPP

#include <bitmask/dynamic_bitmask.hpp>
#include <bitmask/bitmask_system.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXSettingTypes.hpp>

static Bitmask GetSettingsBitmask(const unsigned int settingsType, BitmaskSystem bs)
{
	Bitmask b = 0;
	b = bs.Encode(b, gfx::COMMAND_TYPES::COMMAND, MaskType::COMMAND);
	b = bs.Encode(b, gfx::SUB_COMMAND_TYPES::SETTINGS_COMMAND, MaskType::TYPE);
	b = bs.Encode(b, settingsType, MaskType::MESH);
	return b;
}

static Bitmask GetCommandBitmask(const unsigned int subCommand, BitmaskSystem bs)
{
	Bitmask b = 0;
	b = bs.Encode(b, gfx::COMMAND_TYPES::COMMAND, MaskType::COMMAND);
	b = bs.Encode(b, subCommand, MaskType::TYPE);
	return b;
}

#endif