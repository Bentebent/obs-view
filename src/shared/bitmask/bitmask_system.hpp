#ifndef BITMASK_SYSTEM_HPP
#define BITMASK_SYSTEM_HPP

#include "bitmask_definitions.hpp"
#include "dynamic_bitmask.hpp"

class BitmaskSystem
{
public:
	BitmaskSystem();
	~BitmaskSystem();

	/*! Initialization and validation */
	int Init();

	/*! Encodes a bitmask value
		\param bitmask The value of the bitmask to pack the value into
		\param value The value you want to pack
		\param mask_type The mask type to use, defined in bitmask_definitions.hpp
		\return Returns the new composed bitmask
	*/
	Bitmask Encode(const Bitmask& bitmask, const unsigned int& value, const BitmaskType& mask_type) const;

	/*! Decodes a bitmask value 
		\param bitmask The bitmask data you wish to extract from
		\param mask_type The mask type to use, defined in bitmask_definitions.hpp
		\return Returns the value in the specified field of the bitmask
	*/
	unsigned int Decode(const Bitmask& bitmask, const BitmaskType& mask_type) const;

	/*! Prints debug information */
	void PrintDebug() const;

	/*! Prints a bitmask */
	void PrintBitmask(const Bitmask& bitmask) const;
	void PrintBitmaskType(const BitmaskType& mask_type) const;

private:

	BitmaskInfo mask_types[MaskType::NUM_TYPES];

};

#endif