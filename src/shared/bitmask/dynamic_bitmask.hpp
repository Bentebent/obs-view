#ifndef DYNAMIC_BITMASK_HPP
#define DYNAMIC_BITMASK_HPP

#include "bitmask_types.hpp"

struct BitmaskInfo
{
	unsigned short length;
	unsigned short offset;
	Bitmask mask;
};

#endif