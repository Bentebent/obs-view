#include "bitmask_system.hpp"

#include <bitset>
#include <iostream>

	BitmaskSystem::BitmaskSystem()
	{

	}
	BitmaskSystem::~BitmaskSystem()
	{

	}
	int BitmaskSystem::Init()
	{
		if (sizeof(mask_types) / sizeof(BitmaskInfo) != NUM_TYPES || sizeof(Bitmask) != bitmask_length / 8)
		{
			std::cout << "Error: Bitmask / type mismatch.\n";
			return 1;
		}

		unsigned short current_offset = 0;
		for (BitmaskType i = 0; i < NUM_TYPES; i++)
		{
			mask_types[i].length = bitmask_lengths[i];
			mask_types[i].offset = current_offset;
			current_offset += mask_types[i].length;

			// Validate bitmask length > 0
			if (mask_types[i].length == 0)
			{
				// Warning:
				std::cout << "Warning: Bitmask " << i << " length is zero. Ignoring.\n";
				continue;
			}

			// Validate not out of range
			if (mask_types[i].offset + mask_types[i].length > bitmask_length)
			{
				std::cout << "Warning: Bitmask " << i << " out of range. (" <<
					mask_types[i].offset + mask_types[i].length <<
					" bits used of " << bitmask_length << " available)\n";
				return 2;
			}

			// Create bitmask
			unsigned short offset = mask_types[i].offset;
			unsigned short length = mask_types[i].length;
			Bitmask bitmask = mask_max;
			bitmask = bitmask << (bitmask_length - length);
			bitmask = bitmask >> (offset);
			mask_types[i].mask = bitmask;
		}

		return 0;
	}

	Bitmask BitmaskSystem::Encode(const Bitmask& bitmask, const unsigned int& value, const BitmaskType& mask_type) const
	{
#ifdef _DEBUG
		if (value >= pow(2, mask_types[mask_type].length))
			std::cout << "Warning: value too big for assigned bitmask\n";
#endif
		Bitmask result = value;
		
		// Move to right position
		result = result << (bitmask_length - mask_types[mask_type].offset - mask_types[mask_type].length);

		// Stencil the result and write back current bitmask to result
		result = (result & mask_types[mask_type].mask) + (bitmask & ~mask_types[mask_type].mask);

		return result;
	}

	unsigned int BitmaskSystem::Decode(const Bitmask& value, const BitmaskType& mask_type) const
	{
		Bitmask mask = (value & mask_types[mask_type].mask);
		mask = mask >> (bitmask_length - mask_types[mask_type].offset - mask_types[mask_type].length);
		int result = (int)mask;
		return result;
	}

	void BitmaskSystem::PrintDebug() const
	{
		std::cout << "Number of bitmasks: " << sizeof(mask_types) / sizeof(BitmaskInfo) << "\n----------------------\n";
		for (BitmaskType i = 0; i < NUM_TYPES; i++)
		{
			std::bitset<bitmask_length> bintest(mask_types[i].mask);
			std::cout << "Bitmask entry #" << i << "\n\tOffset: " << mask_types[i].offset <<
				"\n\tLength: " << mask_types[i].length << "\n----------------------\n";
		}

		for (BitmaskType i = 0; i < NUM_TYPES; i++)
		{
			std::bitset<bitmask_length> bintest(mask_types[i].mask);
			std::cout << "(" << i << ")\t" << bintest << "\n";
		}
		std::cout << "----------------------\n";
	}

	void BitmaskSystem::PrintBitmask(const Bitmask& bitmask) const
	{
		std::bitset<bitmask_length> bintest(bitmask);
		std::cout << bintest << "\n";
	}

	void BitmaskSystem::PrintBitmaskType(const BitmaskType& mask_type) const
	{
		std::bitset<bitmask_length> bintest(mask_types[mask_type].mask);
		std::cout << bintest << "\n";
	}