#ifndef BITMASK_DEFINITIONS_HPP
#define BITMASK_DEFINITIONS_HPP

#include "bitmask_types.hpp"

/*!	Enumeration of bitmask types */
enum MaskType : BitmaskType
{
	COMMAND,
	TYPE,
	VIEWPORT_ID,
	LAYER,
	TRANSLUCENCY,
	SHADER,
	MESH,
	TEXTURE,
	DEPTH,
	NUM_TYPES
};

/*!	bitmask_lengths is an array containing information about the 
	length of a bitmask entry defined in the "type" enumeration above. */
static const unsigned short bitmask_lengths[MaskType::NUM_TYPES] =
{
	1U, // COMMAND
	8U, // TYPE
	3U, // VIEWPORT_ID
	5U, // LAYER
	2U, // TRANSLUCENCY
	4U, // SHADER
	12U, // MESH
	13U, // TEXTURE
	16U, // DEPTH
};


#endif