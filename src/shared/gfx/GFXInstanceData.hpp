#ifndef SRC_GFX_GFXINSTANCEDATA_HPP
#define SRC_GFX_GFXINSTANCEDATA_HPP

#include <glm/glm.hpp>

namespace gfx
{
	struct InstanceData
	{
		glm::mat4 modelMatrix;
		glm::mat4 prevModelMatrix;
		glm::vec4 albedo_specular_acr_normal;
		glm::vec4 disp_emi_pad_pad;
	};
}

#endif