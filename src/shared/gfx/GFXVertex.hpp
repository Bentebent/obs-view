#ifndef SRC_SHARED_GFX_GFXVERTEX_HPP
#define SRC_SHARED_GFX_GFXVERTEX_HPP

#include <glm/glm.hpp>

namespace gfx
{
	struct AnimatedVertex
	{
		glm::vec4 position;
		glm::vec4 normal;
		glm::vec4 tangent;
		glm::ivec4 boneIndices;
		glm::vec4 boneWeights;
		glm::vec2 uv;
	};

	struct StaticVertex
	{
		glm::vec4 position;
		glm::vec4 normal;
		glm::vec4 tangent;
		glm::vec2 uv;
	};

}

#endif