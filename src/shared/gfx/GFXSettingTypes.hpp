#ifndef SRC_SHARED_GFX_GFXSETTINGSTYPES_HPP
#define SRC_SHARED_GFX_GFXSETTINGSTYPES_HPP

namespace gfx
{
	namespace SETTING_TYPES
	{
		static const unsigned int GAMMA = 0;
		static const unsigned int STATIC_EXPOSURE = 1;
		static const unsigned int WHITE_POINT = 2;
	}
}

#endif