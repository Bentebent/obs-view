#ifndef SRC_SHARED_GFX_GFXVIEWPORT_HPP
#define SRC_SHARED_GFX_GFXVIEWPORT_HPP

#include <glm/glm.hpp>

namespace gfx
{
	struct Viewport
	{
		int width;
		int height;
		int posX;
		int posY;

		glm::mat4 view;
		glm::mat4 projection;
		glm::mat4 prevViewProjection;
	};
}

#endif