#ifndef SRC_SHARED_GFX_GFXMATERIAL_HPP
#define SRC_SHARED_GFX_GFXMATERIAL_HPP

namespace gfx
{
	enum TextureType
	{
		ALBEDO,
		SPECULAR,
		AO_CAVITY_ROUGHNESS,
		NORMAL,
		DISPLACEMENT,
		EMISSIVE
	};

	struct MaterialTextureData
	{
		unsigned int textureID;
		unsigned int bindlessPosition;
	};

	//Material is a collection of texture ids
	struct Material
	{
		unsigned int id;
		MaterialTextureData albedo;
		MaterialTextureData specular;
		MaterialTextureData ao_cavity_roughness;
		MaterialTextureData normal;
		MaterialTextureData displacement;
		MaterialTextureData emissive;
	};
}

#endif