#ifndef TTF_RENDERER_FONT_HPP
#define TTF_RENDERER_FONT_HPP

namespace gfx
{
	typedef unsigned int Font;
	typedef unsigned int FontSize;
}
#endif