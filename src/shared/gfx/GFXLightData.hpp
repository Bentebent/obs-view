#ifndef SRC_GFX_GFXLIGHTDATA_HPP
#define SRC_GFX_GFXLIGHTDATA_HPP

#include <glm/glm.hpp>

namespace gfx
{
	struct LightData
	{
		glm::vec3 position;
		float radius_length;
		glm::vec3 color;
		float intensity;
		glm::vec3 spec_color;
		float spot_penumbra;
		glm::vec3 orientation;
		float spot_angle;
	};
}

#endif