#ifndef SRC_SHARED_GFX_GFXBITMASKDEFINITIONS_HPP
#define SRC_SHARED_GFX_GFXBITMASKDEFINITIONS_HPP

namespace gfx
{
	namespace COMMAND_TYPES
	{
		static const unsigned int COMMAND = 1;
		static const unsigned int RENDER = 0;
	}

	namespace SUB_COMMAND_TYPES
	{
		static const unsigned int RELOAD_SHADERS_COMMAND = 2;
		static const unsigned int SETTINGS_COMMAND = 1;
		static const unsigned int RENDER_COMMAND = 0;
	}

	namespace OBJECT_TYPES
	{
		static const unsigned int OPAQUE_GEOMETRY = 5;
		static const unsigned int LIGHT = 4;
	}

	namespace VIEWPORT_IDS
	{
		static const unsigned int PRIMARY_VIEWPORT = 7;
	}

	namespace LAYER_TYPES
	{
	}

	namespace TRANSLUCENCY_TYPES
	{
		static const unsigned int OPAQUE_TRANSLUCENCY = 4;
		static const unsigned int ADDITIVE_TRANSLUCENCY = 3;
		static const unsigned int SUBTRACTIVE_TRANSLUCENCY = 2;
		static const unsigned int MULTIPLICATIVE_TRANSLUCENCY = 1;
	}

	namespace LIGHT_TYPES
	{
		static const unsigned int POINT_SHADOW = 6;
		static const unsigned int SPOT_SHADOW = 5;
		static const unsigned int DIR_SHADOW = 4;
		static const unsigned int POINT = 3;
		static const unsigned int SPOT = 2;
		static const unsigned int DIR = 1;
		static const unsigned int AMBIENT = 0;

		static const unsigned int COUNT = 7;
	}


}


#endif