#ifndef SRC_GFX_FBO_FBOCONTAINER_HPP
#define SRC_GFX_FBO_FBOCONTAINER_HPP

#include "FBOTexture.hpp"
#include <GLEW/glew.h>
#include <vector>
namespace gfx
{
	class FBOContainer
	{
	public:
		FBOContainer();
		~FBOContainer();

		void Initialize(int screenWidth, int screenHeight);
		void Resize(int screenWidth, int screenHeight);

		GLuint m_FBO;

		FBOTexture* m_FBOTexture_D24S8_1;
		FBOTexture* m_FBOTexture_RGBA32_1;
		FBOTexture* m_FBOTexture_RGBA32_2;
		FBOTexture* m_FBOTexture_RGBA16_1;
		FBOTexture* m_FBOTexture_RGBA8_1;
		FBOTexture* m_FBOTexture_RGBA8_2;
		FBOTexture* m_FBOTexture_RGBA8_3;

		std::vector<FBOTexture*> m_bloomTextures;
		std::vector<FBOTexture*> m_intermediateBlurTextures;

		void BindGBuffer();
		void BindFBOTextures(int fboTextureCount, FBOTexture** textureArray, bool depthStencil);
	private:
		std::vector<int> m_colorAttachmentDefines;
	};
}

#endif