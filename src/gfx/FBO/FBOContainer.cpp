#include "FBOContainer.hpp"
#include <gfx/GFXDefines.hpp>
#include <iostream>
#include <glm/glm.hpp>
namespace gfx
{
	FBOContainer::FBOContainer()
	{
		m_FBOTexture_D24S8_1	= new FBOTexture();
		m_FBOTexture_RGBA32_1	= new FBOTexture();
		m_FBOTexture_RGBA32_2	= new FBOTexture();
		m_FBOTexture_RGBA16_1	= new FBOTexture();
		m_FBOTexture_RGBA8_1	= new FBOTexture();
		m_FBOTexture_RGBA8_2	= new FBOTexture();
		m_FBOTexture_RGBA8_3	= new FBOTexture();

		for (int i = 0; i < GFX_HDR_BLOOM_DOWNSAMPLES; i++)
		{
			m_bloomTextures.push_back(new FBOTexture());
			m_intermediateBlurTextures.push_back(new FBOTexture());
		}

		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT0);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT1);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT2);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT3);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT4);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT5);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT6);
		m_colorAttachmentDefines.push_back(GL_COLOR_ATTACHMENT7);
	}

	FBOContainer::~FBOContainer()
	{
		delete m_FBOTexture_D24S8_1;
		delete m_FBOTexture_RGBA32_1;
		delete m_FBOTexture_RGBA16_1;
		delete m_FBOTexture_RGBA8_1;
		delete m_FBOTexture_RGBA8_2;
		delete m_FBOTexture_RGBA8_3;
	}


	void FBOContainer::Initialize(int screenWidth, int screenHeight)
	{
		//Generate the FBO
		glGenFramebuffers(1, &m_FBO);
		glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

		m_FBOTexture_D24S8_1->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8);

		m_FBOTexture_RGBA32_1->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA, GL_UNSIGNED_BYTE);
		m_FBOTexture_RGBA32_2->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA, GL_UNSIGNED_BYTE);

		m_FBOTexture_RGBA16_1->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA16F, GL_RGBA, GL_UNSIGNED_BYTE);

		m_FBOTexture_RGBA8_1->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
		m_FBOTexture_RGBA8_2->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
		m_FBOTexture_RGBA8_3->Initialize(GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA16F, GL_RGBA, GL_UNSIGNED_BYTE);

		for (int i = 0; i < GFX_HDR_BLOOM_DOWNSAMPLES; i++)
		{
			m_bloomTextures[i]->Initialize(GL_TEXTURE_2D, GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA, GL_UNSIGNED_BYTE);
			m_intermediateBlurTextures[i]->Initialize(GL_TEXTURE_2D, GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA, GL_UNSIGNED_BYTE);
		}

		Resize(screenWidth, screenHeight);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D,	m_FBOTexture_D24S8_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,		GL_TEXTURE_2D,	m_FBOTexture_RGBA16_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,		GL_TEXTURE_2D,	m_FBOTexture_RGBA8_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,		GL_TEXTURE_2D,	m_FBOTexture_RGBA8_2->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3,		GL_TEXTURE_2D,	m_FBOTexture_RGBA8_3->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4,		GL_TEXTURE_2D,	m_FBOTexture_RGBA32_1->GetTextureHandle(), 0);


		// define outputs
		GLenum drawBuffers[] = { GL_NONE, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
		glDrawBuffers(6, drawBuffers);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		
		if (status != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "Error initialising the FBO\n";

		//reset the framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void FBOContainer::Resize(int screenWidth, int screenHeight)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		m_FBOTexture_D24S8_1->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA16_1->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA8_1->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA8_2->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA8_3->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA32_1->UpdateResolution(screenWidth, screenHeight);
		m_FBOTexture_RGBA32_2->UpdateResolution(screenWidth, screenHeight);


		float x = screenWidth;
		float y = screenHeight;
		glm::vec2 size = glm::vec2(x, y);
		for (int i = 0; i < m_bloomTextures.size(); i++)
		{
			m_bloomTextures[i]->UpdateResolution(size.x, size.y);
			m_intermediateBlurTextures[i]->UpdateResolution(size.x, size.y);
			size *= 0.5f;
		}
	}

	void FBOContainer::BindGBuffer()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_FBOTexture_D24S8_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_FBOTexture_RGBA16_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_FBOTexture_RGBA8_1->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_FBOTexture_RGBA8_2->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_FBOTexture_RGBA8_3->GetTextureHandle(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, m_FBOTexture_RGBA32_1->GetTextureHandle(), 0);

		// define outputs
		GLenum drawBuffers[] = { GL_NONE, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
		glDrawBuffers(6, drawBuffers);
	}

	void FBOContainer::BindFBOTextures(int fboTextureCount, FBOTexture** textureArray, bool depthStencil)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);
		GLenum* drawBuffers = new GLenum[fboTextureCount];
		int i = 0;
		//First texture should always be your depth stencil
		if (depthStencil)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, textureArray[0]->GetTextureHandle(), 0);
			drawBuffers[0] = GL_NONE;
			i++;
		}

		for (; i < fboTextureCount; i++)
		{
			int defineIndex = 0;
			if (depthStencil)
				defineIndex = i - 1;
			else
				defineIndex = i;

			glFramebufferTexture2D(GL_FRAMEBUFFER, m_colorAttachmentDefines[defineIndex], GL_TEXTURE_2D, textureArray[i]->GetTextureHandle(), 0);
			drawBuffers[i] = m_colorAttachmentDefines[defineIndex];
		}
		
		//GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(fboTextureCount, drawBuffers);
		delete drawBuffers;
	}

}