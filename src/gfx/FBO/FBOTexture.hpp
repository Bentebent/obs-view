#ifndef SRC_GFX_FBO_FBOTEXTURE_HPP
#define SRC_GFX_FBO_FBOTEXTURE_HPP

#include <GLEW/glew.h>

namespace gfx
{
	class FBOTexture
	{
	private:
		GLuint m_textureHandle;
		GLenum m_textureTarget;
		GLenum m_type;
		GLint m_magFilter;
		GLint m_minFilter;
		GLint m_wrapS;
		GLint m_wrapT;
		GLint m_internalFormat;
		GLint m_format;
		GLint m_baseLevel;
		GLint m_maxLevel;

		int m_height;
		int m_width;

		void GenerateTexture();
		void GenerateMipMappedTexture(int resolution);
	public:

		FBOTexture();
		~FBOTexture();

		void Initialize(GLenum textureTarget, GLint magFilter, GLint minFilter, GLint wrapModeS,
			GLint wrapModeT, GLint internalFormat, GLint format, GLenum type);

		void InitializeMipMapped(GLenum textureTarget, GLint magFilter, GLint minFilter, GLint wrapModeS,
			GLint wrapModeT, GLint internalFormat, GLint format, GLenum type, GLint textureBaseLevel, GLint textureMaxLevel, int resolution);

		void UpdateResolution(int width, int height);

		inline GLuint GetTextureHandle()
		{
			return m_textureHandle;
		}

		inline int GetWidth()
		{
			return m_width;
		}

		inline int GetHeight()
		{
			return m_height;
		}
	};
}

#endif