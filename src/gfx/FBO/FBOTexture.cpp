#include "FBOTexture.hpp"

namespace gfx
{
	FBOTexture::FBOTexture()
	{
		m_textureHandle = 0;
		m_textureTarget = 0;
		m_magFilter = 0;
		m_minFilter = 0;
		m_wrapS = 0;
		m_wrapT = 0;
		m_internalFormat = 0;
		m_format = 0;
		m_width = 0;
		m_height = 0;
		m_baseLevel = 0;
		m_maxLevel = 0;
	}

	FBOTexture::~FBOTexture()
	{
		glDeleteTextures(1, &m_textureHandle);
	}

	void FBOTexture::Initialize(GLenum textureTarget, GLint magFilter, GLint minFilter, GLint wrapModeS,
		GLint wrapModeT, GLint internalFormat, GLint format, GLenum type)
	{
		m_textureHandle = 0;
		m_textureTarget = textureTarget;

		m_magFilter = magFilter;
		m_minFilter = minFilter;
		m_wrapS = wrapModeS;
		m_wrapT = wrapModeT;

		m_internalFormat = internalFormat;
		m_format = format;
		m_type = type;

		this->GenerateTexture();
	}

	void FBOTexture::InitializeMipMapped(GLenum textureTarget, GLint magFilter, GLint minFilter, GLint wrapModeS,
		GLint wrapModeT, GLint internalFormat, GLint format, GLenum type, GLint textureBaseLevel, GLint textureMaxLevel, int resolution)
	{
		m_textureHandle = 0;
		m_textureTarget = textureTarget;

		m_magFilter = magFilter;
		m_minFilter = minFilter;
		m_wrapS = wrapModeS;
		m_wrapT = wrapModeT;

		m_internalFormat = internalFormat;
		m_format = format;
		m_type = type;

		m_baseLevel = textureBaseLevel;
		m_maxLevel = textureMaxLevel;
	}

	void FBOTexture::UpdateResolution(int width, int height)
	{
		//No need to resize if they're the same size
		if (m_width == width && m_height == height)
			return;

		glBindTexture(m_textureTarget, m_textureHandle);

		glTexImage2D(m_textureTarget, 0, m_internalFormat, width, height, 0, m_format, m_type, nullptr);

		m_width = width;
		m_height = height;
	}

	void FBOTexture::GenerateTexture()
	{
		glGenTextures(1, &m_textureHandle);
		glBindTexture(m_textureTarget, m_textureHandle);

		glTexParameteri(m_textureTarget, GL_TEXTURE_MAG_FILTER, m_magFilter);
		glTexParameteri(m_textureTarget, GL_TEXTURE_MIN_FILTER, m_minFilter);
		glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_S, m_wrapS);
		glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_T, m_wrapT);
	}

	void FBOTexture::GenerateMipMappedTexture(int resolution)
	{
		glGenTextures(1, &m_textureHandle);
		glBindTexture(m_textureTarget, m_textureHandle);

		glTexParameteri(m_textureTarget, GL_TEXTURE_MAG_FILTER, m_magFilter);
		glTexParameteri(m_textureTarget, GL_TEXTURE_MIN_FILTER, m_minFilter);
		glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_S, m_wrapS);
		glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_T, m_wrapT);

		glTexImage2D(m_textureTarget, 0, m_internalFormat, resolution, resolution, 0, m_format, m_type, nullptr);

		GLfloat border[4] = { 1.0f, 1.0f, 0.0f, 0.0f };
		glTexParameterfv(m_textureTarget, GL_TEXTURE_BORDER_COLOR, border);

		glTexParameteri(m_textureTarget, GL_TEXTURE_BASE_LEVEL, m_baseLevel);
		glTexParameteri(m_textureTarget, GL_TEXTURE_MAX_LEVEL, m_maxLevel);
		glGenerateMipmap(m_textureTarget);

		m_width = resolution;
		m_height = resolution;
	}

}