#include "SettingsContainer.hpp"

namespace gfx
{
	SettingsContainer& Settings()
	{
		static SettingsContainer sc;
		return sc;
	}


	SettingsContainer::SettingsContainer()
	{
		//HDR and tonemapping settings
		m_whitePoint = glm::vec3(1.0f);
		m_gamma = 2.2f;
		m_staticExposure = 1.0f;

		m_showDebugRT = true;
	}

	SettingsContainer::~SettingsContainer()
	{

	}
}