#ifndef SRC_GFX_SETTINGS_SETTINGSCONTAINER_HPP
#define SRC_GFX_SETTINGS_SETTINGSCONTAINER_HPP

#include <glm/glm.hpp>

namespace gfx
{
	class SettingsContainer
	{
	public:
		friend SettingsContainer& Settings();

		//HDR and tonemapping settings
		glm::vec3 m_whitePoint;
		float m_gamma;
		float m_staticExposure;
		float m_deltaTime;

		//Framebuffer dimensions
		int m_clientWidth;
		int m_clientHeight;

		//Debug settings
		bool m_showDebugRT;

	private:
		SettingsContainer();
		~SettingsContainer();
	};

	SettingsContainer& Settings();
}


#endif