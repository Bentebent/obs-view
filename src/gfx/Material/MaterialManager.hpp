#ifndef SRC_GFX_MATERIAL_MATERIALMANAGER_HPP
#define SRC_GFX_MATERIAL_MATERIALMANAGER_HPP
#include <gfx/GFXMaterial.hpp>
#include <vector>
#include <Shaders/ShaderManager.hpp>
#include <Shaders/UniformContainer.hpp>

namespace gfx
{
	class MaterialManager
	{
	public:
		friend MaterialManager& MaterialManagerInstance();

		void Initialize(ShaderManager* shaderManager);

		void CreateMaterial(unsigned long long int& id);
		void DeleteMaterial(const unsigned long long int& id);
		int SetTexture(const unsigned long long int& materialID, const unsigned long long int& textureID, TextureType type);
		void RemoveTexture(const unsigned long long int& materialID, const unsigned long long int& textureID);

		void UpdateMaterialsTexture(const unsigned long long int& textureID, const unsigned int& bindlessPosition);

		void GetShaderID(unsigned int& shaderID, const char* shaderName);
		GLuint GetShaderProgramID(unsigned int& shaderID);

		UniformContainer* GetUniformContainer(unsigned int& shaderID);

		inline Material GetMaterial(const unsigned long long int& materialID)
		{
			unsigned int index = FindMaterial(materialID);

			if (index != std::numeric_limits<unsigned int>::max())
				return m_materials[index];

			//TODO: Change to something more specific in the return, so the renderer doesn't have to guess
			Material m = Material();

			return m;
		}

	private:
		MaterialManager();
		~MaterialManager();

		ShaderManager* m_shaderManager;

		std::vector<Material> m_materials;
		unsigned long long m_idCounter;

		inline const unsigned int FindMaterial(unsigned long long id)
		{
			for (unsigned int i = 0; i < m_materials.size(); i++)
			if (m_materials[i].id == static_cast<unsigned int>(id))
				return i;

			return std::numeric_limits<unsigned int>::max();
		}
	};

	MaterialManager& MaterialManagerInstance();
}

#endif