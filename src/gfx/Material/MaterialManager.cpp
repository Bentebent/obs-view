#include "MaterialManager.hpp"
#include <gfx/GFXDefines.hpp>
#include <Texture/TextureManager.hpp>

namespace gfx
{
	MaterialManager& MaterialManagerInstance()
	{
		static MaterialManager mm;
		return mm;
	}


	MaterialManager::MaterialManager()
	{
		m_idCounter = 1;
	}

	MaterialManager::~MaterialManager()
	{

	}

	void MaterialManager::Initialize(ShaderManager* shaderManager)
	{
		m_shaderManager = shaderManager;
	}

	void MaterialManager::CreateMaterial(unsigned long long int& id)
	{
		Material material;
		material.id = static_cast<unsigned int>(m_idCounter);
		m_idCounter++;
		m_materials.push_back(material);
		id = material.id;
	}

	void MaterialManager::DeleteMaterial(const unsigned long long int& id)
	{
		unsigned int index = FindMaterial(id);
		if (index != std::numeric_limits<unsigned int>::max())
		{
			m_materials.erase(m_materials.begin() + index);
		}
	}

	int MaterialManager::SetTexture(const unsigned long long int& materialID, const unsigned long long int& textureID, TextureType type)
	{
		unsigned int index = FindMaterial(materialID);
		if (index != std::numeric_limits<unsigned int>::max())
		{
			switch (type)
			{
			case TextureType::ALBEDO:
				m_materials[index].albedo.textureID = textureID;
				m_materials[index].albedo.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			case TextureType::AO_CAVITY_ROUGHNESS:
				m_materials[index].ao_cavity_roughness.textureID = textureID;
				m_materials[index].ao_cavity_roughness.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			case TextureType::DISPLACEMENT:
				m_materials[index].displacement.textureID = textureID;
				m_materials[index].displacement.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			case TextureType::EMISSIVE:
				m_materials[index].emissive.textureID = textureID;
				m_materials[index].emissive.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			case TextureType::NORMAL:
				m_materials[index].normal.textureID = textureID;
				m_materials[index].normal.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			case TextureType::SPECULAR:
				m_materials[index].specular.textureID = textureID;
				m_materials[index].specular.bindlessPosition = TextureManagerInstance().GetTexture(textureID).bindlessPosition;
				break;
			}
			//m_materials[index].textures.push_back(textureID);
			return GFX_SUCCESS;
		}
		else
		{
			return GFX_FAILURE;
		}
	}
	
	void MaterialManager::UpdateMaterialsTexture(const unsigned long long int& textureID, const unsigned int& bindlessPosition)
	{
		for (int i = 0; i < m_materials.size(); i++)
		{
			if (m_materials[i].albedo.textureID == textureID)
				m_materials[i].albedo.bindlessPosition = bindlessPosition;

			if (m_materials[i].ao_cavity_roughness.textureID == textureID)
				m_materials[i].ao_cavity_roughness.bindlessPosition = bindlessPosition;

			if (m_materials[i].displacement.textureID == textureID)
				m_materials[i].displacement.bindlessPosition = bindlessPosition;

			if (m_materials[i].emissive.textureID == textureID)
				m_materials[i].emissive.bindlessPosition = bindlessPosition;

			if (m_materials[i].normal.textureID == textureID)
				m_materials[i].normal.bindlessPosition = bindlessPosition;

			if (m_materials[i].specular.textureID == textureID)
				m_materials[i].specular.bindlessPosition = bindlessPosition;
		}
	}

	void MaterialManager::RemoveTexture(const unsigned long long int& materialID, const unsigned long long int& textureID)
	{
		unsigned int index = FindMaterial(materialID);
		if (index != std::numeric_limits<unsigned int>::max())
		{
			//for (unsigned int i = 0; i < m_materials[index].textures.size(); i++)
			//{
			//	if (m_materials[index].textures[i] == textureID)
			//	{
			//		m_materials[index].textures.erase(m_materials[index].textures.begin() + i);
			//	}
			//}
		}
	}

	void MaterialManager::GetShaderID(unsigned int& shaderID, const char* shaderName)
	{
		shaderID = m_shaderManager->GetShaderProgramIndex(std::string(shaderName));
	}

	GLuint MaterialManager::GetShaderProgramID(unsigned int& shaderID)
	{
		return m_shaderManager->GetShaderProgramID(shaderID);
	}


	UniformContainer* MaterialManager::GetUniformContainer(unsigned int& shaderID)
	{
		return m_shaderManager->GetUniformContainer(shaderID);
	}
}