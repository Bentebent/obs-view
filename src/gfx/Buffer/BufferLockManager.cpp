#include "BufferLockManager.hpp"
#include <logger/Logger.hpp>

namespace gfx
{
	GLuint64 kOneSecondInNanoSeconds = 1000000000;


	BufferLockManager::BufferLockManager(bool cpuUpdates)
	{
		m_CPUUpdates = cpuUpdates;
	}

	BufferLockManager::~BufferLockManager()
	{
		for (std::vector<BufferLock>::iterator it = m_bufferLocks.begin(); it != m_bufferLocks.end(); ++it) 
		{
			Cleanup(&*it);
		}

		m_bufferLocks.clear();
	}

	void BufferLockManager::WaitForLockedRange(unsigned int lockBeginBytes, unsigned int lockLength)
	{
		BufferRange testRange = { lockBeginBytes, lockLength };
		std::vector<BufferLock> swapLocks;

		for (std::vector<BufferLock>::iterator it = m_bufferLocks.begin(); it != m_bufferLocks.end(); ++it)
		{
			if (testRange.Overlaps(it->range))
			{
				Wait(&it->syncObj);
				Cleanup(&*it);
			}
			else
			{
				swapLocks.push_back(*it);
			}
		}

		m_bufferLocks.swap(swapLocks);
	}

	void BufferLockManager::LockRange(unsigned int lockBeginBytes, unsigned int lockLength)
	{
		BufferRange newRange = { lockBeginBytes, lockBeginBytes };
		GLsync syncName = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		BufferLock newLock = { newRange, syncName };

		m_bufferLocks.push_back(newLock);
	}

	void BufferLockManager::Wait(GLsync* syncObj)
	{
		if (m_CPUUpdates)
		{
			GLbitfield waitFlags = 0;
			GLuint64 waitDuration = 0;

			while (1)
			{
				GLenum waitRet = glClientWaitSync(*syncObj, waitFlags, waitDuration);

				if (waitRet == GL_ALREADY_SIGNALED || waitRet == GL_CONDITION_SATISFIED) 
				{
					return;
				}

				if (waitRet == GL_WAIT_FAILED) 
				{
					LOG_FATAL << "Waiting failed for some reason" << std::endl;
					return;
				}

				// After the first time, need to start flushing, and wait for a looong time.
				waitFlags = GL_SYNC_FLUSH_COMMANDS_BIT;
				waitDuration = kOneSecondInNanoSeconds;
			}
		}
		else
		{
			glWaitSync(*syncObj, 0, GL_TIMEOUT_IGNORED);
		}
	}

	void BufferLockManager::Cleanup(BufferLock* bufferLock)
	{
		glDeleteSync(bufferLock->syncObj);
	}

}