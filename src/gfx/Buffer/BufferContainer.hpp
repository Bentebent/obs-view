#ifndef SRC_GFX_BUFFER_BUFFERCONTAINER_HPP
#define SRC_GFX_BUFFER_BUFFERCONTAINER_HPP

#include <Buffer/SSBO.hpp>
#include <gfx/GFXInstanceData.hpp>
#include <gfx/GFXLightData.hpp>

namespace gfx
{
	class BufferContainer
	{
	public:
		friend BufferContainer& BufferContainerInstance();

		void Initialize();
		void DestroyAll();

		SSBO<InstanceData>* m_instanceData;
		SSBO<LightData>* m_lightData;

	private:
		BufferContainer();
		~BufferContainer();
	};

	BufferContainer& BufferContainerInstance();
}


#endif