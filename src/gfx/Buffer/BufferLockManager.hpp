#ifndef SRC_GFX_BUFFER_BUFFERLOCKMANAGER_HPP
#define SRC_GFX_BUFFER_BUFFERLOCKMANAGER_HPP

#include <Buffer/BufferDefines.hpp>

#include <GLEW/glew.h>
#include <vector>

namespace gfx
{
	class BufferLockManager
	{
	public:
		BufferLockManager(bool cpuUpdates);
		~BufferLockManager();

		void WaitForLockedRange(unsigned int lockBeginBytes, unsigned int lockLength);
		void LockRange(unsigned int lockBeginBytes, unsigned int lockLength);

	private:
		void Wait(GLsync* syncobj);
		void Cleanup(BufferLock* bufferLock);

		std::vector<BufferLock> m_bufferLocks;
		bool m_CPUUpdates;
	};
}

#endif