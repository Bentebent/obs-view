#include "BufferContainer.hpp"
#include <gfx/GFXDefines.hpp>

namespace gfx
{
	BufferContainer& BufferContainerInstance()
	{
		static BufferContainer bc;
		return bc;
	}

	BufferContainer::BufferContainer()
	{

	}

	BufferContainer::~BufferContainer()
	{

	}

	void BufferContainer::Initialize()
	{
		m_instanceData = new SSBO<InstanceData>(0, GFX_MAX_COMMAND_SIZE);
		m_lightData = new SSBO<LightData>(3, GFX_MAX_LIGHTS);
	}

	void BufferContainer::DestroyAll()
	{
		delete m_instanceData;
		delete m_lightData;
	}
}