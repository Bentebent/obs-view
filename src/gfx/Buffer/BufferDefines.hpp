#ifndef SRC_GFX_BUFFER_BUFFERDEFINES_HPP
#define SRC_GFX_BUFFER_BUFFERDEFINES_HPP

#include <GLEW/glew.h>

namespace gfx
{
	struct BufferRange
	{
		unsigned int startOffset;
		unsigned int length;

		bool Overlaps(const BufferRange& rhs) const 
		{
			return startOffset < (rhs.startOffset + rhs.length) && rhs.startOffset < (startOffset + length);
		}
	};

	struct BufferLock
	{
		BufferRange range;
		GLsync syncObj;
	};
}

#endif