#ifndef SRC_GFX_BUFFER_SSBO_HPP
#define SRC_GFX_BUFFER_SSBO_HPP

#include <Buffer/BufferLockManager.hpp>

#include <GLEW/glew.h>
#include <vector>
namespace gfx
{
	template<class T>
	class SSBO
	{
	public:
		SSBO(unsigned int bindingPoint, unsigned int size)
		{
			m_startDestOffset = 0;
			m_bufferSize = 0;

			m_bufferLockManager = new BufferLockManager(true);

			glGenBuffers(1, &m_buffer);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPoint, m_buffer);

			m_bufferSize = 3 * sizeof(T)* size;
			const GLbitfield flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
			glBufferStorage(GL_SHADER_STORAGE_BUFFER, m_bufferSize, nullptr, flags);
			m_dataPtr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_bufferSize, flags);
		}

		~SSBO()
		{

		}

		void UpdateData(std::vector<T>& data)
		{
			unsigned int rangeSize = data.size() * sizeof(T);
			//m_bufferLockManager->WaitForLockedRange(m_startDestOffset, rangeSize);

			for (unsigned int i = 0; i < data.size(); ++i)
			{
				unsigned int offset = m_startDestOffset + i * sizeof(T);
				auto dst = (uint8_t*)m_dataPtr + offset;

				memcpy(dst, &data[i], sizeof(T));
			}

			//m_bufferLockManager->LockRange(m_startDestOffset, rangeSize);
			//m_startDestOffset = (m_startDestOffset + rangeSize) % m_bufferSize;
		}

	private:
		unsigned int m_startDestOffset;
		unsigned int m_bufferSize;
		void* m_dataPtr;
		GLuint m_buffer;

		BufferLockManager* m_bufferLockManager;
	};
}

#endif