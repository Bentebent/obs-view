#ifndef SRC_GFX_VIEWPORT_VIEWPORTCONTAINER_HPP
#define SRC_GFX_VIEWPORT_VIEWPORTCONTAINER_HPP

#include <GLEW/glew.h>
#include <vector>

#include <gfx/GFXViewport.hpp>
#include <gfx/GFXDefines.hpp>

namespace gfx
{
	class ViewportContainer
	{
	public:
		friend ViewportContainer& VPContainerInstance();

		int SetVP( Viewport vp, int index);
		int SetVPCamera(glm::mat4 view, glm::mat4 proj, int index);

		inline const Viewport& GetViewport(int index) { if (index >= GFX_VIEWPORT_MAX) { Viewport vp; return vp; } else return m_viewports[index]; }
		inline const Viewport& GetCurrentViewport() { return m_viewports[m_currentViewport]; }

		void inline SetCurrentVP(int index) { m_currentViewport = index; }
		
	private:
		ViewportContainer();
		~ViewportContainer();

		std::vector<Viewport> m_viewports;
		int m_currentViewport;
	};

	ViewportContainer& VPContainerInstance();
}


#endif