#include "ViewportContainer.hpp"


namespace gfx
{
	ViewportContainer& VPContainerInstance()
	{
		static ViewportContainer vpc;
		return vpc;
	}

	ViewportContainer::ViewportContainer()
	{
		m_viewports.resize(GFX_VIEWPORT_MAX);
	}

	ViewportContainer::~ViewportContainer()
	{

	}

	int ViewportContainer::SetVP(Viewport vp, int index)
	{
		if (index >= GFX_VIEWPORT_MAX)
			return GFX_FAILURE;

		m_viewports[index] = vp;

		return GFX_SUCCESS;
	}

	int ViewportContainer::SetVPCamera(glm::mat4 view, glm::mat4 proj, int index)
	{
		if (index >= GFX_VIEWPORT_MAX)
			return GFX_FAILURE;
		m_viewports[index].prevViewProjection = m_viewports[index].projection * m_viewports[index].view;
		m_viewports[index].view = view;
		m_viewports[index].projection = proj;

		return GFX_SUCCESS;
	}


};
