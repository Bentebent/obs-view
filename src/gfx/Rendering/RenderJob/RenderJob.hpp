#ifndef SRC_GFX_RENDERING_RENDERJOB_RENDERJOB_HPP
#define SRC_GFX_RENDERING_RENDERJOB_RENDERJOB_HPP

#include <gfx/GFXInstanceData.hpp>
#include <bitmask/dynamic_bitmask.hpp>
namespace gfx
{
	struct RenderJob
	{
		Bitmask bitmask;
		void* data;
	};
}

#endif