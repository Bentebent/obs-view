#include "RenderJobManager.hpp"
#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXDefines.hpp>

#include <Mesh/MeshManager.hpp>
#include <Material/MaterialManager.hpp>
#include <Texture/TextureManager.hpp>
#include <Shaders/ShaderManager.hpp>

#include <Settings/SettingsContainer.hpp>
#include <gfx/GFXSettingTypes.hpp>

#include <algorithm>
#include <cassert>
#include <iostream>

namespace gfx
{
	int compare_ints(const void* a, const void* b)   // comparison function
	{
		gfx::RenderJob arg1 = *reinterpret_cast<const gfx::RenderJob*>(a);
		gfx::RenderJob arg2 = *reinterpret_cast<const gfx::RenderJob*>(b);
		if (arg1.bitmask > arg2.bitmask) return -1;
		if (arg1.bitmask < arg2.bitmask) return 1;
		return 0;
	}


	RenderJobManager& RenderJobManagerInstance( int option )
	{
		static RenderJobManager rjm1;
		static RenderJobManager rjm2;
		
		static RenderJobManager* current = &rjm1;
		static RenderJobManager* next	 = &rjm2;

		switch( option )
		{
		case USE_CURRENT_BUFFER :
			return *current;
			break;
		
		case USE_NEXT_BUFFER : 
			return *next;
			break;
		
		case SWAP_JOB_BUFFERS : 
			if( current == &rjm1 )
			{
				current = &rjm2;
				next	= &rjm1;
			}
			else
			{
				current = &rjm1;
				next	= &rjm2;
			}

			return *current;
			break;
		
		default :
			return *current;
			break;
		}
	}


	RenderJobManager::RenderJobManager()
	{
		m_renderJobs.reserve(100000);
		m_renderBatchCount = 0;
		m_bitmaskSystem.Init();
	}

	RenderJobManager::~RenderJobManager()
	{

	}

	void RenderJobManager::Initialize(ShaderContainer* shaderContainer)
	{
		m_shaderContainer = shaderContainer;
	}

	void RenderJobManager::Sort()
	{
		std::qsort(m_renderJobs.data(), m_renderJobs.size(), sizeof(RenderJob), compare_ints);
	}

	void RenderJobManager::Decode()
	{
		m_instanceData.clear();
		m_renderBatchCount = -1;

		bool loadingLights = false;

		unsigned int currentShader = std::numeric_limits<decltype(currentShader)>::max();
		unsigned int currentLayer = std::numeric_limits<decltype(currentLayer)>::max();
		unsigned int currentViewport = std::numeric_limits<decltype(currentViewport)>::max();
		unsigned int currentTransluceny = std::numeric_limits<decltype(currentTransluceny)>::max();
		unsigned int currentMaterial = std::numeric_limits<decltype(currentMaterial)>::max();
		unsigned int currentMesh = std::numeric_limits<decltype(currentMesh)>::max();
		 
		unsigned int command = std::numeric_limits<decltype(command)>::max();
		unsigned int type = std::numeric_limits<decltype(type)>::max();
		unsigned int viewport = std::numeric_limits<decltype(viewport)>::max();
		unsigned int layer = std::numeric_limits<decltype(layer)>::max();
		unsigned int transluceny = std::numeric_limits<decltype(transluceny)>::max();
		unsigned int shader = std::numeric_limits<decltype(shader)>::max();
		unsigned int mesh = std::numeric_limits<decltype(mesh)>::max();
		unsigned int material = std::numeric_limits<decltype(material)>::max();

		InstanceData instanceData;
		Material mat;
		IndirectMesh iMesh;

		for (std::vector<RenderJob>::iterator it = m_renderJobs.begin(); it != m_renderJobs.end(); ++it)
		{
			command = m_bitmaskSystem.Decode(it->bitmask, MaskType::COMMAND);
			
			if (command == COMMAND_TYPES::COMMAND)
			{
				type = m_bitmaskSystem.Decode(it->bitmask, MaskType::TYPE);

				if (type == SUB_COMMAND_TYPES::SETTINGS_COMMAND)
				{
					DecodeSettings(*it);
				}
				else if (type == SUB_COMMAND_TYPES::RENDER_COMMAND)
				{

				}
				else if (type == SUB_COMMAND_TYPES::RELOAD_SHADERS_COMMAND)
				{
					m_shaderContainer->LoadShaders();
				}
			}
			else
			{
				type = m_bitmaskSystem.Decode(it->bitmask, MaskType::TYPE);

				if (type == OBJECT_TYPES::OPAQUE_GEOMETRY)
				{
					bool changedViewport			= false;
					bool changedShader				= false;
					bool changedTranslucency		= false;


					viewport = m_bitmaskSystem.Decode(it->bitmask, MaskType::VIEWPORT_ID);
					
					if (viewport != currentViewport)
					{
						currentViewport = viewport;
						changedViewport = true;
					}

					transluceny = m_bitmaskSystem.Decode(it->bitmask, MaskType::TRANSLUCENCY);

					if (transluceny != currentTransluceny)
					{
						currentTransluceny = transluceny;
						changedTranslucency = true;
					}

					shader = m_bitmaskSystem.Decode(it->bitmask, MaskType::SHADER);

					if (shader != currentShader)
					{
						currentShader = shader;
						changedShader = true;
					}

					material = m_bitmaskSystem.Decode(it->bitmask, MaskType::TEXTURE);

					if (material != currentMaterial)
					{
						currentMaterial = material;
						mat = MaterialManagerInstance().GetMaterial(currentMaterial);

						instanceData.albedo_specular_acr_normal = glm::vec4(mat.albedo.bindlessPosition, mat.specular.bindlessPosition, mat.ao_cavity_roughness.bindlessPosition, mat.normal.bindlessPosition);
						instanceData.disp_emi_pad_pad = glm::vec4(mat.displacement.bindlessPosition, mat.emissive.bindlessPosition, 0, 0);

						//instanceData.albedo_specular_acr_normal = glm::vec4(mat.albedo.textureID, mat.specular.textureID, mat.ao_cavity_roughness.textureID, mat.normal.textureID);
						//instanceData.disp_emi_pad_pad = glm::vec4(mat.displacement.textureID, mat.emissive.textureID, 0, 0);
					}

					mesh = m_bitmaskSystem.Decode(it->bitmask, MaskType::MESH);

					if (mesh != currentMesh)
					{
						currentMesh = mesh;
						iMesh = MeshManagerInstance().GetIndirectMesh(currentMesh);
					}

					//If anything has changed, move on to a new batch
					//else, keep filling current batch

					if (changedShader | changedTranslucency | changedViewport)
					{
						m_renderBatchCount++;
						//m_renderBatches[m_renderBatchCount].shaderID			= currentShader;
						m_renderBatches[m_renderBatchCount].shaderID			= MaterialManagerInstance().GetShaderProgramID(currentShader);
						m_renderBatches[m_renderBatchCount].uniformContainer	= MaterialManagerInstance().GetUniformContainer(currentShader);
						m_renderBatches[m_renderBatchCount].viewportID			= currentViewport;
						m_renderBatches[m_renderBatchCount].translucency		= currentTransluceny;

						DrawElementsIndirectBindlessCommandNV cmd;
						cmd.cmd.count = iMesh.indexCount;
						cmd.cmd.instanceCount = 1;
						cmd.cmd.firstIndex = 0;
						cmd.cmd.baseVertex = 0;
						cmd.cmd.baseInstance = 0;

						cmd.reserved = 0;
						cmd.indexBuffer.index = 0;
						cmd.indexBuffer.reserved = 0;
						cmd.indexBuffer.address = iMesh.ib_addrs;
						cmd.indexBuffer.length = iMesh.ib_sizes;

						cmd.vertexBuffers[0].index = 0;
						cmd.vertexBuffers[0].reserved = 0;
						cmd.vertexBuffers[0].address = iMesh.vbo_addrs + offsetof(StaticVertex, position);
						cmd.vertexBuffers[0].length = iMesh.vbo_sizes - offsetof(StaticVertex, position);

						cmd.vertexBuffers[1].index = 1;
						cmd.vertexBuffers[1].reserved = 0;
						cmd.vertexBuffers[1].address = iMesh.vbo_addrs + offsetof(StaticVertex, normal);
						cmd.vertexBuffers[1].length = iMesh.vbo_sizes - offsetof(StaticVertex, normal);

						cmd.vertexBuffers[2].index = 2;
						cmd.vertexBuffers[2].reserved = 0;
						cmd.vertexBuffers[2].address = iMesh.vbo_addrs + offsetof(StaticVertex, tangent);
						cmd.vertexBuffers[2].length = iMesh.vbo_sizes - offsetof(StaticVertex, tangent);

						cmd.vertexBuffers[3].index = 3;
						cmd.vertexBuffers[3].reserved = 0;
						cmd.vertexBuffers[3].address = iMesh.vbo_addrs + offsetof(StaticVertex, uv);
						cmd.vertexBuffers[3].length = iMesh.vbo_sizes - offsetof(StaticVertex, uv);
						m_renderBatches[m_renderBatchCount].m_commands.clear();
						m_renderBatches[m_renderBatchCount].m_commands.push_back(cmd);

						instanceData.modelMatrix = ((InstanceData*)it->data)->modelMatrix;
						instanceData.prevModelMatrix = ((InstanceData*)it->data)->prevModelMatrix;
						m_instanceData.push_back(instanceData);
					}
					else
					{
						DrawElementsIndirectBindlessCommandNV cmd;
						cmd.cmd.count = iMesh.indexCount;
						cmd.cmd.instanceCount = 1;
						cmd.cmd.firstIndex = 0;
						cmd.cmd.baseVertex = 0;
						cmd.cmd.baseInstance = 0;

						cmd.reserved = 0;
						cmd.indexBuffer.index = 0;
						cmd.indexBuffer.reserved = 0;
						cmd.indexBuffer.address = iMesh.ib_addrs;
						cmd.indexBuffer.length = iMesh.ib_sizes;

						cmd.vertexBuffers[0].index = 0;
						cmd.vertexBuffers[0].reserved = 0;
						cmd.vertexBuffers[0].address = iMesh.vbo_addrs + offsetof(StaticVertex, position);
						cmd.vertexBuffers[0].length = iMesh.vbo_sizes - offsetof(StaticVertex, position);

						cmd.vertexBuffers[1].index = 1;
						cmd.vertexBuffers[1].reserved = 0;
						cmd.vertexBuffers[1].address = iMesh.vbo_addrs + offsetof(StaticVertex, normal);
						cmd.vertexBuffers[1].length = iMesh.vbo_sizes - offsetof(StaticVertex, normal);

						cmd.vertexBuffers[2].index = 2;
						cmd.vertexBuffers[2].reserved = 0;
						cmd.vertexBuffers[2].address = iMesh.vbo_addrs + offsetof(StaticVertex, tangent);
						cmd.vertexBuffers[2].length = iMesh.vbo_sizes - offsetof(StaticVertex, tangent);

						cmd.vertexBuffers[3].index = 3;
						cmd.vertexBuffers[3].reserved = 0;
						cmd.vertexBuffers[3].address = iMesh.vbo_addrs + offsetof(StaticVertex, uv);
						cmd.vertexBuffers[3].length = iMesh.vbo_sizes - offsetof(StaticVertex, uv);

						m_renderBatches[m_renderBatchCount].m_commands.push_back(cmd);
						instanceData.modelMatrix = ((InstanceData*)it->data)->modelMatrix;
						instanceData.prevModelMatrix = ((InstanceData*)it->data)->prevModelMatrix;
						bool same = instanceData.modelMatrix == instanceData.prevModelMatrix;
						m_instanceData.push_back(instanceData);
					}

				}
				else if (type == OBJECT_TYPES::LIGHT && !loadingLights)
				{
					DecodeLights(std::distance(m_renderJobs.begin(), it));
					loadingLights = true;
				}
			}
		}
	}

	void RenderJobManager::DecodeLights(unsigned int lightStartIndex)
	{
		m_lightData.clear();

		unsigned int command = std::numeric_limits<decltype(command)>::max();
		unsigned int type = std::numeric_limits<decltype(type)>::max();
		unsigned int viewport = std::numeric_limits<decltype(viewport)>::max();
		unsigned int layer = std::numeric_limits<decltype(layer)>::max();
		unsigned int transluceny = std::numeric_limits<decltype(transluceny)>::max();
		unsigned int shader = std::numeric_limits<decltype(shader)>::max();
		unsigned int lightType = std::numeric_limits<decltype(lightType)>::max();
		unsigned int material = std::numeric_limits<decltype(material)>::max();

		int lightCount = 0;

		memset(m_totalLights, 0, sizeof(m_totalLights));

		for (std::vector<RenderJob>::iterator it = m_renderJobs.begin() + lightStartIndex; it != m_renderJobs.end(); ++it)
		{
			type = m_bitmaskSystem.Decode(it->bitmask, MaskType::TYPE);
			lightType = m_bitmaskSystem.Decode(it->bitmask, MaskType::MESH);
			if (type != OBJECT_TYPES::LIGHT)
			{
				return;
			}

			if (lightCount < GFX_MAX_LIGHTS)
			{
				m_lightData.push_back(*reinterpret_cast<LightData*>(it->data));
				m_totalLights[lightType]++;
				lightCount++;
			}
		}
	}

	void RenderJobManager::DecodeSettings(const RenderJob& rj)
	{
		unsigned int settingsType = m_bitmaskSystem.Decode(rj.bitmask, MaskType::MESH);

		switch (settingsType)
		{
		case SETTING_TYPES::GAMMA:
			Settings().m_gamma = *(float*)rj.data;
			break;
		case SETTING_TYPES::STATIC_EXPOSURE:
			Settings().m_staticExposure = *(float*)rj.data;
			break;
		case SETTING_TYPES::WHITE_POINT:
			break;
		}
	}

	void RenderJobManager::AddRenderJob(Bitmask bitmask, void* data)
	{
		RenderJob rj;
		rj.bitmask = bitmask;
		rj.data = data;
		m_renderJobs.push_back(rj);
	}

	void RenderJobManager::ClearRenderJobs()
	{
		m_renderJobs.clear();
	}

	std::vector<RenderJob>& RenderJobManager::GetRenderJobs()
	{
		return m_renderJobs;
	}

	std::vector<InstanceData>& RenderJobManager::GetInstanceData()
	{
		return m_instanceData;
	}

	std::vector<LightData>& RenderJobManager::GetLightData()
	{
		return m_lightData;
	}

}