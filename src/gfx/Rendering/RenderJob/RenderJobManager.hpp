#ifndef SRC_GFX_RENDERING_RENDERJOB_RENDERJOBMANAGER_HPP
#define SRC_GFX_RENDERING_RENDERJOB_RENDERJOBMANAGER_HPP

#include "RenderJob.hpp"
#include "RenderBatch.hpp"
#include <gfx/GFXLightData.hpp>
#include <Shaders/ShaderContainer.hpp>

#include <bitmask/dynamic_bitmask.hpp>
#include <bitmask/bitmask_system.hpp>
#include <vector>

#define USE_CURRENT_BUFFER	0
#define USE_NEXT_BUFFER		1
#define SWAP_JOB_BUFFERS	2

namespace gfx
{
	class RenderJobManager
	{
	public:
		friend RenderJobManager& RenderJobManagerInstance( int option );

		void Initialize(ShaderContainer* shaderContainer);

		void Sort();
		void Decode();
		void AddRenderJob(Bitmask bitmask, void* data);
		void ClearRenderJobs();
		std::vector<RenderJob>& GetRenderJobs();
		std::vector<InstanceData>& GetInstanceData();
		std::vector<LightData>& GetLightData();

		inline RenderBatch* GetRenderBatches(){ return m_renderBatches; }
		inline int GetRenderBatchCount() { return m_renderBatchCount; }
		inline unsigned int GetLightCount(int lightType)
		{ 
			return m_totalLights[lightType]; 
		}

	private:
		RenderJobManager();
		~RenderJobManager();

		void DecodeLights(unsigned int lightStartIndex);
		void DecodeSettings(const RenderJob& rj);

		std::vector<RenderJob> m_renderJobs;
		std::vector<InstanceData> m_instanceData;
		BitmaskSystem m_bitmaskSystem;
		RenderBatch m_renderBatches[1000];
		int m_renderBatchCount;

		unsigned int m_totalLights[LIGHT_TYPES::COUNT];
		std::vector<LightData> m_lightData;

		ShaderContainer* m_shaderContainer;
	};

	RenderJobManager& RenderJobManagerInstance( int option = USE_CURRENT_BUFFER );
}

#endif