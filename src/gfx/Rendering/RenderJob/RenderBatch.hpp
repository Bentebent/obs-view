#ifndef SRC_GFX_RENDERING_RENDERJOB_RENDERBATCH_HPP
#define SRC_GFX_RENDERING_RENDERJOB_RENDERBATCH_HPP

#include <gfx/GFXBitmaskDefinitions.hpp>
#include <Rendering/MultidrawIndirect/MDIDefinitions.hpp>
#include <Shaders/UniformContainer.hpp>

#include <GLEW/glew.h>
#include <vector>
namespace gfx
{
	struct RenderBatch
	{
		int viewportID;
		int shaderID;
		int translucency;
		UniformContainer* uniformContainer;
		std::vector<DrawElementsIndirectBindlessCommandNV> m_commands;
	};
}

#endif