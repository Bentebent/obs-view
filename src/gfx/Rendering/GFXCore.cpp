#include "GFXCore.hpp"

#include <Viewport/ViewportContainer.hpp>
#include <Buffer/BufferContainer.hpp>

#include <Mesh/MeshManager.hpp>
#include <Material/MaterialManager.hpp>

#include <Settings/SettingsContainer.hpp>

#include <Rendering/RenderJob/RenderJobManager.hpp>
#include <Rendering/RenderJob/RenderBatch.hpp>
#include <DebugSystems/DebugManager.hpp>
#include <utility/HighresTimer.hpp>
#include <iostream>
#include <sstream>
#include <iomanip>


//#define GPUTIME
#ifdef GPUTIME
#define CT(x, y)\
{\
if (updateStats && m_showStatistics) \
	{ \
	GLuint64 startTime, stopTime; \
	unsigned int queryID[2]; \
	glGenQueries(2, queryID); \
	glQueryCounter(queryID[0], GL_TIMESTAMP); \
	x; \
	glQueryCounter(queryID[1], GL_TIMESTAMP); \
	GLint stopTimerAvailable = 0; \
while (!stopTimerAvailable)\
		{\
		glGetQueryObjectiv(queryID[1], \
		GL_QUERY_RESULT_AVAILABLE, \
		&stopTimerAvailable); \
		}\
		glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime); \
		glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime); \
		GLuint64 result = (stopTime - startTime) / 1000.0; \
		std::chrono::duration<GLuint64, std::micro> ms(result); \
		m_subsystemTimes.push_back(std::pair<const char*, std::chrono::microseconds>(y, ms)); \
	} \
	else \
	{ \
	x; \
	} \
}
#else
#define CT(x, y)\
{\
	Timer().Start(); \
	x; \
	Timer().Stop(); \
	std::chrono::microseconds ms = Timer().GetDelta(); \
	std::cout << y << " ms: " << std::fixed << std::setw(7) << std::setprecision(4) << std::setfill('0') << ms.count() / 1000.0f << std::endl; \
}
#endif


namespace gfx
{
	GFXCore& GFXCoreInstance()
	{
		static GFXCore gfxCore;
		return gfxCore;
	}

	GFXCore::GFXCore()
	{

	}

	GFXCore::~GFXCore()
	{

	}

	void GFXCore::Initialize(int screenWidth, int screenHeight)
	{
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		m_shaderManager = new ShaderManager();
		m_shaderContainer = new ShaderContainer();
		m_FBOContainer = new FBOContainer();

		m_shaderContainer->Initialize(m_shaderManager, m_FBOContainer);

		TextureManagerInstance().Initialize();
		BufferContainerInstance().Initialize();
		MaterialManagerInstance().Initialize(m_shaderManager);
		RenderJobManagerInstance().Initialize(m_shaderContainer);
		RenderJobManagerInstance(SWAP_JOB_BUFFERS);
		RenderJobManagerInstance().Initialize(m_shaderContainer);
		m_FBOContainer->Initialize(screenWidth, screenHeight);

		m_shaderContainer->LoadShaders();

		Settings().m_clientWidth = screenWidth;
		Settings().m_clientHeight = screenHeight;

		//{{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }}
		//std::string test = "{{ 1.0, 0.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0, 0.0 }, { 0.0, 0.0, 1.0, 0.0 }, { 0.0, 0.0, 0.0, 1.0 }}";
		//glm::mat4 testVec = StringToMat4(test);
		//
		//std::vector<std::string> values = Split(test, ' ');
		//std::string foo = Trim(test);
		//std::cout << test << std::endl;
		//std::cout << foo << std::endl;

		glGenBuffers(1, &m_cmd_buffer);
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_cmd_buffer);
		glBufferStorage(GL_DRAW_INDIRECT_BUFFER, GFX_MAX_COMMAND_SIZE * sizeof(DrawElementsIndirectBindlessCommandNV), nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_DYNAMIC_STORAGE_BIT);
		m_cmd_ptr = glMapBufferRange(GL_DRAW_INDIRECT_BUFFER, 0, GFX_MAX_COMMAND_SIZE * sizeof(DrawElementsIndirectBindlessCommandNV), GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
	}

	void GFXCore::Resize(int screenWidth, int screenHeigt)
	{
		Settings().m_clientWidth = screenWidth;
		Settings().m_clientHeight = screenHeigt;

		m_FBOContainer->Resize(screenWidth, screenHeigt);
	}

	void GFXCore::Render(float dt)
	{
		Settings().m_deltaTime = dt;

		RenderJobManagerInstance().Sort();
		RenderJobManagerInstance().Decode();

		//m_FBOContainer->BindGBuffer();

		float x = Settings().m_clientWidth;
		float y = Settings().m_clientHeight;

		//glClearStencil(0);

		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		// Clear depth RT
		float c[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
		//glClearBufferfv(GL_COLOR, 1, &glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)[0]);

		//glDisable(GL_DEPTH_TEST);
		
		//glViewport(0, 0, Settings().m_clientWidth, Settings().m_clientHeight);

		//m_shaderManager->UseProgram("Skybox");
		UniformContainer::BindAll(m_shaderManager->GetUniformContainer("Skybox")->uniformBindFunctions);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		RenderBatch* rb = RenderJobManagerInstance().GetRenderBatches();

		glEnableClientState(GL_ELEMENT_ARRAY_UNIFIED_NV);
		glEnableClientState(GL_VERTEX_ATTRIB_ARRAY_UNIFIED_NV);

		glVertexAttribFormatNV(0, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex));
		glVertexAttribFormatNV(1, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex));
		glVertexAttribFormatNV(2, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex));
		glVertexAttribFormatNV(3, 2, GL_FLOAT, GL_FALSE, sizeof(StaticVertex));
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);
		glDisable(GL_BLEND);

		int currentShaderID = -1;
		int currentViewportID = -1;
		BufferContainerInstance().m_instanceData->UpdateData(RenderJobManagerInstance().GetInstanceData());

		for (int i = 0; i <= RenderJobManagerInstance().GetRenderBatchCount(); i++)
		{
			bool changedShader = false;

			if (rb->shaderID != currentShaderID)
			{
				//We should probably do some blendstate magic and shizz here too?
				glUseProgram(rb->shaderID);
				currentShaderID = rb->shaderID;
				changedShader = true;
			}

			if (currentViewportID != rb->viewportID || changedShader)
			{
				VPContainerInstance().SetCurrentVP(rb->viewportID);

				glViewport(VPContainerInstance().GetViewport(rb->viewportID).posX, VPContainerInstance().GetViewport(rb->viewportID).posY,
					VPContainerInstance().GetViewport(rb->viewportID).width, VPContainerInstance().GetViewport(rb->viewportID).height);

				UniformContainer::BindAll(rb->uniformContainer->uniformBindFunctions);
			}
			
			memcpy(m_cmd_ptr, &*rb[i].m_commands.begin(), sizeof(DrawElementsIndirectBindlessCommandNV)* rb[i].m_commands.size());
		
			glMemoryBarrier(GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT);
		
			glMultiDrawElementsIndirectBindlessNV(GL_TRIANGLES, GL_UNSIGNED_INT, nullptr, rb[i].m_commands.size(), 0, 4);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//glDisable(GL_BLEND);
		//glDisable(GL_DEPTH_TEST);
		//glDisable(GL_STENCIL_TEST);

		//Temp variables shared by subsequent render passes
		FBOTexture* textureArray[10];

		/////////////////////////////////////////////TESTING FUCK YOU///////////////////////////////////////////////////////////////////////////////
		//m_shaderManager->UseProgram("PBRLighting");

		BufferContainerInstance().m_lightData->UpdateData(RenderJobManagerInstance().GetLightData());

		UniformContainer::BindAll(m_shaderManager->GetUniformContainer("PBRLighting")->uniformBindFunctions);

		glDispatchCompute(GLuint((x + GFX_LIGHTS_WORK_GROUP_SIZE - 1) / float(GFX_LIGHTS_WORK_GROUP_SIZE)), GLuint((y + GFX_LIGHTS_WORK_GROUP_SIZE - 1) / float(GFX_LIGHTS_WORK_GROUP_SIZE)), 1);

		/////////////////////////////////////////////TESTING FUCK YOU///////////////////////////////////////////////////////////////////////////////

		//textureArray[0] = m_FBOContainer->m_FBOTexture_RGBA32_2;
		//m_FBOContainer->BindFBOTextures(1, textureArray, false);
		//
		//m_shaderManager->UseProgram("Brightpass");
		UniformContainer::BindAll(m_shaderManager->GetUniformContainer("Brightpass")->uniformBindFunctions);
		
		glDrawArrays(GL_POINTS, 0, 1);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		/////////////////////////////////////////////TESTING FUCK YOU///////////////////////////////////////////////////////////////////////////////
	
		glm::vec2 size = glm::vec2(x, y);
		for (int i = 0; i < GFX_HDR_BLOOM_DOWNSAMPLES; i++)
		{
			glViewport(0, 0, size.x, size.y);

			textureArray[0] = m_FBOContainer->m_intermediateBlurTextures[i];
			m_FBOContainer->BindFBOTextures(1, textureArray, false);

			glClear(GL_COLOR_BUFFER_BIT);

			m_shaderManager->UseProgram("GaussianBlurH");

			if (i == 0)
			{
				TextureManagerInstance().BindTexture(
					m_FBOContainer->m_FBOTexture_RGBA32_2->GetTextureHandle(),
					m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("GaussianBlurH"), "gTexture"),
					0,
					GL_TEXTURE_2D);
			}
			else
			{
				TextureManagerInstance().BindTexture(
					m_FBOContainer->m_bloomTextures[i - 1]->GetTextureHandle(),
					m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("GaussianBlurH"), "gTexture"),
					0,
					GL_TEXTURE_2D);
			}
			m_shaderManager->SetUniform((GLfloat)size.x, (GLfloat)size.y, m_shaderManager->GetUniformLocation("GaussianBlurH", "gScreenDimensions"));

			glDrawArrays(GL_POINTS, 0, 1);

			textureArray[0] = m_FBOContainer->m_bloomTextures[i];
			m_FBOContainer->BindFBOTextures(1, textureArray, false);

			m_shaderManager->UseProgram("GaussianBlurV");

			glClear(GL_COLOR_BUFFER_BIT);

			TextureManagerInstance().BindTexture(
				m_FBOContainer->m_intermediateBlurTextures[i]->GetTextureHandle(),
				m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("GaussianBlurV"), "gTexture"),
				0,
				GL_TEXTURE_2D);
			m_shaderManager->SetUniform((GLfloat)size.x, (GLfloat)size.y, m_shaderManager->GetUniformLocation("GaussianBlurV", "gScreenDimensions"));
			glDrawArrays(GL_POINTS, 0, 1);
			size *= 0.5f;
		}

		/////////////////////////////////////////////TESTING FUCK YOU///////////////////////////////////////////////////////////////////////////////
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//glViewport(0, 0, Settings().m_clientWidth, Settings().m_clientHeight);
		//textureArray[0] = m_FBOContainer->m_FBOTexture_RGBA32_2;
		//m_FBOContainer->BindFBOTextures(1, textureArray, false);
		//
		//m_shaderManager->UseProgram("Bloom");
		UniformContainer::BindAll(m_shaderManager->GetUniformContainer("Bloom")->uniformBindFunctions);
		
		glDrawArrays(GL_POINTS, 0, 1);
		
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		/////////////////////////////////////////////TESTING FUCK YOU///////////////////////////////////////////////////////////////////////////////
		RenderDebugTexture(0, 0, Settings().m_clientWidth, Settings().m_clientHeight, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA32_2->GetTextureHandle());

		if (Settings().m_showDebugRT)
		{
			RenderDebugTexture(0, 0, 256, 144, "FSQuadDepth", m_FBOContainer->m_FBOTexture_D24S8_1->GetTextureHandle());
			RenderDebugTexture(256, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA16_1->GetTextureHandle());
			RenderDebugTexture(512, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_1->GetTextureHandle());
			RenderDebugTexture(768, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_2->GetTextureHandle());
			RenderDebugTexture(1024, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_3->GetTextureHandle());
		}
		//RenderDebugTexture(0, 0, 256, 144, "FSQuadDepth", m_FBOContainer->m_FBOTexture_D24S8_1->GetTextureHandle());
		//RenderDebugTexture(256, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA16_1->GetTextureHandle());
		//RenderDebugTexture(512, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_1->GetTextureHandle());
		//RenderDebugTexture(768, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_2->GetTextureHandle());
		//RenderDebugTexture(1024, 0, 256, 144, "FSQuad", m_FBOContainer->m_FBOTexture_RGBA8_3->GetTextureHandle());

		//RenderDebugTexture(0, 0, 256, 144, "FSQuad", m_FBOContainer->m_bloomTextures[0]->GetTextureHandle());
		//RenderDebugTexture(256, 0, 256, 144, "FSQuad", m_FBOContainer->m_bloomTextures[1]->GetTextureHandle());
		//RenderDebugTexture(512, 0, 256, 144, "FSQuad", m_FBOContainer->m_bloomTextures[2]->GetTextureHandle());
		//RenderDebugTexture(768, 0, 256, 144, "FSQuad", m_FBOContainer->m_bloomTextures[3]->GetTextureHandle());
		//RenderDebugTexture(1024, 0, 256, 144, "FSQuad", m_FBOContainer->m_bloomTextures[5]->GetTextureHandle());

		//     .'/,-Y"     "~-.  
        //    l.Y             ^.           
        //    /\               _\_      "Doh!"   
        //   i            ___/"   "\ 
        //   |          /"   "\   o !   
        //   l         ]     o !__./        Merge Conflict above?!
        //    \ _  _    \.___./    "~\  
        //     X \/ \            ___./  
        //    ( \ ___.   _..--~~"   ~`-.               Use 144 as height for debug textures 
        //     ` Z,--   /               \              as it is to scale with 16/9 aspect ratio
        //       \__.  (   /       ______) 
        //         \   l  /-----~~" /      
        //          Y   \          /            Woohooo! Donuts!
        //          |    "x______.^ 
        //          |           \    
        //          j            Y 


		// Render debug information
		glViewport(0, 0, Settings().m_clientWidth, Settings().m_clientHeight);
		debugManagerRenderPointer->Render(m_shaderManager);

		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);

		RenderJobManagerInstance().ClearRenderJobs();
	}

	void GFXCore::RenderDebugTexture(int vpX, int vpY, int vpSX, int vpSY, std::string shaderProgram, GLuint textureID)
	{
		if (shaderProgram == "FSQuad")
		{
			m_shaderManager->UseProgram("FSQuad");

			glViewport(vpX, vpY, vpSX, vpSY);
			TextureManagerInstance().BindTexture(
				textureID,
				m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("FSQuad"), "gTexture"),
				0,
				GL_TEXTURE_2D);
			glDrawArrays(GL_POINTS, 0, 1);
		}
		else if (shaderProgram == "FSQuadDepth")
		{
			m_shaderManager->UseProgram("FSQuadDepth");

			m_shaderManager->SetUniform(0.01f, m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("FSQuadDepth"), "gNearZ"));
			m_shaderManager->SetUniform(1000.0f, m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("FSQuadDepth"), "gFarZ"));

			glViewport(vpX, vpY, vpSX, vpSY);
			TextureManagerInstance().BindTexture(
				textureID,
				m_shaderManager->GetUniformLocation(m_shaderManager->GetShaderProgramID("FSQuadDepth"), "gTexture"),
				0,
				GL_TEXTURE_2D);
			glDrawArrays(GL_POINTS, 0, 1);
		}
	}
}