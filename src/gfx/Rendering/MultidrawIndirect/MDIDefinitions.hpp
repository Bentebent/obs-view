#ifndef SRC_GFX_RENDERING_MULTIDRAWINDIRECT_HPP
#define SRC_GFX_RENDERING_MULTIDRAWINDIRECT_HPP

#include <GLEW/glew.h>

namespace gfx
{
	// --------------------------------------------------------------------------------------------------------------------
	// Indirect structures
	struct DrawArraysIndirectCommand
	{
		GLuint count;
		GLuint primCount;
		GLuint first;
		GLuint baseInstance;
	};

	// --------------------------------------------------------------------------------------------------------------------
	struct DrawElementsIndirectCommand
	{
		GLuint count;
		GLuint instanceCount;
		GLuint firstIndex;
		GLuint baseVertex;
		GLuint baseInstance;
	};


	// --------------------------------------------------------------------------------------------------------------------
	struct BindlessPtrNV
	{
		GLuint   index;
		GLuint   reserved;
		GLuint64 address;
		GLuint64 length;
	};

	// --------------------------------------------------------------------------------------------------------------------
	struct DrawArraysIndirectBindlessCommandNV
	{
		DrawArraysIndirectCommand   cmd;
		GLuint                      reserved;
		BindlessPtrNV               vertexBuffers[4];
	};

	// --------------------------------------------------------------------------------------------------------------------
	struct DrawElementsIndirectBindlessCommandNV
	{
		DrawElementsIndirectCommand cmd;
		GLuint                      reserved;
		BindlessPtrNV               indexBuffer;
		BindlessPtrNV               vertexBuffers[4];
	};
}

#endif
