#ifndef SRC_GFX_RENDERING_GFXCORE_HPP
#define SRC_GFX_RENDERING_GFXCORE_HPP

#include <GLEW/glew.h>
#include <Shaders/ShaderContainer.hpp>
#include <Shaders/ShaderManager.hpp>
#include <Buffer/BufferLockManager.hpp>
#include <Buffer/SSBO.hpp>
#include <gfx/GFXInstanceData.hpp>
#include <Rendering/MultidrawIndirect/MDIDefinitions.hpp>
#include <Texture/TextureManager.hpp>
#include <FBO/FBOContainer.hpp>

namespace gfx
{
	class GFXCore
	{
	public:
		friend GFXCore& GFXCoreInstance();
		void Initialize(int screenWidth, int screenHeight);
		void Resize(int screenWidth, int screenHeigt);
		void Render(float dt);
	private:
		GFXCore();
		~GFXCore();

		void RenderDebugTexture(int vpX, int vpY, int vpSX, int vpSY, std::string shaderProgram, GLuint textureID);

		ShaderManager* m_shaderManager;
		ShaderContainer* m_shaderContainer;

		FBOContainer* m_FBOContainer;

		std::vector<DrawElementsIndirectBindlessCommandNV> m_commands;
		GLuint m_cmd_buffer;
		void *m_cmd_ptr;
		
		GLuint test;
		GLuint64 handle;
		GLuint64 handle2;

		std::vector<GLuint64> handles;
		GLuint m_handleBuffer;

		unsigned int m_screenWidth;
		unsigned int m_screenHeight;
	};

	GFXCore& GFXCoreInstance();
}

#endif