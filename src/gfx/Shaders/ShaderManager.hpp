#ifndef SRC_GFX_SHADERS_SHADERMANAGER_HPP
#define SRC_GFX_SHADERS_SHADERMANAGER_HPP

#include "ShaderBaseModel.hpp"
#include "UniformContainer.hpp"
#include <utility/TextFile.hpp>
#include <glm/glm.hpp>

#define SHADER_LOAD_DEBUG

namespace gfx
{
	class ShaderManager
	{
	private:
		ShaderBaseModel* m_shaderData;
		TextFile* m_textFile;

	public:

		ShaderManager();
		~ShaderManager();

		void DeleteAllShaders();

		void LoadShader(const char* filepath, const char* defineString, std::string shaderKey, GLenum type);

		void AttachShader(std::string shaderKey, std::string shaderProgramKey);
		void DetachShader(std::string shaderKey, std::string shaderProgramKey);

		void CreateProgram(std::string shaderProgramKey);

		void UseProgram(std::string shaderProgramKey);

		void LinkProgram(std::string shaderProgramKey);

		GLuint GetShaderID(std::string shaderKey);
		GLuint GetShaderProgramID(std::string shaderProgramKey);

		GLuint GetShaderProgramID(int shaderProgramIndex);
		int GetShaderProgramIndex(std::string shaderProgramKey);

		void DeleteProgram(std::string shaderProgramKey);
		void DeleteShader(std::string shaderKey);

		void ResetProgram();

		GLint GetUniformLocation(std::string shaderProgramKey, std::string uniformName);
		GLint GetUniformLocation(GLuint shaderProgramID, std::string uniformName);

		void SaveUniformContainer(UniformContainer* uc, std::string shaderProgramKey);
		UniformContainer* GetUniformContainer(int shaderProgramIndex);
		UniformContainer* GetUniformContainer(std::string shaderProgramKey);

		//GLint GetUniformLocation(GLint programKey, std::string uniformName);

		void SetUniform(GLint x, GLuint uniformLocation);
		void SetUniform(GLint x, GLint y, GLuint uniformLocation);
		void SetUniform(GLint x, GLint y, GLint z, GLuint uniformLocation);
		void SetUniform(GLint x, GLint y, GLint z, GLint w, GLuint uniformLocation);

		void SetUniform(GLuint x, GLuint uniformLocation);
		void SetUniform(GLuint x, GLuint y, GLuint uniformLocation);
		void SetUniform(GLuint x, GLuint y, GLuint z, GLuint uniformLocation);
		void SetUniform(GLuint x, GLuint y, GLuint z, GLuint w, GLuint uniformLocation);


		void SetUniform(GLfloat x, GLuint uniformLocation);
		void SetUniform(GLfloat x, GLfloat y, GLuint uniformLocation);
		void SetUniform(GLfloat x, GLfloat y, GLfloat z, GLuint uniformLocation);
		void SetUniform(GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLuint uniformLocation);

		void SetUniform(int count, glm::vec2 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::vec3 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::vec4 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::ivec2 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::ivec3 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::ivec4 vec, GLuint uniformLocation);
		void SetUniform(int count, glm::mat2 mat, GLuint uniformLocation);
		void SetUniform(int count, glm::mat3 mat, GLuint uniformLocation);
		void SetUniform(int count, glm::mat4 mat, GLuint uniformLocation);
	};
}

#endif