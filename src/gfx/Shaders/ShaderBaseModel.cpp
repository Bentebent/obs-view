#include "ShaderBaseModel.hpp"

#include <limits>

namespace gfx
{
	ShaderBaseModel::ShaderBaseModel()
	{
		m_shader = new std::map<std::string, GLuint>();
		m_shaderProgram = new std::map<std::string, GLuint>();
		m_uniformContainers = new std::map<std::string, UniformContainer*>();
	}

	ShaderBaseModel::~ShaderBaseModel()
	{
		for (std::map<std::string, GLuint>::iterator it = m_shaderProgram->begin(); it != m_shaderProgram->end(); it++)
			glDeleteProgram(it->second);

		for (std::map<std::string, GLuint>::iterator it = m_shader->begin(); it != m_shader->end(); it++)
			glDeleteShader(it->second);

		for (std::map<std::string, UniformContainer*>::iterator it = m_uniformContainers->begin(); it != m_uniformContainers->end(); it++)
		{
			for (int i = 0; i < it->second->deleteFunctions.size(); i++)
				it->second->deleteFunctions[i]();

			it->second->deleteFunctions.clear();
			it->second->uniformBindFunctions.clear();
			it->second->uniformValues.clear();
			it->second->uniformLocations.clear();
		}

		delete (m_shaderProgram);
		delete (m_shader);
		delete (m_uniformContainers);
	}

	void ShaderBaseModel::DeleteAllShaders()
	{
		for (std::map<std::string, GLuint>::iterator it = m_shaderProgram->begin(); it != m_shaderProgram->end(); it++)
			glDeleteProgram(it->second);

		for (std::map<std::string, GLuint>::iterator it = m_shader->begin(); it != m_shader->end(); it++)
			glDeleteShader(it->second);

		for (std::map<std::string, UniformContainer*>::iterator it = m_uniformContainers->begin(); it != m_uniformContainers->end(); it++)
		{
			for (int i = 0; i < it->second->deleteFunctions.size(); i++)
				it->second->deleteFunctions[i]();

			it->second->deleteFunctions.clear();
			it->second->uniformBindFunctions.clear();
			it->second->uniformValues.clear();
			it->second->uniformLocations.clear();
		}

		m_shaderProgram->clear();
		m_shader->clear();
		m_shaderProgramNames.clear();
		m_uniformContainers->clear();
	}


	GLuint ShaderBaseModel::GetShaderID(std::string shaderKey)
	{
		std::map<std::string, GLuint>::iterator it = m_shader->find(shaderKey);

		if (it == m_shader->end()){
			return 0;
		}
		else
		{
			return it->second;
		}
	}
	GLuint ShaderBaseModel::GetShaderProgram(std::string shaderProgramKey)
	{
		std::map<std::string, GLuint>::iterator it = m_shaderProgram->find(shaderProgramKey);

		if (it == m_shaderProgram->end())
		{
			return std::numeric_limits<GLuint>::max();
		}
		else
		{
			return it->second;
		}
	}

	int ShaderBaseModel::GetShaderProgramListIndex(std::string shaderProgramKey)
	{
		for (int i = 0; i < m_shaderProgramNames.size(); i++)
		{
			if (m_shaderProgramNames[i] == shaderProgramKey)
				return i;
		}

		return -1;
	}

	GLuint ShaderBaseModel::GetShaderProgram(int shaderProgramListIndex)
	{
		std::map<std::string, GLuint>::iterator it = m_shaderProgram->find(m_shaderProgramNames[shaderProgramListIndex]);

		if (it == m_shaderProgram->end())
		{
			return std::numeric_limits<GLuint>::max();
		}
		else
		{
			return it->second;
		}
	}

	UniformContainer* ShaderBaseModel::GetUniformContainer(int shaderProgramListIndex)
	{
		std::map<std::string, UniformContainer*>::iterator it = m_uniformContainers->find(m_shaderProgramNames[shaderProgramListIndex]);

		if (it == m_uniformContainers->end())
		{
			return nullptr;
		}
		else
		{
			return it->second;
		}
	}

	UniformContainer* ShaderBaseModel::GetUniformContainer(std::string shaderProgramKey)
	{
		std::map<std::string, UniformContainer*>::iterator it = m_uniformContainers->find(shaderProgramKey);

		if (it == m_uniformContainers->end())
		{
			return nullptr;
		}
		else
		{
			return it->second;
		}
	}


	bool ShaderBaseModel::SaveShader(std::string shaderKey, GLuint shaderID)
	{
		return m_shader->insert(std::pair<std::string, GLuint>(shaderKey, shaderID)).second;
	}

	bool ShaderBaseModel::SaveUniformContainer(UniformContainer* uniformContainer, std::string shaderProgramKey)
	{
		return m_uniformContainers->insert(std::pair<std::string, UniformContainer*>(shaderProgramKey, uniformContainer)).second;
	}

	bool ShaderBaseModel::SaveShaderProgram(std::string shaderProgramKey, GLuint shaderProgramID)
	{
		m_shaderProgramNames.push_back(shaderProgramKey);
		return m_shaderProgram->insert(std::pair<std::string, GLuint>(shaderProgramKey, shaderProgramID)).second;
	}



	void ShaderBaseModel::DeleteShader(std::string shaderKey)
	{
		m_shader->erase(shaderKey);
	}

	void ShaderBaseModel::DeleteShaderProgram(std::string shaderProgramKey)
	{
		m_shaderProgram->erase(shaderProgramKey);

		for (int i = 0; i < m_shaderProgramNames.size(); i++)
		{
			if (m_shaderProgramNames[i] == shaderProgramKey)
			{
				m_shaderProgramNames.erase(m_shaderProgramNames.begin() + i);
				return;
			}
		}
	}
}