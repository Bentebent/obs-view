#include "ShaderManager.hpp"
#include <iostream>

namespace gfx
{
	ShaderManager::ShaderManager()
	{
		m_shaderData = new ShaderBaseModel();
		m_textFile = new TextFile();

	}

	ShaderManager::~ShaderManager()
	{
		delete m_shaderData;
		delete m_textFile;
	}

	void ShaderManager::DeleteAllShaders()
	{
		m_shaderData->DeleteAllShaders();
	}

	void ShaderManager::LoadShader(const char* filepath, const char* defineString, std::string shaderKey, GLenum type)
	{
		try
		{
			m_textFile->LoadFile(filepath);
		}
		catch (std::exception e)
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << (std::string("Could not load file from ") + filepath + ": " + e.what()).c_str();
			#endif
		}

		glGetError();

		GLuint shaderID = glCreateShader(type);
		const char* source = m_textFile->GetCString();
		std::string intermediateSource = defineString + std::string(source);
		source = intermediateSource.c_str();

		if (shaderID != 0)
		{
			m_shaderData->SaveShader(shaderKey, shaderID);
			glShaderSource(shaderID, 1, &source, NULL);
			glCompileShader(shaderID);
		}

		GLint compileStatus;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compileStatus);

		if (glGetError() != GL_NO_ERROR || compileStatus == GL_FALSE)
		{

			if (shaderID != 0)
			{
				m_shaderData->DeleteShader(shaderKey);
			}

			GLsizei* length = new GLsizei;
			glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, length); //Get the length of the compilation log
			char* compilationLog = new char[*length];			 //Create the needed char array

			glGetShaderInfoLog(shaderID, *length, NULL, compilationLog); //Get the compilation log

			#ifdef SHADER_LOAD_DEBUG
			std::cout << "ERROR: \nCompilation log of shader " + shaderKey + ":\n" + std::string(compilationLog).c_str();
			#endif
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << "Compiled shader " + shaderKey << std::endl;
			#endif
		}
			
	}

	void ShaderManager::AttachShader(std::string shaderKey, std::string shaderProgramKey)
	{
		glGetError();

		GLuint shaderID = m_shaderData->GetShaderID(shaderKey);
		GLuint shaderProgramID = m_shaderData->GetShaderProgram(shaderProgramKey);

		if (shaderProgramID != 0 && shaderID != 0)
		{
			glAttachShader(shaderProgramID, shaderID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader with associated key does exist!");
			#endif
		}

		if (glGetError() != GL_NO_ERROR)
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ((std::string("ERROR: Could not attach shader ") + std::string(shaderKey) + std::string(" to ") + std::string(shaderProgramKey)).c_str());
			#endif
		}
	}

	void ShaderManager::DetachShader(std::string shaderKey, std::string shaderProgramKey)
	{
		glGetError();

		GLuint shaderID = m_shaderData->GetShaderID(shaderKey);
		GLuint shaderProgramID = m_shaderData->GetShaderProgram(shaderProgramKey);

		if (shaderProgramID != 0 && shaderID != 0)
		{
			glDetachShader(shaderProgramID, shaderID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader or shader-program with associated key does exist!");
			#endif
		}

		if (glGetError() != GL_NO_ERROR)
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << (("ERROR: Could not detach shader " + std::string(shaderKey) + " from " + std::string(shaderProgramKey)).c_str());
			#endif
		}
	}

	void ShaderManager::CreateProgram(std::string shaderProgramKey)
	{
		glGetError();

		GLuint shaderProgramID = glCreateProgram();

		if (shaderProgramID != 0)
		{
			m_shaderData->SaveShaderProgram(shaderProgramKey, shaderProgramID);
		}

		if (glGetError() != GL_NO_ERROR)
		{

			if (shaderProgramID != 0)
			{
				m_shaderData->DeleteShaderProgram(shaderProgramKey);
			}

			#ifdef SHADER_LOAD_DEBUG
			std::cout << (("ERROR: Could not create shader-program " + shaderProgramKey).c_str());
			#endif
		}
	}

	void ShaderManager::UseProgram(std::string shaderProgramKey)
	{
		//glGetError();

		GLuint shaderProgramID = m_shaderData->GetShaderProgram(shaderProgramKey);

		if (shaderProgramID != 0)
		{
			glUseProgram(shaderProgramID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader-program with associated key does exist!");
			#endif
		}

		//if (glGetError() != GL_NO_ERROR)
		//{
		//	std::cout << "ERROR: Could not use shaderProgram " + shaderProgramKey + "!\n";
		//}
	}

	void ShaderManager::LinkProgram(std::string shaderProgramKey)
	{
		glGetError();

		GLuint shaderProgramID = m_shaderData->GetShaderProgram(shaderProgramKey);

		if (shaderProgramID != 0)
		{
			glLinkProgram(shaderProgramID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader-program with associated key does exist!");
			#endif
		}

		GLint linkStatus;
		glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &linkStatus);

		if (glGetError() != GL_NO_ERROR || linkStatus == GL_FALSE)
		{

			GLsizei* length = new GLsizei;
			glGetProgramiv(shaderProgramID, GL_INFO_LOG_LENGTH, length); //Get the length of the compilation log
			char* linkingLog = new char[*length];			 //Create the needed char array

			glGetProgramInfoLog(shaderProgramID, *length, NULL, linkingLog); //Get the compilation log

			#ifdef SHADER_LOAD_DEBUG
			std::cout << (("ERROR: \nLinker log of shader-programm " + shaderProgramKey + ":\n" + std::string(linkingLog)).c_str());
			#endif
		}
	}

	void ShaderManager::SaveUniformContainer(UniformContainer* uc, std::string shaderProgramKey)
	{
		m_shaderData->SaveUniformContainer(uc, shaderProgramKey);
	}

	GLuint ShaderManager::GetShaderID(std::string shaderKey)
	{
		return m_shaderData->GetShaderID(shaderKey);
	}

	GLuint ShaderManager::GetShaderProgramID(std::string shaderProgramKey)
	{
		return m_shaderData->GetShaderProgram(shaderProgramKey);
	}

	GLuint ShaderManager::GetShaderProgramID(int shaderProgramIndex)
	{
		return m_shaderData->GetShaderProgram(shaderProgramIndex);
	}

	int ShaderManager::GetShaderProgramIndex(std::string shaderProgramKey)
	{
		return m_shaderData->GetShaderProgramListIndex(shaderProgramKey);
	}

	UniformContainer* ShaderManager::GetUniformContainer(int shaderProgramIndex)
	{
		return m_shaderData->GetUniformContainer(shaderProgramIndex);
	}

	UniformContainer* ShaderManager::GetUniformContainer(std::string shaderProgramKey)
	{
		return m_shaderData->GetUniformContainer(shaderProgramKey);
	}

	void ShaderManager::DeleteProgram(std::string shaderProgramKey)
	{
		glGetError();

		GLuint shaderProgramID = m_shaderData->GetShaderProgram(shaderProgramKey);

		if (shaderProgramID != 0)
		{
			glDeleteProgram(shaderProgramID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader-program with associated key does exist!");
			#endif
		}

		if (glGetError() != GL_NO_ERROR)
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << (("ERROR: Could not delete shader-program " + shaderProgramKey).c_str());
			#endif
		}
	}

	void ShaderManager::DeleteShader(std::string shaderKey)
	{
		glGetError();

		GLuint shaderID = m_shaderData->GetShaderID(shaderKey);

		if (shaderID != 0)
		{
			glDeleteShader(shaderID);
		}
		else
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << ("ERROR: No shader-program with associated key does exist!");
			#endif
		}


		if (glGetError() != GL_NO_ERROR)
		{
			#ifdef SHADER_LOAD_DEBUG
			std::cout << (("ERROR: Could not delete shader " + shaderKey).c_str());
			#endif
		}
	}

	void ShaderManager::ResetProgram()
	{
		glUseProgram(0);
	}


	GLint ShaderManager::GetUniformLocation(std::string shaderProgramKey, std::string uniformName)
	{
		return glGetUniformLocation(GetShaderProgramID(shaderProgramKey), uniformName.c_str());
	}

	GLint ShaderManager::GetUniformLocation(GLuint shaderProgramID, std::string uniformName)
	{
		return glGetUniformLocation(shaderProgramID, uniformName.c_str());
	}

	void SetUniform(GLint x, GLuint uniformLocation)
	{
		glUniform1i(uniformLocation, x);
	}

	void SetUniform(GLint x, GLint y, GLuint uniformLocation)
	{
		glUniform2i(uniformLocation, x, y);
	}

	void SetUniform(GLint x, GLint y, GLint z, GLuint uniformLocation)
	{
		glUniform3i(uniformLocation, x, y, z);
	}

	void SetUniform(GLint x, GLint y, GLint z, GLint w, GLuint uniformLocation)
	{
		glUniform4i(uniformLocation, x, y, z, w);
	}

	void ShaderManager::SetUniform(GLuint x, GLuint uniformLocation)
	{
		glUniform1ui(uniformLocation, x);
	}

	void ShaderManager::SetUniform(GLuint x, GLuint y, GLuint uniformLocation)
	{
		glUniform2ui(uniformLocation, x, y);
	}

	void ShaderManager::SetUniform(GLuint x, GLuint y, GLuint z, GLuint uniformLocation)
	{
		glUniform3ui(uniformLocation, x, y, z);
	}

	void ShaderManager::SetUniform(GLuint x, GLuint y, GLuint z, GLuint w, GLuint uniformLocation)
	{
		glUniform4ui(uniformLocation, x, y, z, w);
	}

	void ShaderManager::SetUniform(GLfloat x, GLuint uniformLocation)
	{
		glUniform1f(uniformLocation, x);
	}
	void ShaderManager::SetUniform(GLfloat x, GLfloat y, GLuint uniformLocation)
	{
		glUniform2f(uniformLocation, x, y);
	}

	void ShaderManager::SetUniform(GLfloat x, GLfloat y, GLfloat z, GLuint uniformLocation)
	{
		glUniform3f(uniformLocation, x, y, z);
	}

	void ShaderManager::SetUniform(GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLuint uniformLocation)
	{
		glUniform4f(uniformLocation, x, y, z, w);
	}

	void ShaderManager::SetUniform(int count, glm::vec2 vec, GLuint uniformLocation)
	{
		glUniform2fv(uniformLocation, count, &vec[0]);
	}

	void ShaderManager::SetUniform(int count, glm::vec3 vec, GLuint uniformLocation)
	{
		glUniform3fv(uniformLocation, count, &vec[0]);
	}

	void ShaderManager::SetUniform(int count, glm::vec4 vec, GLuint uniformLocation)
	{
		glUniform4fv(uniformLocation, count, &vec[0]);
	}


	void ShaderManager::SetUniform(int count, glm::ivec2 vec, GLuint uniformLocation)
	{
		glUniform2iv(uniformLocation, count, &vec[0]);
	}

	void ShaderManager::SetUniform(int count, glm::ivec3 vec, GLuint uniformLocation)
	{
		glUniform3iv(uniformLocation, count, &vec[0]);
	}

	void ShaderManager::SetUniform(int count, glm::ivec4 vec, GLuint uniformLocation)
	{
		glUniform4iv(uniformLocation, count, &vec[0]);
	}


	void ShaderManager::SetUniform(int count, glm::mat2 mat, GLuint uniformLocation)
	{
		glUniformMatrix2fv(uniformLocation, count, GL_FALSE, &mat[0][0]);
	}

	void ShaderManager::SetUniform(int count, glm::mat3 mat, GLuint uniformLocation)
	{
		glUniformMatrix3fv(uniformLocation, count, GL_FALSE, &mat[0][0]);
	}

	void ShaderManager::SetUniform(int count, glm::mat4 mat, GLuint uniformLocation)
	{
		glUniformMatrix4fv(uniformLocation, count, GL_FALSE, &mat[0][0]);
	}
}