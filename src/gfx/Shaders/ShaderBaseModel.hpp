#ifndef SRC_GFX_SHADERS_SHADERBASEMODEL_HPP
#define SRC_GFX_SHADERS_SHADERBASEMODEL_HPP

#include "UniformContainer.hpp"

#include <map>
#include <vector>
#include <string>
#include <GLEW/glew.h>

namespace gfx
{
	class ShaderBaseModel
	{
		friend class ShaderManager;

		

	private:
		std::map<std::string, GLuint>* m_shader;
		std::map<std::string, GLuint>* m_shaderProgram;
		std::map<std::string, UniformContainer*>* m_uniformContainers;
		std::vector<std::string> m_shaderProgramNames;

		ShaderBaseModel();
		~ShaderBaseModel();

	public:

		GLuint GetShaderID(std::string shaderKey);
		GLuint GetShaderProgram(std::string shaderProgramKey);
		GLuint GetShaderProgram(int shaderProgramListIndex);
		int GetShaderProgramListIndex(std::string shaderProgramKey);

		UniformContainer* GetUniformContainer(int shaderProgramListIndex);
		UniformContainer* GetUniformContainer(std::string shaderProgramKey);

		bool SaveShader(std::string shaderKey, GLuint shaderID);
		bool SaveShaderProgram(std::string shaderProgramKey, GLuint shaderProgramID);
		bool SaveUniformContainer(UniformContainer* uniformContainer, std::string shaderProgramKey);

		void DeleteShader(std::string shaderKey);
		void DeleteShaderProgram(std::string shaderProgramKey);

		void DeleteAllShaders();
	};
}


#endif