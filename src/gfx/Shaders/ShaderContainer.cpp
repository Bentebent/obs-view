#include "ShaderContainer.hpp"

#include <utility/StringHelper.hpp>

#include "UniformContainer.hpp"

#include <Settings/SettingsContainer.hpp>
#include <Viewport/ViewportContainer.hpp>
#include <Rendering/RenderJob/RenderJobManager.hpp>
#include <FBO/FBOContainer.hpp>
#include <Texture/TextureManager.hpp>

#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

namespace gfx
{
	ShaderContainer::ShaderContainer()
	{

	}

	ShaderContainer::~ShaderContainer()
	{

	}

	void ShaderContainer::Initialize(ShaderManager* shaderManager, FBOContainer* fboContainer)
	{
		m_shaderManager = shaderManager;
		m_FBOContainer = fboContainer;
	}

	void ShaderContainer::LoadShaders()
	{
		std::map<std::string, std::vector<std::string>> defines;
		std::vector<std::string> myDefines;
		LoadShaders(defines);
	}


	void ShaderContainer::LoadShaders(std::map<std::string, std::vector<std::string>> defines)
	{
		m_shaderManager->DeleteAllShaders();

		std::string baseFolder = "shaders/";

		std::string line;
		std::ifstream myfile("shaders/ShaderList.txt");
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				line = baseFolder + line;

				#ifdef SHADER_LOAD_DEBUG
				std::cout << "Loading shader " + line << std::endl;
				#endif

				LoadShader(line, defines);

				#ifdef SHADER_LOAD_DEBUG
				std::cout << "Finished loading shader " + line << std::endl;		
				#endif

			}
			myfile.close();
		}
		else std::cout << "Failed to open ShaderList" << std::endl;

	}

	void ShaderContainer::LoadShader(std::string shaderASM, std::map<std::string, std::vector<std::string>> defines)
	{
		std::string line;
		std::string shaderProgramName;
		std::string scriptPath;
		std::ifstream myfile(shaderASM);
		std::vector<std::string> shaderNames;

		if (myfile.is_open())
		{
			getline(myfile, shaderProgramName);
			
			while (getline(myfile, line))
			{
				if (line == "VS")
				{
					getline(myfile, line);
					std::string shaderDefines;
					std::string shaderName = shaderProgramName + "VS";
					shaderNames.push_back(shaderName);

					shaderDefines = CheckDefines(defines, shaderName);

					m_shaderManager->LoadShader(line.c_str(), shaderDefines.c_str(), shaderName, GL_VERTEX_SHADER);
				}
				else if (line == "GS")
				{
					getline(myfile, line);
					std::string shaderDefines;
					std::string shaderName = shaderProgramName + "GS";
					shaderNames.push_back(shaderName);

					shaderDefines = CheckDefines(defines, shaderName);

					m_shaderManager->LoadShader(line.c_str(), shaderDefines.c_str(), shaderName, GL_GEOMETRY_SHADER);
				}
				else if (line == "FS")
				{
					getline(myfile, line);

					std::string shaderDefines;
					std::string shaderName = shaderProgramName + "FS";
					shaderNames.push_back(shaderName);
					
					shaderDefines = CheckDefines(defines, shaderName);

					m_shaderManager->LoadShader(line.c_str(), shaderDefines.c_str(), shaderName, GL_FRAGMENT_SHADER);
				}
				else if (line == "CS")
				{
					getline(myfile, line);

					std::string shaderDefines;
					std::string shaderName = shaderProgramName + "CS";
					shaderNames.push_back(shaderName);

					shaderDefines = CheckDefines(defines, shaderName);

					m_shaderManager->LoadShader(line.c_str(), shaderDefines.c_str(), shaderName, GL_COMPUTE_SHADER);
				}
				else if (line == "SCRIPT")
				{
					getline(myfile, line);
					scriptPath = line;
					std::cout << scriptPath << std::endl;
				}

				//std::cout << line << std::endl;
			}
			myfile.close();
		}
		else std::cout << "Failed to open " << shaderASM << std::endl;

		//Create program
		m_shaderManager->CreateProgram(shaderProgramName);

		//Attach shaders
		for (int i = 0; i < shaderNames.size(); i++)
		{
			m_shaderManager->AttachShader(shaderNames[i], shaderProgramName);
		}

		//Link program
		m_shaderManager->LinkProgram(shaderProgramName);

		//Detach shaders, linked program should be fine
		for (int i = 0; i < shaderNames.size(); i++)
		{
			m_shaderManager->DetachShader(shaderNames[i], shaderProgramName);
		}

		//Delete detached shaders
		for (int i = 0; i < shaderNames.size(); i++)
		{
			m_shaderManager->DeleteShader(shaderNames[i]);
		}

		//Parse script file if present
		ParseScript(scriptPath, shaderProgramName);
	}

	void ShaderContainer::ParseScript(std::string scriptPath, std::string shaderProgramName)
	{
		std::string line;
		std::ifstream myfile(scriptPath);

		UniformContainer* uc = new UniformContainer;

		GLuint shaderProgramID = m_shaderManager->GetShaderProgramID(shaderProgramName);
		std::vector<GLuint> uniformLocations;
		std::vector<std::function<void()>> uniformBinds;
		int uniformIndex = 0;
		int bindPointIndex = 0;

		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				if (line == "UNIFORM")
				{
					std::string uniformName;
					std::string uniformType;
					std::string value;

					//Get uniform location
					getline(myfile, uniformName);
					uc->uniformLocations.push_back(m_shaderManager->GetUniformLocation(shaderProgramID, uniformName));

					//Get type and value
					getline(myfile, uniformType);
					getline(myfile, value);

					//Parse value depending on uniformType
					if (uniformType == "float")
					{
						if (value == "gamma")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								float val = Settings().m_gamma;
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});
						}
						else if (value == "static_exposure")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								float val = Settings().m_staticExposure;
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});
						}
						else if (value == "deltaTime")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								float val = Settings().m_deltaTime;
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});
						}
						else
						{
							float val = std::stof(value);

							uc->uniformBindFunctions.push_back([=]()
							{
								//float val = *(float*)uc->uniformValues[uniformIndex];
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});
						}
					}
					else if (uniformType == "int")
					{
						if (value == "placeholder")
						{
						}
						else
						{
							//int val = std::stoi(value);
							//
							//uc->uniformBindFunctions.push_back([=]()
							//{
							//	GLuint uniformLoc = uc->uniformLocations[uniformIndex];
							//	this->m_shaderManager->SetUniform(val, uniformLoc);
							//});
						}
					}
					else if (uniformType == "uint")
					{
						if (value == "pointlightCount")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								unsigned int val = RenderJobManagerInstance().GetLightCount(LIGHT_TYPES::POINT);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});

						}
						else if (value == "directionalLightCount")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								unsigned int val = RenderJobManagerInstance().GetLightCount(LIGHT_TYPES::DIR);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});

						}
						else if (value == "spotLightCount")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								unsigned int val = RenderJobManagerInstance().GetLightCount(LIGHT_TYPES::SPOT);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});

						}
						else
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								unsigned int val = std::stoul(value);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(val, uniformLoc);
							});
						}
					}
					else if (uniformType == "bool")
					{

					}
					else if (uniformType == "mat3")
					{

					}
					else if (uniformType == "mat4")
					{
						if (value == "view")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								Viewport vp = VPContainerInstance().GetCurrentViewport();
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, vp.view, uniformLoc);
							});
						}
						else if (value == "projection")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								Viewport vp = VPContainerInstance().GetCurrentViewport();
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, vp.projection, uniformLoc);
							});
						}
						else if (value == "invProjView")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								glm::mat4 view = VPContainerInstance().GetViewport(0).view;
								glm::mat4 proj = VPContainerInstance().GetViewport(0).projection;

								glm::mat4 invProjView = glm::inverse(proj * view);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, invProjView, uniformLoc);
							});
						}
						else if (value == "prevProjView")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								glm::mat4 prevViewProj = VPContainerInstance().GetViewport(0).prevViewProjection;

								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, prevViewProj, uniformLoc);
							});
						}
						else
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								glm::mat4 mat = StringToMat4(std::string(value));

								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, mat, uniformLoc);
							});
						}
					}
					else if (uniformType == "vec2")
					{
					}
					else if (uniformType == "vec3")
					{
						if (value == "eyePosition")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								glm::mat4 view = VPContainerInstance().GetCurrentViewport().view;
								glm::vec3 cameraPos = -glm::vec3(view[3]) * glm::mat3(view);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, cameraPos, uniformLoc);
							});
						}
						else if (value == "whitePoint")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, Settings().m_whitePoint, uniformLoc);
							});
						}
						else
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, StringToVec3(std::string(value)), uniformLoc);
							});
						}
					}
					else if (uniformType == "vec4")
					{
						uc->uniformBindFunctions.push_back([=]()
						{
							GLint uniformLoc = uc->uniformLocations[uniformIndex];
							this->m_shaderManager->SetUniform(1, StringToVec4(std::string(value)), uniformLoc);
						});
					}
					else if (uniformType == "ivec2")
					{
						if (value == "framebufferDim")
						{
							uc->uniformBindFunctions.push_back([=]()
							{
								glm::ivec2 framebufferDim = glm::ivec2(Settings().m_clientWidth, Settings().m_clientHeight);
								GLint uniformLoc = uc->uniformLocations[uniformIndex];
								this->m_shaderManager->SetUniform(1, framebufferDim, uniformLoc);
							});
						}
					}
					else if (uniformType == "ivec3")
					{
						
					}
					else if (uniformType == "ivec4")
					{

					}
					else if (uniformType == "sampler2D")
					{
						GLuint textureHandle;

						if (value == "depthStencil")
							textureHandle = m_FBOContainer->m_FBOTexture_D24S8_1->GetTextureHandle();
						else if (value == "normal" || value == "RGBA16_1")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA16_1->GetTextureHandle();
						else if (value == "albedo" || value == "RGBA8_1")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA8_1->GetTextureHandle();
						else if (value == "specular" || value == "RGBA8_2")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA8_2->GetTextureHandle();
						else if (value == "roughness" || value == "RGBA8_3")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA8_3->GetTextureHandle();
						else if (value == "RGBA32_1")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA32_1->GetTextureHandle();
						else if (value == "RGBA32_2")
							textureHandle = m_FBOContainer->m_FBOTexture_RGBA32_2->GetTextureHandle();
						else if (value == "Bloom_1")
							textureHandle = m_FBOContainer->m_bloomTextures[0]->GetTextureHandle();
						else if (value == "Bloom_2")
							textureHandle = m_FBOContainer->m_bloomTextures[1]->GetTextureHandle();
						else if (value == "Bloom_3")
							textureHandle = m_FBOContainer->m_bloomTextures[2]->GetTextureHandle();
						else if (value == "Bloom_4")
							textureHandle = m_FBOContainer->m_bloomTextures[3]->GetTextureHandle();
						else if (value == "Bloom_5")
							textureHandle = m_FBOContainer->m_bloomTextures[4]->GetTextureHandle();
						else if (value == "Bloom_6")
							textureHandle = m_FBOContainer->m_bloomTextures[5]->GetTextureHandle();
						else if (value == "Bloom_7")
							textureHandle = m_FBOContainer->m_bloomTextures[6]->GetTextureHandle();
						else if (value == "Bloom_8")
							textureHandle = m_FBOContainer->m_bloomTextures[7]->GetTextureHandle();

						uc->uniformBindFunctions.push_back([=]()
						{
							GLint uniformLoc = uc->uniformLocations[uniformIndex];
							TextureManagerInstance().BindTexture(textureHandle, uniformLoc, bindPointIndex, GL_TEXTURE_2D);
						});
						bindPointIndex++;
					}

					uniformIndex++;
				}
				else if (line == "IMAGETEXTURE")
				{
					std::string writeStatus;
					std::string format;
					std::string fboTexture;

					getline(myfile, writeStatus);
					getline(myfile, format);
					getline(myfile, fboTexture);

					GLenum access;
					GLenum internalFormat; 
					GLuint textureHandle;

					if (writeStatus == "writeOnly")
						access = GL_WRITE_ONLY;
					else if (writeStatus == "readOnly")
						access = GL_READ_ONLY;
					else if (writeStatus == "readWrite")
						access = GL_READ_WRITE;

					if (format == "RGBA32F")
						internalFormat = GL_RGBA32F;
					else if (format == "RGBA16F")
						internalFormat = GL_RGBA16F;

					if (fboTexture == "depthStencil")
					{
						textureHandle = m_FBOContainer->m_FBOTexture_D24S8_1->GetTextureHandle();
					}
					else if (fboTexture == "RGBA32_1")
					{
						textureHandle = m_FBOContainer->m_FBOTexture_RGBA32_1->GetTextureHandle();
					}
					else if (fboTexture == "RGBA32_2")
					{
						textureHandle = m_FBOContainer->m_FBOTexture_RGBA32_2->GetTextureHandle();
					}
					else if (fboTexture == "RGBA16_1")
					{
						textureHandle = m_FBOContainer->m_FBOTexture_RGBA16_1->GetTextureHandle();
					}

					uc->uniformBindFunctions.push_back([=]()
					{
						glBindImageTexture(bindPointIndex, textureHandle, 0, GL_FALSE, 0, access, internalFormat);
					});

					bindPointIndex++;
				}
				else if (line == "OPENGLFUNC")
				{
					std::string functionName;
					std::string value;

					//Get function type
					getline(myfile, functionName);

					//Get value
					getline(myfile, value);

					if (functionName == "glClearStencil")
					{
						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glClearStencil(0);
						});

					}
					else if (functionName == "glClear")
					{
						//glClear

						int count = std::stoi(value);
						GLbitfield bf = 0;

						for (int i = 0; i < count; i++)
						{
							std::string bitfieldString;
							getline(myfile, bitfieldString);

							if (bitfieldString == "COLOR")
							{
								bf |= GL_COLOR_BUFFER_BIT;
							}
							else if (bitfieldString == "DEPTH")
							{
								bf |= GL_DEPTH_BUFFER_BIT;
							}
							else if (bitfieldString == "ACCUM")
							{
								bf |= GL_ACCUM_BUFFER_BIT;
							}
							else if (bitfieldString == "STENCIL")
							{
								bf |= GL_STENCIL_BUFFER_BIT;
							}
						}

						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glClear(bf);
						});
					}
					else if (functionName == "glClearBufferfv")
					{
						//glClearBufferfv

						std::string drawBufferIndex;
						std::string stringColor;

						getline(myfile, drawBufferIndex);
						getline(myfile, stringColor);
						
						GLenum buffer;

						if (value == "COLOR")
							buffer = GL_COLOR;
						else if (value == "DEPTH")
							buffer = GL_DEPTH;
						else if (value == "STENCIL")
							buffer = GL_STENCIL;

						int drawBufferID = std::stoi(drawBufferIndex);
						glm::vec4 color = StringToVec4(stringColor);
						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glClearBufferfv(buffer, drawBufferID, &color[0]);
						});
					}
					else if (functionName == "glDisable")
					{
						GLenum cap;

						if (value == "DEPTH_TEST")
							cap = GL_DEPTH_TEST;
						else if (value == "STENCIL_TEST")
							cap = GL_STENCIL_TEST;
						else if (value == "BLEND")
							cap == GL_BLEND;

						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glDisable(cap);
						});

					}
					else if (functionName == "glEnable")
					{
						GLenum cap;

						if (value == "DEPTH_TEST")
							cap = GL_DEPTH_TEST;
						else if (value == "STENCIL_TEST")
							cap = GL_STENCIL_TEST;
						else if (value == "BLEND")
							cap == GL_BLEND;

						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glEnable(cap);
						});
					}
					else if (functionName == "glViewport")
					{
						if (value == "CURRENT")
						{
							uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
							{
								Viewport vp = VPContainerInstance().GetCurrentViewport();
								glViewport(vp.posX, vp.posY, vp.width, vp.height);
							});

						}
						else if (value == "VPIndex")
						{
							std::string index;
							getline(myfile, index);
							int vpIndex = std::stoi(index);
							uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
							{
								Viewport vp = VPContainerInstance().GetViewport(vpIndex);
								glViewport(vp.posX, vp.posY, vp.width, vp.height);
							});
						}
						else if (value == "CONSTANT")
						{
							std::string posX;
							std::string posY;
							std::string width;
							std::string height;

							getline(myfile, posX);
							getline(myfile, posY);
							getline(myfile, width);
							getline(myfile, height);

							int x = std::stoi(posX);
							int y = std::stoi(posY);
							int w = std::stoi(width);
							int h = std::stoi(height);

							uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
							{
								glViewport(x, y, w, h);
							});
						}
					}
					else if (functionName == "glUseProgram")
					{
						GLint shaderID = m_shaderManager->GetShaderProgramID(value);
						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							glUseProgram(shaderID);
						});
					}
				}
				else if (line == "FRAMEBUFFER")
				{
					std::string count;
					getline(myfile, count);

					if (count == "GBUFFER")
					{
						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							m_FBOContainer->BindGBuffer();
						});
					}
					else
					{
						int fboCount = std::stoi(count);
						
						std::vector<std::string> values;
						for (int i = 0; i < fboCount; i++)
						{
							std::string value;
							getline(myfile, value);
							values.push_back(value);
						}

						uc->uniformBindFunctions.insert(uc->uniformBindFunctions.begin() + 0, [=]()
						{
							FBOTexture* textureArray[10];
							bool depthStencil = false;
							for (int i = 0; i < values.size(); i++)
							{
								if (values[i] == "depthStencil")
								{
									textureArray[i] = m_FBOContainer->m_FBOTexture_D24S8_1;
									depthStencil = true;
								}
								else if (values[i] == "normal" || values[i] == "RGBA16_1")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA16_1;
								else if (values[i] == "albedo" || values[i] == "RGBA8_1")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA8_1;
								else if (values[i] == "specular" || values[i] == "RGBA8_2")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA8_2;
								else if (values[i] == "roughness" || values[i] == "RGBA8_3")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA8_3;
								else if (values[i] == "RGBA32_1")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA32_1;
								else if (values[i] == "RGBA32_2")
									textureArray[i] = m_FBOContainer->m_FBOTexture_RGBA32_2;
								else if (values[i] == "Bloom_1")
									textureArray[i] = m_FBOContainer->m_bloomTextures[0];
								else if (values[i] == "Bloom_2")
									textureArray[i] = m_FBOContainer->m_bloomTextures[1];
								else if (values[i] == "Bloom_3")
									textureArray[i] = m_FBOContainer->m_bloomTextures[2];
								else if (values[i] == "Bloom_4")
									textureArray[i] = m_FBOContainer->m_bloomTextures[3];
								else if (values[i] == "Bloom_5")
									textureArray[i] = m_FBOContainer->m_bloomTextures[4];
								else if (values[i] == "Bloom_6")
									textureArray[i] = m_FBOContainer->m_bloomTextures[5];
								else if (values[i] == "Bloom_7")
									textureArray[i] = m_FBOContainer->m_bloomTextures[6];
								else if (values[i] == "Bloom_8")
									textureArray[i] = m_FBOContainer->m_bloomTextures[7];
							}


							m_FBOContainer->BindFBOTextures(fboCount, textureArray, depthStencil);
						});
					}
				}
			}
		}

		m_shaderManager->SaveUniformContainer(uc, shaderProgramName);
	}

	std::string ShaderContainer::CheckDefines(std::map<std::string, std::vector<std::string>> defines, std::string shaderName)
	{
		std::string shaderDefines;
		for (std::map<std::string, std::vector<std::string>>::iterator it = defines.begin(); it != defines.end(); it++)
		{
			if (it->first == shaderName)
			{
				for (int i = 0; i < it->second.size(); i++)
					shaderDefines += it->second.at(i) + "\n";
			}
		}
		return shaderDefines;
	}
}