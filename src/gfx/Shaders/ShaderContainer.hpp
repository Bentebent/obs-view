#ifndef SRC_GFX_SHADERS_SHADERCONTAINER_HPP
#define SRC_GFX_SHADERS_SHADERCONTAINER_HPP

#include "ShaderManager.hpp"
#include <FBO/FBOContainer.hpp>

#include <glm/glm.hpp>

#include <vector>
#include <map>
#include <string>
#define SHADER_LOAD_DEBUG

namespace gfx
{
	class ShaderContainer
	{
	public:
		ShaderContainer();
		~ShaderContainer();

		void Initialize(ShaderManager* shaderManager, FBOContainer* fboContainer);

		void LoadShaders(std::map<std::string, std::vector<std::string>> defines);
		void LoadShaders();
	private:
		ShaderManager* m_shaderManager;
		FBOContainer* m_FBOContainer;
		void LoadShader(std::string shaderASM, std::map<std::string, std::vector<std::string>> defines);
		std::string CheckDefines(std::map<std::string, std::vector<std::string>> defines, std::string shaderName);
		void ParseScript(std::string scriptPath, std::string shaderProgramName);

	};
}

#endif