#ifndef SRC_GFX_SHADERS_UNIFORMCONTAINER_HPP
#define SRC_GFX_SHADERS_UNIFORMCONTAINER_HPP

#include <vector>
#include <functional>
#include <GLEW/glew.h>

namespace gfx
{
	struct UniformContainer
	{
		std::vector<GLuint> uniformLocations;
		std::vector<void*> uniformValues;
		std::vector<std::function<void()>> uniformBindFunctions;
		std::vector<std::function<void()>> deleteFunctions;

		static void BindAll(const std::vector<std::function<void()>>& bindFunctions)
		{
			for (int i = 0; i < bindFunctions.size(); i++)
				bindFunctions[i]();
		}
	};
}

#endif