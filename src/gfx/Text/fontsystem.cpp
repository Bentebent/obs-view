#define PRINT_FONT_DEBUG_INFO
#ifdef PRINT_FONT_DEBUG_INFO
#include <iostream>
#endif

#include "fontsystem.hpp"
#include <algorithm>
#include <fstream>

#define STB_TRUETYPE_IMPLEMENTATION  // force following include to generate implementation
#include "stb_truetype.hpp"

#define UNICODE_REPLACEMENT 0xFFFD

namespace gfx
{
	const FontSize FONT_SIZE_MIN = 8;
	const FontSize FONT_SIZE_MAX = 72;

	/*----------------STATIC MEMBERS----------------*/
	Glyph FontSystem::m_nullGlyph = { 0, 0, 0, 0, 0, 0, 0, 0 };
	std::vector<FontData> FontSystem::m_fontData;
	Font FontSystem::m_fontIdCount = 0;
	FontData* FontSystem::m_fontDataCache = nullptr;
	/*----------------------------------------------*/
	GlyphNode::GlyphNode(unsigned short x, unsigned short y, 
		unsigned short w, unsigned short h)
	{
		m_left = 0;
		m_right = 0;
		m_x = x;
		m_y = y;
		m_width = w;
		m_height = h;
		m_unicode_value = 0;
	}
	
	void GlyphNode::Release()
	{
		if (m_left != nullptr)
		{
			m_left->Release();
			delete m_left;
		}
		if(m_right != nullptr)
		{
			m_right->Release();
			delete m_right;
		}
	}

	GlyphNode* GlyphNode::Insert(Glyph& glyph)
	{
		if (m_left != nullptr || m_right != nullptr) // Were not in a leaf node
		{
			GlyphNode* newNode = m_left->Insert(glyph);
			if (newNode) return newNode;
			return m_right->Insert(glyph);
		}
		else
		{
			if (m_unicode_value)
			{
				return nullptr; // This node is occupied :(
			}
			if (glyph.width > m_width || glyph.height > m_height)
			{
				return nullptr; // This glyph is too big to fit in this node :(
			}
			if (glyph.width == m_width && glyph.height == m_height)
			{
				glyph.texture_x = m_x;
				glyph.texture_y = m_y;
				m_unicode_value = glyph.unicode_value;
				return this; // This glyph fits perfectly! :D
			}

			unsigned short dw = m_width - glyph.width;
			unsigned short dh = m_height - glyph.height;

			if (dw > dh) // Split vertically
			{
				m_left = new GlyphNode(m_x, m_y, glyph.width, m_height);
				m_right = new GlyphNode(m_x + glyph.width, m_y, dw, m_height);
			}
			else // Split horizontally
			{
				m_left = new GlyphNode(m_x, m_y, m_width, glyph.height);
				m_right = new GlyphNode(m_x, m_y + glyph.height, m_width, dh);
			}
			return m_left->Insert(glyph);
		}
	}


	bool FontSystem::UnloadFont(Font font)
	{
		for (unsigned int i = 0; i < m_fontData.size(); i++)
		{
			if (m_fontData[i].fontID == font)
			{
				std::string fontName = m_fontData[i].fileName;
				m_fontData[i].fontID = 0;
				m_fontData[i].size = 0;
				m_fontData[i].fileName = "";
				delete[] m_fontData[i].ttfFile;
				m_fontData[i].fontInfo = { 0 };
				delete[] m_fontData[i].fontAtlas;
				m_fontData[i].atlasWidth = 0;
				m_fontData[i].atlasHeight = 0;
				m_fontData[i].glyphs.clear();
				m_fontData[i].glyphTree->Release();
				delete m_fontData[i].glyphTree;

				glDeleteTextures(1, &m_fontData[i].atlasTexture);

				#ifdef PRINT_FONT_DEBUG_INFO
				std::cout << "Successfully unloaded font \'" << fontName << "\'\n";
				#endif

				return true;
			}
		}
		return false;
	}

	Font FontSystem::LoadFont(const std::string &file, FontSize size)
	{
		#ifdef PRINT_FONT_DEBUG_INFO
		std::cout << "Attempting to load font \'" << file << "\' " << size << "px...";
		#endif

		if (size < FONT_SIZE_MIN)
		{
			size = FONT_SIZE_MIN;
			#ifdef PRINT_FONT_DEBUG_INFO
			std::cout << " font size is too small (min: " << FONT_SIZE_MIN << ")...";
			#endif
		}
		if (size > FONT_SIZE_MAX)
		{
			size = FONT_SIZE_MAX;
			#ifdef PRINT_FONT_DEBUG_INFO
			std::cout << " font size is too large (max: " << FONT_SIZE_MAX << ")...";
			#endif
		}

		Font font = findFont(file, size);

		if (!font)
		{
			font = createFont(file, size);

			#ifdef PRINT_FONT_DEBUG_INFO
			if (font)
			{
				std::cout << " success\n";
			}
			else
			{
				std::cout << " failed\n";
			}
			#endif
		}
		else
		{
			#ifdef PRINT_FONT_DEBUG_INFO
			std::cout << " already loaded\n";
			#endif
		}
		return font;
	
	}

	const Glyph& FontSystem::GetGlyph(Font font, unsigned short unicode_value)
	{
		FontData* fontData;
		if (m_fontDataCache && m_fontDataCache->fontID == font)
		{
			fontData = m_fontDataCache;
		}
		else
		{
			fontData = getFontData(font);
			m_fontDataCache = fontData;
		}

		Glyph& glyph = findGlyph(fontData, unicode_value);
		if (glyph.unicode_value != UNICODE_REPLACEMENT)
		{
			return glyph;
		}


		int glyphIndex = stbtt_FindGlyphIndex(&fontData->fontInfo, unicode_value);
		if (glyphIndex == 0)
		{
			return m_nullGlyph;
		}
		else
		{
			Glyph& glyph2 = loadGlyph(fontData, unicode_value);
			if (glyph2.unicode_value != 0)
			{
				return glyph2;
			}
		}

		#ifdef PRINT_FONT_DEBUG_INFO
		std::cout << "Failed to create glyph for unicode U+" 
			<< std::hex << std::uppercase << unicode_value 
			<< std::dec << std::nouppercase << "\n";
		#endif
		return m_nullGlyph;
	}

	const unsigned char* FontSystem::GetAtlasData(Font font, unsigned int& w, unsigned int& h)
	{
		FontData* fontData = getFontData(font);
		if (fontData)
		{
			w = fontData->atlasWidth;
			h = fontData->atlasHeight;
			return fontData->fontAtlas;
		}
		return nullptr;
	}

	Font FontSystem::findFont(const std::string &file, FontSize size)
	{
		for (unsigned int i = 0; i < m_fontData.size(); i++)
		{
			if (m_fontData[i].fileName == file)
			{
				return m_fontData[i].fontID;
			}
		}
		return 0;
	}

	bool FontSystem::loadTTF(FontData& data, const std::string &file)
	{
		std::ifstream in(file, std::ios::in | std::ios::binary);
		if (in)
		{
			in.seekg(0, std::ios::end);
			unsigned int buf_size = static_cast<unsigned int>(in.tellg());
			data.ttfFile = new unsigned char[buf_size];
			in.seekg(0, std::ios::beg);
			in.read((char*)data.ttfFile, buf_size);
			in.close();

			if (stbtt_InitFont(&data.fontInfo, data.ttfFile, stbtt_GetFontOffsetForIndex(data.ttfFile, 0)))
			{
				return true;
			}

			// If the initialization failed we need to delete the allocated buffer
			delete[] data.ttfFile;
			data.ttfFile = nullptr;
		}
		return false;
	}

	void FontSystem::generateASCII(Font font)
	{
		for (unsigned short c = 0x0020; c < 0x007F; c++)
		{
			GetGlyph(font, c);
		}
	}

	void FontSystem::addFontData(FontData data)
	{
		for (unsigned int i = 0; i < m_fontData.size(); i++)
		{
			if (m_fontData[i].fontID == 0)
			{
				m_fontData[i] = data;
				return;
			}
		}
		m_fontData.push_back(data);
	}

	Font FontSystem::createFont(const std::string &file, FontSize size)
	{
		FontData data;
		data.fontID = 0;
		if (loadTTF(data, file))
		{
			data.fontID = ++m_fontIdCount;
			data.size = size;
			data.fileName = file;
			data.atlasWidth = 512;
			data.atlasHeight = 512;
			data.glyphTree = new GlyphNode(0, 0, data.atlasWidth, data.atlasHeight);
			data.fontAtlas = new unsigned char[data.atlasWidth * data.atlasHeight];

			float scale = stbtt_ScaleForPixelHeight(&data.fontInfo, (float)data.size);
			stbtt_GetFontVMetrics(&data.fontInfo, &data.ascent, &data.descent, &data.lineGap);
			data.ascent = static_cast<int>(static_cast<float>(data.ascent) * scale);
			data.descent = static_cast<int>(static_cast<float>(data.descent) * scale);
			data.lineGap = static_cast<int>(static_cast<float>(data.lineGap) * scale);

			glGenTextures(1, &data.atlasTexture);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, data.atlasTexture);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_R8,
				data.atlasWidth,
				data.atlasHeight,
				0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				data.fontAtlas);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			addFontData(data);
			m_nullGlyph = loadGlyph(&data, UNICODE_REPLACEMENT);
			generateASCII(data.fontID);

			return data.fontID;
		}
		return 0;
	}

	FontData* FontSystem::getFontData(Font font)
	{
		for (unsigned int i = 0; i < m_fontData.size(); i++)
		{
			if (m_fontData[i].fontID == font)
			{
				return &m_fontData[i];
			}
		}
		return nullptr;
	}

	bool FontSystem::glyphCompare(Glyph a, unsigned short b)
	{
		return (a.unicode_value < b);
	}

	Glyph& FontSystem::findGlyph(FontData* fontData, unsigned short unicode_value)
	{
		// The glyphs list in fontData is sorted, use a binary search to find the correct value
		std::vector<Glyph>::const_iterator it = std::lower_bound(fontData->glyphs.begin(), fontData->glyphs.end(), unicode_value, glyphCompare);
		if (it != fontData->glyphs.end() && it->unicode_value == unicode_value)
		{
			return fontData->glyphs[it - fontData->glyphs.begin()];
		}
		return m_nullGlyph;
	}

	Glyph& FontSystem::insertGlyph(FontData *fontData, const Glyph& glyph)
	{
		// The glyphs list in fontData is sorted, use a binary search to find the correct value
		std::vector<Glyph>::const_iterator it = std::lower_bound(fontData->glyphs.begin(), fontData->glyphs.end(), glyph.unicode_value, glyphCompare);
		if (it != fontData->glyphs.end())
		{
			int position = it - fontData->glyphs.begin();
			fontData->glyphs.insert(it, glyph);
			return fontData->glyphs[position];
		}
		else
		{
			fontData->glyphs.push_back(glyph);
			return fontData->glyphs[fontData->glyphs.size() - 1];
		}
	}

	Glyph& FontSystem::loadGlyph(FontData *fontData, unsigned short unicode_value)
	{
		unsigned char *bitmap = 0;
		int w, h, xoff, yoff, advance, lsb;
		float scale = stbtt_ScaleForPixelHeight(&fontData->fontInfo, static_cast<float>(fontData->size));
		stbtt_GetCodepointHMetrics(&fontData->fontInfo, unicode_value, &advance, &lsb);
		bitmap = stbtt_GetCodepointBitmap(
			&fontData->fontInfo, 
			0, 
			stbtt_ScaleForPixelHeight(&fontData->fontInfo, static_cast<float>(fontData->size)),
			unicode_value,
			&w, &h, 
			&xoff, &yoff);



		Glyph glyph;
		glyph.unicode_value = unicode_value;
		glyph.width = static_cast<unsigned short>(w);
		glyph.height = static_cast<unsigned short>(h);
		glyph.advance = static_cast<unsigned short>(advance*scale);
		glyph.bearing_x = static_cast<unsigned short>(xoff);
		glyph.bearing_y = static_cast<unsigned short>(yoff);

		if (!allocateAtlasSpace(fontData, glyph))
		{
			return m_nullGlyph;
		}

		copyToAtlas(glyph, fontData, bitmap);
		delete[] bitmap;

		return insertGlyph(fontData, glyph);
		
	}

	bool FontSystem::allocateAtlasSpace(FontData* fontData, Glyph& glyph)
	{
		//Find free spot using bin packing algorithm or something like that
		if (fontData->glyphTree->Insert(glyph))
		{
			return true;
		}
		return false;
	}

	void FontSystem::copyToAtlas(const Glyph& glyph, FontData* fontData, unsigned char *bitmap)
	{
		unsigned short x = glyph.texture_x;
		unsigned short y = glyph.texture_y;
		unsigned short w = glyph.width;
		unsigned short h = glyph.height;


		for (unsigned short i = 0; i < h * w; i++)
		{
			unsigned short xb = i % w;
			unsigned short yb = i / w;

			unsigned int xa = x + xb;
			unsigned int ya = y + yb;

			unsigned int atlasIndex = xa + ya * fontData->atlasWidth;
			fontData->fontAtlas[atlasIndex] = bitmap[i];
		}

		// Copy data to texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fontData->atlasTexture);
		glTexSubImage2D(
			GL_TEXTURE_2D,
			0,
			0,
			0,
			fontData->atlasWidth,
			fontData->atlasHeight,
			GL_RED,
			GL_UNSIGNED_BYTE,
			fontData->fontAtlas );
		GLenum err = glGetError();
		std::cout << glGetError() << std::endl;

	}

	GLuint FontSystem::GetAtlasTextureGL(Font font)
	{
		FontData* fontData = getFontData(font);
		return fontData->atlasTexture;
	}
}