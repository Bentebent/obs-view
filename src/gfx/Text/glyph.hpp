#ifndef SRC_GFX_TEXT_GLYPH_HPP
#define SRC_GFX_TEXT_GLYPH_HPP
namespace gfx
{
	struct Glyph
	{
		// Unicode value e.g. 0x03A9 for the greek letter omega
		unsigned short unicode_value;

		// Width of the glyph in pixels
		unsigned short width;

		// Height of the glyph in pixels
		unsigned short height;

		// Distance to advance in pixels
		unsigned short advance;

		// Horizontal offset in pixels
		short bearing_x;

		// Vertical offset in pixels
		short bearing_y;

		// Texel coordinate x
		unsigned short texture_x;

		// Texel coordinate y
		unsigned short texture_y;
	};
}
#endif