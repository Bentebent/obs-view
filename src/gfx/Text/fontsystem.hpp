#ifndef SRC_GFX_TEXT_FONTSYSTEM_H
#define SRC_GFX_TEXT_FONTSYSTEM_H

#include <gfx/GFXFont.hpp>
#include "glyph.hpp"
#include "stb_truetype.hpp"
#include <vector>
#include <string>
#include <map>
#include <GLEW/glew.h>
namespace gfx
{
	class GlyphNode
	{
	public:
		GlyphNode(unsigned short x, unsigned short y, 
			unsigned short w, unsigned short h);

		GlyphNode*	Insert(Glyph& glyph);
		void		Release();

	private:
		GlyphNode*		m_left;
		GlyphNode*		m_right;
		unsigned short	m_x;
		unsigned short	m_y;
		unsigned short	m_width;
		unsigned short	m_height;
		unsigned short	m_unicode_value;
	};

	struct FontData
	{
		Font fontID;
		FontSize size;
		std::string fileName;
		unsigned char *ttfFile;
		stbtt_fontinfo fontInfo;
		int ascent, descent, lineGap;
		unsigned short atlasWidth;
		unsigned short atlasHeight;
		unsigned char *fontAtlas;
		GLuint atlasTexture;
		GlyphNode *glyphTree; // Used to allocate space in the font atlas
		std::vector<Glyph> glyphs; // Sorted
	};

	class FontSystem
	{
	public:
		/* Loads a font
		\param file Filepath to the font to be loaded
		\param size Pixel size of the font
		\return Returns a reference to the loaded font, value is 0 if failed. */
		static Font LoadFont(const std::string &file, FontSize size);

		/* Unloads a font
		\param font Reference to the font to be unloaded */
		static bool UnloadFont(Font font);
		
		/* Gets a glyph object describing information required for rendering characters.
		\param font Reference to the font to use.
		\param unicode_value The unicode value for the character (e.g. 0x03A9 for capital Omega).
		\return Returns a reference to a glyph object, returns nullglyph if the font or value is invalid. */
		static const Glyph& GetGlyph(Font font, unsigned short unicode_value);

		/* Returns the byte data contained in a font atlas */
		static const unsigned char* GetAtlasData(Font font, unsigned int& w, unsigned int& h);

		/* Binds a atlas as a texture */
		static GLuint GetAtlasTextureGL(Font font);

	private:

		/* Checks if a font is loaded. Returns the font ID if the font is loaded, returns 0 otherwise.*/
		static Font findFont(const std::string &file, FontSize size);

		/* Creates a font from file with specified size. Returns ID of the created font object if successful, otherwise returns 0. */
		static Font createFont(const std::string &file, FontSize size);

		/* Loads the information from a ttf file into a font object. Returns true if successful. */
		static bool loadTTF(FontData& data, const std::string &file);

		/* Generates the first 128 glyphs of the specified font. */
		static void generateASCII(Font font);

		/* Adds a new font to the fontData container */
		static void addFontData(FontData data);

		/* Gets a pointer to the font data associated with a font reference */
		static FontData* getFontData(Font font);

		/* Returns glyph information for a specified font and unicode value. 
		If the glyph doesnt exists, it will return a glyph with unicode value of 0 */
		static Glyph& findGlyph(FontData* fontData, unsigned short unicode_value);
		
		/* Comparison function for binary search */
		static bool glyphCompare(Glyph b, unsigned short a);

		/* Loads a glyph for a selected font and returns a reference to that glyph */
		static Glyph& loadGlyph(FontData *fontData, unsigned short glyphIndex);

		/* Inserts a glyph into the list of glyphs */
		static Glyph& insertGlyph(FontData *fontData, const Glyph& glyph);

		/* Copy a character bitmap data into the font atlas */
		static void copyToAtlas(const Glyph& glyph, FontData* fontData, unsigned char *bitmap);

		/* Finds a location in the font atlas for a glyph. Returns false if failed. */
		static bool allocateAtlasSpace(FontData* fontData, Glyph& glyph);

		/*----------------STATIC MEMBERS----------------*/

		// Container for all font objects.
		static std::vector<FontData> m_fontData;

		// Null glyph for use in case a glyph is invalid
		static Glyph m_nullGlyph;
		
		// Count of used font IDs
		static Font m_fontIdCount;

		// Cached font data to avoid lookups for each requested glyph
		static FontData* m_fontDataCache;

		/*----------------------------------------------*/

	};
}
#endif