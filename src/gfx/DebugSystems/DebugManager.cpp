#include "DebugManager.hpp"
#include <codecvt>
#include <iostream>
#include <glm/glm.hpp>
#include <Texture/TextureManager.hpp>
#include <Settings/SettingsContainer.hpp>
#include <Viewport/ViewportContainer.hpp>



#define M_PI 3.14159265359
#define M_2PI 6.28318530718

namespace gfx
{
	DebugManager* debugManagerQueuePointer;
	DebugManager* debugManagerRenderPointer;
	DebugManager debugManagerA;
	DebugManager debugManagerB;

	void DebugManager::Init(const std::string& debugFont, int fontSize)
	{
		m_debugFont = FontSystem::LoadFont(debugFont, fontSize);

		// Create text vertex buffers
		glGenBuffers(1, &m_textVBO);
		glBindBuffer(GL_ARRAY_BUFFER, m_textVBO);
		glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW);

		glGenVertexArrays(1, &m_textVAO);
		glBindVertexArray(m_textVAO);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(DebugTextVertex), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribIPointer(1, 4, GL_UNSIGNED_INT, sizeof(DebugTextVertex), (void*)(4 * sizeof(float)));
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Create line vertex buffers
		glGenBuffers(1, &m_lineVBO);
		glBindBuffer(GL_ARRAY_BUFFER, m_lineVBO);
		glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW);

		glGenVertexArrays(1, &m_lineVAO);
		glBindVertexArray(m_lineVAO);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(DebugLineVertex), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(DebugLineVertex), (void*)(3 * sizeof(float)));
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//Sphere template
		{
			// Horizontal
			for (int i = 0; i < m_sphereSegments; i++)
			{
				LineObject lo;
				lo.sx = static_cast<float>(cos(M_2PI * (i / static_cast<float>(m_sphereSegments))));
				lo.sy = 0.0f;
				lo.sz = static_cast<float>(sin(M_2PI * (i / static_cast<float>(m_sphereSegments))));

				lo.ex = static_cast<float>(cos(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				lo.ey = 0.0f;
				lo.ez = static_cast<float>(sin(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				m_sphereTemplate.push_back(lo);
			}

			// Vertical 1
			for (int i = 0; i < m_sphereSegments; i++)
			{
				LineObject lo;
				lo.sx = static_cast<float>(cos(M_2PI * (i / static_cast<float>(m_sphereSegments))));
				lo.sy = static_cast<float>(sin(M_2PI * (i / static_cast<float>(m_sphereSegments))));
				lo.sz = 0.0f;

				lo.ex = static_cast<float>(cos(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				lo.ey = static_cast<float>(sin(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				lo.ez = 0.0f;
				m_sphereTemplate.push_back(lo);
			}

			// Vertical 1
			for (int i = 0; i < m_sphereSegments; i++)
			{
				LineObject lo;
				lo.sx = 0.0f;
				lo.sy = static_cast<float>(sin(M_2PI * (i / static_cast<float>(m_sphereSegments))));
				lo.sz = static_cast<float>(cos(M_2PI * (i / static_cast<float>(m_sphereSegments))));

				lo.ex = 0.0f;
				lo.ey = static_cast<float>(sin(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				lo.ez = static_cast<float>(cos(M_2PI * ((i + 1) / static_cast<float>(m_sphereSegments))));
				m_sphereTemplate.push_back(lo);
			}
		}

		


	}
	void DebugManager::Render(ShaderManager* shaderManager)
	{
		renderLines(shaderManager);
		renderText(shaderManager);
	}
	void DebugManager::AddText(const std::string& text, int x, int y, float* color)
	{
		TextObject to;
		to.text = UnicodeHelper::UTF8DecodeStr(text);
		to.x = static_cast<float>(x);
		to.y = static_cast<float>(y);
		to.r = color[0];
		to.g = color[1];
		to.b = color[2];
		to.a = color[3];
		m_textObjects.push_back(to);
	}
	void DebugManager::AddLine(float* start, float* end, float* color)
	{
		LineObject lo;
		lo.sx = start[0];
		lo.sy = start[1];
		lo.sz = start[2];

		lo.ex = end[0];
		lo.ey = end[1];
		lo.ez = end[2];

		lo.r = color[0];
		lo.g = color[1];
		lo.b = color[2];
		lo.a = color[3];
		m_lineObjects.push_back(lo);
	}
	void DebugManager::AddSphere(float* pos, float r, float* color)
	{
		LineObject lo;
		lo.r = color[0];
		lo.g = color[1];
		lo.b = color[2];
		lo.a = color[3];
		for (unsigned int i = 0; i < m_sphereTemplate.size(); i++)
		{
			lo.ex = m_sphereTemplate[i].ex * r + pos[0];
			lo.ey = m_sphereTemplate[i].ey * r + pos[1];
			lo.ez = m_sphereTemplate[i].ez * r + pos[2];
			lo.sx = m_sphereTemplate[i].sx * r + pos[0];
			lo.sy = m_sphereTemplate[i].sy * r + pos[1];
			lo.sz = m_sphereTemplate[i].sz * r + pos[2];
			m_lineObjects.push_back(lo);
		}
	}

	void DebugManager::AddAABB(float* min, float* max, float* color)
	{
		LineObject lo;
		lo.r = color[0];
		lo.g = color[1];
		lo.b = color[2];
		lo.a = color[3];

		// Min vertex
		lo.sx = min[0];
		lo.sy = min[1];
		lo.sz = min[2];

		// 1
		lo.ex = max[0];
		lo.ey = min[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		// 2
		lo.ex = min[0];
		lo.ey = max[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		// 3
		lo.ex = min[0];
		lo.ey = min[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

		// Bottom verts
		lo.sx = min[0];
		lo.sy = max[1];
		lo.sz = min[2];

		// 4
		lo.ex = max[0];
		lo.ey = max[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		// 5
		lo.ex = min[0];
		lo.ey = max[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

		// Max vertex
		lo.sx = max[0];
		lo.sy = max[1];
		lo.sz = max[2];

		// 1
		lo.ex = min[0];
		lo.ey = max[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

		// 2
		lo.ex = max[0];
		lo.ey = min[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

		// 3
		lo.ex = max[0];
		lo.ey = max[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		// Bottom verts
		lo.sx = max[0];
		lo.sy = min[1];
		lo.sz = max[2];

		// 4
		lo.ex = min[0];
		lo.ey = min[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

		// 5
		lo.ex = max[0];
		lo.ey = min[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		// rest of the vertical lines
		lo.sx = max[0];
		lo.sy = min[1];
		lo.sz = min[2];
		lo.ex = max[0];
		lo.ey = max[1];
		lo.ez = min[2];
		m_lineObjects.push_back(lo);

		lo.sx = min[0];
		lo.sy = max[1];
		lo.sz = max[2];
		lo.ex = min[0];
		lo.ey = min[1];
		lo.ez = max[2];
		m_lineObjects.push_back(lo);

	}

	void DebugManager::AddOBB(float* min, float* max, float* rot, float* color)
	{
	}
	void DebugManager::AddText3D(const std::string& text, float* pos, float* color)
	{
	}

	void DebugManager::renderText(ShaderManager* shaderManager)
	{
		if (m_textObjects.empty()) return;

		std::vector< DebugTextVertex > vertices;
		std::vector< std::pair< GLint, glm::vec4 > > colorsBatches;
		unsigned int atlas_w, atlas_h;
		FontSystem::GetAtlasData(m_debugFont, atlas_w, atlas_h);

		float clientWidth = static_cast<float>(Settings().m_clientWidth);
		float clientHeight = static_cast<float>(Settings().m_clientHeight);

		unsigned short spaceWidth;
		{
			Glyph space_glyph = FontSystem::GetGlyph(m_debugFont, 0x0020);
			spaceWidth = space_glyph.advance;
		}
		unsigned short tabWidth = 4 * spaceWidth;

		for (unsigned int i = 0; i < m_textObjects.size(); i++)
		{
			float step = 0;
			float x = m_textObjects[i].x;
			float y = clientHeight - m_textObjects[i].y;

			glm::vec4 color;
			color.r = m_textObjects[i].r;
			color.g = m_textObjects[i].g;
			color.b = m_textObjects[i].b;
			color.a = m_textObjects[i].a;

			std::pair< GLint, glm::vec4 > colorBatch;
			colorBatch.first = 0;
			colorBatch.second = color;

			for (obs_ushort c : m_textObjects[i].text)
			{
				Glyph glyph = FontSystem::GetGlyph(m_debugFont, static_cast<unsigned short>(c));
				DebugTextVertex vertex;
				vertex.x = 2.0f * ((x + static_cast<float>(glyph.bearing_x) + step) / clientWidth) - 1.0f;
				vertex.y = 2.0f * ((y - static_cast<float>(glyph.height + glyph.bearing_y)) / clientHeight) - 1.0f;
				vertex.w = 2.0f * (glyph.width / clientWidth);
				vertex.h = 2.0f * (glyph.height / clientHeight);
				vertex.u0 = glyph.texture_x;
				vertex.v0 = glyph.texture_y + glyph.height;
				vertex.u1 = glyph.texture_x + glyph.width;
				vertex.v1 = glyph.texture_y;

				if (c > 0x0020)
				{
					vertices.push_back(vertex);
					colorBatch.first++;
				}
				
				if (c == 0x0009) // tab
				{
					step += tabWidth - static_cast<unsigned short>(step) % (tabWidth);
				}
				else
				{
					step += glyph.advance;
				}

			}
			colorsBatches.push_back(colorBatch);
		}


		glBindBuffer(GL_ARRAY_BUFFER, m_textVBO);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(DebugTextVertex), NULL, GL_STREAM_DRAW);
		DebugTextVertex* bufferData = (DebugTextVertex*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		memcpy(bufferData, &vertices[0], vertices.size() * sizeof(DebugTextVertex));

		glUnmapBuffer(GL_ARRAY_BUFFER);

		shaderManager->UseProgram("Text");


		GLint loc = shaderManager->GetUniformLocation("Text", "textureAtlas");
		TextureManagerInstance().BindTexture(
			FontSystem::GetAtlasTextureGL(m_debugFont),
			loc,
			0,
			GL_TEXTURE_2D);
		GLint fontColorLocation = shaderManager->GetUniformLocation("Text", "fontColor");

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBindVertexArray(m_textVAO);

		GLint vertexOffset = 0;
		for (unsigned int i = 0; i < colorsBatches.size(); i++)
		{
			shaderManager->SetUniform(1, colorsBatches[i].second, fontColorLocation);
			glDrawArrays(GL_POINTS, vertexOffset, colorsBatches[i].first);
			vertexOffset += colorsBatches[i].first;
		}
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_BLEND);

		glBindBuffer(GL_ARRAY_BUFFER, 0); 
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);

		m_textObjects.clear();
	}
	void DebugManager::renderLines(ShaderManager* shaderManager)
	{
		if (m_lineObjects.empty()) return;

		std::vector< std::pair< GLint, glm::vec4 > > batches;
		std::vector<DebugLineVertex> vertices;
		DebugLineVertex vertex;
		glm::vec4 lastColor = glm::vec4(-1.0f, -1.0f, -1.0f, -1.0f);
		std::pair< GLint, glm::vec4 > colorBatch;
		colorBatch.first = 1;
		colorBatch.second = lastColor;

		for (unsigned int i = 0; i < m_lineObjects.size(); i++)
		{
			vertex.sx = m_lineObjects[i].sx;
			vertex.sy = m_lineObjects[i].sy;
			vertex.sz = m_lineObjects[i].sz;

			vertex.ex = m_lineObjects[i].ex;
			vertex.ey = m_lineObjects[i].ey;
			vertex.ez = m_lineObjects[i].ez;

			vertices.push_back(vertex);

			glm::vec4 color;
			color.r = m_lineObjects[i].r;
			color.g = m_lineObjects[i].g;
			color.b = m_lineObjects[i].b;
			color.a = m_lineObjects[i].a;

			if (color != lastColor)
			{
				colorBatch.first = 1;
				colorBatch.second = color;
				batches.push_back(colorBatch);
			}
			else
			{
				batches[batches.size() - 1].first++;
			}
			lastColor = color;
		}
		glBindBuffer(GL_ARRAY_BUFFER, m_lineVBO);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(DebugLineVertex), NULL, GL_STREAM_DRAW);
		DebugLineVertex* bufferData = (DebugLineVertex*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		memcpy(bufferData, &vertices[0], vertices.size() * sizeof(DebugLineVertex));

		glUnmapBuffer(GL_ARRAY_BUFFER);

		shaderManager->UseProgram("LineDebug");
		GLint fontColorLocation = shaderManager->GetUniformLocation("LineDebug", "gColor");

		glm::mat4 view = VPContainerInstance().GetViewport(0).view;
		glm::mat4 proj = VPContainerInstance().GetViewport(0).projection;

		shaderManager->SetUniform(1, proj, shaderManager->GetUniformLocation(shaderManager->GetShaderProgramID("LineDebug"), "gProjectionMatrix"));
		shaderManager->SetUniform(1, view, shaderManager->GetUniformLocation(shaderManager->GetShaderProgramID("LineDebug"), "gViewMatrix"));

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBindVertexArray(m_lineVAO);

		GLint vertexOffset = 0;
		for (unsigned int i = 0; i < batches.size(); i++)
		{
			shaderManager->SetUniform(1, batches[i].second, fontColorLocation);
			glDrawArrays(GL_POINTS, vertexOffset, batches[i].first);
			vertexOffset += batches[i].first;
		}
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_BLEND);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		m_lineObjects.clear();
	}

}