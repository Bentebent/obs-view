#ifndef SRC_GFX_DEBUGSYSTEMS_DEBUGMANAGER_HPP
#define SRC_GFX_DEBUGSYSTEMS_DEBUGMANAGER_HPP

#include <Text/fontsystem.hpp>
#include <vector>
#include <string>
#include <GLEW/glew.h>
#include <Shaders/ShaderManager.hpp>
#include <utility/UnicodeHelp.hpp>

namespace gfx
{
	class DebugManager
	{
	public:
		void Init(const std::string& debugFont, int fontSize);
		void Render(ShaderManager* shaderManager);

		void AddText(const std::string& text, int x, int y, float* color);
		void AddLine(float* start, float* end, float* color);
		void AddSphere(float* pos, float r, float* color);
		void AddAABB(float* min, float* max, float* color);

		// Not implemented
		void AddOBB(float* min, float* max, float* rot, float* color);
		void AddText3D(const std::string& text, float* pos, float* color);

	private:

		struct DebugTextVertex
		{
			float x, y, w, h;
			unsigned int u0, v0, u1, v1;
		};

		struct DebugLineVertex
		{
			float sx, sy, sz;
			float ex, ey, ez;
		};

		struct TextObject
		{
			obs_ustring text;
			float x, y;
			float r, g, b, a;
		};

		struct LineObject
		{
			float sx, sy, sz;
			float ex, ey, ez;
			float r, g, b, a;
		};

		void renderText(ShaderManager* shaderManager);
		void renderLines(ShaderManager* shaderManager);

		/*----------------STATIC MEMBERS----------------*/

		/* Font used for debug text */
		Font m_debugFont;

		/* Vector containing all text to be rendered for one frame */
		std::vector<TextObject> m_textObjects;

		/* Vector containing all line vertices to be drawn */
		std::vector<LineObject> m_lineObjects;

		/* Vector containing a template of lines for spheres */
		std::vector<LineObject> m_sphereTemplate;

		/* Number of segments in a sphere */
		const int m_sphereSegments = 16;

		/* Buffer to hold text vertices */
		GLuint m_textVAO;
		GLuint m_textVBO;

		/* Buffer to hold line vertices */
		GLuint m_lineVAO;
		GLuint m_lineVBO;

		/*----------------------------------------------*/
	};

	extern DebugManager* debugManagerQueuePointer;
	extern DebugManager* debugManagerRenderPointer;

	extern DebugManager debugManagerA;
	extern DebugManager debugManagerB;
}

#endif