#ifndef SRC_GFX_MESH_MESHDATA_HPP
#define SRC_GFX_MESH_MESHDATA_HPP

#include <GLEW/glew.h>

namespace gfx
{
	struct Mesh
	{
		unsigned int id;
		unsigned int indexCount;
		GLuint VAO;
		GLuint IBO;
	};

	struct IndirectMesh
	{
		unsigned int id;
		unsigned int indexCount;
		GLuint ib;
		GLuint64 ib_addrs;
		GLsizeiptr ib_sizes;
		GLuint vb;
		GLuint64 vbo_addrs;
		GLsizeiptr vbo_sizes;
	};
}

#endif