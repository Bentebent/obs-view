#include "MeshManager.hpp"
#include "gfx/GFXDefines.hpp"
#include <iostream>
namespace gfx
{
	MeshManager& MeshManagerInstance()
	{
		static MeshManager meshManager;
		return meshManager;
	}

	MeshManager::MeshManager()
	{
		m_idCounter = 1;
	}

	MeshManager::~MeshManager()
	{

	}

	template<>
	int MeshManager::LoadMesh<StaticVertex>(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, StaticVertex* verts, int* indices)
	{
		Mesh mesh;
		GLuint VBO;
		GLuint VAO;
		GLuint IBO;

		//Generate VBO
		glGenBuffers(1, &VBO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeVerts * sizeof(StaticVertex), verts, GL_STATIC_DRAW);

		//Generate IBO
		glGenBuffers(1, &IBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), indices, GL_STATIC_DRAW);

		//Generate VAO
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		//Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)0);
		//glVertexAttribDivisor(0, 0);

		//Normal
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(4 * sizeof(float)));
		//glVertexAttribDivisor(1, 0);

		//Tangent
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(8 * sizeof(float)));
		//glVertexAttribDivisor(2, 0);

		//UV
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(12 * sizeof(float)));
		//glVertexAttribDivisor(2, 0);

		IndirectMesh iMesh;

		glGenBuffers(1, &iMesh.ib);
		glGenBuffers(1, &iMesh.vb);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iMesh.ib);
		glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), &indices[0], 0);
		glGetBufferParameterui64vNV(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &iMesh.ib_addrs);
		glMakeBufferResidentNV(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
		iMesh.ib_sizes = sizeIndices * sizeof(int);

		glBindBuffer(GL_ARRAY_BUFFER, iMesh.vb);
		glBufferStorage(GL_ARRAY_BUFFER, sizeVerts * sizeof(StaticVertex), &verts[0], 0);
		glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &iMesh.vbo_addrs);
		glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
		iMesh.vbo_sizes = sizeVerts * sizeof(StaticVertex);

		iMesh.id = static_cast<unsigned int>(m_idCounter);
		iMesh.indexCount = sizeIndices;
		m_indirectMeshes.push_back(iMesh);

		//glGenBuffers(1, &*m_ibs.begin());
		//glGenBuffers(1, &*m_vbs.begin());
		//
		//for (size_t u = 0; u < _objectCount; ++u)
		//{
		//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibs[u]);
		//	glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(UntexturedObjectsProblem::Index), &*_indices.begin(), 0);
		//	glGetBufferParameterui64vNV(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &m_ib_addrs[u]);
		//	glMakeBufferResidentNV(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
		//	m_ib_sizes[u] = _indices.size() * sizeof(UntexturedObjectsProblem::Index);
		//
		//	glBindBuffer(GL_ARRAY_BUFFER, m_vbs[u]);
		//	glBufferStorage(GL_ARRAY_BUFFER, _vertices.size() * sizeof(UntexturedObjectsProblem::Vertex), &*_vertices.begin(), 0);
		//	glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &m_vbo_addrs[u]);
		//	glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
		//	m_vbo_sizes[u] = _vertices.size() * sizeof(UntexturedObjectsProblem::Vertex);
		//}

		mesh.id = static_cast<unsigned int>(m_idCounter);
		m_idCounter++;
		mesh.VAO = VAO;
		mesh.IBO = IBO;
		mesh.indexCount = sizeIndices;
		m_meshes.push_back(mesh);

		meshID = mesh.id;

		return GFX_SUCCESS;
	}

	template<>
	int MeshManager::LoadMesh<AnimatedVertex>(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, AnimatedVertex* verts, int* indices)
	{
		Mesh mesh;
		GLuint VBO;
		GLuint VAO;
		GLuint IBO;


		//Generate VBO
		glGenBuffers(1, &VBO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeVerts * sizeof (AnimatedVertex), verts, GL_STATIC_DRAW);

		//Generate IBO
		glGenBuffers(1, &IBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), indices, GL_STATIC_DRAW);

		//Generate VAO
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		//Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)0);
		glVertexAttribDivisor(0, 0);

		//Normal
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(4 * sizeof(float)));
		glVertexAttribDivisor(1, 0);

		//Tangent
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(8 * sizeof(float)));
		glVertexAttribDivisor(2, 0);

		//bone indices
		glEnableVertexAttribArray(3);
		glVertexAttribIPointer(3, 4, GL_INT, sizeof(AnimatedVertex), (void*)(12 * sizeof(int)));
		glVertexAttribDivisor(3, 0);

		//bone weights
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(16 * sizeof(float)));
		glVertexAttribDivisor(4, 0);

		//UV
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(20 * sizeof(float)));
		glVertexAttribDivisor(5, 0);

		mesh.id = static_cast<unsigned int>(m_idCounter);
		m_idCounter++;
		mesh.VAO = VAO;
		mesh.IBO = IBO;
		mesh.indexCount = sizeIndices;
		m_meshes.push_back(mesh);

		meshID = mesh.id;

		return GFX_SUCCESS;
	}

	template<>
	int MeshManager::ReplaceMesh<StaticVertex>(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, StaticVertex* verts, int* indices)
	{
		Mesh mesh;
		GLuint VBO;
		GLuint VAO;
		GLuint IBO;

		DeleteMesh(meshID);

		//Generate VBO
		glGenBuffers(1, &VBO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeVerts * sizeof(StaticVertex), verts, GL_STATIC_DRAW);

		//Generate IBO
		glGenBuffers(1, &IBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), indices, GL_STATIC_DRAW);

		//Generate VAO
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		//Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)0);
		//glVertexAttribDivisor(0, 0);

		//Normal
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(4 * sizeof(float)));
		//glVertexAttribDivisor(1, 0);

		//Tangent
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(8 * sizeof(float)));
		//glVertexAttribDivisor(2, 0);

		//UV
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(12 * sizeof(float)));
		//glVertexAttribDivisor(2, 0);

		IndirectMesh iMesh = m_indirectMeshes[meshID];

		glGenBuffers(1, &iMesh.ib);
		glGenBuffers(1, &iMesh.vb);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iMesh.ib);
		glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), &indices[0], 0);
		glGetBufferParameterui64vNV(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &iMesh.ib_addrs);
		glMakeBufferResidentNV(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
		iMesh.ib_sizes = sizeIndices * sizeof(int);

		glBindBuffer(GL_ARRAY_BUFFER, iMesh.vb);
		glBufferStorage(GL_ARRAY_BUFFER, sizeVerts * sizeof(StaticVertex), &verts[0], 0);
		glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &iMesh.vbo_addrs);
		glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
		iMesh.vbo_sizes = sizeVerts * sizeof(StaticVertex);

		//iMesh.id = static_cast<unsigned int>(m_idCounter);
		iMesh.indexCount = sizeIndices;
		m_indirectMeshes[meshID] = iMesh;

		//glGenBuffers(1, &*m_ibs.begin());
		//glGenBuffers(1, &*m_vbs.begin());
		//
		//for (size_t u = 0; u < _objectCount; ++u)
		//{
		//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibs[u]);
		//	glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(UntexturedObjectsProblem::Index), &*_indices.begin(), 0);
		//	glGetBufferParameterui64vNV(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &m_ib_addrs[u]);
		//	glMakeBufferResidentNV(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
		//	m_ib_sizes[u] = _indices.size() * sizeof(UntexturedObjectsProblem::Index);
		//
		//	glBindBuffer(GL_ARRAY_BUFFER, m_vbs[u]);
		//	glBufferStorage(GL_ARRAY_BUFFER, _vertices.size() * sizeof(UntexturedObjectsProblem::Vertex), &*_vertices.begin(), 0);
		//	glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &m_vbo_addrs[u]);
		//	glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
		//	m_vbo_sizes[u] = _vertices.size() * sizeof(UntexturedObjectsProblem::Vertex);
		//}

		mesh.id = static_cast<unsigned int>(m_idCounter);
		
		mesh.VAO = VAO;
		mesh.IBO = IBO;
		mesh.indexCount = sizeIndices;
		m_meshes[meshID] = mesh;

		meshID = mesh.id;

		return GFX_SUCCESS;
	}

	template<>
	int MeshManager::ReplaceMesh<AnimatedVertex>(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, AnimatedVertex* verts, int* indices)
	{
		Mesh mesh;
		GLuint VBO;
		GLuint VAO;
		GLuint IBO;

		//Generate VBO
		glGenBuffers(1, &VBO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeVerts * sizeof (AnimatedVertex), verts, GL_STATIC_DRAW);

		//Generate IBO
		glGenBuffers(1, &IBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeIndices * sizeof(int), indices, GL_STATIC_DRAW);

		//Generate VAO
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		//Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)0);
		glVertexAttribDivisor(0, 0);

		//Normal
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(4 * sizeof(float)));
		glVertexAttribDivisor(1, 0);

		//Tangent
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(8 * sizeof(float)));
		glVertexAttribDivisor(2, 0);

		//bone indices
		glEnableVertexAttribArray(3);
		glVertexAttribIPointer(3, 4, GL_INT, sizeof(AnimatedVertex), (void*)(12 * sizeof(int)));
		glVertexAttribDivisor(3, 0);

		//bone weights
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(16 * sizeof(float)));
		glVertexAttribDivisor(4, 0);

		//UV
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(AnimatedVertex), (void*)(20 * sizeof(float)));
		glVertexAttribDivisor(5, 0);

		mesh.id = static_cast<unsigned int>(m_idCounter);
		m_idCounter++;
		mesh.VAO = VAO;
		mesh.IBO = IBO;
		mesh.indexCount = sizeIndices;
		m_meshes.push_back(mesh);

		meshID = mesh.id;

		return GFX_SUCCESS;
	}

	void MeshManager::BindMesh(int index)
	{
		glBindVertexArray(m_meshes.at(index).VAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_meshes.at(index).IBO);
	}

	int MeshManager::DeleteMesh(int index)
	{
		glDeleteVertexArrays(1, &m_meshes[index].VAO);
		glDeleteBuffers(1, &m_meshes[index].IBO);

		glDeleteBuffers(1, &m_indirectMeshes[index].ib);
		glDeleteBuffers(1, &m_indirectMeshes[index].vb);

		return GFX_SUCCESS;
	}

	int MeshManager::EraseMesh(int index)
	{
		return GFX_SUCCESS;
	}

}