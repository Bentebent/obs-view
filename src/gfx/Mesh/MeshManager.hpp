#ifndef SRC_GFX_MESH_MESHMANAGER_HPP
#define SRC_GFX_MESH_MESHMANAGER_HPP

#include <GLEW/glew.h>
#include <gfx/GFXVertex.hpp>
#include "MeshData.hpp"

#include <vector>

namespace gfx
{
	class MeshManager
	{
	public:
		friend MeshManager& MeshManagerInstance();

		template<class T>
		int LoadMesh(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, T* verts, int* indices);

		template<class T>
		int ReplaceMesh(unsigned int& meshID, const int& sizeVerts, const int& sizeIndices, T* verts, int* indices);

		int DeleteMesh(int index);
		int EraseMesh(int index);

		void BindMesh(int index);
		inline Mesh GetMesh(int index){ return m_meshes[index]; }
		inline IndirectMesh GetIndirectMesh(int index) { return m_indirectMeshes[index-1]; }
	
	private:
		MeshManager();
		~MeshManager();

		std::vector<Mesh> m_meshes;
		std::vector<IndirectMesh> m_indirectMeshes;
		unsigned long long int m_idCounter;
	};

	MeshManager& MeshManagerInstance();
}

#endif