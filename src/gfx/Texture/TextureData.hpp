#ifndef SRC_GFX_TEXTURE_TEXTURE_DATA_HPP
#define SRC_GFX_TEXTURE_TEXTURE_DATA_HPP

#include <GLEW/glew.h>

namespace gfx
{
	struct TextureData
	{
		unsigned int id;
		GLuint textureHandle;
		std::string path;

		GLuint64 bindlessHandle;
		unsigned int bindlessPosition;
	};

	struct BindlessTextureData
	{
		GLuint64 textureHandle;
	};
}

#endif