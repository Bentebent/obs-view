﻿#include "TextureManager.hpp"
#include <logger/Logger.hpp>
#include <Material/MaterialManager.hpp>

namespace gfx
{
	TextureManager& TextureManagerInstance()
	{
		static TextureManager tm;
		return tm;
	}

	TextureManager::TextureManager()
	{
		
	}

	TextureManager::~TextureManager()
	{

	}

	void TextureManager::Initialize()
	{
		m_2DidCounter = 0;
		m_cubeMapIdCounter = 0;

		glGenBuffers(1, &m_2DTextureBuffer);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_2DTextureBuffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, 1000 * sizeof(GLuint64), nullptr, GL_STATIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_2DTextureBuffer);

		glGenBuffers(1, &m_cubeMapTextureBuffer);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_cubeMapTextureBuffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, 1000 * sizeof(GLuint64), nullptr, GL_STATIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_cubeMapTextureBuffer);

		
	}

	void TextureManager::LoadTexture(unsigned int& id, const char* path, unsigned char* data, int width, int height, bool decal, bool replace)
	{
		if (replace)
		{
			ReplaceTexture(id, data, GL_TEXTURE_2D, GL_RGBA32F, GL_RGBA, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR,
				GL_REPEAT, GL_REPEAT, width, height, path);
			return;
		}
			

		//Check if the texture has already been loaded, if so, fuck off back with id
		for (int i = 0; i < m_2DTextures.size(); i++)
		{
			if (m_2DTextures[i].path == path)
			{
				id = m_2DTextures[i].id;
				return;
			}
		}

        /*
		LoadTexture(id, data, GL_TEXTURE_2D, GL_RGBA, GL_RGBA, GL_LINEAR, GL_LINEAR,
			GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, width, height);
        */

		if (decal)
		{
			//NO MIPMAP FOR UUU
			//FOR NOW
			LoadTexture(id, data, GL_TEXTURE_2D, GL_RGBA32F, GL_RGBA, GL_NEAREST, GL_NEAREST,
				GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, width, height, path);
		}
		else
		{
			//if (RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_OFF)
			//{
			//	LoadTexture(id, data, GL_TEXTURE_2D, GL_RGBA, GL_RGBA, GL_LINEAR, GL_LINEAR,
			//		GL_REPEAT, GL_REPEAT, width, height);
			//}
			//else
			//{
			LoadTexture(id, data, GL_TEXTURE_2D, GL_RGBA32F, GL_RGBA, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR,
					GL_REPEAT, GL_REPEAT, width, height, path);
			//}
		}
	}
		
	void TextureManager::LoadTexture(unsigned int& id, unsigned char* data, const GLenum& target,
		const GLenum& internalFormat, const GLenum& format,
		const GLint& minFilter, const GLint& magFilter,
		const GLint& wrapS, const GLint& wrapT,
		int width, int height, const char* path)
	{


		GLuint textureHandle;
		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_2D, textureHandle);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
		
		//if (RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_AF)
		//{
			GLfloat maxAniso = 0.0f;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);
		//}

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		//if (RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_ON || RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_AF)
		//{
			glGenerateMipmap(GL_TEXTURE_2D);
		//}

		BindlessTextureData bTexture;
		GLuint64 bindlessHandle = glGetTextureHandleNV(textureHandle);
		glMakeTextureHandleResidentNV(bindlessHandle);

		bTexture.textureHandle = bindlessHandle;

		m_2DBindlessTextures.push_back(bTexture);

		TextureData texture;
		texture.id = static_cast<unsigned int>(m_2DidCounter);
		texture.path = std::string(path);
		m_2DidCounter++;
		texture.textureHandle = textureHandle;
		texture.bindlessHandle = bindlessHandle;
		texture.bindlessPosition = m_2DBindlessTextures.size() - 1;
		m_2DTextures.push_back(texture);

		id = texture.id;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_2DTextureBuffer);
		GLuint64* p = (GLuint64*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_2DBindlessTextures.size() * sizeof(GLuint64),
			GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);

		memcpy(p, m_2DBindlessTextures.data(), m_2DBindlessTextures.size() * sizeof(GLuint64));

		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}

	void TextureManager::ReplaceTexture(unsigned int& id, unsigned char* data, const GLenum& target,
		const GLenum& internalFormat, const GLenum& format,
		const GLint& minFilter, const GLint& magFilter,
		const GLint& wrapS, const GLint& wrapT,
		int width, int height, const char* path)
	{

		DeleteTexture(id, false);

		GLuint textureHandle;
		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_2D, textureHandle);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);

		//if (RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_AF)
		//{
		GLfloat maxAniso = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);
		//}

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		//if (RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_ON || RenderSettings::settings[GFX_MIPMAPS] == GFX_MIPMAPS_AF)
		//{
		glGenerateMipmap(GL_TEXTURE_2D);
		//}

		BindlessTextureData bTexture;
		GLuint64 bindlessHandle = glGetTextureHandleNV(textureHandle);
		glMakeTextureHandleResidentNV(bindlessHandle);

		bTexture.textureHandle = bindlessHandle;

		TextureData texture = m_2DTextures[FindTexture(id)];
		m_2DBindlessTextures[texture.bindlessPosition] = bTexture;

		texture.path = std::string(path);
		texture.textureHandle = textureHandle;
		texture.bindlessHandle = bindlessHandle;
		m_2DTextures[FindTexture(id)] = texture;

		id = texture.id;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_2DTextureBuffer);
		GLuint64* p = (GLuint64*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_2DBindlessTextures.size() * sizeof(GLuint64),
			GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);

		memcpy(p, m_2DBindlessTextures.data(), m_2DBindlessTextures.size() * sizeof(GLuint64));

		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}


	void TextureManager::LoadCubemap(unsigned int& id, const char* path, unsigned char* posX, unsigned char* negX, unsigned char* posY, unsigned char* negY, unsigned char* posZ, unsigned char* negZ, int width, int height)
	{
		GLfloat maxAniso = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);

		GLuint textureHandle;
		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureHandle);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posX);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negX);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posY);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negY);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posZ);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negZ);

		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

		BindlessTextureData bTexture;
		GLuint64 bindlessHandle = glGetTextureHandleNV(textureHandle);
		glMakeTextureHandleResidentNV(bindlessHandle);
		
		bTexture.textureHandle = bindlessHandle;
		
		m_cubeMapBindlessTextures.push_back(bTexture);
		
		TextureData texture;
		texture.id = static_cast<unsigned int>(m_cubeMapIdCounter);
		texture.path = std::string(path);
		m_cubeMapIdCounter++;
		texture.textureHandle = textureHandle;
		texture.bindlessHandle = bindlessHandle;
		texture.bindlessPosition = m_2DBindlessTextures.size() - 1;
		m_cubeMapTextures.push_back(texture);
		
		id = texture.id;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_cubeMapTextureBuffer);
		GLuint64* p = (GLuint64*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_cubeMapBindlessTextures.size() * sizeof(GLuint64),
			GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
		
		memcpy(p, m_cubeMapBindlessTextures.data(), m_cubeMapBindlessTextures.size() * sizeof(GLuint64));
		
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	}

	void TextureManager::Load3DTexture(unsigned int& id, int width, int height, int depth, unsigned char* data)
	{
		GLuint textureHandle;
		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_3D, textureHandle);

		GLfloat maxAniso = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);

		//glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);

		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

		//glGenerateMipmap(GL_TEXTURE_3D);

		TextureData texture;
		texture.id = static_cast<unsigned int>(m_2DidCounter);
		m_2DidCounter++;
		texture.textureHandle = textureHandle;
		m_2DTextures.push_back(texture);

		id = texture.id;
	}

	void TextureManager::Load3DTexture(unsigned int& id, int width, int height, int depth, char* filepath)
	{
		unsigned char* data;
		int w;
		int h;
		data = stbi_load(filepath, &w, &h, 0, 0);

        if( data )
        {
		    Load3DTexture(id, width, height, depth, data);
            stbi_image_free( data );
        }
        else
        {
            LOG_FATAL << "Unable to load LUT texture." << std::endl;
        }
	}

	void TextureManager::DeleteTexture(unsigned long long int id, bool erase)
	{
		unsigned int index = FindTexture(id);

		if (index != std::numeric_limits<unsigned int>::max())
		{
			//Remove the bindless handle from memory
			glMakeTextureHandleNonResidentNV(m_2DTextures[index].bindlessHandle);
			glDeleteTextures(1, &m_2DTextures[index].textureHandle);

			if (erase)
			{
				for (int i = index; i < m_2DTextures.size(); i++)
				{
					m_2DTextures[i].bindlessPosition -= 1;
					MaterialManagerInstance().UpdateMaterialsTexture(m_2DTextures[i].id, m_2DTextures[i].bindlessPosition);
				}

			
				m_2DTextures.erase(m_2DTextures.begin() + index);
				m_2DBindlessTextures.erase(m_2DBindlessTextures.begin() + index);

				glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_2DTextureBuffer);
				GLuint64* p = (GLuint64*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_2DBindlessTextures.size() * sizeof(GLuint64),
					GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);

				memcpy(p, m_2DBindlessTextures.data(), m_2DBindlessTextures.size() * sizeof(GLuint64));

				glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
			}
		}
	}

	///////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	//////////////////////////////
	//////////////////////
	//////////////HERE BE DRAGONS, BEWARE///////////////
	////////////////////////////////////////////////////
	/////////////////////////////////////////////////////
	/////////////HURR DURR
    //////////////////////I FEAR NOT DRAGONS////////////////
	GLuint TextureManager::LoadFromMemory(
		unsigned char* data, const GLenum& target,
		const GLenum& internalFormat, const GLenum& format,
		const GLint& minFilter, const GLint& magFilter,
		const GLint& wrapS, const GLint& wrapT,
		int width, int height)
	{
		GLuint textureHandle;
		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_2D, textureHandle);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		return textureHandle;
	}

	GLuint TextureManager::LoadFromFile(
		const char* filepath, const GLenum& target,
		const GLenum& internalFormat, const GLenum& format,
		const GLint& minFilter, const GLint& magFilter,
		const GLint& wrapS, const GLint& wrapT)
	{
		unsigned char* data;
		GLuint textureHandle;
		int width, height;
		data = stbi_load(filepath, &width, &height, 0, 0);

		textureHandle = LoadFromMemory(data, target, internalFormat, format, minFilter, magFilter, wrapS, wrapT, width, height);

		stbi_image_free(data);

		return textureHandle;
	}

	void TextureManager::BindTexture(GLuint textureHandle, GLint uniform, GLuint position, GLenum target)
	{
		glUniform1i(uniform, position);
		glActiveTexture(GL_TEXTURE0 + position);
		glBindTexture(target, textureHandle);
	}

	void TextureManager::UnbindTexture()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}


}
