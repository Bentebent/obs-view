#ifndef SRC_GFX_TEXTURE_SPARSEBINDLESSTEXTUREARRAY_HPP
#define SRC_GFX_TEXTURE_SPARSEBINDLESSTEXTUREARRAY_HPP
/*
#include <utility/DDSLoader.hpp>
#include <GLEW/glew.h>
#include <map>
#include <queue>
#include <tuple>
#include <vector>

namespace gfx
{
	class Texture2D;
	class Texture2DContainer;
	class TextureManager;

	struct TextureAddress
	{
		GLuint64 containerHandle;
		GLfloat texturePage;
		GLint reserved;
	};

	struct DenseTextureAddress
	{
		GLuint containerIndex;
		GLfloat layer;
		GLint reserved[2];
	};

	class Texture2DContainer
	{
	public:
		Texture2DContainer(bool sparse, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei slices);
		~Texture2DContainer();
		GLsizei HasRoom() const;
		GLsizei VirtualAlloc();
		void VirtualFree(GLsizei slice);

		void Commit(Texture2D* texture);
		void Free(Texture2D* texture);

		void CompressedTexSubImage3D(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid * data);

		// Returns the handle to this container.
		GLuint64 GetHandle() const { return m_handle; }
		GLuint GetTextureID() const { return m_textureID; }
	private:
		GLuint64 m_handle;
		GLuint m_textureID;
		std::queue<GLsizei> m_freeList;

		const GLsizei m_width;
		const GLsizei m_height;
		const GLsizei m_levels;
		const GLsizei m_slices;
		GLsizei m_XTileSize;
		GLsizei m_YTileSize;

		void ChangeCommitment(GLsizei slice, GLboolean commit);
	};

	class Texture2D
	{
	public:
		Texture2D(Texture2DContainer* container, GLsizei sliceNum);
		~Texture2D();
		void Commit();
		void Free();

		void CompressedTexSubImage2D(GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid* data);

		const Texture2DContainer* GetTexture2DContainer() const { return m_container; }
		inline GLsizei GetSliceNum() const { return (GLsizei)m_sliceNum; }

		TextureAddress GetAddress() const
		{
			TextureAddress ta = { m_container->GetHandle(), m_sliceNum };
			return ta;
		}

		GLuint GetTextureID() const { return m_container->GetTextureID(); }

	private:
		Texture2DContainer* m_container;

		// Kept as a float because that's how we need to write the uniform, and that should be done
		// much more often than changing data or adjusting residency.
		GLfloat m_sliceNum;
	};

	class TextureManager
	{
	public:
		TextureManager();
		~TextureManager();

		Texture2D* NewTexture2D(GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
		Texture2D* NewTexture2DFromDetails(const TextureDetails* textureDetails);

		void Free(Texture2D* texture);

		// maxNumTextures <= 0 will cause allocation of maximum number of layers
		bool Initialize(bool sparse = true, GLsizei maxNumTextures = -1);
		void Shutdown();
	private:

		std::map<std::tuple<GLsizei, GLenum, GLsizei, GLsizei>, std::vector<Texture2DContainer*>> m_textureArrays2D;
		GLsizei m_maxTextureArrayLevels;
		bool m_initialized;
		bool m_sparse;

		Texture2D* AllocTexture2D(GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
	};

}
*/
#endif