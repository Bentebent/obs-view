#include "SparseBindlessTextureArray.hpp"
#include <algorithm>
#include <cassert>
/*
namespace gfx
{
	Texture2D::Texture2D(Texture2DContainer* container, GLsizei sliceNum)
	{
		m_container = container;
		m_sliceNum = (GLfloat)sliceNum;
	}

	Texture2D::~Texture2D()
	{
		Free();
		m_container->VirtualFree(GLsizei(m_sliceNum));
	}

	void Texture2D::Commit()
	{
		m_container->Commit(this);
	}

	void Texture2D::Free()
	{
		m_container->Free(this);

	}

	void Texture2D::CompressedTexSubImage2D(GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid* data)
	{
		m_container->CompressedTexSubImage3D(level, xoffset, yoffset, GetSliceNum(), width, height, 1, format, imageSize, data);
	}


	Texture2DContainer::Texture2DContainer(bool sparse, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei slices)
		: m_handle(0)
		, m_width(width)
		, m_height(height)
		, m_levels(levels)
		, m_slices(slices)
		, m_XTileSize(0)
		, m_YTileSize(0)
	{
		glGenTextures(1, &m_textureID);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_textureID);

		if (sparse) 
		{
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_SPARSE_ARB, GL_TRUE);

			// TODO: This could be done once per internal format. For now, just do it every time.
			GLint indexCount = 0,
				xSize = 0,
				ySize = 0,
				zSize = 0;

			GLint bestIndex = -1,
				bestXSize = 0,
				bestYSize = 0;

			glGetInternalformativ(GL_TEXTURE_2D_ARRAY, internalformat, GL_NUM_VIRTUAL_PAGE_SIZES_ARB, 1, &indexCount);
			for (GLint i = 0; i < indexCount; ++i) 
			{
				glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_VIRTUAL_PAGE_SIZE_INDEX_ARB, i);
				glGetInternalformativ(GL_TEXTURE_2D_ARRAY, internalformat, GL_VIRTUAL_PAGE_SIZE_X_ARB, 1, &xSize);
				glGetInternalformativ(GL_TEXTURE_2D_ARRAY, internalformat, GL_VIRTUAL_PAGE_SIZE_Y_ARB, 1, &ySize);
				glGetInternalformativ(GL_TEXTURE_2D_ARRAY, internalformat, GL_VIRTUAL_PAGE_SIZE_Z_ARB, 1, &zSize);

				// For our purposes, the "best" format is the one that winds up with Z=1 and the largest x and y sizes.
				if (zSize == 1) {
					if (xSize >= bestXSize && ySize >= bestYSize) {
						bestIndex = i;
						bestXSize = xSize;
						bestYSize = ySize;
					}
				}
			}

			// This would mean the implementation has no valid sizes for us, or that this format doesn't actually support sparse
			// texture allocation. Need to implement the fallback. TODO: Implement that.
			assert(bestIndex != -1);

			m_XTileSize = bestXSize;

			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_VIRTUAL_PAGE_SIZE_INDEX_ARB, bestIndex);
		}

		// We've set all the necessary parameters, now it's time to create the sparse texture.
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, levels, internalformat, width, height, m_slices);
		for (GLsizei i = 0; i < m_slices; ++i) 
		{
			m_freeList.push(i);
		}

		if (sparse) 
		{
			m_handle = glGetTextureHandleARB(m_textureID);
			assert(m_handle != 0);
			glMakeTextureHandleResidentARB(m_handle);
		}
	}

	// ------------------------------------------------------------------------------------------------
	Texture2DContainer::~Texture2DContainer()
	{
		// If this fires, it means there was a texture leaked somewhere.
		assert(m_freeList.size() == m_slices);

		if (m_handle != 0) 
		{
			glMakeTextureHandleNonResidentARB(m_handle);
			m_handle = 0;
		}
		glDeleteTextures(1, &m_textureID);
	}

	// ------------------------------------------------------------------------------------------------
	GLsizei Texture2DContainer::HasRoom() const
	{
		return m_freeList.size() > 0;
	}

	// ------------------------------------------------------------------------------------------------
	GLsizei Texture2DContainer::VirtualAlloc()
	{
		GLsizei retVal = m_freeList.front();
		m_freeList.pop();
		return retVal;
	}

	// ------------------------------------------------------------------------------------------------
	void Texture2DContainer::VirtualFree(GLsizei slice)
	{
		m_freeList.push(slice);
	}

	// ------------------------------------------------------------------------------------------------
	void Texture2DContainer::Commit(Texture2D* texture)
	{
		assert(texture->GetTexture2DContainer() == this);

		ChangeCommitment(texture->GetSliceNum(), GL_TRUE);
	}

	// ------------------------------------------------------------------------------------------------
	void Texture2DContainer::Free(Texture2D* texture)
	{
		assert(texture->GetTexture2DContainer() == this);
		ChangeCommitment(texture->GetSliceNum(), GL_FALSE);
	}

	// ------------------------------------------------------------------------------------------------
	void Texture2DContainer::CompressedTexSubImage3D(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid * data)
	{
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_textureID);
		glCompressedTexSubImage3D(GL_TEXTURE_2D_ARRAY, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
	}

	// ------------------------------------------------------------------------------------------------
	void Texture2DContainer::ChangeCommitment(GLsizei slice, GLboolean commit)
	{
		if (glTexturePageCommitmentEXT == nullptr) 
			return;

		GLenum err = glGetError();

		GLsizei levelWidth = m_width;
		GLsizei	levelHeight = m_height;

		for (int level = 0; level < m_levels; ++level) 
		{
			//THERE SHOULD BE A TARGET HERE
			//FUCKING OPENGL SHIT CANT FOLLOW SPEC FUCKING SHITLORDS OMG I HATE IT SO MUCH
			glTexturePageCommitmentEXT(m_textureID, GL_TEXTURE_2D_ARRAY, level, 0, 0, slice, levelWidth, levelHeight, 1, commit);
			levelWidth = std::max(levelWidth / 2, 1);
			levelHeight = std::max(levelHeight / 2, 1);
		}

		err = glGetError();
	}


	TextureManager::TextureManager()
	{
		m_initialized = false;
		m_maxTextureArrayLevels = 0;
	}

	TextureManager::~TextureManager()
	{
		assert(m_initialized == false);
	}

	Texture2D* TextureManager::NewTexture2D(GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height)
	{
		assert(m_initialized);
		Texture2D* retTex = AllocTexture2D(levels, internalformat, width, height);
		retTex->Commit();

		return retTex;
	}

	Texture2D* TextureManager::NewTexture2DFromDetails(const TextureDetails* textureDetails)
	{
		Texture2D* retTex = AllocTexture2D(textureDetails->szMipMapCount, textureDetails->glFormat, textureDetails->dwWidth, textureDetails->dwHeight);
		retTex->Commit();
		
		size_t offset = 0;
		for (int mip = 0; mip < textureDetails->szMipMapCount; ++mip) 
		{
			retTex->CompressedTexSubImage2D(mip, 0, 0, textureDetails->MipMapWidth(mip), textureDetails->MipMapHeight(mip), 
				textureDetails->glFormat, textureDetails->pSizes[mip], (char*)textureDetails->pPixels + offset);
			offset += textureDetails->pSizes[mip];
		}

		return retTex;
	}

	void TextureManager::Free(Texture2D* texture)
	{
		texture->Free();
	}

	// maxNumTextures <= 0 will cause allocation of maximum number of layers
	bool TextureManager::Initialize(bool sparse, GLsizei maxNumTextures)
	{
		assert(m_initialized == false);

		m_maxTextureArrayLevels = maxNumTextures;
		m_sparse = sparse;

		if (maxNumTextures <= 0) 
		{
			if (sparse) 
			{
				glGetIntegerv(GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_ARB, &m_maxTextureArrayLevels);
			}
			else 
			{
				glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &m_maxTextureArrayLevels);
			}
		}

		m_initialized = true;
		return true;
	}

	void TextureManager::Shutdown()
	{
		for (auto containIt = m_textureArrays2D.begin(); containIt != m_textureArrays2D.end(); ++containIt)
		{
			for (auto ptrIt = containIt->second.begin(); ptrIt != containIt->second.end(); ++ptrIt) 
			{
				delete *ptrIt;
			}
		}

		m_textureArrays2D.clear();

		m_initialized = false;
	}

	Texture2D* TextureManager::AllocTexture2D(GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height)
	{
		Texture2DContainer* memArray = nullptr;

		auto texType = std::make_tuple(levels, internalformat, width, height);
		auto arrayIt = m_textureArrays2D.find(texType);
		if (arrayIt == m_textureArrays2D.end())
		{
			m_textureArrays2D[texType] = std::vector<Texture2DContainer*>();
			arrayIt = m_textureArrays2D.find(texType);
			assert(arrayIt != m_textureArrays2D.end());
		}

		for (auto it = arrayIt->second.begin(); it != arrayIt->second.end(); ++it) 
		{
			if ((*it)->HasRoom()) 
			{
				memArray = (*it);
				break;
			}
		}

		if (memArray == nullptr) 
		{
			memArray = new Texture2DContainer(m_sparse, levels, internalformat, width, height, m_maxTextureArrayLevels);
			arrayIt->second.push_back(memArray);
		}

		assert(memArray);
		return new Texture2D(memArray, memArray->VirtualAlloc());
	}
	
}*/