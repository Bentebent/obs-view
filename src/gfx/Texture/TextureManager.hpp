#ifndef SRC_GFX_TEXTURE_TEXTURE_MANAGER_HPP
#define SRC_GFX_TEXTURE_TEXTURE_MANAGER_HPP

#define STBI_HEADER_FILE_ONLY
#include <GLEW/glew.h>
#include <vector>
#include <limits>
#include <string> 

//#include <utility/stb_image.cpp>
#include <utility/stb_image.hpp>
#include "TextureData.hpp"

namespace gfx
{
	class TextureManager
	{
	public:
		friend TextureManager& TextureManagerInstance();

		static GLuint LoadFromMemory(
			unsigned char* data, const GLenum& target,
			const GLenum& internalFormat, const GLenum& format,
			const GLint& minFilter, const GLint& magFilter,
			const GLint& wrapS, const GLint& wrapT,
			int width, int height);

		static GLuint LoadFromFile(
			const char* filepath, const GLenum& target,
			const GLenum& internalFormat, const GLenum& format,
			const GLint& minFilter, const GLint& magFilter,
			const GLint& wrapS, const GLint& wrapT);

		static void BindTexture(GLuint textureHandle, GLint uniform, GLuint position, GLenum target);
		static void UnbindTexture();

		void Initialize();

	
		void LoadTexture(unsigned int& id, const char* path, unsigned char* data, int width, int height, bool decal, bool replace = false);
		
		void LoadTexture(unsigned int& id, unsigned char* data, const GLenum& target,
			const GLenum& internalFormat, const GLenum& format,
			const GLint& minFilter, const GLint& magFilter,
			const GLint& wrapS, const GLint& wrapT,
			int width, int height, const char* path);

		void ReplaceTexture(unsigned int& id, unsigned char* data, const GLenum& target,
			const GLenum& internalFormat, const GLenum& format,
			const GLint& minFilter, const GLint& magFilter,
			const GLint& wrapS, const GLint& wrapT,
			int width, int height, const char* path);

		void LoadCubemap(unsigned int& id, const char* path, unsigned char* posX, unsigned char* negX, unsigned char* posY, unsigned char* negY, unsigned char* posZ, unsigned char* negZ, int width, int height);

		void Load3DTexture(unsigned int& id, int width, int height, int depth, unsigned char* data);

		void Load3DTexture(unsigned int& id, int width, int height, int depth, char* filepath);

		void DeleteTexture(unsigned long long int id, bool erase = true);

		inline TextureData GetTexture(const unsigned long long int& textureID) 
		{ 
			unsigned int index = FindTexture(textureID);

			if (index != std::numeric_limits<unsigned int>::max())
				return m_2DTextures[index];

			TextureData t;
			t.id = std::numeric_limits<unsigned int>::max();
			t.textureHandle = std::numeric_limits<unsigned int>::max();

			return t;
		}

		inline TextureData GetCubeMapTexture(const unsigned long long int& textureID)
		{
			unsigned int index = FindTexture(textureID);

			if (index != std::numeric_limits<unsigned int>::max())
				return m_cubeMapTextures[index];

			TextureData t;
			t.id = std::numeric_limits<unsigned int>::max();
			t.textureHandle = std::numeric_limits<unsigned int>::max();

			return t;
		}

	private:
		TextureManager();
		~TextureManager();

		GLuint m_2DTextureBuffer;

		std::vector<TextureData> m_2DTextures;
		std::vector<BindlessTextureData> m_2DBindlessTextures;
		unsigned long long int m_2DidCounter;

		GLuint m_cubeMapTextureBuffer;

		std::vector<TextureData> m_cubeMapTextures;
		std::vector<BindlessTextureData> m_cubeMapBindlessTextures;
		unsigned long long int m_cubeMapIdCounter;

		inline const unsigned int FindTexture(unsigned long long id)
		{
			for (unsigned int i = 0; i < m_2DTextures.size(); i++)
				if (m_2DTextures[i].id == static_cast<unsigned int>(id))
					return i;

			return std::numeric_limits<unsigned int>::max();
		}

		inline const unsigned int FindCubeMapTexture(unsigned long long id)
		{
			for (unsigned int i = 0; i < m_cubeMapTextures.size(); i++)
			if (m_cubeMapTextures[i].id == static_cast<unsigned int>(id))
				return i;

			return std::numeric_limits<unsigned int>::max();
		}
	};

	TextureManager& TextureManagerInstance();
}

#endif 
