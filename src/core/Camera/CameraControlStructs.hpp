#ifndef SRC_CORE_CAMERA_CONTROL_STRUCTS_HPP
#define SRC_CORE_CAMERA_CONTROL_STRUCTS_HPP


#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace obs
{
	class Camera;

	enum CameraControlType
	{
		FREE_FLYING,
		FPS_CAMERA,
	};

	struct FreeFlyingCameraController
	{
		void Update( Camera* camera, float deltaTime );
	};

	struct FpsCameraController
	{
		float offset[3];
		float up[3];
		int parentEntity;

		void Update( Camera* camera, float deltaTime );
	};

	union Controller
	{
		FreeFlyingCameraController freeCamera;
		FpsCameraController fpsCamera;
	};


	struct CameraController
	{
		CameraController()
		{
			type = CameraControlType::FREE_FLYING;
		}

		void Update( Camera* camera, float deltaTime )
		{
			if( type == CameraControlType::FREE_FLYING )
			{
				controller.freeCamera.Update( camera, deltaTime );
			}
			else
			{
				controller.fpsCamera.Update( camera, deltaTime );
			}
		}
		
		Controller controller;
		CameraControlType type;
	};
}

#endif
