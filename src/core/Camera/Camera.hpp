#ifndef SRC_CORE_CAMERA_CAMERA_HPP
#define SRC_CORE_CAMERA_CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <SDL/SDL.h>

#include <Camera/CameraControlStructs.hpp>

namespace obs
{
	

	class Camera
	{
	public:
		Camera(int width, int height);
		~Camera();

		void LookAt(glm::vec3 target, glm::vec3 up);
		void Move(float dt, const Uint8* state, int relMouseX, int relMouseY);
		glm::vec3 Move(float dt, int relMouseX, int relMouseY, glm::vec3 up, glm::vec3 currentPos );

		inline void SetViewMatrix(const glm::mat4& view) { m_viewMatrix = view; }
		inline void SetProjectionMatrix(const glm::mat4& proj) { m_projectionMatrix = proj; }

		inline void SetPosition(const glm::vec3& position) { m_position = position; }
		inline void SetForward(const glm::vec3& forward) { m_forward = forward; }

		inline void SetNear(const float& nearZ) { m_near = nearZ;}
		inline void SetFar(const float& farZ) { m_far = farZ; }
		inline void SetFOV(const float& fov) { m_fov = fov; }

		inline void SetWidth(const int& width) { m_width = width; }
		inline void SetHeight(const int& height) { m_height = height; }

		inline const glm::mat4& GetProjectionMatrix() { return m_projectionMatrix; }
		inline const glm::mat4& GetViewMatrix() { return m_viewMatrix; }

		inline const glm::vec3 GetForward() { return m_forward; }
		inline const glm::vec3 GetPosition() { return m_position; }
		
		inline const float GetNear() { return m_near; }
		inline const float GetFar() { return m_far; }
		inline const float GetFOV() { return m_fov; }

		inline void RebuildProjection(){ m_projectionMatrix = glm::perspective(m_fov, (float)m_width / (float)m_height, m_near, m_far); }

		void Update( float deltaTime );
		void MakeFreeFlying();
		void MakeFpsCamera( int parentObject, glm::vec3 offset, glm::vec3 up );

	private:

		glm::vec3 m_position;
		glm::quat m_orientation;
		glm::vec3 m_forward;
		glm::vec3 m_up;
		glm::vec3 m_right;

		glm::mat4x4 m_viewMatrix;
		glm::mat4x4 m_projectionMatrix;

		float m_near;
		float m_far;
		float m_fov;

		float m_heading;

		int m_width;
		int m_height;

		float m_speed;

		CameraController m_controller;
	};
}

#endif

/*


enum CameraBehaviour
{
CAMERA_BEHAVIOR_FIRST_PERSON,
CAMERA_BEHAVIOR_SPECTATOR,
CAMERA_BEHAVIOR_FLIGHT,
CAMERA_BEHAVIOR_ORBIT
};

Camera();
~Camera();

void LookAt(const glm::vec3& target);
void LookAt(const glm::vec3& eye, const glm::vec3& target, const glm::vec3& up);
void Move(float dx, float dy, float dz);
void Move(const glm::vec3& direction, const glm::vec3& amount);
void Perspective(float fovx, float aspect, float znear, float zfar);
void Rotate(float headingDegrees, float pitchDegrees, float rollDegrees);
void RotateSmoothly(float headingDegrees, float pitchDegrees, float rollDegrees);
void UndoRoll();
void UpdatePosition(const glm::vec3 &direction, float elapsedTimeSec);
void Zoom(float zoom, float minZoom, float maxZoom);

const glm::vec3& GetAcceleration() const;
CameraBehaviour GetBehavior() const;
const glm::vec3& GetCurrentVelocity() const;
const glm::vec3& GetPosition() const;
float GetOrbitMinZoom() const;
float GetOrbitMaxZoom() const;
float GetOrbitOffsetDistance() const;
float GetOrbitPitchMaxDegrees() const;
float GetOrbitPitchMinDegrees() const;
const glm::quat &GetOrientation() const;
float GetRotationSpeed() const;
const glm::mat4& GetProjectionMatrix() const;
const glm::vec3& GetVelocity() const;
const glm::vec3& GetViewDirection() const;
const glm::mat4 &GetViewMatrix() const;
const glm::mat4 &GetViewProjectionMatrix() const;
const glm::vec3 &GetXAxis() const;
const glm::vec3 &GetYAxis() const;
const glm::vec3 &GetZAxis() const;
bool preferTargetYAxisOrbiting() const;

*/