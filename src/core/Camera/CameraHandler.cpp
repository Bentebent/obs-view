#include "CameraHandler.hpp"




obs::CameraHandler::CameraHandler()
{
}


obs::CameraHandler::~CameraHandler()
{
}


void obs::CameraHandler::Initialize()
{
	m_activeCamera = m_cameras.size();
	m_cameras.push_back( Camera( 100, 100 ) );
	m_lockedInterface = false;
}


int obs::CameraHandler::CreateCamera()
{
	if( m_lockedInterface )
	{
		return -1;
	}

	if( m_openList.size() != 0 )
	{
		int index = m_openList.back();
		m_openList.pop_back();
		return index;
	}
	else
	{
		m_cameras.push_back( Camera( 100, 100 ) );
		return m_cameras.size() - 1;
	}
}


obs::Camera* obs::CameraHandler::GetCamera( int index )
{

	if( index >= 0 && index < m_cameras.size() && !m_lockedInterface )
		return &m_cameras[ index ];
	return nullptr;
}


obs::Camera* obs::CameraHandler::GetActiveCamera()
{
	if( m_activeCamera < m_cameras.size() )
		return &m_cameras[ m_activeCamera ];
	return nullptr;
}


void obs::CameraHandler::SetActiveCamera( int index )
{
	if( index >= 0 && index < m_cameras.size() && !m_lockedInterface )
		m_activeCamera = index;
}


std::vector< obs::Camera >* obs::CameraHandler::GetCameras()
{
	return &m_cameras;
}


void obs::CameraHandler::RemoveCamera( int index )
{
	if( !m_lockedInterface )
		m_openList.push_back( index );
}


void obs::CameraHandler::RemoveCameras()
{
	if( m_lockedInterface )
	{
		return;
	}

	m_activeCamera = -1;
	m_cameras.clear();
}

int obs::CameraHandler::GetActiveCameraIndex()
{
	return m_activeCamera;
}

void obs::CameraHandler::LockCameras( bool locked )
{
	m_lockedInterface = locked;
}


#include <CoreFramework/Core.hpp>
#include <bullet/btBulletDynamicsCommon.h>


void obs::CameraHandler::MoveCameraWithMouse( int index, float dt, int relMouseX, int relMouseY, glm::vec3 up, int parentObjectId, glm::vec3 offset )
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);

	if( m_lockedInterface )
	{ return; }

	if( index >= 0 && index < m_cameras.size() && !m_lockedInterface )
	{
		obs::TransformationComponent* tc = GET_DATA< obs::TransformationComponent >( parentObjectId );
		obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( parentObjectId );

		if( pc != nullptr )
		{
			glm::vec3 movement = m_cameras[ index ].Move( dt, relMouseX, relMouseY, up, tc->GetPositionVec3( *tc ) + offset );

			if( pc->shapeIndex >= 0 )
			{
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );

				btVector3 vel = body->getLinearVelocity();
				body->setLinearVelocity( btVector3( movement.x, -9.82f, movement.z ) );

				if (state[SDL_SCANCODE_SPACE])
				{
					//body->applyImpulse( btVector3( 0, 1, 0 ), btVector3( 0,0,0 ) );
				}		
			}
		}
	}
}