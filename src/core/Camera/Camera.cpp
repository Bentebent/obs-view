#include "Camera.hpp"
#include <SDL/SDL.h>
#include <iostream>

#include <CoreFramework/Core.hpp>

//THIS CAMERA WAS STOLEN AT
//https://github.com/pabennett/glblox/blob/master/lib/camera.cpp

namespace obs
{
	Camera::Camera(int width, int height)
	{
		m_position = glm::vec3(0.0f, 0.0f, -10.0f);
		m_orientation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
		m_forward = glm::vec3(0.0f, 0.0f, -1.0f);
		m_right = glm::vec3(1.0f, 0.0f, 0.0f);
		m_up = glm::vec3(0.0f, 1.0f, 0.0f);

		m_fov = 45.0f;
		m_far = 1000.0f;	
		m_near = 0.01f;

		m_speed = 15.0f;

		m_width = width;
		m_height = height;
		m_viewMatrix = glm::toMat4(m_orientation);
		m_viewMatrix = glm::translate(m_viewMatrix, m_position);
		m_projectionMatrix = glm::perspective(m_fov, (float)width / (float)height, m_near, m_far);

	}

	Camera::~Camera()
	{

	}

	void Camera::LookAt(glm::vec3 target, glm::vec3 up)
	{
		m_viewMatrix = glm::lookAt(m_position, target, up);

		m_right = glm::vec3(m_viewMatrix[0][0], m_viewMatrix[1][0], m_viewMatrix[2][0]);
		m_up = glm::vec3(m_viewMatrix[0][1], m_viewMatrix[1][1], m_viewMatrix[2][1]);
		m_forward = glm::vec3(m_viewMatrix[0][2], m_viewMatrix[1][2], m_viewMatrix[2][2]);

		m_orientation = glm::quat_cast( m_viewMatrix );
	}

	glm::vec2 smooth = glm::vec2(0.0f);
	void Camera::Move(float dt, const Uint8* state, int relMouseX, int relMouseY )
	{
		glm::fquat rotation;		
		float mouseSense = 0.05f;
		float acc = 50.0f;
		
		if (relMouseY != 0)
		{
			smooth.y += (float)relMouseY;
		}

		if (relMouseX != 0)
		{
			smooth.x += (float)relMouseX;
		}

		rotation = glm::angleAxis(smooth.y * mouseSense, glm::vec3(1.0f, 0.0f, 0.0f));
		m_orientation = rotation * m_orientation;
		m_orientation = glm::normalize(m_orientation);

		rotation = glm::angleAxis(smooth.x * mouseSense, glm::vec3(0.0f, 1.0f, 0.0f));
		m_orientation = m_orientation * rotation;
		m_orientation = glm::normalize(m_orientation);

		glm::vec2 prevSmooth = smooth;
		smooth -= glm::sign(smooth) * glm::vec2(glm::abs(smooth) * acc) * dt;
		if (glm::sign(smooth.x) != glm::sign(prevSmooth.x)) smooth.x = 0.0f;
		if (glm::sign(smooth.y) != glm::sign(prevSmooth.y)) smooth.y = 0.0f;
		

		m_viewMatrix = glm::mat4_cast(m_orientation);

		m_right = glm::vec3(m_viewMatrix[0][0], m_viewMatrix[1][0], m_viewMatrix[2][0]);
		m_up = glm::vec3(m_viewMatrix[0][1], m_viewMatrix[1][1], m_viewMatrix[2][1]);
		m_forward = glm::vec3(m_viewMatrix[0][2], m_viewMatrix[1][2], m_viewMatrix[2][2]);

		if (state[SDL_SCANCODE_W])
			m_position -= m_forward * m_speed * dt;
		if (state[SDL_SCANCODE_S])
			m_position += m_forward * m_speed * dt;
		if (state[SDL_SCANCODE_A])
			m_position -= m_right * m_speed * dt;
		if (state[SDL_SCANCODE_D])
			m_position += m_right * m_speed * dt;
		if (state[SDL_SCANCODE_SPACE])
			m_position += m_up * m_speed * dt;
		if (state[SDL_SCANCODE_LCTRL])
			m_position -= m_up * m_speed * dt;

		m_viewMatrix[3][0] = -glm::dot( m_right, m_position );
		m_viewMatrix[3][1] = -glm::dot( m_up, m_position );
		m_viewMatrix[3][2] = -glm::dot( m_forward, m_position );

	}

	glm::vec2 smooth2 = glm::vec2(0.0f);
	glm::vec3 Camera::Move(float dt, int relMouseX, int relMouseY, glm::vec3 up, glm::vec3 currentPos)
	{
		const Uint8 *state = obs::inputHandler.GetCurrentKeyStates();
		glm::fquat rotation;		
		
		float mouseSense = 0.05f;
		float acc = 50.0f;
		
		if (relMouseY != 0)
		{
			smooth2.y += (float)relMouseY;
		}

		if (relMouseX != 0)
		{
			smooth2.x += (float)relMouseX;
		}

		rotation = glm::angleAxis(smooth2.y * mouseSense, glm::vec3(1.0f, 0.0f, 0.0f));
		m_orientation = rotation * m_orientation;
		m_orientation = glm::normalize(m_orientation);

		rotation = glm::angleAxis(smooth2.x * mouseSense, glm::vec3(0.0f, 1.0f, 0.0f));
		m_orientation = m_orientation * rotation;
		m_orientation = glm::normalize(m_orientation);

		glm::vec2 prevsmooth2 = smooth2;
		smooth2 -= glm::sign(smooth2) * glm::vec2(glm::abs(smooth2) * acc) * dt;
		if (glm::sign(smooth2.x) != glm::sign(prevsmooth2.x)) smooth2.x = 0.0f;
		if (glm::sign(smooth2.y) != glm::sign(prevsmooth2.y)) smooth2.y = 0.0f;

		m_viewMatrix = glm::mat4_cast(m_orientation);

		m_right = glm::vec3(m_viewMatrix[0][0], m_viewMatrix[1][0], m_viewMatrix[2][0]);
		m_up = glm::vec3(m_viewMatrix[0][1], m_viewMatrix[1][1], m_viewMatrix[2][1]);
		m_forward = glm::vec3(m_viewMatrix[0][2], m_viewMatrix[1][2], m_viewMatrix[2][2]);

		glm::vec3 direction = glm::vec3( 0.0f );
		if (state[SDL_SCANCODE_W])
			direction -= m_forward;
		if (state[SDL_SCANCODE_S])
			direction += m_forward;
		if (state[SDL_SCANCODE_A])
			direction -= m_right;
		if (state[SDL_SCANCODE_D])
			direction += m_right;
	
		// project direction down to the plane
		if( glm::dot( direction, direction ) > 0.0f )
			direction = glm::normalize( direction );

		glm::vec3 x = glm::vec3( 1, 0, 0 );
		glm::vec3 z = glm::vec3( 0, 0, 1 );

		glm::vec3 original = direction;

		direction = (glm::dot( x, original ) / glm::dot( x, x )) * x;
		direction += (glm::dot( z, original ) / glm::dot( z, z )) * z;

		if( glm::dot( direction, direction ) > 0.0f )
			direction = glm::normalize( direction );

		m_position = currentPos;
		m_viewMatrix[3][0] = -glm::dot( m_right, m_position );
		m_viewMatrix[3][1] = -glm::dot( m_up, m_position );
		m_viewMatrix[3][2] = -glm::dot( m_forward, m_position );

		return direction * m_speed;
	}


	void Camera::Update( float deltaTime )
	{
		m_controller.Update( this, deltaTime );	
	}

	void Camera::MakeFreeFlying()
	{
		m_controller.type = CameraControlType::FREE_FLYING;
	}

	void Camera::MakeFpsCamera( int parentObject, glm::vec3 offset, glm::vec3 up )
	{
		m_controller.type = CameraControlType::FPS_CAMERA;
		m_controller.controller.fpsCamera.parentEntity = parentObject;
		std::memcpy( m_controller.controller.fpsCamera.offset, &offset[0], sizeof( glm::vec3 ) );
		std::memcpy( m_controller.controller.fpsCamera.up, &up[0], sizeof( glm::vec3 ) );
	}

}