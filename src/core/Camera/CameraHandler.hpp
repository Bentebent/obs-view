#ifndef SRC_CORE_CAMERA_HANDLER_HPP
#define SRC_CORE_CAMERA_HANDLER_HPP

#include <Camera/Camera.hpp>
#include <vector> 

namespace obs
{
	class CameraHandler
	{
	
	public:
		CameraHandler();
		~CameraHandler();

		/*!
			Adds a default camera to the handler and sets this one
			as active camera.
		*/
		void Initialize();

		/*!
			Adds or reuses an available index and returns that index as 
			an identifier for the camera.
		*/
		int CreateCamera();

		/*!
			Reterns a pointer to the camera at the supplied index.
			If index is outside range nullptr is returned instead.
		*/
		Camera* GetCamera( int index );

		/*!
			Returns the camera at the active camera index.
			If active camera is outside vector range due to removal of 
			cameras return is nullptr.
		*/
		Camera* GetActiveCamera();

		/*!
			Sets the active camera index to the supplied index.
			If index is outside range nullptr is returned instead.
		*/
		void SetActiveCamera( int index );
		
		/*!
			Returns a pointer to the vector of cameras.
		*/
		std::vector< Camera >* GetCameras();
		
		/*!
			Adds the camera slot to the open list.
		*/
		void RemoveCamera( int index );

		/*!
			Empties the list of cameras.
		*/
		void RemoveCameras();

		/*!
			returns the index of the active camera.
		*/
		int GetActiveCameraIndex();

		/*!
			Will lock the interface and return nullptr from all functions.
		*/
		void LockCameras( bool lock );

		/*
			Will move the camera at the supplied index by the mouse and keyboard. If parentObjectId is not -1 that object will be moved instead
			and the camera position merged with the supplied offset. Up direction limits the movement in that direction.
		*/
		void MoveCameraWithMouse( int index, float dt, int relMouseX, int relMouseY, glm::vec3 up, int parentObjectId, glm::vec3 offset );
		
	private:

		std::vector< Camera > m_cameras;
		std::vector< int > m_openList;
		int	m_activeCamera;

		bool m_lockedInterface;

	};
}

#endif
