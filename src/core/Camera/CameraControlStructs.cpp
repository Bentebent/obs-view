#include "CameraControlStructs.hpp"

#include <cstring>

#include <Camera/Camera.hpp>
#include <CoreFramework/Core.hpp>

#include <BulletFramework/PhysicsHandler.hpp>
#include <bullet/btBulletDynamicsCommon.h>

void obs::FreeFlyingCameraController::Update( Camera* camera, float deltaTime )
{
	const Uint8 *cameraInputs = obs::inputHandler.GetCurrentKeyStates();
	glm::vec2 mouseDelta = obs::inputHandler.GetMouseDelta();

	camera->Move( deltaTime, cameraInputs, mouseDelta.x, mouseDelta.y );
}

void obs::FpsCameraController::Update( Camera* camera, float deltaTime )
{
	obs::TransformationComponent* tc = GET_DATA< obs::TransformationComponent >( parentEntity );
	obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( parentEntity );
	glm::vec2 mouseDelta = obs::inputHandler.GetMouseRelativeCenter();

	glm::vec3 upVector;
	std::memcpy( &upVector[0], up, sizeof( glm::vec3 ) );

	glm::vec3 offsetVector;
	std::memcpy( &offsetVector[0], offset, sizeof( glm::vec3 ) );

	if( pc != nullptr )
	{
		glm::vec3 movement = camera->Move( deltaTime, mouseDelta.x, mouseDelta.y, upVector, tc->GetPositionVec3( *tc ) + offsetVector );

		if( pc->shapeIndex >= 0 )
		{
			btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );

			btVector3 vel = body->getLinearVelocity();
			body->setLinearVelocity( btVector3( movement.x, -9.82f, movement.z ) );
		}
	}
}