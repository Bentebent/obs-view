#ifndef LUA_INTERFACE_DEFINITION_HPP
#define LUA_INTERFACE_DEFINITION_HPP



#include <LuaFramework/InterfaceBindings/PhysicsBinding.hpp>
#include <LuaFramework/InterfaceBindings/RenderingBinding.hpp>
#include <LuaFramework/InterfaceBindings/TransformationBinding.hpp>
#include <LuaFramework/InterfaceBindings/LightBinding.hpp>
#include <LuaFramework/InterfaceBindings/CameraBinding.hpp>
#include <LuaFramework/InterfaceBindings/DataHandlerBinding.hpp>
#include <LuaFramework/InterfaceBindings/InputBinding.hpp>
#include <LuaFramework/InterfaceBindings/PickingBinding.hpp>
#include <LuaFramework/InterfaceBindings/SceneGraphBinding.hpp>

struct lua_State;

namespace obs
{
	struct LuaInterfaceDefinition
	{
		LuaInterfaceDefinition( lua_State* L ) :

			renderingBinding( L ),
			physicsBinding( L ),
			transformationBinding( L ),
			lightBinding( L ),
			cameraBinding( L ),
			dataHandlerBinding( L ),
			inputBinding( L ),
			pickingBinding( L ),
			sceneGraphBinding( L )

		{ 
		}

		RenderingBinding renderingBinding;
		PhysicsBinding physicsBinding;
		TransformationBinding transformationBinding;
		LightBinding lightBinding;
		CameraBinding cameraBinding;
		DataHandlerBinding dataHandlerBinding;
		InputBinding inputBinding;
		PickingBinding pickingBinding;
		SceneGraphBinding sceneGraphBinding;
	};
}

#endif


