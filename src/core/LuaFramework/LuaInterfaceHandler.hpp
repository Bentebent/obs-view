#ifndef LUA_INTERFACE_HANDLER_HPP
#define LUA_INTERFACE_HANDLER_HPP

#include <lua/lua.hpp>
#include <CoreFramework/SystemHandler/SystemHandlerTimeStruct.hpp>

#include <string>

#define LUA_INTERFACE_NAME "eli"
#define LUA_INPUT_TABLE_NAME "input"

namespace obs
{
	class LuaInterfaceHandler
	{
	private:
		void InitializeInterface();
	
	public:
		LuaInterfaceHandler();
		~LuaInterfaceHandler();

		/*
			Will initialize the lua state.
			- Returns true if successfull.
		*/
		bool Initialize();

		/*
			Will run the script, attempt to call the scripts initialize function, and check if "Update" function is eligable.
			- Returns true if successfull.
		*/
		bool LoadFile( std::string file );

		/*
			Will call the loaded "Update" if present.
		*/
		void Update( obs::SystemHandlerTimeStruct time );

		/*
			Will close the lua state, re-initialize it and run the latest run script.
		*/
		void ReloadAll();

		/*
			Will close the lua_State.
		*/
		void Close();

	private:
		lua_State* m_state;

		/*
			The latest loaded script run by LoadFile.
		*/
		std::string m_currentScript;
	};
}

#endif