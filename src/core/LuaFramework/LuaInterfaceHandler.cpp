#include "LuaInterfaceHandler.hpp"

#include <LuaFramework/LuaInterfaceDefiniton.hpp>
#include <logger/Logger.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>


obs::LuaInterfaceHandler::LuaInterfaceHandler()
{
	m_state = nullptr;
	m_currentScript = "";
}

obs::LuaInterfaceHandler::~LuaInterfaceHandler()
{
	if( m_state )
	{
		lua_close( m_state );
	}
}

bool obs::LuaInterfaceHandler::Initialize()
{
	m_state = luaL_newstate();
	if( m_state == nullptr )
		return false;

	luaL_openlibs( m_state );
	if( m_state == nullptr )
		return false;

	InitializeInterface();

	return true;
}

bool CheckLuaError(lua_State *L, int status)
{
	if (status != 0) 
	{
		LOG_ERROR << "Lua -- "<< lua_tostring(L, -1) << std::endl;
		lua_pop(L, 1);
		return true;
	}
	return false;
}


bool obs::LuaInterfaceHandler::LoadFile( std::string file )
{
	m_currentScript = file;

	int result = luaL_loadfile( m_state, file.c_str() );
	if( CheckLuaError( m_state, result ) )
	{
		return false;
	}

	result = lua_pcall( m_state, 0, 0, 0);
	if( CheckLuaError( m_state, result ) )
	{
		return false;
	}

	lua_getglobal( m_state, "Init" );
	result = lua_pcall( m_state, 0, 0, 0);
	if( CheckLuaError( m_state, result ) )
	{
		LOG_DEBUG << "Failed to run \"Init\" function for script: " << file << std::endl;
	}

	return true;
}

void obs::LuaInterfaceHandler::Update( obs::SystemHandlerTimeStruct time )
{
	lua_getglobal( m_state, "Update" );
	lua_pushnumber( m_state, time.delta );
	lua_pushnumber( m_state, time.realtimeDelta );
	int result = lua_pcall( m_state, 2, 0, 0);
}



void obs::LuaInterfaceHandler::InitializeInterface()
{
	int errorCheck = lua_gettop( m_state );

	// create interface table...
	lua_newtable( m_state );
	lua_setglobal( m_state, LUA_INTERFACE_NAME );

	if( errorCheck != lua_gettop( m_state ) )
	{
		LOG_ERROR << "Could not initialize lua interface" << std::endl;
		return;
	}
	
	// initialize the lua-engine api...
	LuaInterfaceDefinition initializer( m_state );

#ifdef GENERATE_LUA_API_FILE
	LuaHtmlApiGenerator::GenerateHtml();
#endif

	int o = 0;
}

void obs::LuaInterfaceHandler::Close()
{
	lua_close( m_state );
	m_state = nullptr;
}

void obs::LuaInterfaceHandler::ReloadAll()
{
	Close();

	Initialize();

	if( m_currentScript.size() != 0 )
	{
		LoadFile( m_currentScript );
	}
}