#include "LuaHtmlGenerator.hpp"

#include <logger/Logger.hpp>

#include <fstream>
#include <algorithm>

std::vector< LuaApiInfo > LuaHtmlApiGenerator::info;

#define HTML_FILE_NAME "luaApi.html"


void LuaHtmlApiGenerator::GenerateHtml()
{
	// open and empty the target file...
	std::fstream ff;
	ff.open( HTML_FILE_NAME, std::ios::out | std::ios::trunc );
	if( ff.is_open() != true )
	{
		LOG_ERROR << "Could not open file " << HTML_FILE_NAME << " for writing. Lua api page will not be generated." << std::endl;
		ResetGenerator();
	}

	// sort the entries...
	std::sort( info.begin(), info.end(), []( const LuaApiInfo& A, const LuaApiInfo& B ) 
	{
		if( A.category > B.category )
		{
			return true;
		}
		return false;
	} );

	// generate html...
	ff << 
	"<!DOCTYPE html>" << 
	"<html>" << 
	"<head>" << 
	"<style>" << 

	"table, td, th {" << 
	"    border-top: 1px solid gray;" << 
	"    border-bottom: 1px solid gray;" << 
	"    border-collapse:collapse;" << 
	"	 padding-right: 25px;" << 
	"    padding-bottom: 10px;" <<
    "    vertical-align: top;" << 
	"}" << 
	
	"th {" << 
	"    background-color: gray;" << 
	"    color: white;" << 
	"}" << 

	"tr:nth-child(even) {" <<
		"background-color: #EEEEEE;" <<
	"}" <<
	
	"</style>" <<
	"</head>" << 
	"<body>" << std::endl;

	ff << "<table>" << std::endl;

	//"    width: 150px;" <<

	// help macros...
#define th( width ) ff << "<th width=\"" <<  width << "\">" << std::endl; //ff << "<th>" << std::endl;
#define th_end ff << "</th>" << std::endl;
#define tr ff << "<tr>" << std::endl;
#define tr_end ff << "</tr>" << std::endl;
#define td ff << "<td>" << std::endl;
#define td_end ff << "</td>" << std::endl;

	tr
		th( 210 ) ff << "Name" << std::endl; th_end
		th( 200 ) ff << "Parameter info" << std::endl; th_end
		th( 180 ) ff << "Return info" << std::endl; th_end
		th( 500 ) ff << "Description" << std::endl; th_end
	tr_end

	for( int i = 0; i < info.size(); i++ )
	{
		tr
			td 
				ff << info[i].name << std::endl;
			td_end
			td 
				ff << info[i].parameterDescription << std::endl;
			td_end

			td 
				ff << info[i].returnDescription << std::endl;
			td_end

			td 
				ff << info[i].description << std::endl;
			td_end
		tr_end
	}	

	
	
	ff << "</table>" << std::endl;
	ff << "</body>" << std::endl;
	ff << "</html>" << std::endl;

	// finished...
	ff.close();
	ResetGenerator();
}





void LuaHtmlApiGenerator::ResetGenerator()
{
	info.clear();
	categoryCounter = 0;
}