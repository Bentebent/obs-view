#ifndef LUA_CONTENT_UTILITY_HPP
#define LUA_CONTENT_UTILITY_HPP

#include <CoreFramework/Core.hpp>

#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>


/*
	Attempts to delete 1x reference to the mesh contained in the bitmask.
	Bitmask does not have to have a mesh and the component can be nullptr.
	Will set the data to invalid number.
*/
static void LuaFreeMesh( obs::RenderingComponent* rc )
{
	if( rc )
	{
		unsigned int value = obs::bitmaskSystem.Decode( rc->bitmask, MaskType::MESH );
		if( value != 0 )
			obs::contentManager.FreeFromValue( value, "__luaMeshFree" );
		obs::bitmaskSystem.Encode( rc->bitmask, 0, MaskType::MESH );
	}
}

/*
	Attempts to delete 1x reference to the material contained in the bitmask.
	Bitmask does not have to have a material and the component can be nullptr.
	Will set the data to invalid number.
*/
static void LuaFreeMaterial( obs::RenderingComponent* rc )
{
	if( rc )
	{
		unsigned int value = obs::bitmaskSystem.Decode( rc->bitmask, MaskType::TEXTURE );
		if( value != 0 )
			obs::contentManager.FreeFromValue( value, "__luaMaterialFree" );
		obs::bitmaskSystem.Encode( rc->bitmask, 0, MaskType::TEXTURE );
	}
}

/*
	Attempt to delete the data allocated for the physics shape and deregister it from the physics system.
	Component do not have to hava a physics shape and the component can be nullptr.
	Will set the data to invalid number.
*/
static void LuaFreePhysicsShape( obs::PhysicsComponent* pc )
{
	if( pc != nullptr )
	{
		obs::physicsHandler.DestoryShape( pc->shapeIndex );
		pc->shapeIndex = -1;
	}
}

#endif
