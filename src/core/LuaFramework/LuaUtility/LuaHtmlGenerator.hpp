#ifndef LUA_GENERATE_HTML_HPP
#define LUA_GENERATE_HTML_HPP

#ifndef GENERATE_LUA_API_FILE
#define LUA_ADD_FUNCTION_TO_INTERFACE( L, name, function, category, stringAboutType, description ) luau_setfunction( L, "setPosition", SetPosition );
#define LUA_NEW_HTML_CATEGORY ;
#else

#include <vector>
#include <string>

/*
	Description needed.
*/
#define LUA_NEW_HTML_CATEGORY categoryCounter++;
#define LUA_ADD_FUNCTION_TO_INTERFACE( L, name, function, paramDescription, returnDescription, description ) \
										luau_setfunction( L, name, function ); LuaHtmlApiGenerator::info.push_back( LuaApiInfo( name, paramDescription, returnDescription, description ) )

/*
	Description needed.
*/
static int categoryCounter = 0;

/*
	Description needed.
*/
struct LuaApiInfo
{
	LuaApiInfo( std::string nameOfCall, std::string parameterInfo_, std::string returnDescription_ ,std::string description_ )
		: name( nameOfCall ), parameterDescription( parameterInfo_ ), returnDescription( returnDescription_ ), description( description_ )
	{
		category = categoryCounter;
	}

	std::string name;
	std::string parameterDescription;
	std::string returnDescription;
	std::string description;
	int category;
};

/*
	Description needed.
*/
class LuaHtmlApiGenerator
{
private:
	/*
		Description needed.
	*/
	static void ResetGenerator();

public:

	/*
		Description needed.
	*/
	static std::vector< LuaApiInfo > info;

	/*
		Description needed.
	*/
	static void GenerateHtml();
};


#endif
#endif
