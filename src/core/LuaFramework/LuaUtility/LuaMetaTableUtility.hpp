#ifndef LUA_UTILTIY_H
#define LUA_UTILITY_H

#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>

#define ENTITY_META_TYPE "metatype_core_entity"



/*
	Syntax juice when overloading the garbage collection in lua.
*/
typedef int LuaEntity;


/*
	Creates a new LuaEntity userdata in lua.
*/
static LuaEntity* LuaUNewEntity( lua_State * L )
{
    LuaEntity * ent = (LuaEntity*)lua_newuserdata( L, sizeof( LuaEntity ) ); 
    luaL_getmetatable( L, ENTITY_META_TYPE );
    lua_setmetatable( L, -2 );
    return ent;
}


#endif



