#include "RenderingBinding.hpp"

#include <logger/Logger.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <Components/RenderingComponent.hpp>
#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>

#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

extern "C"
{
	static int CreateComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::dataHandler.AllocateData< obs::RenderingComponent >( *ent );
        }
        else
        {
            luaL_error( L, "create_renderingComponent requires LuaEntity as parameter." );
        }
		return 0;
	}


	static int InitializeRenderingComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 3 && lua_isstring( L, 2 ) && lua_isstring( L, 3 ) )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = obs::dataHandler.GetData< obs::RenderingComponent >( *ent );
			if( rc != nullptr )
			{
				// if component already has a material or mesh, delete them
				LuaFreeMesh( rc );
				LuaFreeMaterial( rc );

				std::string materialPath = lua_tostring( L, 2 );
				std::string meshPath = lua_tostring( L, 3 );

				if( obs::contentManager.Load( materialPath, obs::ContentLoadPolicy::BLOCKING) )
				{
					obs::MaterialData* materialData = (obs::MaterialData*) obs::contentManager.GetContentData(materialPath);
					unsigned int materialID = materialData->materialID;
					unsigned int shaderID = materialData->shaderID;

					if( obs::contentManager.Load( meshPath, obs::ContentLoadPolicy::BLOCKING) )
					{
						unsigned int meshID = ((obs::ObjModelData*)obs::contentManager.GetContentData(meshPath))->modelID;

						Bitmask myMask;
					
						// default meta properties....
						myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
						myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::OPAQUE_GEOMETRY, MaskType::TYPE);

						// assign material and mesh...
						myMask = obs::bitmaskSystem.Encode(myMask, materialID, MaskType::TEXTURE);
						myMask = obs::bitmaskSystem.Encode(myMask, meshID, MaskType::MESH);
						myMask = obs::bitmaskSystem.Encode(myMask, shaderID, MaskType::SHADER);

						// default viewPort
						myMask = obs::bitmaskSystem.Encode(myMask, 0, MaskType::VIEWPORT_ID);

						rc->bitmask = myMask;
						rc->renderMe = true;
					}
					else
					{
						luaL_error( L, "initializeRenderingComponent -- Cannot find mesh file" );
					}
				}
				else
				{
					luaL_error( L, "initializeRenderingComponent -- Cannot find material file" );
				}
			}
			else
			{
				luaL_error( L, "initializeRenderingComponent -- entity %i does not have a RenderingComponent.", *ent );
			}
		}
		else
		{
			luaL_error( L, "initializeRenderingComponent requires LuaEntity, path to material file and mesh file path as parameters." );
		}

		return 0;
	}


	static int SetViewPort( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 && lua_isnumber( L, 2 ) )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				rc->bitmask = obs::bitmaskSystem.Encode( rc->bitmask, 0, MaskType::VIEWPORT_ID);
			}
			else
			{
				luaL_error( L, "setViewport -- entity %i does not have a RenderingComponent.", *ent );
			}
		}
		else
		{
			luaL_error( L, "setViewport requires LuaEntity and viewportIndex as parameters." );
		}
		
		return 0;
	}

	
	static int SetMesh( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 && lua_isstring( L, 2 ) )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				std::string meshPath = lua_tostring( L, 3 );
				if( obs::contentManager.Load( meshPath, obs::ContentLoadPolicy::BLOCKING) )
				{
					unsigned int meshID = ((obs::ObjModelData*)obs::contentManager.GetContentData(meshPath))->modelID;
					LuaFreeMesh( rc );
					rc->bitmask = obs::bitmaskSystem.Encode( rc->bitmask, meshID, MaskType::MESH);
				}
				else
				{
					luaL_error( L, "setMesh -- Cannot find mesh file" );
				}
			}
			else
			{
				luaL_error( L, "setMesh -- entity %i does not have a RenderingComponent.", *ent );
			}
		}
		else
		{
			luaL_error( L, "setMesh requires LuaEntity and mesh file path as parameters." );
		}

		return 0;
	}

	static int SetMaterial( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 && lua_isstring( L, 2 ) )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				std::string meshPath = lua_tostring( L, 3 );
				if( obs::contentManager.Load( meshPath, obs::ContentLoadPolicy::BLOCKING) )
				{
					unsigned int meshID = ((obs::ObjModelData*)obs::contentManager.GetContentData(meshPath))->modelID;
					LuaFreeMesh( rc );
					rc->bitmask = obs::bitmaskSystem.Encode( rc->bitmask, meshID, MaskType::MESH);
				}
				else
				{
					luaL_error( L, "setMaterial -- Cannot find mesh file" );
				}
			}
			else
			{
				luaL_error( L, "setMaterial -- entity %i does not have a RenderingComponent.", *ent );
			}
		}
		else
		{
			luaL_error( L, "setMaterial requires LuaEntity and mesh file path as parameters." );
		}

		return 0;
	}


	static int RemoveMesh( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				LuaFreeMesh( rc );
			}
		}
		else
		{
			luaL_error( L, "removeMesh requires LuaEntity as parameter." );
		}
		return 0;
	}


	static int RemoveMaterial( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				LuaFreeMaterial( rc );
			}
		}
		else
		{
			luaL_error( L, "removeMaterial requires LuaEntity as parameter." );
		}
		return 0;
	}

	static int setRenderBool( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 && lua_isboolean( L, 2 ) )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
			if( rc )
			{
				rc->renderMe = lua_toboolean( L, 2 );
			}
		}
		else
		{
			luaL_error( L, "setRenderMe requires LuaEntity as parameter." );
		}
		return 0;
	}
}


obs::RenderingBinding::RenderingBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );

		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "create_renderingComponent", CreateComponent,					
			"LuaEntity", "", "Attempts to allocate a new RenderingComponent for the entity" );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "initializeRenderingComponent", InitializeRenderingComponent,	
			"LuaEntity, material file, mesh file", "", 
			"Will attempt to load mesh and material and assign to the entity. Will use default viewPort, render command and OPAQUE_GEOMETRY type, and set renderMe to true." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setMesh", SetMesh,		
			"LuaEntity, mesh file path", "", "Will attempt to set a mesh for the entity" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setMaterial", SetMaterial,	
			"LuaEntity, material file path", "", "Will attempt to set material for the entity" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "removeMesh",	RemoveMesh,		
			"LuaEntity", "", "Will attempt to remove the assigned mesh if present" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "removeMaterial",	RemoveMaterial,	
			"LuaEntity", "", "Will attempt to remove the assigned material if present" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setRenderMe", setRenderBool,	
			"LuaEntity, bool", "", "Will attempt to set the renderMe value of the RenderingComponent" );


	lua_pop( L, 1);
	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for RenderingComponent" << std::endl;
		assert( false );
	}
}

