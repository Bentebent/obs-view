#include "PickingBinding.hpp"

#include <logger/Logger.hpp>

#include <Components/PhysicsComponent.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <BulletFramework/PhysicsHandler.hpp>
#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

#include <bullet/btBulletDynamicsCommon.h>

extern "C"
{
	static int LuaTestRay( lua_State* L )
	{
		if( lua_gettop( L ) == 7 || lua_gettop( L ) == 8 )
		{
			float x = lua_tonumber( L, 1 );
			float y = lua_tonumber( L, 2 );
			float z = lua_tonumber( L, 3 );

			float x2 = lua_tonumber( L, 4 );
			float y2 = lua_tonumber( L, 5 );
			float z2 = lua_tonumber( L, 6 );

			float length = lua_tonumber( L, 7 );

			obs::RayResult res = obs::physicsHandler.SendRay( glm::vec3( x, y, z ), glm::vec3( x2, y2, z2 ), length );

			if( lua_gettop( L ) == 8 )
			{
				bool debug = lua_toboolean( L, 8 );
				if( debug )
				{
					res.DrawDebug();
				}
			}

			if( res.HasHit() )
			{
				lua_pushnumber( L, res.m_distanceToHit );

				glm::vec3 hitPos = res.m_from + res.m_dir * res.m_length;
				lua_pushnumber( L, hitPos.x );
				lua_pushnumber( L, hitPos.y );
				lua_pushnumber( L, hitPos.z );

				lua_pushnumber( L, res.m_shapeHitByRay->m_obs_parentEntity );
			}
			else
			{
				for( int i = 0; i < 5; i++ )
				{
					lua_pushnil( L );
				}
				return 5;
			}
		}
		else 
		{
			luaL_error( L, "checkRay requires 3 float for origin, 3 float for direction \
						   and length as parameters. Last parameter bool is optional." );
		}

		// from, to, lenght

		// hit length, hit pos, hit entity...

		return 0;
	}








}

const char* description( std::string msg ) { return msg.c_str(); }

obs::PickingBinding::PickingBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "checkRay", LuaTestRay,		
			"3 float origin, 3 float dir, length, bool", "distance to hit, position of hit, entity id", 
			"Will send a ray in the bullet environment and return 5 values. If ray failes all return values will be zero." );
		
		

	lua_pop( L, 1);


	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for pickingBinding" << std::endl;
		assert( false );
	}
}

