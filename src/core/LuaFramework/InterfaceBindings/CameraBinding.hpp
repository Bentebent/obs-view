#ifndef CAMERA_LUA_BINDING_HPP
#define CAMERA_LUA_BINDING_HPP

struct lua_State;

namespace obs
{
	class CameraBinding
	{
	public:
		CameraBinding( lua_State* L );
	};
}

#endif