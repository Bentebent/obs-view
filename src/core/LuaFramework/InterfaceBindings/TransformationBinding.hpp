#ifndef TRANSFORMATION_COMPONENT_LUA_BINDING
#define TRANSFORMATION_COMPONENT_LUA_BINDING

struct lua_State;

namespace obs
{
	class TransformationBinding
	{
	public:
		TransformationBinding( lua_State* L );
	};
}

#endif