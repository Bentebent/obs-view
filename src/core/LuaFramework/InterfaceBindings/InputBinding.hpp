#ifndef INPUT_LUA_BINDING
#define INPUT_LUA_BINDING

#define INPUT_META_TABLE_TYPE "input_meta_table_type"
#define INPUT_BINDING_META_TYPE "meta_type_input_binding"

struct lua_State;

namespace obs
{
	class InputBinding
	{
	public:
		InputBinding( lua_State* L );
	};
}

#endif