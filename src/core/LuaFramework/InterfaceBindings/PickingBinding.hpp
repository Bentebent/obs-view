#ifndef PICKING_SYSTEM_BINDING_HPP
#define PICKING_SYSTEM_BINDING_HPP

struct lua_State;

namespace obs
{
	class PickingBinding
	{
	public:
		PickingBinding( lua_State* L );
	};
}

#endif