#ifndef SCENE_GRAPH_BINDING_HPP
#define SCENE_GRAPH_BINDING_HPP

struct lua_State;

namespace obs
{
	class SceneGraphBinding
	{
	public:
		SceneGraphBinding( lua_State* L );
	};
}

#endif