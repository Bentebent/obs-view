#include "SceneGraphBinding.hpp"

#include <logger/Logger.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <BulletFramework/PhysicsHandler.hpp>
#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

#include <bullet/btBulletDynamicsCommon.h>

extern "C"
{
	static int AddSocketConstraint( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_isuserdata(  L, 2 ) && lua_gettop( L ) == 5  )
		{
			LuaEntity* parent = (LuaEntity*)lua_touserdata( L, 1 );
			LuaEntity* child = (LuaEntity*)lua_touserdata( L, 2 );

			obs::PhysicsComponent* pc_parent = GET_DATA<obs::PhysicsComponent>( *parent );
			obs::PhysicsComponent* pc_child = GET_DATA<obs::PhysicsComponent>( *child );

			if( pc_parent && pc_child && pc_parent != pc_child )
			{
				float x = static_cast<float>(lua_tonumber( L,	3 ));  
				float y = static_cast<float>(lua_tonumber( L,	4 ));  
				float z = static_cast<float>(lua_tonumber( L,	5 ));  

				btRigidBody* parent_body = obs::physicsHandler.GetRigidBody( pc_parent->shapeIndex );
				btRigidBody* child_body = obs::physicsHandler.GetRigidBody( pc_child->shapeIndex );

				btPoint2PointConstraint* constraint = new btPoint2PointConstraint( *parent_body, *child_body, btVector3( x, y, z ), btVector3( 0,0,0 ) );

				parent_body->addConstraintRef( constraint );
				child_body->addConstraintRef( constraint );

				obs::physicsHandler.AddConstraint( constraint );
			}

			return 0;
		}
		else
		{
			luaL_error( L, "addSocetConstraint requires parent entity, child entity and x,y,z offset from parent as parameters" );
		}

		return 0;
	}	

	static int MakeChildParent( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_isuserdata(  L, 2 ) )
		{
			LuaEntity* parent = (LuaEntity*)lua_touserdata( L, 1 );
			LuaEntity* child = (LuaEntity*)lua_touserdata( L, 2 );

			obs::dataHandler.AddChild( *parent, *child );
			return 0;
		}

		return 0;
	}

	static int MakeIndependent( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
		{
			LuaEntity* child = (LuaEntity*)lua_touserdata( L, 2 );

			obs::dataHandler.RemoveFromParent( *child );
			return 0;
		}

		return 0;
	}

	static int GetParent( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
		{
			LuaEntity* child = (LuaEntity*)lua_touserdata( L, 2 );

			int parent = obs::dataHandler.GetParent( *child );
			if( parent < 0 )
				lua_pushnil( L );
			else
			{
				LuaEntity* userDataEntity = LuaUNewEntity( L );
				*userDataEntity = parent;
				obs::dataHandler.AddLuaReference( parent );
			}
			return 1;
		}
		return 0;
	}

	static int GetChildren( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
		{
			LuaEntity* parent = (LuaEntity*)lua_touserdata( L, 1 );

			const int* children = obs::dataHandler.GetChildren( *parent );
			int nrChildren = obs::dataHandler.GetNrChildren( *parent );
			if( nrChildren < 1 )
			{
				lua_pushnil( L );
			}
			else
			{
				lua_newtable( L );
				for( int i = 0; i < nrChildren; i++ )
				{
					LuaEntity* userDataEntity = LuaUNewEntity( L );
					*userDataEntity = children[i];
					obs::dataHandler.AddLuaReference( children[i] );
					lua_rawseti( L, -2, i+1 );      
				}
			}
			return 1;
		}
		return 0;
	}

}

obs::SceneGraphBinding::SceneGraphBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addSocetConstraint", AddSocketConstraint,		
			"Parent entity, child entity", 
			"", 
			"Will connect the parent with the child using a point to point socket." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "makeChildParent", MakeChildParent,		
			"Parent entity, child entity", 
			"", 
			"Will connect the parent with the child using the data handler." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "makeIndependent", MakeIndependent,		
			"child entity", 
			"", 
			"Will remove child from parent in the dataHandler structure." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getParent", GetParent,		
			"child entity", 
			"", 
			"Will return nil if no parent or integer id of parent" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getChildren", GetChildren,		
			"parent entity", 
			"", 
			"Will return nil if no children or a table of lua entities of the children" );


	lua_pop( L, 1);
	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for SceneGraphBinding" << std::endl;
		assert( false );
	}
}

