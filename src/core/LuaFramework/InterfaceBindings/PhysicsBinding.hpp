#ifndef PHYSICS_COMPONENT_BINDING_HPP
#define PHYSICS_COMPONENT_BINDING_HPP

struct lua_State;

namespace obs
{
	class PhysicsBinding
	{
	public:
		PhysicsBinding( lua_State* L );
	};
}

#endif