#ifndef RENDERING_COMPONENT_BINDING_HPP
#define RENDERING_COMPONENT_BINDING_HPP

struct lua_State;

namespace obs
{
	class RenderingBinding
	{
	public:
		RenderingBinding( lua_State* L );
	};
}

#endif