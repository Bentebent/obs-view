#include "CameraBinding.hpp"

#include <logger/Logger.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>
#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>


extern "C"
{
	static int SetCameraPosition( lua_State* L )
	{
		if( lua_gettop( L ) == 4 )
		{
			glm::vec3 pos = glm::vec3( 0.0f );
			int cameraIndex = lua_tonumber( L, 1 );

			for( int i = 0; i < 3; i++ )
			{
				int index = -(3 - i);
				if( lua_isnumber( L, index ) )
				{
					pos[ i ] = static_cast<float>(lua_tonumber( L, index ));
				}
			}

			obs::Camera* c = obs::cameraHandler.GetCamera( cameraIndex );
			if( c != nullptr )
				c->SetPosition( pos );

			return 0;
		}
		
		luaL_error( L, "setCameraPosition requires 4 parameters; active camera index, x, y, z" );
		return 0;
	}

	static int SetCameraLookAt( lua_State* L )
	{
		if( lua_gettop( L ) == 4 )
		{
			glm::vec3 target = glm::vec3( 0.0f );
			int cameraIndex = lua_tonumber( L, 1 );

			for( int i = 0; i < 3; i++ )
			{
				int index = -(3 - i);
				if( lua_isnumber( L, index ) )
				{
					target[ i ] = static_cast<float>(lua_tonumber( L, index ));
				}
			}

			obs::Camera* c = obs::cameraHandler.GetCamera( cameraIndex );
			if( c != nullptr )
				c->LookAt( target, glm::vec3( 0.0f, 1.0f, 0.0f ) );

			return 0;
		}
		
		luaL_error( L, "setCameraLookAt requires 4 parameters; active camera index, x, y, z" );
		return 0;
	}

	static int GetActiveCameraIndex( lua_State* L )
	{
		lua_pushinteger( L, obs::cameraHandler.GetActiveCameraIndex() );
		return 1;
	}

	static int SetActiveCameraIndex( lua_State* L )
	{
		if( lua_gettop( L ) == 1 )
		{
			int cameraIndex = lua_tointeger( L, 1 );
			obs::cameraHandler.SetActiveCamera( cameraIndex );
			return 0;
		}

		luaL_error( L, "setActiveCamera requires 1 parameter; camera index" );
		return 0;
	}

	static int ConvertToFpsCamera( lua_State* L )
	{
		if( lua_gettop( L ) == 8 )
		{
			int cameraIndex = lua_tointeger( L, 1 );
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 2 );

			glm::vec3 offset;
			for( int i = 0; i < 3; i++ )
			{
				int index = (3 + i);
				if( lua_isnumber( L, index ) )
				{
					offset[ i ] = static_cast<float>(lua_tonumber( L, index ));
				}
			}

			glm::vec3 up;
			for( int i = 0; i < 3; i++ )
			{
				int index = (6 + i);
				if( lua_isnumber( L, index ) )
				{
					up[ i ] = static_cast<float>(lua_tonumber( L, index ));
				}
			}

			obs::Camera* camera = obs::cameraHandler.GetCamera( cameraIndex );
			if( camera != nullptr )
			{
				camera->MakeFpsCamera( *ent, offset, up );
			}
			return 0;
		}

		luaL_error( L, "convertToFpsCamera requires 8 parameters; camera index, parent entity, xyz offset, xyz up" );
		return 0;
	}

	static int ConvertToFreeCamera( lua_State* L )
	{
		if( lua_gettop( L ) == 1 )
		{
			int cameraIndex = lua_tointeger( L, 1 );

			obs::Camera* camera = obs::cameraHandler.GetCamera( cameraIndex );
			if( camera != nullptr )
			{
				camera->MakeFreeFlying();
			}
			return 0;
		}

		luaL_error( L, "convertToFreeCamera requires 1 parameter; camera index" );
		return 0;
	}
}


obs::CameraBinding::CameraBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		
        LUA_ADD_FUNCTION_TO_INTERFACE( L, "setCameraPosition", SetCameraPosition, 
			"camera index, x, y, z ", "", "Will set the position of the camera at the supplied index" );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setCameraLookAt", SetCameraLookAt, 
			"camera index, x, y, z", "", "Will set the camera at the supplied index to look at the position" );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getActiveCamera", GetActiveCameraIndex, 
			"", "index", "Return is the index for the currently active camera" );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setActiveCamera", SetActiveCameraIndex, 
			"index", "", "Will set the active camera to the supplied index." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "convertToFpsCamera", ConvertToFpsCamera, 
			"camera index, parent entity, xyz offset, xyz up", "", "Makes the camera behave like a fps-camera attatched to the entity with the supplied \
			offset and using the up vector as jump direction etc." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "convertToFreeCamera", ConvertToFreeCamera, 
			"camera index", "", "Makes the camera behave like a free flying camera." );


	lua_pop( L, 1);

	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for obs::Camera" << std::endl;
		assert( false );
	}
}


