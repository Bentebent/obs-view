#ifndef LIGHT_COMPONENT_BINDING_HPP
#define LIGHT_COMPONENT_BINDING_HPP

struct lua_State;

namespace obs
{
	class LightBinding
	{
	public:
		LightBinding( lua_State* L );
	};
}

#endif