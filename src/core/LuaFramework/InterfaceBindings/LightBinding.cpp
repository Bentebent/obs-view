#include "LightBinding.hpp"

#include <logger/Logger.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>

#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

extern "C"
{
	static int CreateComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );

			obs::dataHandler.AllocateData< obs::LightComponent >( *ent );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );
			
			Bitmask myMask = 0;
			myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
			myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::LIGHT, MaskType::TYPE);
			myMask = obs::bitmaskSystem.Encode(myMask, gfx::LIGHT_TYPES::POINT, MaskType::MESH);

			lcomp->bitmask = myMask;
        }
        else
        {
            luaL_error( L, "create_lightComponent requires LuaEntity as parameter." );
        }
		return 0;
	}

	static int GetLightColor( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					lua_pushnumber( L, lcomp->color[i] );
				}
				return 3;
			}
			return 0;
        }
            
        luaL_error( L, "getLightColor requires LuaEntity as parameter" );
		return 0;
	}

	static int SetLightColor( lua_State* L )
	{
		if( lua_gettop( L ) == 4 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					int index = -(3 - i);
					if( lua_isnumber( L, index ) )
					{
						lcomp->color[ i ] = static_cast<float>(lua_tonumber( L, index ));
					}
				}
				for( int i = 0; i < 4; i++ )
				{
					lua_pop( L, -1 );
				}
				return 0;
			}
			return 0;
		}
		
		luaL_error( L, "setLightColor requires 4 parameters; LuaEntity, r, g, b" );
		return 0;
	}

	static int GetLightSpecular( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					lua_pushnumber( L, lcomp->specularColor[i] );
				}
				return 3;
			}
			return 0;
        }
            
        luaL_error( L, "getLightSpecular requires LuaEntity as parameter" );
		return 0;
	}

	static int SetLightSpecular( lua_State* L )
	{
		if( lua_gettop( L ) == 4 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					int index = -(3 - i);
					if( lua_isnumber( L, index ) )
					{
						lcomp->specularColor[ i ] = static_cast<float>(lua_tonumber( L, index ));
					}
				}
				for( int i = 0; i < 4; i++ )
				{
					lua_pop( L, -1 );
				}
				return 0;
			}
			return 0;
		}
		
		luaL_error( L, "setLightColor requires 4 parameters; LuaEntity, r, g, b" );
		return 0;
	}

	static int SetLightIntensity( lua_State* L )
	{
		if( lua_gettop( L ) == 2 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				
				lcomp->intensity = static_cast<float>(lua_tonumber( L, 2 ));
				
				lua_pop( L, -1 );
				lua_pop( L, -1 );

				return 0;
			}
			return 0;
		}
		
		luaL_error( L, "setLightIntensity requires 2 parameters; LuaEntity, intensity" );
		return 0;
	}

	static int GetLightIntensity( lua_State* L )
	{
		if( lua_gettop( L ) == 2 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::LightComponent* lcomp = obs::dataHandler.GetData< obs::LightComponent >( *ent );

			if( lcomp != nullptr )
			{
				lua_pushnumber( L, lcomp->intensity );
				return 1;
			}
			return 0;
		}
		
		luaL_error( L, "getLightIntensity requires LuaEntity as parameter" );
		return 0;
	}


}


obs::LightBinding::LightBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );

		

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "create_lightComponent", CreateComponent,					
			"LuaEntity", "", "Attempts to allocate a new LightComponent for the entity and initialize the respective bitmask to Render, Light and Point" );
		
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getLightColor", GetLightColor,					
			"LuaEntity", "r, g, b", "Attempts to get the color of the corresponding LightComponent" );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setLightColor", SetLightColor,					
			"LuaEntity, r, g, b", "", "Attempts to set the color of the corresponding LightComponent" );


		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getLightSpecular", GetLightSpecular,					
			"LuaEntity", "r, g, b", "Attempts to get the specular color of the corresponding LightComponent" );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setLightSpecular", SetLightSpecular,					
			"LuaEntity, r, g, b", "", "Attempts to set the specular color of the corresponding LightComponent" );


		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getLightIntensity", GetLightIntensity,					
			"LuaEntity", "intensity", "Attempts to get the intensity of the corresponding LightComponent" );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setLightIntensity", SetLightIntensity,					
			"LuaEntity, intensity", "", "Attempts to set the intensity of the corresponding LightComponent" );
		


	lua_pop( L, 1);
	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for RenderingComponent" << std::endl;
		assert( false );
	}
}

