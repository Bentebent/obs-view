#include "TransformationBinding.hpp"

#include <logger/Logger.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <Components/TransformationComponent.hpp>
#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>

#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

extern "C"
{
	static int CreateComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::dataHandler.AllocateData< obs::TransformationComponent >( *ent );
        }
        else
        {
            luaL_error( L, "create_transformationComponent requires LuaEntity as parameter." );
        }
		return 0;
	}


	static int GetPosition( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					lua_pushnumber( L, tcomp->position[i] );
				}
				return 3;
			}
        }
        
        {
            luaL_error( L, "getPosition requires LuaEntity as parameter" );
			for( int i = 0; i < 3; i++ )
			{
				lua_pushnil( L );
			}
        }
		return 3;
	}

	static int SetPosition( lua_State* L )
	{
		if( lua_gettop( L ) == 4 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					int index = -(3 - i);
					if( lua_isnumber( L, index ) )
					{
						tcomp->position[i] = static_cast<float>(lua_tonumber( L, index ));  
					}
				}
				for( int i = 0; i < 4; i++ )
				{
					lua_pop( L, -1 );
				}
				return 0;
			}
			return 0;
		}
		
		luaL_error( L, "setPosition requires 4 parameters; LuaEntity, x, y, z" );
		return 0;
	}
	

	static int GetRotation( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 4; i++ )
				{
					lua_pushnumber( L, tcomp->rotation[i] );
				}
				return 4;
			}
        }
        
        {
            luaL_error( L, "getRotation requires LuaEntity as parameter" );
			for( int i = 0; i < 4; i++ )
			{
				lua_pushnil( L );
			}
        }
		return 4;
	}

	static int SetRotation( lua_State* L )
	{
		
		if( lua_gettop( L ) == 5 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 4; i++ )
				{
					int index = -(4 - i);
					if( lua_isnumber( L, index ) )
					{
						tcomp->rotation[i] = static_cast<float>(lua_tonumber( L, index ));  
					}
				}
				for( int i = 0; i < 4; i++ )
				{
					lua_pop( L, -1 );
				}
				return 0;
			}
		}
		
		luaL_error( L, "setRotation requires 5 parameters; LuaEntity, x, y, z, w" );
		return 0;
	}

	static int GetScale( lua_State* L )
	{
		int entityId = -1;
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					lua_pushnumber( L, tcomp->scale[i] );
				}
				return 3;
			}
        }
        
        luaL_error( L, "getScale requires LuaEntity as parameter" );
		for( int i = 0; i < 3; i++ )
		{
			lua_pushnil( L );
		}
		return 3;
	}

	static int setScale( lua_State* L )
	{
		int entityId = -1;
		if( lua_gettop( L ) == 4 )
		{
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::TransformationComponent* tcomp = obs::dataHandler.GetData< obs::TransformationComponent >( *ent );

			if( tcomp != nullptr )
			{
				for( int i = 0; i < 3; i++ )
				{
					int index = -(3 - i);
					if( lua_isnumber( L, index ) )
					{
						tcomp->scale[i] = static_cast<float>(lua_tonumber( L, index ));  
					}
				}
				for( int i = 0; i < 4; i++ )
				{
					lua_pop( L, -1 );
				}
				return 0;
			}
		}		
		luaL_error( L, "setScale requires 4 parameters; LuaEntity, x, y, z" );
		return 0;
	}

}




obs::TransformationBinding::TransformationBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "create_transformationComponent", CreateComponent,	
			"LuaEntity", "", "Attempts to allocate a new RenderingComponent for the entity" );


		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getPosition", GetPosition,							
			"LuaEntity", "x, y, z", "Attempts to retrieve the x, y and z position of the entity." );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setPosition", SetPosition,							
			"LuaEntity, x, y, z", "", "Attempts to set the position for the object." );


		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getRotation", GetRotation,					
			"LuaEntity", "x, y, z, w", "Attempts to retrieve the corresponding rotation quaternion for the entity"  );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setRotation", SetRotation,					
			"LuaEntity, x, y, z, w", "", "Attempts to set the corresponding quaternion for the entity"  );


		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getScale", GetScale,							
			"LuaEntity", "x, y, z", "Attempts to retrieve the corresponding x, y and z scale"  );
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setScale", setScale,							
			"LuaEntity, x, y, z", "", "Attempts to set x, y and z scale for the entity"  );


	lua_pop( L, 1);



	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for RenderingComponent" << std::endl;
		assert( false );
	}
}

