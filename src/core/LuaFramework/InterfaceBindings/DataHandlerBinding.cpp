#include "DataHandlerBinding.hpp"

#include <logger/Logger.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>
#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

extern "C"
{
	static int CreateObject( lua_State* L )
	{
		int entity = obs::dataHandler.AllocateObject();
		if( entity != -1 )
		{
			LuaEntity* userDataEntity = LuaUNewEntity( L );
            *userDataEntity = entity;
		}
		else
		{
			lua_pushnil( L );
		}

		return 1;
	}

	static int DestroyObject( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
			LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			int nrRefs = obs::dataHandler.GetLuaReferences( *ent );
			if( nrRefs > 1 )
				obs::dataHandler.RemoveLuaReference( *ent );
			else
			{			
				obs::RenderingComponent* rc = GET_DATA< obs::RenderingComponent >( *ent );
				LuaFreeMesh( rc );
				LuaFreeMaterial( rc );

				obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( *ent );
				LuaFreePhysicsShape( pc );

				obs::dataHandler.DeleteObject( *ent );
			}
        }
        else
        {
            luaL_error( L, "__gc on LuaEntity did not recieve userdata as parameter" );
        }
		return 0;
	}
}


obs::DataHandlerBinding::DataHandlerBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;


	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		

        LUA_ADD_FUNCTION_TO_INTERFACE( L, "CreateEntity", CreateObject, 
			"", "LuaEntity", "Will attempt to allocate a new entity." );




		if( luaL_newmetatable( L, ENTITY_META_TYPE ) == 0 )
        {
            LOG_ERROR << "Metatable entity already exists!" << std::endl;
        }
			lua_pushstring( L, "__gc" );
            lua_pushcfunction( L, DestroyObject );
			lua_settable( L, -3 );

	lua_pop( L, 1);
	lua_pop( L, 1);

	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for worldPositionComponent" << std::endl;
		assert( false );
	}
}


