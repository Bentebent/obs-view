#ifndef DATA_HANDLER_BINDING_HPP
#define DATA_HANDLER_BINDING_HPP

struct lua_State;

namespace obs
{
	class DataHandlerBinding
	{
	public:
		DataHandlerBinding( lua_State* L );
	};
}

#endif