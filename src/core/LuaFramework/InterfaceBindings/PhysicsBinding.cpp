#include "PhysicsBinding.hpp"

#include <logger/Logger.hpp>

#include <Components/PhysicsComponent.hpp>

#include <LuaFramework/LuaUtility/LuaHtmlGenerator.hpp>

#include <CoreFramework/Core.hpp>

#include <lua/lua.hpp>
#include <LuaFramework/LuaUtility/LuaUtility.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <cassert>

#include <LuaFramework/LuaUtility/LuaMetaTableUtility.hpp>

#include <BulletFramework/PhysicsHandler.hpp>
#include <LuaFramework/LuaUtility/LuaContentUtility.hpp>

#include <bullet/btBulletDynamicsCommon.h>


extern "C"
{
	static int CreateComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::dataHandler.AllocateData< obs::PhysicsComponent >( *ent );
        }
        else
        {
            luaL_error( L, "create_physicsComponent requires LuaEntity as parameter" );
        }
		return 0;
	}

	static int RemoveComponent( lua_State* L )
	{
		if( lua_isuserdata(  L, -1 ) && lua_gettop( L ) == 1 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			LuaFreePhysicsShape( pc );
			obs::dataHandler.DeleteData< obs::PhysicsComponent >( *ent );
        }
        else
        {
            luaL_error( L, "destroy_physicsComponent requires LuaEntity as parameter" );
        }
		return 0;
	}

	static int CreatePlaneShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 5 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				glm::vec4 planeDefinition;
				for( int i = 0; i < 4; i++ )
				{
					int index = -(4 - i);
					if( lua_isnumber( L, index ) )
					{
						planeDefinition[i] = static_cast<float>(lua_tonumber( L, index ));  
					}
				}

				pc->shapeIndex = obs::physicsHandler.CreatePlaneShape( planeDefinition, *ent );
			}
        }
        else
        {
            luaL_error( L, "addPlaneShape requires LuaEntity, x, y, z and d-value as parameters" );
        }

		return 0;
	}

	static int CreateSphereShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 3 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				float radius = static_cast<float>(lua_tonumber( L, 2 ));  
				float mass = static_cast<float>(lua_tonumber( L, 3 ));  

				pc->shapeIndex = obs::physicsHandler.CreateShpereShape( radius, mass, *ent );
			}
        }
        else
        {
            luaL_error( L, "addSphereShape requires LuaEntity, radius and mass as parameters" );
        }

		return 0;
	}

	static int CreateMeshShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 4 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				std::string path = lua_tostring( L, 2 );
				float scale = static_cast<float>(lua_tonumber( L, 3 ));  
				float mass = static_cast<float>(lua_tonumber( L, 4 ));  

				pc->shapeIndex = obs::physicsHandler.CreateMeshShape( path, scale, mass, *ent );
			}
        }
        else
        {
            luaL_error( L, "addTriangleShape requires LuaEntity, path to triangleMesh, scale and mass as parameters" );
        }

		return 0;
	}

	static int CreateBoxShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 5 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				float width = static_cast<float>(lua_tonumber( L, 2 ));  
				float height = static_cast<float>(lua_tonumber( L, 3 ));  
				float length = static_cast<float>(lua_tonumber( L, 4 ));  
				
				float mass = static_cast<float>(lua_tonumber( L, 5 ));  

				pc->shapeIndex = obs::physicsHandler.CreateBoxShape( width, height, length, mass, *ent );
			}
        }
        else
        {
            luaL_error( L, "addBoxShape requires LuaEntity, width, height, length and mass as parameters" );
        }

		return 0;
	}

	static int CreateCylinderShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 4 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				float radius = static_cast<float>(lua_tonumber( L, 2 ));  
				float height = static_cast<float>(lua_tonumber( L, 3 ));  
				float mass = static_cast<float>(lua_tonumber( L, 4 ));  

				pc->shapeIndex = obs::physicsHandler.CreateCylinderShape( radius, height, mass, *ent );
			}
        }
        else
        {
            luaL_error( L, "addCylinderShape requires LuaEntity, radius, height and mass as parameters" );
        }

		return 0;
	}

	static int CreateCapsuleShape( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 4 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				LuaFreePhysicsShape( pc );

				float radius = static_cast<float>(lua_tonumber( L, 2 ));  
				float height = static_cast<float>(lua_tonumber( L, 3 ));  
				float mass = static_cast<float>(lua_tonumber( L, 4 ));  

				pc->shapeIndex = obs::physicsHandler.CreateCapsuleShape( radius, height, mass, *ent );
			}
        }
        else
        {
            luaL_error( L, "addCapsuleShape requires LuaEntity, radius, height and mass as parameters" );
        }

		return 0;
	}


	static int SetStaticShapePosition( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 4 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				float x = static_cast<float>(lua_tonumber( L, 2 ));  
				float y = static_cast<float>(lua_tonumber( L, 3 ));  
				float z = static_cast<float>(lua_tonumber( L, 4 ));  
			
				obs::physicsHandler.SetPositionOfStaticShape( pc->shapeIndex, glm::vec3( x, y, z ) );
			}
        }
        else
        {
            luaL_error( L, "setStaticShapePosition requires LuaEntity, x, y, and z as parameters" );
        }

		return 0;
	}
	
	static int SetStaticShapeRotation( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 5 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				float x = static_cast<float>(lua_tonumber( L, 2 ));  
				float y = static_cast<float>(lua_tonumber( L, 3 ));  
				float z = static_cast<float>(lua_tonumber( L, 4 ));
				float w = static_cast<float>(lua_tonumber( L, 5 ));  
			
				obs::physicsHandler.SetRotationOfStaticShape( pc->shapeIndex, glm::quat( x, y, z, w ) );
			}
        }
        else
        {
            luaL_error( L, "setStaticShapeRotation requires LuaEntity, x, y, z and w as parameters" );
        }

		return 0;
	}

	static int SetAngularFactor( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 4 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );
				if( body != nullptr )
				{
					float x = static_cast<float>(lua_tonumber( L, 2 ));  
					float y = static_cast<float>(lua_tonumber( L, 3 ));  
					float z = static_cast<float>(lua_tonumber( L, 4 ));

					body->setAngularFactor( btVector3( x, y, z ));
				}
			}
        }
        else
        {
            luaL_error( L, "setAngularFactor requires LuaEntity x, y and z as parameters" );
        }
		return 0;
	}

	static int SetAlwaysActive( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				bool neverSleep = lua_toboolean( L, 2 );
				obs::physicsHandler.SetAlwaysActive( pc->shapeIndex, neverSleep );
			}
        }
        else
        {
            luaL_error( L, "setAlwaysActive requires LuaEntity and bool as parameters" );
        }
		return 0;
	}

	static int SetFrictionCoefficient( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) && lua_gettop( L ) == 2 )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				float friction = static_cast<float>(lua_tonumber( L, 2 ));
				obs::physicsHandler.SetFrictionCoef( pc->shapeIndex, friction );
			}
        }
        else
        {
            luaL_error( L, "setFrictionCoef requires LuaEntity and float as parameters" );
        }
		return 0;
	}

	static int GetNumerOfCollisions( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				lua_pushinteger( L, pc->nrCollisions );
				return 1;
			}
        }
        else
        {
            luaL_error( L, "getNrCollisions requires LuaEntity as parameter" );
        }
		return 0;
	}

	static int MakeGhost( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );
				body->setCollisionFlags( body->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE );
				body->setGravity( btVector3( 0,0,0 ) );
			}
        }
        else
        {
            luaL_error( L, "makeGhost requires LuaEntity as parameter" );
        }
		return 0;
	}

	static int MakeKinematic( lua_State* L )
	{
		if( lua_isuserdata(  L, 1 ) )
        {
            LuaEntity* ent = (LuaEntity*)lua_touserdata( L, 1 );
			obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( *ent );
			if( pc != nullptr )
			{
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );
				
				body->setCollisionFlags( body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );	
			}
        }
        else
        {
            luaL_error( L, "makeKinematic requires LuaEntity as parameter" );
        }
		return 0;
	}
}

obs::PhysicsBinding::PhysicsBinding( lua_State* L )
{
	LUA_NEW_HTML_CATEGORY;

	int errorCheck = lua_gettop( L );
	lua_getglobal( L, LUA_INTERFACE_NAME );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "create_physicsComponent", CreateComponent,		
			"LuaEntity", "", "Attempts to allocate a new PhysicsComponent for the entity." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addPlaneShape", CreatePlaneShape,				
			"LuaEntity, x, y, z, d-value", "", "Attempts to allocate a new plane shape for the entity." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addSphereShape", CreateSphereShape,				
			"LuaEntity, radius, mass", "", "Attempts to allocate a new sphere shape for \
			the entity and calculate inertia from radius and mass." );
	
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addTriangleShape", CreateMeshShape,				
			"LuaEntity, path, scale, mass", "", "Attempts to allocate a new sphere shape for \
			the entity and calculate inertia from radius and mass." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addBoxShape", CreateBoxShape,				
			"LuaEntity, width, height, length, mass", "", "Attempts to allocate a new box shape \
			for the entity and calculate inertia if mass is not zero." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addCylinderShape", CreateCylinderShape, 	
			"LuaEntity, radius, height, mass", "", "Will attempt to create a shpere shape \
			with the supplied properties centered around the objects position." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "addCapsuleShape", CreateCapsuleShape, 	
			"LuaEntity, radius, height, mass", "", "Will attempt to create a capsule shape \
			with the supplied properties centered around the objects position." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setStaticShapePosition", SetStaticShapePosition,				
			"LuaEntity, x, y, z", "", "Must be called after creation of the shape bound to the entity. \
			Should the mass of the shape be set to 0 the position and rotation must be set explicitly." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setStaticShapeRotation", SetStaticShapeRotation,				
			"LuaEntity, x, y, z", "", "Must be called after creation of the shape bound to the entity. \
			Should the mass of the shape be set to 0 the position and rotation must be set explicitly." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setAngularFactor", SetAngularFactor, 	
			"LuaEntity, x, y, z", "", "Will attempt to set the angular factor for the respective ridgidBody." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setAlwaysActive", SetAlwaysActive, 	
			"LuaEntity, bool", "", "Will attempt to set the physics property to always acrive, \
			disallowing the physics system from freezing the update." );		

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "setFrictionCoef", SetFrictionCoefficient, 	
			"LuaEntity, float", "", "Will attempt to set the physics friction coefficient to supplied value." );
		
		LUA_ADD_FUNCTION_TO_INTERFACE( L, "getNrCollisions", GetNumerOfCollisions, 	
			"LuaEntity", "int", "Will return the number of collisions that has been detected in the latest frame with the physics \
			shape associated with the object." );		

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "makeGhost", MakeGhost, 	
			"LuaEntity", "", "Will attempt to make the collision shape of the entity to a ghost shape, ignoring gravity and collisions." );

		LUA_ADD_FUNCTION_TO_INTERFACE( L, "makeKinematic", MakeKinematic, 	
			"LuaEntity", "", "Will attempt to make the collision shape of the entity to a movable static shape, ignoring gravity and \
			moving others but not itself when colliding." );
		
	lua_pop( L, 1);


	if( errorCheck != lua_gettop( L ) )
	{
		LOG_ERROR << "Error creating interface binding for physicsComponent" << std::endl;
		assert( false );
	}
}

