#include "InputHandler.hpp"

#include <CoreFramework/Core.hpp>

#include <cstring>

obs::InputHandler::InputHandler()
{
	std::memset( m_current, 0, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );
	std::memset( m_prev,	0, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );

	std::memset( m_keyDown,	0, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );
	std::memset( m_single,	0, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );
}


obs::InputHandler::~InputHandler()
{
}


const Uint8* obs::InputHandler::GetCurrentKeyStates()
{
	return m_current;
}


const Uint8* obs::InputHandler::GetPrevKeyStates()
{
	return m_prev;
}


const Uint8* obs::InputHandler::GetSinglePressedKeyStates()
{
	return m_single;
}

void obs::InputHandler::Update()
{

	const Uint8* newState = SDL_GetKeyboardState(NULL);
	
	std::memcpy( m_prev, m_current, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );
	std::memcpy( m_current, newState, OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES );

	int ppp = OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES;

	for( int i = 0; i < OBS_SIZE_OF_INPUT_KEY_ARRAY; i++ )
	{
		if( m_prev[i] == false && m_current[i] == true && m_keyDown[i] == false )
		{
			m_single[i] = true;
			m_keyDown[i] = 1;
		}
		else if( m_prev[i] == true && m_current[i] == false )
		{
			m_keyDown[i] = false;
		}
		else 
		{
			m_single[i] = false;
		}
	}

	int relMouseX = 0;
	int relMouseY = 0;
	Uint32 mouseState;

	//while (SDL_PollEvent(&m_event));

	mouseState = SDL_GetRelativeMouseState(&relMouseX, &relMouseY);
	m_mouseDelta = glm::vec2(relMouseX, relMouseY);

	mouseState = SDL_GetMouseState(&relMouseX, &relMouseY);
	m_mousePos = glm::vec2(relMouseX, relMouseY);
}

void obs::InputHandler::SetRelativeMode(bool enabled)
{
	SDL_SetRelativeMouseMode(SDL_bool(enabled));
}

glm::vec2 obs::InputHandler::GetMouseRelativeCenter()
{
	return m_mousePos - glm::vec2( obs::windowHandler.GetWindowWidth(), obs::windowHandler.GetWindowHeight() ) * 0.5f;
}

void obs::InputHandler::WrapMouseInsideWindow()
{
	glm::vec2 mid = glm::vec2( obs::windowHandler.GetWindowWidth(), obs::windowHandler.GetWindowHeight() ) * 0.5f;
	SDL_WarpMouseInWindow( obs::windowHandler.GetWindow(), mid.x, mid.y );
}


glm::vec2 obs::InputHandler::GetMousePos()
{
	return m_mousePos;
}


glm::vec2 obs::InputHandler::GetMouseDelta()
{
	return m_mouseDelta;
}