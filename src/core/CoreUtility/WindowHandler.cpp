#include "WindowHandler.hpp"
#include "WindowModes.hpp"
#include <iostream>
namespace obs
{
	WindowHandler::WindowHandler()
	{
		m_window = nullptr;
		m_windowed = true;
		m_hasFocus = true;
	}

	WindowHandler::~WindowHandler()
	{

	}


	int Filter(void* userdata, SDL_Event* event)
	{
		switch (event->type)
		{
			case SDL_WINDOWEVENT_SHOWN:
			case SDL_WINDOWEVENT_MOVED:
			case SDL_WINDOWEVENT_RESIZED:
			case SDL_WINDOWEVENT_SIZE_CHANGED:
			case SDL_WINDOWEVENT_MINIMIZED:
			case SDL_WINDOWEVENT_MAXIMIZED:
			case SDL_WINDOWEVENT_RESTORED:
			case SDL_WINDOWEVENT_ENTER:
			case SDL_WINDOWEVENT_LEAVE:
			case SDL_WINDOWEVENT_FOCUS_GAINED:
			case SDL_WINDOWEVENT_FOCUS_LOST:
				return 1;
		}

	}
	
	void WindowHandler::Initialize(int GLMajor, int GLMinor, int width, int height, WindowModes windowMode, bool vSync)
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GLMajor);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GLMinor);

		unsigned int flags = 0;

		switch (windowMode)
		{
			case WindowModes::WM_FULL_SCREEN:
				flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN;
				m_windowed = false;
				break;
			case WindowModes::WM_FULL_SCREEN_BORDERLESS:
				flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP;
				m_windowed = false;
				break;
			case WindowModes::WM_WINDOWED:
				flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;
				m_windowed = true;
				break;
			case WindowModes::WM_WINDOWED_BORDERLESS:
				flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS;
				m_windowed = true;
				break;
		}
		
		m_window = SDL_CreateWindow("Project Obscurity", SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, width, height, flags);

		m_windowWidth = width;
		m_windowHeight = height;

		if (m_window == nullptr)
			SDL_Log(SDL_GetError());

		m_context = SDL_GL_CreateContext(m_window);

		SDL_GL_SwapWindow(m_window);

		glewExperimental = true;
		GLenum error = glewInit();

		if (GLEW_OK != error)
			std::cout << "Unable to init GLEW." << std::endl;

		SetVSync(vSync);

		SDL_SetEventFilter((SDL_EventFilter)&Filter, nullptr);
		
		(SDL_FALSE);
	}

	void WindowHandler::SetVSync(bool vSync)
	{
		if (vSync)
			SDL_GL_SetSwapInterval(1);
		else
			SDL_GL_SetSwapInterval(0);
	}

	void WindowHandler::SetWindowMode(WindowModes windowMode)
	{
		switch (windowMode)
		{
		case WindowModes::WM_FULL_SCREEN:
			SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
			m_windowed = false;
			break;
		case WindowModes::WM_FULL_SCREEN_BORDERLESS:
			SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN_DESKTOP);
			m_windowed = false;
			break;
		case WindowModes::WM_WINDOWED:
			SDL_SetWindowFullscreen(m_window, 0);
			SDL_SetWindowBordered(m_window, SDL_TRUE);
			m_windowed = true;
			break;
		case WindowModes::WM_WINDOWED_BORDERLESS:
			SDL_SetWindowFullscreen(m_window, 0);
			SDL_SetWindowBordered(m_window, SDL_FALSE);
			m_windowed = true;
			break;
		}
	}

	void WindowHandler::SetResolution(int width, int height)
	{
		if (m_windowed)
		{
			SDL_SetWindowSize(m_window, width, height);
		}
	}



	int WindowHandler::WindowEvents()
	{
		SDL_PollEvent(&m_event);
		m_resizedWindow = false;
		switch (m_event.type)
		{
			case SDL_WINDOWEVENT:
				switch (m_event.window.event) 
				{
				case SDL_WINDOWEVENT_RESIZED:
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					gfx::Resize(m_event.window.data1, m_event.window.data2);
					m_windowWidth = m_event.window.data1;
					m_windowHeight = m_event.window.data2;
					m_resizedWindow = true;
					#ifdef WINDOW_DEBUG
					SDL_Log("Window %d resized to %dx%d",
						m_event.window.windowID,
						m_event.window.data1,
						m_event.window.data2);
					#endif // WINDOW_DEBUG
					break;
					case SDL_WINDOWEVENT_SHOWN:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d shown", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_HIDDEN:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d hidden", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_EXPOSED:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d exposed", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_MOVED:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d moved to %d,%d",
								m_event.window.windowID, m_event.window.data1,
								m_event.window.data2);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_MINIMIZED:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d minimized", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_MAXIMIZED:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d maximized", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_RESTORED:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d restored", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_ENTER:
						#ifdef WINDOW_DEBUG
							SDL_Log("Mouse entered window %d", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_LEAVE:
						#ifdef WINDOW_DEBUG
							SDL_Log("Mouse left window %d", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						break;
					case SDL_WINDOWEVENT_FOCUS_GAINED:
						#ifdef WINDOW_DEBUG
						SDL_Log("Window %d gained keyboard focus", m_event.window.windowID);
						#endif // WINDOW_DEBUG
							m_hasFocus = true;
						break;
					case SDL_WINDOWEVENT_FOCUS_LOST:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d lost keyboard focus", m_event.window.windowID);
						#endif // WINDOW_DEBUG
							m_hasFocus = false;
						break;
					case SDL_WINDOWEVENT_CLOSE:
						#ifdef WINDOW_DEBUG
							SDL_Log("Window %d closed", m_event.window.windowID);
						#endif // WINDOW_DEBUG
						return WINDOWED_CLOSED;
						break;
				}
				break;
		}
		return 0;
	}


	void WindowHandler::SwapBuffers()
	{
		SDL_GL_SwapWindow(m_window);
	}
}
