#ifndef SRC_CORE_INDEX_VECTOR_HPP
#define SRC_CORE_INDEX_VECTOR_HPP

#include <vector>

namespace obs
{
	template< class T >
	class IndexVector
	{
	public:
		
		IndexVector() 
		{
		}
		
		~IndexVector() 
		{
		}

		/*!
			\return index to access the supplied value.
		*/
		int push_back( const T& value )
		{
			if( m_open.empty() == false )
			{
				int index = m_open.back();
				m_open.pop_back();
				m_data[ index ] = value;
				return index;
			}
			else
			{
				m_data.push_back( value );
				return m_data.size() - 1;
			}
		}

		/*!
			Invalidates the supplied index and will open it for reuse.
		*/
		void erase( int index )
		{
			m_open.push_back( index );
		}

		unsigned int size()
		{
			return m_data.size();
		}

		T& operator[]( unsigned int index )
		{
			return m_data[ index ];
		}

	private:

		std::vector< T > m_data;
		std::vector< unsigned int > m_open;
	};
}

#endif