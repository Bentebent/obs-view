#ifndef SRC_CORE_WINDOWHANDLING_WINDOWHANDLER_HPP
#define SRC_CORE_WINDOWHANDLING_WINDOWHANDLER_HPP

#include <SDL/SDL.h>
#include <GLEW/glew.h>
#include <gfx/GFXInterface.hpp>

#define WINDOWED_CLOSED 1
//#define WINDOW_DEBUG
namespace obs
{
	enum class WindowModes : unsigned int;

	class WindowHandler
	{
	public:
		WindowHandler();
		~WindowHandler();

		void Initialize(int GLMajor, int GLMinor, int width, int height, WindowModes windowMode, bool vSync);
		
		void SetVSync(bool vSync);
		void SetWindowMode(WindowModes windowMode);
		void SetResolution(int width, int height);
		void SwapBuffers();
		int WindowEvents();

		inline SDL_Window* GetWindow() { return m_window; }
		inline bool HasFocus(){ return m_hasFocus; }
		inline bool WindowResized(){ return m_resizedWindow; }

		inline int GetWindowWidth(){ return m_windowWidth; }
		inline int GetWindowHeight(){ return m_windowHeight; }
	
	private:
		SDL_Window* m_window;
		SDL_GLContext m_context;
		SDL_Event m_event;
		bool m_windowed;
		bool m_hasFocus;
		bool m_resizedWindow;

		int m_windowWidth;
		int m_windowHeight;

	};
}

#endif