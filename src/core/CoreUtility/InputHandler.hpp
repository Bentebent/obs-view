#ifndef SRC_OBS_INPUT_HANDLER_HPP
#define SRC_OBS_INPUT_HANDLER_HPP

#include <SDL/SDL.h>

#define OBS_SIZE_OF_INPUT_KEY_ARRAY SDL_Scancode::SDL_NUM_SCANCODES
#define OBS_SIZE_OF_INPUT_KEY_ARRAY_BYTES OBS_SIZE_OF_INPUT_KEY_ARRAY * sizeof( Uint8 )

#include <glm/glm.hpp>

namespace obs
{

	class InputHandler
	{

	public:
		InputHandler();
		~InputHandler();

		const Uint8* GetCurrentKeyStates();
		const Uint8* GetPrevKeyStates();
		const Uint8* GetSinglePressedKeyStates();


		void WrapMouseInsideWindow();
		void SetRelativeMode(bool enabled);
		glm::vec2 GetMousePos();
		glm::vec2 GetMouseDelta();
		glm::vec2 GetMouseRelativeCenter();

		void Update();

		

	private:
		Uint8 m_current[	OBS_SIZE_OF_INPUT_KEY_ARRAY ];
		Uint8 m_prev[		OBS_SIZE_OF_INPUT_KEY_ARRAY ];

		Uint8 m_keyDown[	OBS_SIZE_OF_INPUT_KEY_ARRAY ];
		Uint8 m_single[		OBS_SIZE_OF_INPUT_KEY_ARRAY ];

		glm::vec2 m_mousePos;
		glm::vec2 m_mouseDelta;

		SDL_Event m_event;

	};
}

#endif