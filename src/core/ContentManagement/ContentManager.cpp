#include "ContentManager.hpp"
#include <logger/Logger.hpp>
#include <ContentManagement/Utility/MurmurHash.hpp>

#define INCLUDE_LOADER( loader_class ) obs::contentManager.AddLoader( new loader_class() );



obs::ContenManager obs::contentManager;

/* Include headers for loaders here */
#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/Loaders/TextureLoader.hpp>
#include <ContentManagement/Loaders/PhysicsMeshLoader.hpp>


/*!
	Defines the loaders handled by the ContentManager.
*/
struct ContentManagerLoaderDefinition
{
	ContentManagerLoaderDefinition()
	{	
		/* Add data configuration here */
		INCLUDE_LOADER( obs::ObjLoader );
		INCLUDE_LOADER(obs::MaterialLoader);
		INCLUDE_LOADER(obs::TextureLoader);
		INCLUDE_LOADER(obs::PhysicsMeshLoader);



	}
} cm__;














/***** ContentManager implementation *****/

obs::ContenManager::ContenManager()
{
}

obs::ContenManager::~ContenManager()
{
	for( int i = 0; i < m_loaders.size(); i++ )
	{
		if( m_loaders[ i ] )
		{
			delete m_loaders[ i ];
		}
	}
	m_loaders.clear();

	m_contentThreadPool.KillThreads();
}

void obs::ContenManager::InitializeThreadPool( int nrThreads )
{
	m_contentThreadPool.Initialize( nrThreads );
}

void obs::ContenManager::KillThreads()
{
	m_contentThreadPool.KillThreads();
}


bool obs::ContenManager::Load( std::string path, obs::ContentLoadPolicy policy )
{
	int dotPos = path.find_last_of(".");
	if( dotPos < 0 )
	{
		LOG_CONTENT_SYS << "File path do not contain a file ending, path: \""<< path << "\"" << std::endl;
		return false;
	}
	
	std::string type = path.substr( dotPos + 1, path.size() );
	if( type.size() == 0 )
	{
		LOG_CONTENT_SYS << "File path do not contain a file ending, path: \""<< path << "\"" << std::endl;
		return false;
	}

	unsigned int hash = MurmurHash2( path.c_str(), path.size(), path.size() );

	int size = m_loaders.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_loaders[ i ]->AcceptType( type ) )
		{
			return m_loaders[ i ]->Load( type, path, hash, policy, m_loaders[ i ] );;
		}
	}

	LOG_CONTENT_SYS << "No loader is accepting ." << type << " files." << std::endl;
	return false;
}

void obs::ContenManager::Free( std::string path )
{
	int dotPos = path.find_last_of(".");
	if( dotPos < 0 )
	{
		LOG_CONTENT_SYS << "File path do not contain a file ending, path: \""<< path << "\"" << std::endl;
		return;
	}

	std::string type = path.substr( dotPos + 1, path.size() ); 
	unsigned int hash = MurmurHash2( path.c_str(), path.size(), path.size() );

	int size = m_loaders.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_loaders[ i ]->AcceptType( type ) )
		{
			m_loaders[ i ]->Free( hash );
			return;
		}
	}
}

void obs::ContenManager::AddLoader( obs::LoaderBase* loader )
{
	m_loaders.push_back( loader );
}

void obs::ContenManager::AddJobb( std::function<void()> function )
{
	m_contentThreadPool.Enqueue( function );
}

void obs::ContenManager::AddLoadEvent( std::function<void()> function )
{
	std::lock_guard<std::mutex> guard( m_eventMutex );
	m_eventList.push_back( function );
}

void obs::ContenManager::HandleEvents()
{
	// loading...
	int size = m_eventList.size(); 
	for( int i = 0; i < size; i++ )
	{
		m_eventList[ i ]();
	}
	m_eventList.clear();

	// deleting...
	size = m_loaders.size();
	for( int i = 0; i < size; i++ )
	{
		if( m_loaders[ i ]->CanFree() )
		{
			m_loaders[ i ]->DoFree();
		}
	}

}

void obs::ContenManager::WaitForOperations()
{
	m_contentThreadPool.Wait();
}

void* obs::ContenManager::GetContentData( std::string path )
{
	int dotPos = path.find_last_of(".");
	if( dotPos < 0 )
	{
		LOG_CONTENT_SYS << "File path do not contain a file ending, path: \""<< path << "\"" << std::endl;
		return nullptr;
	}

	std::string type = path.substr( dotPos + 1, path.size() ); 
	unsigned int hash = MurmurHash2( path.c_str(), path.size(), path.size() );

	int size = m_loaders.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_loaders[ i ]->AcceptType( type ) )
		{
			return m_loaders[ i ]->GetContentData( hash );
		}
	}
	return nullptr;
}

void obs::ContenManager::FreeFromValue( unsigned int value, std::string tag )
{

	int size = m_loaders.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_loaders[ i ]->AcceptType( tag ) )
		{
			AssetContainer* asset = m_loaders[ i ]->GetAssetFromValue( value );
			if( asset == nullptr )
			{
				LOG_ERROR << "Unable to perform free on value: " << value << " with the tag: " << tag << std::endl;
				return;
			}
			
			m_loaders[i]->Free( asset->hash );
			return;
		}
	}
}