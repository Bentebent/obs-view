#ifndef CONTENTMANAGER_HPP
#define CONTENTMANAGER_HPP

#include <ContentManagement/ContentLoadPolicyDefinition.hpp>
#include <ContentManagement/Loaders/LoaderBase.hpp>
#include <ThreadPool/Threadpool.hpp>

#include <vector>
#include <string>
#include <mutex>



namespace obs
{

	class ContenManager
	{
	public:
		/*!
			Does nothing.
		*/
		ContenManager();


		/*!
			Deletes all loaders and kills the threadpool.
		*/
		~ContenManager();

		/*
			Will initialize the internal threadPool.
			If this function is never called, all non-blocking loads will be blocking in practice.
		*/
		void InitializeThreadPool( int nrThreads );
		
		/*
			This call will remove all threads from the internal threadPool. All non-blocking calls 
			after this will be blocking in practice.
		*/
		void KillThreads();

		/*!
			Loading interface, path must include file ending. Policy is BLOCKING or NON_BLOCKING.
			Return true if successfull, even if file is already loaded.
		*/
		bool Load( std::string path, obs::ContentLoadPolicy policy );

		/*!
			Will decrease the reference count for the respective path and will delete it 
			should the count reach zero.
		*/
		void Free( std::string path );


		/*!
			Utility function used when initializing the ContentManager. The provided 
			loader is added to the list of loaders that is searched when loading a file path. 
			The ConentManager takes ownership of this pointer and 
			destorys it in the destructor.
		*/
		void AddLoader( obs::LoaderBase* loader );


		/*!
			Utility function used in the ContentSystem and handles finalizers for non-blocking loads.
			Also calls free on one item in each loader if applicable.
		*/
		void HandleEvents();

		/*!
			If non-blocking load is used to multithread loading of content, calling this function 
			will block the thread untill all loading is finished.
		*/
		void WaitForOperations();


		/*!
			Will return a void* to the struct associated with the file ending.
		*/
		void* GetContentData( std::string path );

		/*!
			Will decrease the reference count of the content corresonding to the handle stored in the data in the AssetContainer.
			Tag is one of the strings that the responsible loader listens to, i.e. ".obj" or ".mat".
		*/
		void FreeFromValue( unsigned int value, std::string tag );

	private:
		/*!
			Friend declaration so that the loaders may access the AddLoadEvent, among others.
		*/
		friend obs::LoaderBase;


		/*!
			Utility function used by LoaderBase and used to add load work to the threadPool.
		*/
		void AddJobb( std::function<void()> function );


		/*!
			Utility function used by LoaderBase to store finalizers. These functions are later called and popped
			in the ContentSystem in the SystemHandler.
		*/
		void AddLoadEvent( std::function<void()> function );

	private:
		std::vector< obs::LoaderBase* > m_loaders;
		std::vector< std::function<void()>> m_eventList;
		
		obs::ThreadPool m_contentThreadPool;
		std::mutex m_eventMutex;
	};

	extern ContenManager contentManager;
}

#endif