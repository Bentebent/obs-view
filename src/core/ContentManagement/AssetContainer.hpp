#ifndef ASSET_CONTAINER_HPP
#define ASSET_CONTAINER_HPP

#include <string>

enum AssetStatus
{
	LOADING,
	CACHED,
	UNLOADING,
};

/*!
	Generalized container for storing content. 
	Data pointer will be specific to the respective loader.
*/
struct AssetContainer
{
	AssetContainer( std::string path, AssetStatus status, unsigned int hash ) 
		: path( path ), status( status ), hash( hash ) 
	{
		references = 0;
		data = nullptr;
	}

	std::string path;
	AssetStatus status;
	unsigned int hash;
	unsigned int references;
	void* data;
};

#endif
