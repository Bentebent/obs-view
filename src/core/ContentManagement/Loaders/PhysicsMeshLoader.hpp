#ifndef CONTENT_PHYSICS_MESH_LOADER_HPP
#define CONTENT_PHYSICS_MESH_LOADER_HPP

#include <ContentManagement/Loaders/LoaderBase.hpp>

#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/PostProcess.h>
#include <assimp/Scene.h>

class btCollisionShape;
class btTriangleIndexVertexArray;
typedef float btScalar;

namespace obs
{
	struct PhysicsMeshData
	{
		std::vector< int > indicies;
		std::vector< btScalar > verticies;

		btTriangleIndexVertexArray* triangleIndexArray;
		btCollisionShape* shape;
	};

	class PhysicsMeshLoader : public LoaderBase
	{
	public:
		PhysicsMeshLoader();
		~PhysicsMeshLoader() override;

		/*!
			Will load the data to cache and return a pointer to it.
		*/
		void* LoadData( std::string type, std::string path ) override;
		
		/*!
			Will apply the result of all finished loads. Called from the ContentSystem-class.
		*/
		void FinishedLoading( unsigned int hash ) override;

		/*!
			Will free one of the hashes in the removal list.
		*/
		void FreeData() override;

		/*
			Compares the unsigned int to the pointer for the btCollisionShape.
		*/
		AssetContainer* GetAssetFromValue( unsigned int value ) override;

	private:
		Assimp::Importer m_importer;



	};
}

#endif