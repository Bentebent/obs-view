#ifndef CONTENT_LOADER_BASE_HPP
#define CONTENT_LOADER_BASE_HPP

#include <ContentManagement/AssetContainer.hpp>
#include <ContentManagement/ContentLoadPolicyDefinition.hpp>

#include <string>
#include <vector>



namespace obs
{
	class LoaderBase abstract
	{

	public:
		virtual ~LoaderBase() { }

		/*!
			Checks the m_acceptList for the "type" value. Type is file ending.
			Returns true if correct.
		*/
		bool AcceptType( std::string type );

		/*!
			fileType is ending of the file name.
			path is the file path to the asset.
			hash is the tag created from the contentManager and returned when loaded.
			policy is BLOCKING  or NON_BLOCKING. 
			loader is pointer to the loader, used to create the internal lambda functions.
		*/
		bool Load( std::string fileType, std::string path, unsigned int hash, obs::ContentLoadPolicy policy, LoaderBase* loader );

		/*!
			Will store the call to free the asset in the m_removalList to be freed in a lazy manner.
			hash is the tag created from the contentManager and returned when loaded.
		*/
		void Free( unsigned int hash );

		/*!
			Will load the associated data to cache.
		*/
		virtual void* LoadData( std::string type, std::string path ) { return nullptr; }

		/*!
			Called once the asset has been loaded to cache.
		*/
		virtual void FinishedLoading( unsigned int hash ) { }
		
		/*!
			Returns true if the m_removalList is not empty.
		*/
		bool CanFree();

		/*!
			Will unallocate one or several instances of handles stored in the m_removalList. Will call the loaders FreeData function.
		*/
		void DoFree();

		/*!
			Will return a pointer to the corresponding struct of the expected data. 
		*/
		void* GetContentData( unsigned int hash );

		/*!
			Will scan assets for the value handle.
			Return is the pointer to the assetContainer or nullptr if not found.
		*/
		virtual AssetContainer* GetAssetFromValue( unsigned int value );

	protected:
		/*!
			Will unallocate one or several instances of handles stored in the m_removalList. Specific to every loader.
		*/
		virtual void FreeData() { }

		std::vector< AssetContainer > m_assetContainers;
		std::vector< std::string > m_acceptList;
		std::vector< unsigned int > m_removalList;

	private:

	};
}
#endif