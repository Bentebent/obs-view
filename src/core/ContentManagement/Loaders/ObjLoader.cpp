#include "ObjLoader.hpp"
#include <logger/Logger.hpp>



obs::ObjLoader::ObjLoader()
{
	m_acceptList.push_back( "dae" );
	m_acceptList.push_back( "obj" );
	m_acceptList.push_back( "__luaMeshFree" );
}


obs::ObjLoader::~ObjLoader()
{
}


#include <thread>
#include <chrono>
void* obs::ObjLoader::LoadData( std::string type, std::string path ) 
{
	std::ifstream file(path, std::ios::in);
	if (!file.fail())
	{
		file.close();
	}
	else
	{
		LOG_ERROR << "Couldn't open file: " << path << std::endl;
		return nullptr;
	}

	const aiScene* scene = m_importer.ReadFile( path, aiProcessPreset_TargetRealtime_Quality | aiProcess_JoinIdenticalVertices );
	

	if (!scene)
	{
		std::cout << m_importer.GetErrorString() << std::endl;
		return nullptr;
	}
	return (void*)scene;
}

#include <gfx/GFXInterface.hpp>

void obs::ObjLoader::FinishedLoading( unsigned int hash ) 
{
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			aiScene* scene = (aiScene*)it->data;
			for (int i = 0; i < scene->mNumMeshes; i++)
			{
				aiMesh* mesh = scene->mMeshes[i];
			

				// faces...
				std::vector<int> faces;
				int nrFaces = mesh->mNumFaces;
				faces.resize( nrFaces * 3 );

				unsigned int faceIndex = 0;
				for (unsigned int t = 0; t < mesh->mNumFaces; t++)
				{
					const aiFace* face = &mesh->mFaces[t];
					memcpy(&faces[faceIndex], face->mIndices, 3 * sizeof(unsigned int));
					faceIndex += 3;
				}

				// vertices...
				std::vector< gfx::StaticVertex > vertices;
				int nrVertices = mesh->mNumVertices;

				bool hasNormals = scene->mMeshes[i]->HasNormals();
				bool hasTangents = scene->mMeshes[i]->HasTangentsAndBitangents();
				bool hasUv = scene->mMeshes[i]->HasTextureCoords(0);

				for (int i = 0; i < nrVertices; i++)
				{
					gfx::StaticVertex v;
					v.position.x = mesh->mVertices[i].x;
					v.position.y = mesh->mVertices[i].y;
					v.position.z = mesh->mVertices[i].z;
					v.position.w = 1.0f;
			
					if( hasNormals )
					{
						v.normal.x = mesh->mNormals[i].x;
						v.normal.y = mesh->mNormals[i].y;
						v.normal.z = mesh->mNormals[i].z;
						v.normal.w = 0.0f;
					}
					else
					{
						memset( &v.normal[0], 0, sizeof(glm::vec4) );
					}

					if( hasTangents )
					{
						v.tangent.x = mesh->mTangents[i].x;
						v.tangent.y = mesh->mTangents[i].y;
						v.tangent.z = mesh->mTangents[i].z;
						v.tangent.w = 0.0f;
					}
					else
					{
						memset( &v.normal[0], 0, sizeof(glm::vec4) );
					}
					
					if( hasUv )
					{
						v.uv.x = mesh->mTextureCoords[0][i].x;
						v.uv.y = mesh->mTextureCoords[0][i].y;
					}
					else
					{
						memset( &v.uv[0], 0, sizeof(glm::vec2) );
					}

					vertices.push_back(v);
				}

				

				unsigned int IdOfMesh = 0;
				int nrIndicies = faces.size();
				gfx::content::LoadMesh<gfx::StaticVertex>(IdOfMesh, nrVertices, nrIndicies, vertices.data(), faces.data());
				

				m_importer.FreeScene();
				it->data = new obs::ObjModelData();
				((ObjModelData*)it->data)->modelID = IdOfMesh;
				break;
			}
			break;
		}
	}
}


void obs::ObjLoader::FreeData() 
{
	unsigned int hash = m_removalList.back();
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			gfx::content::DeleteMesh( ((ObjModelData*)it->data)->modelID );
			delete it->data;

			return;
		}
	}
}


AssetContainer* obs::ObjLoader::GetAssetFromValue( unsigned int value ) 
{
	int size = m_assetContainers.size();
	for( int i = 0; i < size; i++ )
	{
		obs::ObjModelData* data = (obs::ObjModelData*)m_assetContainers[i].data;
		if( data != nullptr )
		{
			if( data->modelID == value )
			{
				return &m_assetContainers[i];
			}
		}
	}

	return nullptr;
}