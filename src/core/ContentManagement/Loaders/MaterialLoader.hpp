#ifndef SRC_CORE_CONTENTMANAGEMENT_LOADERS_MATERIALLOADER_HPP
#define SRC_CORE_CONTENTMANAGEMENT_LOADERS_MATERIALLOADER_HPP

#include <ContentManagement/Loaders/LoaderBase.hpp>
#include <gfx/GFXMaterial.hpp>

#include <vector>
#include <fstream>
#include <string>

namespace obs
{
	struct MaterialData
	{
		unsigned int materialID;
		unsigned int shaderID;
		std::vector<std::string> texturePaths;
	};

	class MaterialLoader : public LoaderBase
	{
	public:
		MaterialLoader();
		~MaterialLoader() override;

		void* LoadData( std::string type, std::string path) override;
		void FinishedLoading(unsigned int hash) override;

		void FreeData() override;

		AssetContainer* GetAssetFromValue( unsigned int value ) override;

	private:
		void LoadShader();
		void LoadTexture(std::string line, gfx::TextureType textureType, unsigned int materialID);

	};
}

#endif