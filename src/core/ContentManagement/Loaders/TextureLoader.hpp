#ifndef SRC_CORE_CONTENTMANAGEMENT_LOADERS_TEXTURELOADER_HPP
#define SRC_CORE_CONTENTMANAGEMENT_LOADERS_TEXTURELOADER_HPP

#include <ContentManagement/Loaders/LoaderBase.hpp>
#include <gfx/GFXMaterial.hpp>

#include <vector>
#include <fstream>
#include <string>

namespace obs
{
	struct TextureData
	{
		unsigned int textureID;
	};

	class TextureLoader : public LoaderBase
	{
	public:
		TextureLoader();
		~TextureLoader() override;

		void* LoadData(std::string type, std::string path) override;
		void FinishedLoading(unsigned int hash) override;

		void FreeData() override;

	private:
		struct TextureLoadingData
		{
			unsigned int width;
			unsigned int height;
			unsigned char* data;
			std::string path;
		};

	};
}

#endif