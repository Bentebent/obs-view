#ifndef CONTENT_OBJ_LOADER_HPP
#define CONTENT_OBJ_LOADER_HPP

#include <ContentManagement/Loaders/LoaderBase.hpp>

#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/PostProcess.h>
#include <assimp/Scene.h>

#include <gfx/GFXInstanceData.hpp>

namespace obs
{
	struct ObjModelData
	{
		int modelID;
	};

	class ObjLoader : public LoaderBase
	{
	public:
		ObjLoader();
		~ObjLoader() override;

		/*!
			Will load the data to cache and return a pointer to it.
		*/
		void* LoadData( std::string type, std::string path ) override;
		
		/*!
			Will apply the result of all finished loads. Called from the ContentSystem-class.
		*/
		void FinishedLoading( unsigned int hash ) override;

		/*!
			Will free one of the hashes in the removal list.
		*/
		void FreeData() override;

		/*
			Will scan the assetContainers for the openglHandle provided and return the hash.
		*/
		AssetContainer* GetAssetFromValue( unsigned int value ) override;

	private:
		Assimp::Importer m_importer;



	};
}

#endif