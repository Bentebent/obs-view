#include "PhysicsMeshLoader.hpp"
#include <logger/Logger.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionShapes/btShapeHull.h>

#include <bullet/HACD/hacdCircularList.h>
#include <bullet/HACD/hacdVector.h>
#include <bullet/HACD/hacdICHull.h>
#include <bullet/HACD/hacdGraph.h>
#include <bullet/HACD/hacdHACD.h>

#include <bullet/ConvexDecomposition/ConvexBuilder.h>


obs::PhysicsMeshLoader::PhysicsMeshLoader()
{
	m_acceptList.push_back( "bullet" );
}


obs::PhysicsMeshLoader::~PhysicsMeshLoader()
{
}

// helper function. Will return the path withough the last file ending
// so that assimp knows how to load it.
std::string StripLastEnding( std::string type, std::string path )
{
	return path.substr( 0, path.size() - type.size() - 1 );
}




void* obs::PhysicsMeshLoader::LoadData( std::string type, std::string path ) 
{
	std::string assimpPath = StripLastEnding( type, path );

	std::ifstream file(assimpPath, std::ios::in);
	if (!file.fail())
	{
		file.close();
	}
	else
	{
		LOG_ERROR << "Couldn't open file: " << assimpPath << std::endl;
		return nullptr;
	}

	const aiScene* scene = m_importer.ReadFile( assimpPath, aiProcessPreset_TargetRealtime_Quality | aiProcess_JoinIdenticalVertices );
	

	if (!scene)
	{
		std::cout << m_importer.GetErrorString() << std::endl;
		return nullptr;
	}

	obs::PhysicsMeshData* data = nullptr;

	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];
		data = new obs::PhysicsMeshData();

		// faces...
		int nrFaces = mesh->mNumFaces;
		data->indicies.reserve( nrFaces * 3 );
	
		for (unsigned int t = 0; t < mesh->mNumFaces; t++)
		{
			const aiFace* face = &mesh->mFaces[t];
			data->indicies.push_back( face->mIndices[0] );
			data->indicies.push_back( face->mIndices[1] );
			data->indicies.push_back( face->mIndices[2] );
		}


		int nrVertices = mesh->mNumVertices;	
		data->verticies.reserve( nrVertices );

		for (unsigned int t = 0; t < nrVertices; t++)
		{
			data->verticies.push_back( mesh->mVertices[t].x );
			data->verticies.push_back( mesh->mVertices[t].y );
			data->verticies.push_back( mesh->mVertices[t].z );
		}		
		
		// create trimesh
		btTriangleIndexVertexArray* indexVertexArrays = 
			new btTriangleIndexVertexArray(
				nrFaces,
				data->indicies.data(),
				3 * sizeof( int ),
				nrVertices,
				data->verticies.data(),
				sizeof( btScalar ) * 3 );


		btGImpactMeshShape * trimesh = new btGImpactMeshShape(indexVertexArrays);
						
		data = new obs::PhysicsMeshData();
		data->triangleIndexArray	= indexVertexArrays;
		data->shape					= trimesh;

		break;
	}
	
	return (void*)data;
}


void obs::PhysicsMeshLoader::FinishedLoading( unsigned int hash ) 
{
}


void obs::PhysicsMeshLoader::FreeData() 
{
	unsigned int hash = m_removalList.back();
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			obs::PhysicsMeshData* data = (obs::PhysicsMeshData*)it->data;
			
			delete data->shape;
			delete data->triangleIndexArray;

			return;
		}
	}
}


AssetContainer* obs::PhysicsMeshLoader::GetAssetFromValue( unsigned int value ) 
{
	int size = m_assetContainers.size();
	for( int i = 0; i < size; i++ )
	{
		obs::PhysicsMeshData* data = (obs::PhysicsMeshData*)m_assetContainers[i].data;
		if( data != nullptr )
		{
			if( data->shape == (btCollisionShape*)value )
			{
				return &m_assetContainers[i];
			}
		}
	}

	return nullptr;
}



/* previous version of loader for static unmovable triangle meshes

struct PhysicsMeshData
{
	btTriangleMesh* triangleMesh;
	btCollisionShape* shape;
	btConvexHullShape* convexHullShape;
};

void* obs::PhysicsMeshLoader::LoadData( std::string type, std::string path ) 
{
	float scale = 100.0f;

	std::string assimpPath = StripLastEnding( type, path );

	std::ifstream file(assimpPath, std::ios::in);
	if (!file.fail())
	{
		file.close();
	}
	else
	{
		LOG_ERROR << "Couldn't open file: " << assimpPath << std::endl;
		return nullptr;
	}

	const aiScene* scene = m_importer.ReadFile( assimpPath, aiProcessPreset_TargetRealtime_Quality | aiProcess_JoinIdenticalVertices );
	

	if (!scene)
	{
		std::cout << m_importer.GetErrorString() << std::endl;
		return nullptr;
	}

	obs::PhysicsMeshData* data = nullptr;

	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];

		// faces...
		std::vector<int> indicies;
		int nrFaces = mesh->mNumFaces;
		indicies.resize( nrFaces * 3 );
	
		unsigned int faceIndex = 0;
		for (unsigned int t = 0; t < mesh->mNumFaces; t++)
		{
			const aiFace* face = &mesh->mFaces[t];
			memcpy(&indicies[faceIndex], face->mIndices, 3 * sizeof(unsigned int));
			faceIndex += 3;
		}
	
		int nrVertices = mesh->mNumVertices;	
		btTriangleMesh* trimesh = new btTriangleMesh();

		for( int p = 0; p < nrFaces; p++ )
		{
			btVector3 point[3];
			for( int k = 0; k < 3; k++ )
			{
				point[ k ].setX( mesh->mVertices[ indicies[ p * 3 + k ] ].x * scale );
				point[ k ].setY( mesh->mVertices[ indicies[ p * 3 + k ] ].y * scale );
				point[ k ].setZ( mesh->mVertices[ indicies[ p * 3 + k ] ].z * scale );
				point[ k ].setW( 1.0f );
			}

			trimesh->addTriangle( point[0], point[1], point[2] );
		}

		//create a hull approximation
		btConvexShape* tmpConvexShape = new btConvexTriangleMeshShape( trimesh );
		btShapeHull* hull = new btShapeHull( tmpConvexShape );
		btScalar margin = tmpConvexShape->getMargin();
		hull->buildHull(margin);
		tmpConvexShape->setUserPointer(hull);

		// bv utility...
		btConvexHullShape* convexShape = new btConvexHullShape();

		for( i=0; i < hull->numVertices(); i++ )
		{
			convexShape->addPoint( hull->getVertexPointer()[ i ], false );
		}
		convexShape->recalcLocalAabb();

		delete tmpConvexShape;
		delete hull;

		bool useQuantization = true;
		btCollisionShape* concaveShape = new btBvhTriangleMeshShape(trimesh,useQuantization);



				
		data = new obs::PhysicsMeshData();
		data->triangleMesh		= trimesh;
		data->shape				= concaveShape;
		data->convexHullShape	= convexShape;

		break;
	}
	
	return (void*)data;
}


void obs::PhysicsMeshLoader::FinishedLoading( unsigned int hash ) 
{
}


void obs::PhysicsMeshLoader::FreeData() 
{
	unsigned int hash = m_removalList.back();
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			obs::PhysicsMeshData* data = (obs::PhysicsMeshData*)it->data;
			
			delete data->triangleMesh;
			delete data->shape;
			delete data->convexHullShape;

			return;
		}
	}
}
*/