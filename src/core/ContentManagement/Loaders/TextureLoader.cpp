#include "TextureLoader.hpp"
#include <GLEW/glew.h>
#include <utility/stb_image.hpp>
#include <gfx/GFXInterface.hpp>

namespace obs
{
	TextureLoader::TextureLoader()
	{
		m_acceptList.push_back("png");
		m_acceptList.push_back("jpg");
		m_acceptList.push_back("jpeg");
	}

	TextureLoader::~TextureLoader()
	{

	}

	void* TextureLoader::LoadData(std::string type, std::string path)
	{
		TextureLoadingData* tld = new TextureLoadingData();

		unsigned int textureID;
		unsigned char* data;
		GLuint textureHandle;
		int width, height;
		data = stbi_load(path.c_str(), &width, &height, 0, 0);

		tld->data = data;
		tld->width = width;
		tld->height = height;
		tld->path = path;

		return (void*)tld;
	}

	void TextureLoader::FinishedLoading(unsigned int hash)
	{

		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				TextureLoadingData* tld = (TextureLoadingData*)it->data;

				if (tld->data != nullptr)
				{
					unsigned int textureID;
					gfx::content::LoadTexture(textureID, tld->path.c_str(), tld->data, tld->width, tld->height);
					
					stbi_image_free(tld->data);
					delete tld;
					it->data = new TextureData();
					((TextureData*)it->data)->textureID = textureID;
				}
			}
		}
	}

	void TextureLoader::FreeData()
	{
		unsigned int hash = m_removalList.back();
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				gfx::content::DeleteTexture(((TextureData*)it->data)->textureID);
				delete it->data;

				return;
			}
		}
	}
}