#include "LoaderBase.hpp"
#include <ContentManagement/AssetContainer.hpp>
#include <ContentManagement/ContentManager.hpp>
#include <logger/Logger.hpp>

bool obs::LoaderBase::AcceptType( std::string type ) 
{
	int size = m_acceptList.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_acceptList[i].compare( type ) == 0 )
		{
			return true;
		}
	}
	return false;
}

bool obs::LoaderBase::Load( std::string fileType, std::string path, unsigned int hash, obs::ContentLoadPolicy policy, LoaderBase* loader )
{
	// check if already loaded...
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{	
			it->references++;
			if( it->status == AssetStatus::UNLOADING )
			{
				it->status == AssetStatus::CACHED;
				for( auto objectUpForRemove = m_removalList.begin(); objectUpForRemove != m_removalList.end(); objectUpForRemove++ )
				{
					if( *objectUpForRemove == hash )
					{
						m_removalList.erase( objectUpForRemove );
						return true;
					}
				}
			}
			return true;
		}
	}

	// not loaded, perform load...
	m_assetContainers.push_back( AssetContainer( path, AssetStatus::LOADING, hash ) );
	AssetContainer& newAsset = m_assetContainers.back();
	newAsset.references++;

	if( policy == obs::ContentLoadPolicy::BLOCKING )
	{
		newAsset.data = loader->LoadData( fileType, path );
		if( newAsset.data == nullptr )
		{
			LOG_CONTENT_SYS << "Error occured while loading file \"" << path << "\"" << std::endl;
			return false;
		}
		newAsset.status = AssetStatus::CACHED;
		loader->FinishedLoading( hash );
	}
	else
	{
		contentManager.AddJobb( [ loader, path, fileType, this, hash ]()
		{
			// make the load call...
			void* data = loader->LoadData( fileType, path );
			if( data == nullptr )
			{
				LOG_CONTENT_SYS << "Error occured while loading file \"" << path << "\"" << std::endl;
				return;
			}

			// mark the asset as loaded...
			for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
			{
				if( it->hash == hash )
				{	
					it->data = data;
					contentManager.AddLoadEvent( [loader, hash, this]() 
					{ 						
						for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
						{
							if( it->hash == hash )
							{	
								loader->FinishedLoading( hash );
								it->status = AssetStatus::CACHED;
								return;
							}

							// possible memory leak!
							// load, load complete - add finishedEvent, free, handle event... fixed by order? 

						}
					});
					return;
				}
			}

			// asset is no longer in the list, delete loaded data
			// no destructor call implemented atm.
			if( data )
			{
				delete data;
				return;
			}
		});
		return true;
	}
}

bool obs::LoaderBase::CanFree()
{
	if( m_removalList.size() == 0 )
	{
		return false;
	}
	return true;

}

void obs::LoaderBase::Free( unsigned int hash )
{
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{	
			it->references--;
			if( it->references < 1 )
			{
				it->status = AssetStatus::UNLOADING;
				m_removalList.push_back( hash );
			}

			return;
		}
	}
}

void obs::LoaderBase::DoFree()
{
	unsigned int hash = m_removalList.back();
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			FreeData();
			m_removalList.pop_back();
			m_assetContainers.erase( it );
			break;
		}
	}

}

void* obs::LoaderBase::GetContentData( unsigned int hash )
{
	for( auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++ )
	{
		if( it->hash == hash )
		{
			return it->data;
		}
	}
	return nullptr;
}



AssetContainer* obs::LoaderBase::GetAssetFromValue( unsigned int value )
{
	return nullptr;
}