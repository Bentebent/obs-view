#include "MaterialLoader.hpp"
#include <gfx/GFXInterface.hpp>

#include <iostream>

#include <utility/stb_image.hpp>
#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/Loaders/TextureLoader.hpp>

namespace obs
{
	MaterialLoader::MaterialLoader()
	{
		m_acceptList.push_back("mat");
		m_acceptList.push_back("__luaMaterialFree");
	}

	MaterialLoader::~MaterialLoader()
	{

	}

	void* MaterialLoader::LoadData( std::string type, std::string path )
	{
		std::ifstream myfile(path);

		if (myfile.is_open())
		{
			return &path;
		}
		return nullptr;
	}

	void MaterialLoader::FinishedLoading(unsigned int hash)
	{
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				std::string line;
				std::ifstream myfile(it->path);

				unsigned long long int materialID;

				if (myfile.is_open())
				{
					gfx::content::CreateMaterial(materialID);
					
					it->data = new MaterialData();
					

					while (getline(myfile, line))
					{
						if (line == "ALBEDO")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::ALBEDO, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "SPECULAR")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::SPECULAR, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "AO_CAVITY_ROUGHNESS")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::AO_CAVITY_ROUGHNESS, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "NORMAL")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::NORMAL, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "DISPLACEMENT")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::DISPLACEMENT, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "EMISSIVE")
						{
							getline(myfile, line);
							LoadTexture(line, gfx::TextureType::EMISSIVE, materialID);
							((MaterialData*)it->data)->texturePaths.push_back(line);
						}
						else if (line == "SHADER")
						{
							unsigned int shaderID;
							getline(myfile, line);
							gfx::content::GetShaderID(shaderID, line.c_str());
							((MaterialData*)it->data)->shaderID = shaderID;
							std::cout << shaderID << std::endl;
						}
					}
				}
				((MaterialData*)it->data)->materialID = materialID;
			}
		}
	}

	void MaterialLoader::LoadTexture(std::string line, gfx::TextureType textureType, unsigned int materialID)
	{
		unsigned int textureID;
		contentManager.Load(line, obs::ContentLoadPolicy::BLOCKING);
		textureID = ((TextureData*)contentManager.GetContentData(line))->textureID;
		gfx::content::SetMaterialTexture(materialID, textureID, textureType);
	}

	void MaterialLoader::FreeData()
	{
		unsigned int hash = m_removalList.back();
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				for (int i = 0; i < ((MaterialData*)it->data)->texturePaths.size(); i++)
					contentManager.Free(((MaterialData*)it->data)->texturePaths[i]);

				unsigned long long int tempID = ((MaterialData*)it->data)->materialID;
				gfx::content::DeleteMaterial(tempID);
				delete it->data;

				return;
			}
		}
	}
}

AssetContainer* obs::MaterialLoader::GetAssetFromValue( unsigned int value ) 
{
	int size = m_assetContainers.size();
	for( int i = 0; i < size; i++ )
	{
		obs::MaterialData* data = (obs::MaterialData*)m_assetContainers[i].data;
		if( data != nullptr )
		{
			if( data->materialID == value )
			{
				return &m_assetContainers[i];
			}
		}
	}

	return nullptr;
}