#ifndef CONTENT_LOAD_POLICY_STRUCT_HPP
#define CONTENT_LOAD_POLICY_STRUCT_HPP





namespace obs
{
	enum ContentLoadPolicy
	{ 
		BLOCKING,
		NON_BLOCKING,
	};
}

#endif