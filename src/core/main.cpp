

#include <CoreFramework/Core.hpp>

#include <chrono>
#include <iostream>

#include "Systems/TestSystem.hpp"
#include <CoreUtility/WindowHandler.hpp>
#include <CoreUtility/WindowModes.hpp>
#include <gfx/GFXInterface.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXSettingTypes.hpp>

#include <logger/Logger.hpp>

#include <Systems/TestSystem.hpp>
#include <Components/TestComponent.hpp>
#include <Components/TransformationComponent.hpp>
#include <Components/RenderingComponent.hpp>

#include <CoreFramework/AllocatorInstances.hpp>

#include <Camera/Camera.hpp>

#include <ContentManagement/Loaders/ObjLoader.hpp>
#include <ContentManagement/Loaders/MaterialLoader.hpp>
#include <ContentManagement/ContentManager.hpp>

#include <utility/stb_image.hpp>
#include <utility/BitmaskHelper.hpp>
#include <utility/HighresTimer.hpp>



#include <chrono>

void TestBitmasks()
{
	//BitmaskSystem bitmaskSystem = BitmaskSystem();
	//int rv = bitmaskSystem.Init();
	//assert(rv == 0);
	//bitmaskSystem.PrintDebug();
	//
	//Bitmask testMask;
	//testMask = 0;
	//testMask = bitmaskSystem.Encode(testMask, 11, MaskType::type0);
	//testMask = bitmaskSystem.Encode(testMask, 22, MaskType::type1);
	//testMask = bitmaskSystem.Encode(testMask, 33, MaskType::type2);
	//testMask = bitmaskSystem.Encode(testMask, 44, MaskType::type3);
	//testMask = bitmaskSystem.Encode(testMask, 55, MaskType::type4);
	//testMask = bitmaskSystem.Encode(testMask, 66, MaskType::type5);
	//testMask = bitmaskSystem.Encode(testMask, 77, MaskType::type6);
	//testMask = bitmaskSystem.Encode(testMask, 88, MaskType::type7);
	//
	//std::cout << "\n";
	//bitmaskSystem.PrintBitmask(testMask);
	//std::cout << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type0) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type1) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type2) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type3) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type4) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type5) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type6) << "\n";
	//std::cout << bitmaskSystem.Decode(testMask, MaskType::type7) << "\n";
}

void DrawSomeDebugStuff()
{
	//gfx::debug::DrawString("Ωβμ∞√∑§δ€Φ YEAÄÄÄöÖÖåäöäÅÖÄH!!!! UNICODE, BITCH!!!",
	//	100, 100, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	//
	//gfx::debug::DrawString("Red is the color of blood!",
	//	100, 120, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	//
	//gfx::debug::DrawString("The green is greener on the other side.",
	//	100, 140, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	//
	//gfx::debug::DrawString("I'm blue dabadee dabadoo!",
	//	100, 160, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

	for (int i = 0; i < 256; i++)
	{
		gfx::debug::DrawLine(
			glm::vec3(10.0f * sin(0.5f * (float)i), 10.0f * cos(0.5f * (float)i), 1.0f * (float)i),
			glm::vec3(10.0f * sin(0.5f * float(i + 1)), 10.0f * cos(0.5f * float(i + 1)), 1.0f * float(i + 1)),
			glm::vec4(sin(i * 0.1f), sin(i * 0.1f + 2), sin(i * 0.1f + 4), 1.0f));
	}

	for (int i = 0; i < 17; i++)
	{
		gfx::debug::DrawSphere(glm::vec3(-25.0f, 0.0f, (float)i * 15.0f + 5.0f), 10.0f,
			glm::vec4(sin(i), sin(i + 2), sin(i + 4), 1.0f));
	}

	for (int i = 0; i < 17; i++)
	{
		gfx::debug::DrawAABB(glm::vec3(-55.0f, -5.0f, (float)i * 15.0f - 5.0f), glm::vec3(-45.0f, 5.0f, (float)i * 15.0f + 5.0f),
			glm::vec4(sin(2*i), sin(2*i + 2), sin(2*i + 4), 1.0f));
	}

	gfx::debug::DrawString("This is a character that does not exist: �, This one too: ௵",
		200, 200, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	// Should OBB be implemented? If so, use euler, matrix or quaternion for rotation?
	//gfx::debug::DrawOBB(glm::vec3(-75.0f, -5.0f, -5.0f), glm::vec3(-65.0f, 5.0f, 5.0f),
	//	glm::quat(0.881f, 0.278f, -0.115f, 0.365f), // Rotated first 45 degrees around z, then 35 degrees around x
	//	glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
}

void TestRendering()
{
	obs::Camera* c = obs::cameraHandler.GetActiveCamera();

	unsigned int matID = 0;
	unsigned int shaderID = 0;
	obs::contentManager.Load("content/pbr_textures/Gold.mat", obs::ContentLoadPolicy::BLOCKING);
	matID = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/Gold.mat"))->materialID;

	unsigned int matID2 = 0;
	obs::contentManager.Load("content/pbr_textures/rough_steel.mat", obs::ContentLoadPolicy::BLOCKING);
	matID2 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/rough_steel.mat"))->materialID;

	unsigned int matID3 = 0;
	obs::contentManager.Load("content/pbr_textures/chrome.mat", obs::ContentLoadPolicy::BLOCKING);
	matID3 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/chrome.mat"))->materialID;
	
	unsigned int matID4 = 0;
	obs::contentManager.Load("content/pbr_textures/brushed_metal.mat", obs::ContentLoadPolicy::BLOCKING);
	matID4 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/brushed_metal.mat"))->materialID;

	unsigned int matID5 = 0;
	obs::contentManager.Load("content/pbr_textures/satin.mat", obs::ContentLoadPolicy::BLOCKING);
	matID5 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/satin.mat"))->materialID;

	unsigned int matID6 = 0;
	obs::contentManager.Load("content/pbr_textures/rubber.mat", obs::ContentLoadPolicy::BLOCKING);
	matID6 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/rubber.mat"))->materialID;

	unsigned int matID7 = 0;
	obs::contentManager.Load("content/pbr_textures/diffuse.mat", obs::ContentLoadPolicy::BLOCKING);
	matID7 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/diffuse.mat"))->materialID;

	unsigned int matID8 = 0;
	obs::contentManager.Load("content/pbr_textures/painted_metal.mat", obs::ContentLoadPolicy::BLOCKING);
	matID8 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/painted_metal.mat"))->materialID;

	unsigned int matID9 = 0;
	obs::contentManager.Load("content/pbr_textures/mirror.mat", obs::ContentLoadPolicy::BLOCKING);
	matID9 = ((obs::MaterialData*) obs::contentManager.GetContentData("content/pbr_textures/mirror.mat"))->materialID;

	shaderID = ((obs::MaterialData*)obs::contentManager.GetContentData("content/pbr_textures/Gold.mat"))->shaderID;



	//obs::contentManager.Free("content/materialtest.mat");
	//obs::contentManager.Free("content/materialtest2.mat");

	unsigned int meshID1 = 0;
	//obs::contentManager.Load( "content/apa.dae", obs::ContentLoadPolicy::BLOCKING );
	//meshID1 = ((obs::ObjModelData*)obs::contentManager.GetContentData( "content/apa.dae" ))->modelID;

	unsigned int meshID2 = 0;
	//obs::contentManager.Load( "content/testBox.dae", obs::ContentLoadPolicy::BLOCKING );
	//meshID2 = ((obs::ObjModelData*)obs::contentManager.GetContentData( "content/testBox.dae" ))->modelID;

	unsigned int meshID3 = 0;
	//obs::contentManager.Load("content/spaceship.obj", obs::ContentLoadPolicy::BLOCKING);
	//meshID3 = ((obs::ObjModelData*)obs::contentManager.GetContentData("content/spaceship.obj"))->modelID;

	unsigned int meshID4 = 0;
	obs::contentManager.Load("content/uv_sphere.obj", obs::ContentLoadPolicy::BLOCKING);
	meshID4 = ((obs::ObjModelData*)obs::contentManager.GetContentData("content/uv_sphere.obj"))->modelID;

	unsigned int meshID5 = 0;
	obs::contentManager.Load("content/plane.obj", obs::ContentLoadPolicy::BLOCKING);
	meshID5 = ((obs::ObjModelData*)obs::contentManager.GetContentData("content/plane.obj"))->modelID;

	unsigned int meshID6 = 0;
	//obs::contentManager.Load("content/dragon.obj", obs::ContentLoadPolicy::BLOCKING);
	//meshID6 = ((obs::ObjModelData*)obs::contentManager.GetContentData("content/dragon.obj"))->modelID;

	obs::contentManager.WaitForOperations();

	std::vector< int > objects;

	//BitmaskSystem bitmaskSystem = BitmaskSystem();



	LOG_DEBUG << "content has been loaded" << std::endl;
	int count = 0;
	srand(time(0));

	objects.push_back(obs::dataHandler.AllocateObject());
	obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());

	obs::dataHandler.AllocateData< obs::RenderingComponent >(objects.back());

	obs::TransformationComponent* foobar = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
	//tc->position[0] = i * 5;
	//tc->position[1] = y * 5;
	foobar->position[0] = 125.0f;
	foobar->position[1] = -20.0f;
	foobar->position[2] = -125.0f;

	foobar->scale[0] = 125.0f;
	foobar->scale[1] = 1.0f;
	foobar->scale[2] = 250.0f;

	obs::RenderingComponent* asdf = obs::dataHandler.GetData<obs::RenderingComponent>(objects.back());
	asdf->renderMe = true;

	Bitmask hurf = 0;
	hurf = obs::bitmaskSystem.Encode(hurf, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
	hurf = obs::bitmaskSystem.Encode(hurf, gfx::OBJECT_TYPES::OPAQUE_GEOMETRY, MaskType::TYPE);
	hurf = obs::bitmaskSystem.Encode(hurf, shaderID, MaskType::SHADER);
	hurf = obs::bitmaskSystem.Encode(hurf, 0, MaskType::VIEWPORT_ID);
	hurf = obs::bitmaskSystem.Encode(hurf, matID6, MaskType::TEXTURE);
	hurf = obs::bitmaskSystem.Encode(hurf, meshID5, MaskType::MESH);
	asdf->bitmask = hurf;

	objects.push_back(obs::dataHandler.AllocateObject());
	obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());

	obs::dataHandler.AllocateData< obs::RenderingComponent >(objects.back());

	foobar = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
	//tc->position[0] = i * 5;
	//tc->position[1] = y * 5;
	foobar->position[0] = -125.0f;
	foobar->position[1] = -20.0f;
	foobar->position[2] = -125.0f;

	foobar->scale[0] = 125.0f;
	foobar->scale[1] = 1.0f;
	foobar->scale[2] = 250.0f;

	asdf = obs::dataHandler.GetData<obs::RenderingComponent>(objects.back());
	asdf->renderMe = true;

	hurf = 0;
	hurf = obs::bitmaskSystem.Encode(hurf, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
	hurf = obs::bitmaskSystem.Encode(hurf, gfx::OBJECT_TYPES::OPAQUE_GEOMETRY, MaskType::TYPE);
	hurf = obs::bitmaskSystem.Encode(hurf, shaderID, MaskType::SHADER);
	hurf = obs::bitmaskSystem.Encode(hurf, 0, MaskType::VIEWPORT_ID);
	hurf = obs::bitmaskSystem.Encode(hurf, matID, MaskType::TEXTURE);
	hurf = obs::bitmaskSystem.Encode(hurf, meshID5, MaskType::MESH);
	asdf->bitmask = hurf;



	for (int y = 0; y < 50; y++)
	{
		for (int i = 0; i < 50; i++)
		{
			objects.push_back(obs::dataHandler.AllocateObject());
			obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());

			obs::dataHandler.AllocateData< obs::RenderingComponent >(objects.back());

			obs::TransformationComponent* tc = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
			//tc->position[0] = i * 5;
			//tc->position[1] = y * 5;
			tc->position[0] = rand() % 250;
			tc->position[1] = rand() % 250 - 20;
			tc->position[2] = rand() % 50;
			float randScale = rand() % 5 / 5.0f + 0.5f;
			tc->scale[0] = randScale;
			tc->scale[1] = randScale;
			tc->scale[2] = randScale;
			obs::TransformationComponent::RotateY(*tc, i * y * 0.1f);

			obs::RenderingComponent* rc = obs::dataHandler.GetData<obs::RenderingComponent>(objects.back());
			rc->renderMe = true;

			Bitmask myMask = 0;
			myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
			myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::OPAQUE_GEOMETRY, MaskType::TYPE);
			myMask = obs::bitmaskSystem.Encode(myMask, shaderID, MaskType::SHADER);
			myMask = obs::bitmaskSystem.Encode(myMask, 0, MaskType::VIEWPORT_ID);

			int foo = i % 2;
			int bar = i % 9;

			if (bar == 0)
				myMask = obs::bitmaskSystem.Encode(myMask, matID, MaskType::TEXTURE);
			else if (bar == 1)
				myMask = obs::bitmaskSystem.Encode(myMask, matID2, MaskType::TEXTURE);
			else if (bar == 2)
				myMask = obs::bitmaskSystem.Encode(myMask, matID3, MaskType::TEXTURE);
			else if (bar == 3)
				myMask = obs::bitmaskSystem.Encode(myMask, matID4, MaskType::TEXTURE);
			else if (bar == 4)
				myMask = obs::bitmaskSystem.Encode(myMask, matID5, MaskType::TEXTURE);
			else if (bar == 5)
				myMask = obs::bitmaskSystem.Encode(myMask, matID6, MaskType::TEXTURE);
			else if (bar == 6)
				myMask = obs::bitmaskSystem.Encode(myMask, matID7, MaskType::TEXTURE);
			else if (bar == 7)
				myMask = obs::bitmaskSystem.Encode(myMask, matID8, MaskType::TEXTURE);
			else if (bar == 8)
				myMask = obs::bitmaskSystem.Encode(myMask, matID9, MaskType::TEXTURE);
			//if (foo == 0)
			//	myMask = bitmaskSystem.Encode(myMask, meshID1, MaskType::MESH);
			//else if (foo == 1)
			//	myMask = bitmaskSystem.Encode(myMask, meshID2, MaskType::MESH);
			//else if (foo == 2)
			//	myMask = bitmaskSystem.Encode(myMask, meshID3, MaskType::MESH);
			if (foo == 0)
				myMask = obs::bitmaskSystem.Encode(myMask, meshID4, MaskType::MESH);
			else if (foo == 1)
				myMask = obs::bitmaskSystem.Encode(myMask, meshID4, MaskType::MESH);
			//else if (foo == 5)
			//	myMask = bitmaskSystem.Encode(myMask, meshID6, MaskType::MESH);		

			rc->bitmask = myMask;

			if (count < 256)
			{
				count++;
				objects.push_back(obs::dataHandler.AllocateObject());
				obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());
				
				obs::dataHandler.AllocateData< obs::LightComponent >(objects.back());
				
				tc = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
				//tc->position[0] = i * 5;
				//tc->position[1] = y * 5;
				//tc->position[2] = 0.0f;
				tc->position[0] = rand() % 250;
				tc->position[1] = rand() % 250;
				tc->position[2] = rand() % 50;
				
				obs::LightComponent* lc = obs::dataHandler.GetData<obs::LightComponent>(objects.back());
				lc->intensity = (rand() % 50) + 1;
				lc->color[0] = (rand() % 255) / 255.0f;
				lc->color[1] = (rand() % 255) / 255.0f;
				lc->color[2] = (rand() % 255) / 255.0f;
				
				lc->specularColor[0] = 1.0f;
				lc->specularColor[1] = 1.0f;
				lc->specularColor[2] = 1.0f;
				tc->scale[0] = (rand() % 50) + 1;
				
				myMask = 0;
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::LIGHT, MaskType::TYPE);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::LIGHT_TYPES::POINT, MaskType::MESH);
				
				lc->bitmask = myMask;
			}			
			else if (count < 257)
			{
				count++;
				objects.push_back(obs::dataHandler.AllocateObject());
				obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());
				
				obs::dataHandler.AllocateData< obs::LightComponent >(objects.back());
				
				tc = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
				//tc->position[0] = i * 5;
				//tc->position[1] = y * 5;
				//tc->position[2] = 0.0f;
				tc->position[0] = rand() % 250;
				tc->position[1] = rand() % 250;
				tc->position[2] = rand() % 50;
				
				obs::LightComponent* lc = obs::dataHandler.GetData<obs::LightComponent>(objects.back());
				lc->intensity = 20.05f;
				lc->color[0] = (rand() % 255) / 255.0f;
				lc->color[1] = (rand() % 255) / 255.0f;
				lc->color[2] = (rand() % 255) / 255.0f;
				
				lc->specularColor[0] = 1.0f;
				lc->specularColor[1] = 1.0f;
				lc->specularColor[2] = 1.0f;
				tc->scale[0] = (rand() % 50) + 1;
				
				tc->rotation[0] = 0.0f;
				tc->rotation[1] = 0.0f;
				tc->rotation[2] = 1.0f;
				
				myMask = 0;
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::LIGHT, MaskType::TYPE);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::LIGHT_TYPES::DIR, MaskType::MESH);
				
				lc->bitmask = myMask;
			}			
			else if (count < 500)
			{
				count++;
				objects.push_back(obs::dataHandler.AllocateObject());
				obs::dataHandler.AllocateData< obs::TransformationComponent >(objects.back());

				obs::dataHandler.AllocateData< obs::LightComponent >(objects.back());

				tc = obs::dataHandler.GetData<obs::TransformationComponent>(objects.back());
				//tc->position[0] = i * 5;
				//tc->position[1] = y * 5;
				//tc->position[2] = 0.0f;
				tc->position[0] = rand() % 250;
				tc->position[1] = 5.0f;
				tc->position[2] = rand() % 50;

				obs::LightComponent* lc = obs::dataHandler.GetData<obs::LightComponent>(objects.back());
				lc->intensity = (rand() % 1000) + 1;

				lc->color[0] = (rand() % 255) / 255.0f;
				lc->color[1] = (rand() % 255) / 255.0f;
				lc->color[2] = (rand() % 255) / 255.0f;

				lc->specularColor[0] = 1.0f;
				lc->specularColor[1] = 1.0f;
				lc->specularColor[2] = 1.0f;

				lc->lightSpecific.spotlight.angle = 3.14f / 8.0f;
				lc->lightSpecific.spotlight.penumbraAngle = 0.02f;

				tc->scale[0] = (rand() % 50) + 1;

				tc->rotation[0] = rand() % 250 / 250.0f;
				tc->rotation[1] = -1.0f;
				tc->rotation[2] = rand() % 250 / 250.0f;

				myMask = 0;
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::OBJECT_TYPES::LIGHT, MaskType::TYPE);
				myMask = obs::bitmaskSystem.Encode(myMask, gfx::LIGHT_TYPES::SPOT, MaskType::MESH);

				lc->bitmask = myMask;
			}
		}
	}


}

void LoadCubeMap()
{
	unsigned char* posX;
	unsigned char* negX;
	unsigned char* posY;
	unsigned char* negY;
	unsigned char* posZ;
	unsigned char* negZ;
	int width, height;
	posX = stbi_load("content/cubemaps/test_c00.png", &width, &height, 0, 0);
	negX = stbi_load("content/cubemaps/test_c01.png", &width, &height, 0, 0);
	posY = stbi_load("content/cubemaps/test_c03.png", &width, &height, 0, 0);
	negY = stbi_load("content/cubemaps/test_c02.png", &width, &height, 0, 0);
	posZ = stbi_load("content/cubemaps/test_c04.png", &width, &height, 0, 0);
	negZ = stbi_load("content/cubemaps/test_c05.png", &width, &height, 0, 0);
	unsigned int id;
	gfx::content::LoadCubemap(id, "content/cubemaps/test_c00.png", posX, negX, posY, negY, posZ, negZ, width, height);

	
	posX = stbi_load("content/cubemaps/test1_c00.png", &width, &height, 0, 0);
	negX = stbi_load("content/cubemaps/test1_c01.png", &width, &height, 0, 0);
	negY = stbi_load("content/cubemaps/test1_c02.png", &width, &height, 0, 0);
	posY = stbi_load("content/cubemaps/test1_c03.png", &width, &height, 0, 0);
	posZ = stbi_load("content/cubemaps/test1_c04.png", &width, &height, 0, 0);
	negZ = stbi_load("content/cubemaps/test1_c05.png", &width, &height, 0, 0);
	gfx::content::LoadCubemap(id, "content/cubemaps/test1_c00.png", posX, negX, posY, negY, posZ, negZ, width, height);

	posX = stbi_load("content/cubemaps/test2_c00.png", &width, &height, 0, 0);
	negX = stbi_load("content/cubemaps/test2_c01.png", &width, &height, 0, 0);
	posY = stbi_load("content/cubemaps/test2_c02.png", &width, &height, 0, 0);
	negY = stbi_load("content/cubemaps/test2_c03.png", &width, &height, 0, 0);
	posZ = stbi_load("content/cubemaps/test2_c04.png", &width, &height, 0, 0);
	negZ = stbi_load("content/cubemaps/test2_c05.png", &width, &height, 0, 0);
	gfx::content::LoadCubemap(id, "content/cubemaps/test2_c00.png", posX, negX, posY, negY, posZ, negZ, width, height);

	posX = stbi_load("content/cubemaps/test3_c00.png", &width, &height, 0, 0);
	negX = stbi_load("content/cubemaps/test3_c01.png", &width, &height, 0, 0);
	posY = stbi_load("content/cubemaps/test3_c02.png", &width, &height, 0, 0);
	negY = stbi_load("content/cubemaps/test3_c03.png", &width, &height, 0, 0);
	posZ = stbi_load("content/cubemaps/test3_c04.png", &width, &height, 0, 0);
	negZ = stbi_load("content/cubemaps/test3_c05.png", &width, &height, 0, 0);
	gfx::content::LoadCubemap(id, "content/cubemaps/test3_c00.png", posX, negX, posY, negY, posZ, negZ, width, height);
}


void Initialize()
{
	LogSystem::InitializeLogFile( "Log.txt", false );

	// initialize lua...
	obs::luaInterfaceHandler.Initialize();
	
	
	// errourus behaviour...
	obs::threadPool.Initialize( 4 ); 
	obs::contentManager.InitializeThreadPool( 4 );
	
	int w = 1280, h = 720;

	obs::windowHandler.Initialize(4, 4, w, h, obs::WindowModes::WM_WINDOWED, false);
	
	obs::cameraHandler.Initialize();
	obs::Camera* c = obs::cameraHandler.GetActiveCamera();
	*c = obs::Camera( w, h );

	// initialize gfx...
	gfx::Initialize(w, h);
	obs::bitmaskSystem.Init();

	gfx::Viewport* vp = obs::programAllocator.m_heap.NewObject<gfx::Viewport>( );
	vp->height = h;
	vp->width = w;
	vp->posX = 0;
	vp->posY = 0;
	vp->view = c->GetViewMatrix();
	vp->projection = c->GetProjectionMatrix();
	gfx::SetViewport(0, vp);

	// initialize physics...
	obs::physicsHandler.Initialize();
}

void Destroy()
{
	LogSystem::CloseLogFile();
	obs::luaInterfaceHandler.Close();
	obs::contentManager.KillThreads();
	obs::physicsHandler.Destroy();
}

#include <gfx/GFXViewport.hpp>
#include <gfx/GFXVertex.hpp>
#include <utility/UnicodeHelp.hpp>

int main(int argc, char* argv[])
{

	Initialize();
	//TestBitmasks();
	
	//TestRendering();

	// Feel encouraged to create your own test-scripts.
	// It would be nice to utilize the same constructor scripts in all tests however,
	// it would help build a general interface as well as ensure everything works 
	// in the same way across the system. 
	if (argc > 1)
	{
		std::cout << "arg\n";
		obs::luaInterfaceHandler.LoadFile(argv[1]);
	}
	else
	{
		obs::luaInterfaceHandler.LoadFile("scripts/testScene.lua");
	}
	//obs::luaInterfaceHandler.LoadFile( "scripts/walkableDemo/demoScene.lua" );
	
	LoadCubeMap();

	bool tab				= false;

	float maxFrameRate		= 1.0f / 180.0f;
	float minFrameRate		= 1.0f / 30.0f;
	float deltaTime			= maxFrameRate;
	float nonPausedDelta	= maxFrameRate;
	float frameTime			= maxFrameRate;

	float frameRate			= 0.0f;
	float lastFrameRate		= frameRate;
	float FRSR				= 0.97f; // FrameRate Smoothing Ratio

	int midX				= 1280 / 2;
	int midY				= 720 / 2;
	
	bool windowHadFocus = obs::windowHandler.HasFocus();
	SDL_ShowCursor( tab );
	SDL_WarpMouseInWindow( obs::windowHandler.GetWindow(), midX, midY );
	obs::inputHandler.SetRelativeMode(!tab);

	obs::ThreadPool mainLoopPool;
	mainLoopPool.Initialize( 1 );

	obs::frameAllocator = &obs::frameAllocatorA;
	obs::systemHandler.UpdateSystems(obs::SystemHandlerTimeStruct( deltaTime, nonPausedDelta ));
	obs::frameAllocator = obs::frameAllocator == &obs::frameAllocatorA ? &obs::frameAllocatorB : &obs::frameAllocatorA;
	

	HighresTimer hrTimer;

	while( obs::windowHandler.WindowEvents() != WINDOWED_CLOSED )
	{
		// frame:
		{		
			windowHadFocus = obs::windowHandler.HasFocus();
			const Uint8 *state = obs::inputHandler.GetSinglePressedKeyStates();
			hrTimer.Start();

			obs::Camera* camera = obs::cameraHandler.GetActiveCamera();
			obs::SystemHandlerTimeStruct timeContainer = obs::SystemHandlerTimeStruct( deltaTime, nonPausedDelta );

			obs::inputHandler.Update();
			obs::luaInterfaceHandler.Update( timeContainer );
			
			//DrawSomeDebugStuff();

			if ( windowHadFocus )
			{
				if (obs::windowHandler.WindowResized())
				{
					camera->SetWidth(obs::windowHandler.GetWindowWidth());
					camera->SetHeight(obs::windowHandler.GetWindowHeight());
					camera->RebuildProjection();

					midX = obs::windowHandler.GetWindowWidth() / 2;
					midY = obs::windowHandler.GetWindowHeight() / 2;
					
					gfx::Viewport* vp = obs::programAllocator.m_heap.NewObject<gfx::Viewport>();
					vp->height = obs::windowHandler.GetWindowHeight();
					vp->width = obs::windowHandler.GetWindowWidth();
					vp->posX = 0;
					vp->posY = 0;
					vp->view = camera->GetViewMatrix();
					vp->projection = camera->GetProjectionMatrix();
					gfx::SetViewport(0, vp);
				}

				glm::vec2 mouseDelta = obs::inputHandler.GetMouseRelativeCenter();

				if( !tab )
				{
					//obs::inputHandler.WrapMouseInsideWindow();
					camera->Update( timeContainer.delta );
				}

				if (state[SDL_SCANCODE_TAB])
				{
					tab = !tab;
					SDL_ShowCursor( tab );
					SDL_WarpMouseInWindow( obs::windowHandler.GetWindow(), midX, midY);
					obs::inputHandler.SetRelativeMode(!tab);
				}

				if (state[SDL_SCANCODE_R])
				{
					Bitmask b = GetCommandBitmask(gfx::SUB_COMMAND_TYPES::RELOAD_SHADERS_COMMAND, obs::bitmaskSystem);
					gfx::Execute(b, nullptr);
				}

				if (state[SDL_SCANCODE_ESCAPE])
					break;

				// restart lua without altering the camera...
				if (state[SDL_SCANCODE_F5])
				{
					obs::cameraHandler.LockCameras( true );
					obs::luaInterfaceHandler.ReloadAll();
					obs::cameraHandler.LockCameras( false );
					obs::inputHandler.WrapMouseInsideWindow();
				}

				// restart lua and allow camera reinit...
				if (state[SDL_SCANCODE_F6])
				{
					obs::luaInterfaceHandler.ReloadAll();
					obs::inputHandler.WrapMouseInsideWindow();
				}

				if (state[SDL_SCANCODE_F9])
				{
					static bool mode = false;
					mode = !mode;
					obs::physicsHandler.SetDebugMode( mode );
				}
			}

			mainLoopPool.Enqueue( [ &timeContainer ]()
			{
				obs::frameAllocator->RewindMemory();
				obs::systemHandler.UpdateSystems( timeContainer );
			});

			gfx::SetViewportCamera(0, (glm::mat4*)&camera->GetViewMatrix(), (glm::mat4*)&camera->GetProjectionMatrix());
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			gfx::Render(frameTime);
			obs::windowHandler.SwapBuffers();

			// wait for both main thread and the threadpool to finish, swap buffers before next frame
			mainLoopPool.Wait();
			obs::frameAllocator = obs::frameAllocator == &obs::frameAllocatorA ? &obs::frameAllocatorB : &obs::frameAllocatorA;
			gfx::SwapRenderJobBuffer();
			gfx::debug::SwapDebugBuffers();
			obs::systemHandler.DrawTimeInfo(10, 48);

			// handle delta time values
			hrTimer.Stop();
			double interval = hrTimer.GetDelta();
			frameTime = (float)interval;
			deltaTime = (float)interval;

			// Draw framerate
			frameRate = (frameRate == 0.0f) ? 
				float(1.0 / deltaTime) : 
				lastFrameRate * FRSR + float(1.0 / interval) * (1.0f - FRSR);

			std::string fpsString = ("fps: " + std::to_string(int(frameRate+0.5f)));
			gfx::debug::DrawString(fpsString.c_str(), 11, 29, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
			gfx::debug::DrawString(fpsString.c_str(), 10, 28, glm::vec4(1.0f));
			lastFrameRate = frameRate;
		}

		// time management...
		if( deltaTime > minFrameRate )
		{
			deltaTime = minFrameRate;
		}	
		else if( deltaTime < maxFrameRate )
		{
			int frameDeltaMicroTime = static_cast<int>((maxFrameRate - deltaTime) * 1000000);
			//std::this_thread::sleep_for( std::chrono::microseconds( frameDeltaMicroTime  ) );
			//while ()
			deltaTime = maxFrameRate;
		}
		
	}

	mainLoopPool.KillThreads();
	Destroy();
	return 0;
}



