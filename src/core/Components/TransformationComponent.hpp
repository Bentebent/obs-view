#ifndef SRC_CORE_COMPONENTS_WORLDPOSITIONCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_WORLDPOSITIONCOMPONENT_HPP

#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace obs
{
	
	struct TransformationComponent
	{
		float position[3];
		float scale[3];
		float rotation[4];
		float prevModelMatrix[16];

		TransformationComponent()
		{
			position[0] = 0.0f;
			position[1] = 0.0f;
			position[2] = 0.0f;
			scale[0] = 1.0f;
			scale[1] = 1.0f;
			scale[2] = 1.0f;
			rotation[0] = 0.0f;
			rotation[1] = 0.0f;
			rotation[2] = 0.0f;
			rotation[3] = 1.0f;
			glm::mat4 identity(1.0f);
			memcpy(prevModelMatrix, glm::value_ptr(identity), sizeof(identity));

		}

		TransformationComponent(float px, float py, float pz)
		{
			position[0] = px;
			position[1] = py;
			position[2] = pz;
			scale[0] = 1.0f;
			scale[1] = 1.0f;
			scale[2] = 1.0f;
			rotation[0] = 0.0f;
			rotation[1] = 0.0f;
			rotation[2] = 0.0f;
			rotation[3] = 1.0f;
			glm::mat4 identity(1.0f);
			memcpy(prevModelMatrix, glm::value_ptr(identity), sizeof(identity));
		}

		TransformationComponent(glm::vec3 p, glm::vec3 s, glm::quat r)
		{
			position[0] = p.x;
			position[1] = p.y;
			position[2] = p.z;
			scale[0] = s.x;
			scale[1] = s.y;
			scale[2] = s.z;
			rotation[0] = r[0];
			rotation[1] = r[1];
			rotation[2] = r[2];
			rotation[3] = r[3];
			glm::mat4 identity(1.0f);
			memcpy(prevModelMatrix, glm::value_ptr(identity), sizeof(identity));
		}

		TransformationComponent(glm::vec3 p, glm::vec3 s)
		{
			position[0] = p.x;
			position[1] = p.y;
			position[2] = p.z;
			scale[0] = s.x;
			scale[1] = s.y;
			scale[2] = s.z;
			rotation[0] = 0.0f;
			rotation[1] = 0.0f;
			rotation[2] = 0.0f;
			rotation[3] = 1.0f;
			glm::mat4 identity(1.0f);
			memcpy(prevModelMatrix, glm::value_ptr(identity), sizeof(identity));
		}

		TransformationComponent(glm::vec3 p)
		{
			position[0] = p.x;
			position[1] = p.y;
			position[2] = p.z;
			scale[0] = 1.0f;
			scale[1] = 1.0f;
			scale[2] = 1.0f;
			rotation[0] = 0.0f;
			rotation[1] = 0.0f;
			rotation[2] = 0.0f;
			rotation[3] = 1.0f;
			glm::mat4 identity(1.0f);
			memcpy(prevModelMatrix, glm::value_ptr(identity), sizeof(identity));
		}

		static glm::vec4 GetPositionVec4(const TransformationComponent& tc)
		{
			return glm::vec4(tc.position[0], tc.position[1], tc.position[2], 1.0f);
		}


		static glm::vec3 GetPositionVec3(const TransformationComponent& tc)
		{
			return glm::vec3(tc.position[0], tc.position[1], tc.position[2]);
		}

		static glm::vec3 GetScaleVec3(const TransformationComponent& tc)
		{
			return glm::vec3(tc.scale[0], tc.scale[1], tc.scale[2]);
		}

		static glm::quat GetRotationQuat(float* rotation)
		{
			glm::quat q;
			q.x = rotation[0];
			q.y = rotation[1];
			q.z = rotation[2];
			q.w = rotation[3];

			return q;
		}
		/*!
		Utility function for creating a RotationComponent with the specified angle of rotation around the x-axis.
		\param angleRad The angle in radians to rotate.
		\return A RotationComponent with its members set to the specified rotation.
		*/
		static void RotateX(TransformationComponent &tc, float angleRad)
		{
			return RotateAxis(tc, angleRad, 1.0f, 0.0f, 0.0f);
		}

		/*!
		Utility function for creating a RotationComponent with the specified angle of rotation around the y-axis.
		\param angleRad The angle in radians to rotate.
		\return A RotationComponent with its members set to the specified rotation.
		*/
		static void RotateY(TransformationComponent &tc, float angleRad)
		{
			return RotateAxis(tc, angleRad, 0.0f, 1.0f, 0.0f);
		}

		/*!
		Utility function for creating a RotationComponent with the specified angle of rotation around the z-axis.
		\param angleRad The angle in radians to rotate.
		\return A RotationComponent with its members set to the specified rotation.
		*/
		static void RotateZ(TransformationComponent &tc, float angleRad)
		{
			return RotateAxis(tc, angleRad, 0.0f, 0.0f, 1.0f);
		}

		/*!
		Utility function for creating a RotationComponent with the specified angle of rotation around an arbitrary
		axis.
		\param angleRad The angle in radians to rotate.
		\param axisX The x-component of the vector specifying the rotation axis.
		\param axisY The y-component of the vector specifying the rotation axis.
		\param axisZ The z-component of the vector specifying the rotation axis.
		\return A RotationComponent with its members set to the specified rotation.
		*/
		static void RotateAxis(TransformationComponent &tc, float angleRad, float axisX, float axisY, float axisZ)
		{
			tc.rotation[0] = axisX * (float)std::sin((double)angleRad * 0.5);
			tc.rotation[1] = axisY * (float)std::sin((double)angleRad * 0.5);
			tc.rotation[2] = axisZ * (float)std::sin((double)angleRad * 0.5);
			tc.rotation[3] = (float)std::cos((double)angleRad * 0.5);
		}
	};
}


#endif