#ifndef SRC_CORE_COMPONENTS_LIGHTCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_LIGHTCOMPONENT_HPP

#include <bitmask/dynamic_bitmask.hpp>

namespace obs
{
	struct LightComponent
	{
		Bitmask bitmask;
		float color[3];
		float specularColor[3];
		float intensity;

		union
		{
			struct
			{
				float angle;
				float penumbraAngle;
			} spotlight;
		} lightSpecific;

		LightComponent()
		{
			bitmask = 0;
			color[0] = 0.0f;
			color[1] = 0.0f;
			color[2] = 0.0f;

			specularColor[0] = 0.0f;
			specularColor[1] = 0.0f;
			specularColor[2] = 0.0f;
		}
	};
}

#endif