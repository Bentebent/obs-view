#ifndef COMPONENT_TEST_COMPONENT_HPP
#define COMPONENT_TEST_COMPONENT_HPP

struct TestComponent
{
	float a, b, c;

	TestComponent() 
	{
	}
	
	TestComponent( float x, float y, float z ) : a(x), b(y), c(z) 
	{
	}

};


#endif