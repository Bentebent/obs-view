#ifndef SRC_CORE_COMPONENTS_RENDERINGCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_RENDERINGCOMPONENT_HPP

#include <bitmask/dynamic_bitmask.hpp>

namespace obs
{
	struct RenderingComponent
	{
		Bitmask bitmask;
		bool renderMe;

		RenderingComponent()
		{
			bitmask = 0;
			renderMe = true;
		}

	};
}


#endif