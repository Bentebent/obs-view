#ifndef SRC_CORE_COMPONENTS_PHYSICS_COMPONENT_HPP
#define SRC_CORE_COMPONENTS_PHYSICS_COMPONENT_HPP



namespace obs
{
	struct PhysicsComponent
	{
		int shapeIndex;
		int collisionListIndex;
		int nrCollisions;

		PhysicsComponent()
		{
			shapeIndex = -1;
			collisionListIndex = -1;
			nrCollisions = 0;
		}
	};
}

#endif