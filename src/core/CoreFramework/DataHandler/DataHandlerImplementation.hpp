#ifndef DATA_HANDLER_IMPLEMENTATION_CLASS
#define DATA_HANDLER_IMPLEMENTATION_CLASS


#include "DataArray.hpp"
#include <CoreFramework/DataHandler/DataArrayIndexTemplate.hpp>
#include <CoreFramework/DataHandler/TemplatePresence.hpp>

#include <tuple>
#include <cassert>
#include <algorithm>
#include <cstring>
#include <functional>
#include <memory>
#include <map>

#define NO_CHILDREN -1
#define NO_DATA		-1
#define NOT_A_CHILD -1 


#define DATA_INDEX ArrayIndex< T, std::tuple< systemDefs... >>::index
#define DATAHANDLER_ARRAY_META_SIZE sizeof...( systemDefs )
#define NONEXISTING_COMPONENT_ERROR "Component does not exist in the dataHandler, forgot to include it?"

namespace obs
{
	template < typename... systemDefs >
	class DataHandler
	{
	private:

		/*!
			Struct used to track data indicies in the DataHandlers dataPool.
			NO_DATA or -1 marks that no data of the corresponding type has been allocated for 
			this object.
		*/
		template < typename... systemDefs >
		struct DataIndicies
		{
			int indicies[ DATAHANDLER_ARRAY_META_SIZE ];
			unsigned long bitmask;
		
			int& operator[]( int index )
			{
				return indicies[ index ];
			}
		
			/*!
				Will recalculate the bitmask of the respecitve object.
			*/
			void CalcBitMask()
			{
				bitmask = 0ULL;
				for( int i = 0; i < DATAHANDLER_ARRAY_META_SIZE; ++i )
				{
					if( indicies[ i ] >= 0 )
						bitmask |= 1ULL << i;
				}
			}
		};

		/*!
			Struct used to store scene graph meta data.
		*/
		struct SceneGraphStruct
		{
			int nrChildren;
			int childListIndex;
			int parent;
			int childListLinkIndex;
			int luaReferences;

			SceneGraphStruct() : 
				nrChildren(-1), childListIndex(-1), childListLinkIndex(-1), parent(-1), luaReferences( 1 )
			{
			}
		};

		/*!
			Struct used to store scene graph links between objects.
		*/
		struct ChildStruct
		{
			int objectId;
			int parent;

			ChildStruct( int childId, int parentId )
				: objectId( childId ), parent( parentId )
			{
			}
		};

		/*!
			Will sort the list of childrend so that order is sequential and update 
			scene graph meta data.
		*/
		void UpdateSceneGraph()
		{
			std::sort( m_childList.begin(), m_childList.end(), [this]( const ChildStruct& me, const ChildStruct& other  )
			{
				if( me.parent == other.parent )
				{
					if( me.objectId > other.objectId )
						return true;
					return false;
				}
				else if( me.parent < other.parent )
					return true;
				return false;
			});

			// reset m_m_childListIndex data
			std::memset( m_childListIndex.data(), NO_CHILDREN, m_childListIndex.size() * sizeof( SceneGraphStruct )  );

			// update m_m_childListIndex list
			int size = m_childList.size();
			int parentObject;
			int startIndex;
			int nrChildren;

			for( int i = 0; i < size;  )
			{
				parentObject = m_childList[ i ].parent;
				nrChildren = 0;
				startIndex = i;

				for( ; i < size; ++i )
				{
					if( m_childList[ i ].parent != parentObject )
						break;
					nrChildren++;
				
					m_childListIndex[ m_childList[ i ].objectId ].childListLinkIndex = i;
					m_childListIndex[ m_childList[ i ].objectId ].parent = parentObject;
				}
			
				m_childListIndex[ parentObject ].childListIndex = startIndex;
				m_childListIndex[ parentObject ].nrChildren = nrChildren;
			}
		}

		/*!
			Will sort the data in the arrays to correspond to the scene graph structure.
		*/
		void SortData( int parentID, int childID )
		{
			int size = m_dataPool.size();
			for( int i = 0; i < size; ++i )
			{
				if( m_entities[ childID ][ i ] >= 0 && m_entities[ parentID ][ i ] >= 0 )
				{
					int prevIndex = m_entities[ childID ][ i ];
					int insertionIndex = m_entities[ parentID ][ i ] < m_entities[ childID ][ i ] ?
						m_entities[ parentID ][ i ] + 1 : 
						m_entities[ parentID ][ i ];
				
					((DataArrayBase*)m_dataPool[ i ])->AddChild( m_entities[ childID ][ i ], m_entities[ parentID ][ i ] );
					int newIndex = m_entities[ childID ][ i ];
				
					// the data has been moved erlier in the list...
					if( prevIndex - newIndex > 0 )
					{
						int size = this->m_entities.size();
						for( int q = 0; q < size; ++q )
						{
							if( m_entities[ q ][ i ] >= insertionIndex && m_entities[ q ][ i ] < prevIndex && q != childID )
								m_entities[ q ][ i ] += 1;
						}
					}
				
					// the data has been moved down the list...
					else
					{
						int size = this->m_entities.size();
						for( int q = 0; q < size; ++q )
						{
							if( m_entities[ q ][ i ] <= insertionIndex && m_entities[ q ][ i ] >= prevIndex && q != childID )
								m_entities[ q ][ i ] -= 1;
						}
					}
				}
			}
		
			const int* children = this->GetChildren( childID );
			int nrChildren = this->GetNrChildren( childID );
			for( int i = 0; i < nrChildren; ++i )
			{
				SortData( childID, children[ i * 2 ] );
			}
		}


	public:

		DataHandler()
		{
			this->m_dataPool = {{ ((void*)new DataArray<systemDefs>())... }};
		}


		~DataHandler()
		{
			for( unsigned int i = 0; i < m_dataPool.size(); i++ )
				if( m_dataPool[ i ] != nullptr )
					delete m_dataPool[ i ];
		}

		// object functions
		// ****************************************************************************************************** //

		/*!
			Will create an empty object and return an integer handle to it.
		*/
		unsigned int AllocateObject()
		{
			if( m_openSlots.size() != 0 )
			{
				int index = m_openSlots.back();
				m_openSlots.pop_back();
				m_childListIndex[index] = SceneGraphStruct();

				std::memset( &m_entities[ index ], NO_DATA, DATAHANDLER_ARRAY_META_SIZE * sizeof(int));
				m_entities[ index ].bitmask = 0ULL;
			
				return index;
			}
			else
			{
				unsigned int index = m_entities.size();
				m_entities.push_back( DataIndicies<systemDefs...>() );
				m_childListIndex.push_back( SceneGraphStruct() );

				int sizedd = sizeof( unsigned long long );

				std::memset( &m_entities[ index ], NO_DATA, DATAHANDLER_ARRAY_META_SIZE * sizeof(int));
				m_entities[ index ].bitmask = 0ULL;

				return index;
			}
		}
		
		/*!
			Will remove the object and all associated data.
		*/
		void DeleteObject( int objectId )
		{
			int size = this->m_dataPool.size();
			for( int i = 0; i < size; ++i )
			{
				((DataArrayBase*)m_dataPool[ i ])->DeleteData( m_entities[objectId][ i ] );
			}
			m_openSlots.push_back( objectId );
			RemoveFromParent( objectId );
			
			UpdateBuckets( objectId, 0ULL, m_entities[ objectId ].bitmask );
			std::memset( &m_entities[ objectId ], NO_DATA, DATAHANDLER_ARRAY_META_SIZE * sizeof(int)); /////////////////////////
		}
	
	

		// Data functions 
		// ****************************************************************************************************** //
	
		template< typename T > 
		/*!
			Will allocate the by template specified data. Data type must be trivially copyable and 
			must be one of the types in the template list used to create the handler.
		*/
		void AllocateData( int objectId )
		{
			#ifndef __GNUG__  // Not implemented by gmake
			static_assert( std::is_trivially_copyable<T>::value, "Components must be Pure Data Objects" );
			#endif
			m_entities[objectId][ DATA_INDEX ] = ((DataArray<T>*)m_dataPool[ DATA_INDEX ])->AllocateObject( objectId );
			
			unsigned int oldMask = m_entities[ objectId ].bitmask;
			m_entities[ objectId ].CalcBitMask();
			this->UpdateBuckets( objectId, m_entities[objectId].bitmask, oldMask );
		}

	
		template< typename T >
		/*!
			Will return a pointer to the data of the by template specified type that is associated with the object.
			Will return null_ptr if the object do not have any data of this type allocated.
		*/
		T* GetData( int objectId )
		{
			#ifndef NDEBUG
			if( objectId < 0 && objectId >= (int)this->m_entities.size() )
				std::cout << "faulty objectId used in GetData( int objectId )" << std::endl;
			#endif
			return ((DataArray<T>*)m_dataPool[ DATA_INDEX ])->GetObject( m_entities[objectId][ DATA_INDEX ] );
		}


		template< typename T > 
		/*!
			Will remove the data associated with this object.
		*/
		void DeleteData( int objectId )
		{
			#ifndef NDEBUG
			if( objectId < 0 && objectId >= (int)m_entities.size() )
				std::cout << "faulty objectId used in GetData( int objectId )" << std::endl;
			#endif

			int index = m_entities[objectId][ DATA_INDEX ];
			if( index != NO_DATA )
				((DataArray<T>*)m_dataPool[ DATA_INDEX ])->DeleteData( index );
			m_entities[objectId][ DATA_INDEX ] = NO_DATA;
		
			unsigned int oldMask = m_entities[ objectId ].bitmask;
			m_entities[ objectId ].CalcBitMask();
			this->UpdateBuckets( objectId, m_entities[objectId].bitmask, oldMask );
		}




		// scene graph functions 
		// ****************************************************************************************************** //

		/*!
			Will create a link between the parent and child.
			If makeReadyToGo is false the scene graph links will not be updated until CalculateSceneGraph() is called. 
			Set makeReadyToGo to false if a lot of children are going to be assigned at one time.
		*/
		void AddChild( int parent, int child, bool makeReadyToGo = true )
		{
			// reposition the data...
			SortData( parent, child );

			// create the link between the objects...
			m_childList.push_back( ChildStruct( child, parent ) );
			if( makeReadyToGo == true )
				UpdateSceneGraph();
		}

		/*!
			Will update the links in the scen graph. Must be called after AddChild(..., makeReadyToGo ) if corresponding makeReadyToGo 
			is set to false.
		*/
		void CalculateSceneGraph()
		{
			UpdateSceneGraph();
		}


		/*
			Will remove the link between the parent and child.
			Set makeReadyToGo to false if calling this function repeatively, and call CalculateSceneGraph() when finished
			to update the scene graph links.
		*/
		void RemoveFromParent( int child, bool makeReadyToGo = true )
		{
			if( m_childListIndex[ child ].childListLinkIndex == NOT_A_CHILD )
				return; 

			m_childList.erase( m_childList.begin() + m_childListIndex[ child ].childListLinkIndex );
			if( makeReadyToGo == true )
				UpdateSceneGraph();
		}


		/*!
			Will return a integer pointer to the list of child and corresponding parent.
			Multiply index with two when stepping through the list.
		*/
		const int* GetChildren( int objectId )
		{
			if( m_childListIndex[ objectId ].childListIndex == NO_CHILDREN )
				return nullptr;
			return (int*)&m_childList[ m_childListIndex[ objectId ].childListIndex ];
		}

		int GetParent( int objectId )
		{
			if( m_childListIndex[ objectId ].childListLinkIndex == NOT_A_CHILD )
				return -1; 
			return m_childListIndex[ objectId ].parent;
		}


		/*!
			Will return the number of children associated with this object, not counting grandchildren.
		*/
		int GetNrChildren( int objectId )
		{
			return m_childListIndex[ objectId ].nrChildren;
		}


		/*!
			Will perform the function func for parent and all children.
		*/
		template< typename T >
		void ScenGraphOperation( std::function<void( T, int )> func, T operationValue, int objectID )
		{
			func( operationValue, objectID );
			const int* children = this->GetChildren( objectID );
			int nrChildren = this->GetNrChildren( objectID );
			for( int i = 0; i < nrChildren; ++i )
			{
				ScenGraphOperation( func, operationValue, children[ i * 2 ] );
			}
		}

		void AddLuaReference( int objectId )
		{
			 m_childListIndex[ objectId ].luaReferences++;
		}

		void RemoveLuaReference( int objectId )
		{
			 m_childListIndex[ objectId ].luaReferences--;
		}

		int GetLuaReferences( int objectId )
		{
			 return m_childListIndex[ objectId ].luaReferences;
		}

		// bucket functions
		// ****************************************************************************************************** //
		
		template< typename... ComponentTypes >
		/*
			Will return the bitmask that is crossreferenced with the components in the dataHandler instance.
		*/
		unsigned int GetAspect()
		{
			static const int indicies[] = { GetComponentIndex<ComponentTypes>()... };
			unsigned int mask = 0U;
			for( int i = 0; i < sizeof...(ComponentTypes); i++ )
			{
				mask |= 1U << indicies[ i ];
			}
			return mask;
		}


	private:
		/*!
			Will call the game-namespace SystemHandler instance and update the respective buckets.
		*/
		void UpdateBuckets( int objectID, unsigned long long mask, unsigned long long oldMask  )
		{
			systemHandler.UpdateBuckets( objectID, mask, oldMask );
		}
	
		template< typename T >
		int GetComponentIndex()
		{
			static_assert( Match<T,systemDefs...>::exists, NONEXISTING_COMPONENT_ERROR );
			return ArrayIndex< T, std::tuple< systemDefs... >>::index;
		}

	private:

		std::vector< DataIndicies< systemDefs... > > m_entities;

		std::vector< int > m_openSlots;

		std::vector< SceneGraphStruct > m_childListIndex;
		std::vector< ChildStruct > m_childList;

		std::array< void*, sizeof...(systemDefs) > m_dataPool;
	};

}

#undef NO_CHILDREN
#undef NO_DATA
#undef NOT_A_CHILD
#undef DATA_INDEX
#undef DATAHANDLER_ARRAY_META_SIZE
#undef NONEXISTING_COMPONENT_ERROR

#endif