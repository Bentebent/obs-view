#ifndef DATA_ARRAY_BASE_CLASS
#define DATA_ARRAY_BASE_CLASS


#include <cstring>
#include <array>
#include <vector>


namespace obs
{

	class DataArrayBase
	{
	private:

	public:
		virtual ~DataArrayBase() { }

		virtual void DeleteData( int dataId ) abstract;
		virtual void AddChild( int& childDataIndex, int parentDataIndex ) abstract;

	private:

	};

}

#endif