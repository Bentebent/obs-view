#ifndef DATA_ARRAY_CLASS
#define DATA_ARRAY_CLASS

#include "DataArrayBase.hpp"

#include <cstring>
#include <array>
#include <vector>


namespace obs
{

	template< typename T >
	class DataArray : public DataArrayBase
	{
	private:

		struct sizeObject
		{
			char forSize[ sizeof(T) ];
		};


	public:
	
		DataArray() { }

		~DataArray() override {	}

		int AllocateObject( int objectID )
		{
			if( m_OpenSlots.size() > 0 )
			{
				int index = m_OpenSlots.back();
				std::memcpy( &m_data[ index ], &m_constructorObject, sizeof( T ) );
				m_objectIDs[ index ] = objectID;
				m_OpenSlots.pop_back();
				return index;
			}
			else
			{
				int index = m_data.size();
				m_data.push_back( sizeObject() );
				m_objectIDs.push_back( objectID );
				std::memcpy( &m_data[ index ], &m_constructorObject, sizeof( T ) );
				return index;
			}
		}

	
		T* GetObject( int index ) 
		{ 
			if( index < 0 ) 
				return nullptr;
			else
				return reinterpret_cast<T*>(&m_data[index]); 
		}


		void DeleteData( int index ) override
		{
			if( index >= 0 )
			{
				m_OpenSlots.push_back( index );
			}
		}


		// scene graph functions

		void AddChild( int& childDataIndex, int parentDataIndex ) override
		{
			int insertionIndex = parentDataIndex < childDataIndex ? parentDataIndex + 1 : parentDataIndex;

			sizeObject temp;
			std::memcpy( &temp, &m_data[ childDataIndex ], sizeof( T ) );
			m_data.erase( m_data.begin() + childDataIndex );

			m_data.insert( m_data.begin() + insertionIndex, sizeObject() );
			m_data[ insertionIndex ] = temp;
		
			// update open-slots
			int size = m_OpenSlots.size();
			for( int i = 0; i < size; ++i )
			{
				if( m_OpenSlots[i] >= insertionIndex && m_OpenSlots[i] < childDataIndex  )
					m_OpenSlots[i]++;
			}

			childDataIndex = insertionIndex;
		}



	private:
	
		T m_constructorObject;
		std::vector< sizeObject > m_data;
		std::vector< int > m_OpenSlots;

		// scene graph data
		std::vector< int > m_objectIDs;

	};
}



#endif