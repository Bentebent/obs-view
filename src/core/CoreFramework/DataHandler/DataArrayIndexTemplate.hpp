#ifndef DATA_ARRAY_INDEX_TEMPLATE
#define DATA_ARRAY_INDEX_TEMPLATE


#include <cstdint>
#include <tuple>

template< class T, class Tuple >
struct ArrayIndex;

template< class T, class... Types >
struct ArrayIndex< T, std::tuple< T, Types... >>
{
	static const std::size_t index = 0;
};

template< class T, class U, class... Types >
struct ArrayIndex< T, std::tuple< U, Types... >>
{
	static const std::size_t index = 1 + ArrayIndex< T, std::tuple< Types... >>::index;
};


#endif