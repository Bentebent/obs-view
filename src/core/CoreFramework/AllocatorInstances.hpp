#ifndef CORE_ALLOCATOR_INSTANCES_HPP
#define CORE_ALLOCATOR_INSTANCES_HPP

#include <Memory/LinearAllocator.hpp>
#include <Memory/LinearHeap.hpp>

#include <cstdint>

#define POD_CAST_FRAME_ALLOC( object ) (object*)obs::frameAllocator->m_heap.NewPODArray<char>( sizeof(object) );


#define FRAME_MEMORY_SIZE	10240000
#define PROGRAM_MEMORY_SIZE 10240000



namespace obs
{
	class AllocatorInstance
	{

	public :
		/*!
			Initializes the Allocator instance, parameter is memory size in bytes.
		*/
		AllocatorInstance( unsigned int memorySize );

		/*!
			removes all allocated memory.
		*/
		~AllocatorInstance();

		/*!
			Rewinds the memory stack, calling destructors if there are any.
		*/
		void RewindMemory();

		/*!
			Allocator instance.
		*/
		obs::LinearAllocator m_allocator;

		/*!
			Interface instance.
		*/
		obs::LinearHeap m_heap;

	private:
		int m_memorySize;
		std::int8_t* m_memory;

	};

	/*!
		Frame allocator used to swap between A and B instance, due to external dependencies such 
		as gfx etc.
	*/
	extern AllocatorInstance* frameAllocator;

	/*!
		Frame allocator, this memory is only vaild in the current frame.
	*/
	extern AllocatorInstance frameAllocatorA;

	/*!
		Frame allocator, this memory is only vaild in the current frame.
	*/
	extern AllocatorInstance frameAllocatorB;

	/*!
		Program allocator, this memory is valid throughout the programs life spann.
	*/
	extern AllocatorInstance programAllocator;
}

#endif