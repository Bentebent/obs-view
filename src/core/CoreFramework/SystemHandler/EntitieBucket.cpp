#include "EntitieBucket.hpp"
#include "../Core.hpp"

#include <algorithm>

obs::EntitieBucket::EntitieBucket( unsigned long long bitmask ) : m_includeMask( bitmask ) 
{ 
}

obs::EntitieBucket::~EntitieBucket()
{
}

void obs::EntitieBucket::CheckMask( int objectID, unsigned long long mask, unsigned long long oldMask )
{
	// the object should be in the list...
	if( m_includeMask == (m_includeMask & mask) )
	{
		if( m_entities.size() != 0 && (m_entities.front() == objectID || m_entities.back() == objectID) ) 
		{
			return;
		}

		int start = 0;
		int end = m_entities.size() - 1;
		if( end < 0 || objectID > m_entities[ end ] )
		{
			m_entities.push_back( objectID );
			return;
		}

		while( true )
		{
			int id = (start + end) * 0.5f;
			int sample = m_entities[ id ];

			if( sample == objectID )
			{
				return;
			}

			if( sample < objectID )
			{
				start = id;
			}
			else if( sample > objectID )
			{
				end = id;
			}

			if( end - start < 2 )
			{
				
				m_entities.insert( m_entities.begin() + id + 1, objectID );
				return;
			}
		}
	}

	// the object should be removed from the list...
	if( m_includeMask == (m_includeMask & oldMask) )
	{
		int start = 0;
		int end = m_entities.size() - 1;
		if( end < 0 || objectID > m_entities[ end ] )
		{
			m_entities.pop_back();
			return;
		}

		while( true )
		{
			int id = (start + end) * 0.5f;
			int sample = m_entities[ id ];

			if( sample == objectID )
			{
				m_entities.erase( m_entities.begin() + id );
				return;
			}

			if( sample < objectID )
			{
				start = id;
			}
			else if( sample > objectID )
			{
				end = id;
			}

			if( end - start < 2 )
			{
				
				m_entities.erase( m_entities.begin() + id + 1 );
				return;
			}
		}
	}
}
