#ifndef SYSTEM_HANDLER_TIME_STRUCT
#define SYSTEM_HANDLER_TIME_STRUCT

namespace obs
{

	struct SystemHandlerTimeStruct
	{
		float delta;
		float realtimeDelta;

		SystemHandlerTimeStruct( float delta, float realtimeDelta ) : delta( delta ), realtimeDelta( realtimeDelta ) 
		{
		}
	};

}

#endif