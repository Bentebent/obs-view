#include "SystemHandler.hpp"
#include <utility>
#include "../shared/utility/HighresTimer.hpp"
#include <gfx/GFXInterface.hpp>
#include <iostream>

void obs::SystemHandler::DrawTimeInfo(int x, int y)
{
	//if (temp_incrCounter >= 60)
	//{
	//	static_timings = timings;
	//	temp_incrCounter = 0;
	//}
	//temp_incrCounter++;

	int xoffz = x;
	int yoffs = y;
	int incr = 16;
	int column_offs = 150;
	gfx::debug::DrawString("----- System CPU timings -----", xoffz + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	gfx::debug::DrawString("----- System CPU timings -----", xoffz, yoffs, glm::vec4(1.0f));
	yoffs += incr;
	double totalTime = 0.0;

	for (unsigned int i = 0; i < m_timings.size(); i++)
	{
		totalTime += m_timings[i].second;
		std::string str = std::to_string(m_timings[i].second * 1000.0) + " ms";


		gfx::debug::DrawString(m_timings[i].first.c_str(), xoffz + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		gfx::debug::DrawString(m_timings[i].first.c_str(), xoffz, yoffs, (i % 2 == 0) ? glm::vec4(1.0f, 1.0f, 1.0f, 1.0f) : glm::vec4(0.85f, 0.85f, 0.85f, 1.0f));


		gfx::debug::DrawString(str.c_str(), xoffz + column_offs + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		gfx::debug::DrawString(str.c_str(), xoffz + column_offs, yoffs, (i % 2 == 0) ? glm::vec4(1.0f, 1.0f, 1.0f, 1.0f) : glm::vec4(0.85f, 0.85f, 0.85f, 1.0f));
		yoffs += incr;

	}
	gfx::debug::DrawString("------------------------------", xoffz + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	gfx::debug::DrawString("------------------------------", xoffz, yoffs, glm::vec4(1.0f));
	yoffs += incr;

	
	std::string totalStr = std::to_string(totalTime * 1000.0) + " ms";

	gfx::debug::DrawString("Total", xoffz + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	gfx::debug::DrawString("Total", xoffz, yoffs, glm::vec4(1.0f));

	gfx::debug::DrawString(totalStr.c_str(), xoffz + column_offs + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	gfx::debug::DrawString(totalStr.c_str(), xoffz + column_offs, yoffs, glm::vec4(1.0f));
	yoffs += incr;


	gfx::debug::DrawString("------------------------------", xoffz + 1, yoffs + 1, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	gfx::debug::DrawString("------------------------------", xoffz, yoffs, glm::vec4(1.0f));
	yoffs += incr;
}


obs::SystemHandler::SystemHandler()
{
	m_buckets.reserve( SYSTEM_HANDLER_MAX_NR_BUCKETS );
	m_timingCounter = 0;
}

obs::SystemHandler::~SystemHandler()
{
}

void obs::SystemHandler::UpdateSystems( SystemHandlerTimeStruct timeInfo )
{
	HighresTimer timer;
	m_timings.clear();
	std::pair<std::string, double> timingData;

	for (auto& it = m_systemPool.begin(); it != m_systemPool.end(); it++)
	{
		timer.Start();
			it->second->Update(timeInfo);
		timer.Stop();

		timingData.first = it->second->GetName();
		timingData.second = timer.GetDelta();
		m_timings.push_back(timingData);
	}
}

obs::BaseSystem* obs::SystemHandler::GetSystem( std::string name )
{
	return m_systemPool[name].get();
}


void obs::SystemHandler::UpdateBuckets( int objectID, unsigned long long mask, unsigned long long oldMask )
{
	int size = m_buckets.size();
	for( int i = 0; i < size; ++i )
	{
		m_buckets[ i ].CheckMask( objectID, mask, oldMask );
	}
}


obs::EntitieBucket* obs::SystemHandler::AddBucket( EntitieBucket bucket )
{
	m_buckets.push_back( bucket );
	return &m_buckets.back();
}