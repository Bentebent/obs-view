#ifndef BASE_SYSTEM_CLASS
#define BASE_SYSTEM_CLASS

#include <string>
#include "SystemHandlerTimeStruct.hpp"
#include "EntitieBucket.hpp"

namespace obs
{
	class BaseSystem
	{

	public:
		BaseSystem() { m_name = "Unnamed system"; }

		virtual ~BaseSystem() { }

		virtual void Update( SystemHandlerTimeStruct time ) { }

		std::string GetName() { return m_name; }
	

	protected:
		void SetName( std::string name ) { m_name = name; }

	private:
		/*! Unique name of the system */
		std::string m_name;	
	};
}

#endif