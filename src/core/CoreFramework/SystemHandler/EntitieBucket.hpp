#ifndef ENTITIE_BUCKET
#define ENTITIE_BUCKET

#include <vector>

namespace obs
{
	class EntitieBucket
	{	
	public:

		EntitieBucket( unsigned long long bitmask );
		~EntitieBucket();

		void CheckMask( int objectID, unsigned long long mask, unsigned long long oldMask );

		std::vector< int > m_entities;

		std::vector< int >::iterator begin() { return m_entities.begin(); }
		std::vector< int >::iterator end() { return m_entities.end(); }

	private:
		
		unsigned long long m_includeMask;
	};
}

#endif