#ifndef SYSTEM_HANDLER_CLASS
#define SYSTEM_HANDLER_CLASS

#include <memory>
#include <map>

#include "SystemHandlerTimeStruct.hpp"
#include "EntitieBucket.hpp"
#include "BaseSystem.hpp"

#define SYSTEM_HANDLER_MAX_NR_BUCKETS 1000


namespace obs
{
	class SystemHandler
	{
	private:
	

	public:
		SystemHandler();
		~SystemHandler();

		/*!
			Will Update all systems in the update-list.
			Parameter is struct defining 
		*/
		void UpdateSystems( SystemHandlerTimeStruct timeInfo );

		/*!
			Will push A new system to the system pool and keep it as a shared_ptr. 
			Return value is false if system with the same name already exists.
		*/
		template< class T >
		bool AddSystem()
		{
			int size = m_systemPool.size();
			std::shared_ptr<BaseSystem> system = std::make_shared<T>();
			m_systemPool[ system->GetName() ] = system;
			if(size == m_systemPool.size())
				return false;
			return true;
		}

		/*! 
			Will return a BaseSystem pointer to the specified system.
		*/
		BaseSystem* GetSystem( std::string name );

		/*!
			Will check the objects mask vs the EntitiesBuckets and store or remove then.
		*/
		void UpdateBuckets( int objectID, unsigned long long mask, unsigned long long oldMask );

		/*!
			Will add a bucket and return a pointer to the bucket.
			Note max number of buckets is defined in macro SYSTEM_HANDLER_MAX_NR_BUCKETS.
		*/
		EntitieBucket* AddBucket( EntitieBucket bucket );

		void DrawTimeInfo(int x, int y);
	
	private:
		std::map< std::string, std::shared_ptr<BaseSystem> > m_systemPool;

		std::vector< EntitieBucket > m_buckets;

		std::vector< std::pair<std::string, double> > m_timings;
		int m_timingCounter;
	};
}

#endif


