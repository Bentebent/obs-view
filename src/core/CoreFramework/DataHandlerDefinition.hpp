#ifndef OBS_DATA_HANDLER_IMPLEMENTAION_HPP
#define OBS_DATA_HANDLER_IMPLEMENTAION_HPP

#include <CoreFramework/DataHandler/DataHandlerImplementation.hpp>



/* Include headers for data types here */
#include "Components/TestComponent.hpp"
#include "Components/RenderingComponent.hpp"
#include "Components/PhysicsComponent.hpp"
#include "Components/TransformationComponent.hpp"
#include "Components/LightComponent.hpp"

namespace obs
{

	typedef DataHandler<

	/* Add component classes here */
		TestComponent, 
		RenderingComponent,
		PhysicsComponent,
		TransformationComponent,
		LightComponent

	> ObscurityDataHandler;
}

#endif