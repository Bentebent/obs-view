#include <CoreFramework/Core.hpp>


#define INCLUDE_SYSTEM( systemType ) systemHandler.AddSystem< systemType >();

/* Include headers for systems here */
#include "Systems/TestSystem.hpp"
#include "Systems/Engine/ContentSystem.hpp"
#include "Systems/RenderingSystem.hpp"
#include "Systems/PhysicsSystem.hpp"
#include "Systems/LightSystem.hpp"
#include "Systems/CollisionTrackingSystem.hpp"



obs::SystemHandler obs::systemHandler;

namespace obs
{
	/*!
		Defines the systems handled by the SystemHandler.
	*/
	struct SystemHandlerInitializer
	{
		SystemHandlerInitializer()
		{	
			/* Engine backend, must be first */
			INCLUDE_SYSTEM( obs::ContentSystem );

			/* Add systems here */
			INCLUDE_SYSTEM( obs::TestSystem );
			INCLUDE_SYSTEM( obs::RenderingSystem );
			INCLUDE_SYSTEM( obs::PhysicsSystem );
			INCLUDE_SYSTEM( obs::LightSystem );
			INCLUDE_SYSTEM( obs::CollisionTrackingSystem );
			


		}
	};
}



struct Initializer
{
	Initializer()
	{
		obs::SystemHandlerInitializer();
	}

} systemInit;
