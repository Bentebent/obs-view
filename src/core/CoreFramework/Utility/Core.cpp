#include <CoreFramework/DataHandler/DataHandlerImplementation.hpp>
#include "../Core.hpp"

// instances...
obs::ObscurityDataHandler obs::dataHandler;
obs::ThreadPool obs::threadPool;
BitmaskSystem obs::bitmaskSystem;
obs::LuaInterfaceHandler obs::luaInterfaceHandler;
obs::PhysicsHandler obs::physicsHandler;
obs::WindowHandler obs::windowHandler;
obs::CameraHandler obs::cameraHandler;
obs::InputHandler obs::inputHandler;