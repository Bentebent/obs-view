#ifndef OBS_CORE_SCENEGRAPH_UTILITY
#define OBS_CORE_SCENEGRAPH_UTILITY

#include <CoreFramework/Core.hpp>
#include <glm/glm.hpp>

namespace obs
{
	void SetPosition( int entity, glm::vec3 position, obs::TransformationComponent* tcomp )
	{
		glm::vec3 delta = position - obs::TransformationComponent::GetPositionVec3( *tcomp );
		obs::dataHandler.ScenGraphOperation<glm::vec3>( []( glm::vec3 delta, int objectId )
		{
			obs::TransformationComponent* childTcomp = GET_DATA< obs::TransformationComponent >( objectId );
			childTcomp->position[0] += delta.x;
			childTcomp->position[1] += delta.y;
			childTcomp->position[2] += delta.z;

		}, delta, entity );
	}



}
#endif