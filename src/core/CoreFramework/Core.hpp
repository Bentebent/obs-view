#ifndef DATA_HANDLER_CLASS
#define DATA_HANDLER_CLASS

#include <CoreFramework/DataHandlerDefinition.hpp>
#include <CoreFramework/SystemHandler/SystemHandler.hpp>
#include <ThreadPool/Threadpool.hpp>
#include <logger/Logger.hpp>
#include <bitmask/bitmask_system.hpp>
#include <LuaFramework/LuaInterfaceHandler.hpp>
#include <BulletFramework/PhysicsHandler.hpp>
#include <CoreUtility/WindowHandler.hpp>
#include <Camera/CameraHandler.hpp>
#include <CoreUtility/InputHandler.hpp>


#define GET_SYSTEM( name_of_system_without_quotes ) (obs::name_of_system_without_quotes*)obs::systemHandler.GetSystem( #name_of_system_without_quotes );
#define GET_DATA obs::dataHandler.GetData


/* Include headers for data types here */
#include "Components/TestComponent.hpp"
#include "Components/RenderingComponent.hpp"
#include "Components/TransformationComponent.hpp"


namespace obs
{
	extern ObscurityDataHandler dataHandler; 
	extern SystemHandler systemHandler;
	extern ThreadPool threadPool;
	extern BitmaskSystem bitmaskSystem;
	extern LuaInterfaceHandler luaInterfaceHandler;
	extern PhysicsHandler physicsHandler;
	extern WindowHandler windowHandler;
	extern CameraHandler cameraHandler;
	extern InputHandler inputHandler;
}
#endif