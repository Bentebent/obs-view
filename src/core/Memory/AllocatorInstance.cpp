#include <CoreFramework\AllocatorInstances.hpp>





obs::AllocatorInstance* obs::frameAllocator;

obs::AllocatorInstance obs::frameAllocatorA(	FRAME_MEMORY_SIZE		);
obs::AllocatorInstance obs::frameAllocatorB(	FRAME_MEMORY_SIZE		);
obs::AllocatorInstance obs::programAllocator(	PROGRAM_MEMORY_SIZE		);


obs::AllocatorInstance::AllocatorInstance( unsigned int frameMemory )  
	: m_allocator( nullptr, 0 ), m_heap( m_allocator )
{
	m_memorySize = frameMemory;
	m_memory = new std::int8_t[ frameMemory ];
	m_allocator = obs::LinearAllocator( m_memory, frameMemory );
	m_heap = obs::LinearHeap( m_allocator );
	this;
}

obs::AllocatorInstance::~AllocatorInstance()
{
	m_heap.Rewind();
	m_allocator.Rewind();

	if( m_memory )
	{
		delete m_memory;
		m_memory = nullptr;
	}
}

void obs::AllocatorInstance::RewindMemory()
{
	m_heap.Rewind();
	m_allocator.Rewind();
}