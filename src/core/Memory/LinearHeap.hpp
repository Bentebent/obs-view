#ifndef obs_TEMP_HEAP_HPP
#define obs_TEMP_HEAP_HPP

#include <Memory/Finalizer.hpp>
#include <Memory/LinearAllocator.hpp>

#include <cstring>
#include <type_traits>

namespace obs
{
/*!
	Memory manager for user defined life time allocations, all objects that are allocated with the LinearHeap are invalidated 
	on call to Rewind. Objects allocated with AllocateObject have their destructor called on removal.
*/
class LinearHeap
{
	public:
		/*!
			Constructor for LinearHeap.
			\param allocator The linear allocator that is used to allocate memory.
		*/
		explicit LinearHeap(obs::LinearAllocator& allocator);
       
		/*!
			Allocates a new Plain Old Data structure, destructors will not be called on removal for allocations by this function.
		*/
		template<typename T> T* NewPOD()
		{
			static_assert(std::is_pod<T>::value, "Allocation is not of plain old data type");
			return new (m_allocator.Allocate(sizeof(T))) T;
		}

		/*!
			Allocates a new object, destructor will be called on remvoval of the allocation. 
		*/
		template<typename T, typename... Args> T* NewObject(Args... args)
		{
			T* object = new (m_allocator.Allocate(sizeof(T))) T( args... );
			obs::Finalizer* destructors = new (m_allocator.Allocate(sizeof(obs::Finalizer))) obs::Finalizer;

			destructors->m_dataPointer = object;
			destructors->m_destructorCall = &DestructorCall<T>;
			destructors->m_finalizerChain = m_finalizerChain;
			m_finalizerChain = destructors;

			return object;
		}

		/*!
			Resets the internal allocator and removes all allocations, object allocations have their destructor called. 
		*/
		void Rewind();

		/*!
			Allocates a new Plain Old Data structure array with size, destructors will not be called on removal for allocations by this function.
		*/
		template<typename T> T* NewPODArray( int size )
		{
			static_assert(std::is_pod<T>::value, "Allocation is not of plain old data type");
			return new (m_allocator.Allocate(sizeof(T) * size)) T;
		}

		/*!
			Allocates a array of new objects, destructor will be called for every index on remvoval of the allocation. 
		*/
		template<typename T, typename... Args> T* NewObjectArray( int size, Args... args)
		{
			// allocate memory...
			char* objects = reinterpret_cast<char*>(m_allocator.Allocate(sizeof(T) * size));
			obs::Finalizer* destructors = new (m_allocator.Allocate(sizeof(obs::Finalizer) * size)) obs::Finalizer;

			for( int i = 0; i <size; ++i )
			{
				(*static_cast<T*>(reinterpret_cast<void*>(objects+sizeof(T)*i))) = T(args...);
				
				// fix stuff...
				(*static_cast<obs::Finalizer*>(reinterpret_cast<void*>(destructors+sizeof(obs::Finalizer)*i))).m_dataPointer = static_cast<T*>(reinterpret_cast<void*>(objects+sizeof(T)*i));
				(*static_cast<obs::Finalizer*>(reinterpret_cast<void*>(destructors+sizeof(obs::Finalizer)*i))).m_destructorCall = &DestructorCall<T>;
				(*static_cast<obs::Finalizer*>(reinterpret_cast<void*>(destructors+sizeof(obs::Finalizer)*i))).m_finalizerChain = m_finalizerChain;
				m_finalizerChain = static_cast<obs::Finalizer*>(destructors+sizeof(obs::Finalizer)*i);
			}

			return static_cast<T*>(reinterpret_cast<void*>(objects));
		}

	private:
		obs::Finalizer* AllocateWithFinalizer(size_t size);

		obs::LinearAllocator m_allocator;
		void* m_rewindPoint;
		obs::Finalizer* m_finalizerChain;
	};
}


#endif
