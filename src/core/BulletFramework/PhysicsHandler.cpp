#include "PhysicsHandler.hpp"

#include <ContentManagement/ContentManager.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionObject.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <ContentManagement/Loaders/PhysicsMeshLoader.hpp>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include <BulletFramework/Utility/BulletDebugRenderInterface.hpp>



obs::PhysicsHandler::PhysicsHandler()
{
	Destroy();
}

obs::PhysicsHandler::~PhysicsHandler()
{
	Destroy();
}

void obs::PhysicsHandler::Destroy()
{
	if( m_dynamicsWorld )
		delete m_dynamicsWorld;

	if( m_broadphase )
		delete m_broadphase;
	
	if( m_collisionConfiguration )
		delete m_collisionConfiguration;

	if( m_dispatcher )
		delete m_dispatcher;

	if( m_solver )
		delete m_solver;

	if( m_ghostPairCallback )
		delete m_ghostPairCallback;

	if( m_debugInterface )
		delete m_debugInterface;

	m_broadphase				= nullptr;
	m_collisionConfiguration	= nullptr;
	m_dispatcher				= nullptr;
	m_solver					= nullptr;
	m_ghostPairCallback			= nullptr;
	m_dynamicsWorld				= nullptr;
	m_debugInterface			= nullptr;
}



void obs::PhysicsHandler::Initialize()
{
	m_broadphase				= new btDbvtBroadphase();	
	m_collisionConfiguration	= new btDefaultCollisionConfiguration();
    m_dispatcher				= new btCollisionDispatcher(m_collisionConfiguration);
	m_solver					= new btSequentialImpulseConstraintSolver;
	m_ghostPairCallback			= new btGhostPairCallback();
	
	btGImpactCollisionAlgorithm::registerAlgorithm( m_dispatcher );

	m_dynamicsWorld = new btDiscreteDynamicsWorld( m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration );
	m_dynamicsWorld->getPairCache()->setInternalGhostPairCallback(m_ghostPairCallback);

	m_dynamicsWorld->setGravity( btVector3( 0, -9.82f, 0 ) );

	m_debugInterface = new BulletDebugRenderInterface();
	m_dynamicsWorld->setDebugDrawer( m_debugInterface );
}

void obs::PhysicsHandler::SetDebugMode( int mode )
{
	m_debugInterface->setDebugMode( mode );
}

void obs::PhysicsHandler::Step( float deltaTime )
{
	m_dynamicsWorld->stepSimulation( deltaTime, 0 );
	m_dynamicsWorld->debugDrawWorld();
}

void obs::PhysicsHandler::SetAlwaysActive( int index, bool neverSleep )
{
	if( index < 0 || index >= m_shapes.size() )
	{ return; }

	obs::ShapeContainer& shape = m_shapes[ index ];
	m_dynamicsWorld->removeRigidBody( shape.body );
	shape.body->setActivationState( DISABLE_DEACTIVATION );
	m_dynamicsWorld->addRigidBody( shape.body );
}

void obs::PhysicsHandler::SetFrictionCoef( int index, float friction )
{
	if( index < 0 || index >= m_shapes.size() )
	{ return; }

	obs::ShapeContainer& shape = m_shapes[ index ];
	shape.body->setFriction( friction );
}


void obs::PhysicsHandler::DestoryShape( int index )
{
	if( index < 0 || index >= m_shapes.size() )
	{ return; }

	obs::ShapeContainer& shape = m_shapes[ index ];

	while( shape.body->getNumConstraintRefs() )
	{
		btTypedConstraint* constraint = shape.body->getConstraintRef( 0 );
		m_dynamicsWorld->removeConstraint( constraint );
		delete constraint;
	}

	m_dynamicsWorld->removeRigidBody( shape.body );

	if( shape.body )
	{ delete shape.body; }

	if( shape.motionState )
	{ delete shape.motionState; }
	
	if( shape.shape )
	{ 
		if( shape.typeHint == obs::PhysicsShapeType::Individual )
		{
			delete shape.shape; 
		}
		else
		{
			obs::contentManager.FreeFromValue( (unsigned int)shape.shape, "bullet" );
		}
	}

	std::memset( &shape, 0, sizeof( shape ) );
	m_shapes.erase( index );
}

btRigidBody* obs::PhysicsHandler::GetRigidBody( int index )
{
	return m_shapes[ index ].body;
}

int obs::PhysicsHandler::GetIndexFromCollisonObject( btCollisionObject* collisionObject )
{
	int size = m_shapes.size();
	for( int i = 0; i < size; ++i )
	{
		if( m_shapes[ i ].body == (btRigidBody*)collisionObject )
		{
			return i;
		}
	}
	return -1;
}

void obs::PhysicsHandler::SetPositionOfStaticShape( int index, glm::vec3 position )
{
	if( index < 0 || index >= m_shapes.size() )
	{ return; }

	obs::ShapeContainer& shape = m_shapes[ index ];

	if( shape.body->getCollisionFlags() & btRigidBody::CollisionFlags::CF_STATIC_OBJECT )
	{
		int parentEntity = shape.body->m_obs_parentEntity;

		m_dynamicsWorld->removeRigidBody( shape.body );
		btTransform trans;
		shape.motionState->getWorldTransform( trans );
		trans.setOrigin( btVector3( position.x, position.y, position.z ) );
		shape.motionState->setWorldTransform( trans );

		btRigidBody::btRigidBodyConstructionInfo bodyConstructor( 0, shape.motionState, shape.shape, btVector3( 0, 0, 0 ) );
		*shape.body = btRigidBody(bodyConstructor);
		shape.body->m_obs_parentEntity = parentEntity;

		m_dynamicsWorld->addRigidBody( shape.body );
	}
}

void obs::PhysicsHandler::SetRotationOfStaticShape( int index, glm::quat rotation )
{
	if( index < 0 || index >= m_shapes.size() )
	{ return; }

	obs::ShapeContainer& shape = m_shapes[ index ];

	//if( shape.body->getCollisionFlags() & btRigidBody::CollisionFlags::CF_STATIC_OBJECT )
	{
		int parentEntity = shape.body->m_obs_parentEntity;

		m_dynamicsWorld->removeRigidBody( shape.body );

		*shape.motionState = btDefaultMotionState( btTransform( 
			btQuaternion( rotation.x, rotation.y, rotation.z, rotation.w ), 
			shape.body->getWorldTransform().getOrigin() ));
		shape.body->m_obs_parentEntity = parentEntity;

		m_dynamicsWorld->addRigidBody( shape.body );
	}
}