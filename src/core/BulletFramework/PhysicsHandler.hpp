#ifndef PHYSICS_HANDLER_HPP
#define PHYSICS_HANDLER_HPP

#include <BulletFramework/Utility/RayResult.hpp>

class btBroadphaseInterface;
class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
class btRigidBody;
class btCollisionObject;
class btGhostPairCallback;
class btTypedConstraint;

#include <BulletFramework/Utility/ShapeContainer.hpp>
#include <BulletFramework/Utility/GhostShapeContainer.hpp>
#include <glm/fwd.hpp>

#include <CoreUtility/IndexVector.hpp>
#include <string>

namespace obs
{
	class BulletDebugRenderInterface;

	class PhysicsHandler
	{
		
	private:		

	public:
		PhysicsHandler();
		~PhysicsHandler();

		void Initialize();
		void Destroy();
		void Step( float deltaTime );
		void HandleCollisionCallbacks();
		void DestoryShape( int index );

		int CreateShpereShape(		float radius, float mass,								int entity, bool ghostObject = false );
		int CreateBoxShape(			float width, float length, float height, float mass,	int entity, bool ghostObject = false );
		int CreateCylinderShape(	float radius, float height, float mass,					int entity, bool ghostObject = false );
		int CreateCapsuleShape(		float radius, float height, float mass,					int entity, bool ghostObject = false );
		int CreatePlaneShape(		glm::vec4 planeDefinition,								int entity );
		int CreateMeshShape(		std::string path, float scale, float mass,				int entity );
		
		
		void SetPositionOfStaticShape(	int index, glm::vec3 position );
		void SetRotationOfStaticShape(	int index, glm::quat rotation );
		void SetAlwaysActive(			int index, bool neverSleep );
		void SetFrictionCoef(			int index, float friction );

		void AddConstraint( btTypedConstraint* constraint );

		RayResult SendRay( glm::vec3 from, glm::vec3 dir, float langth );
		void SendRay( RayResult& data );

		btRigidBody* GetRigidBody( int index );
		int GetIndexFromCollisonObject( btCollisionObject* collisionObject );

		void SetDebugMode( int mode );

	private:
		btBroadphaseInterface*					m_broadphase;
		btDefaultCollisionConfiguration*		m_collisionConfiguration;
        btCollisionDispatcher*					m_dispatcher;
		btSequentialImpulseConstraintSolver*	m_solver;
		btDiscreteDynamicsWorld*				m_dynamicsWorld;
		btGhostPairCallback*					m_ghostPairCallback;
		BulletDebugRenderInterface*				m_debugInterface;


	private:
		IndexVector< ShapeContainer >			m_shapes;
		IndexVector< GhostShapeContainer >		m_ghosts;
	};

}

#endif