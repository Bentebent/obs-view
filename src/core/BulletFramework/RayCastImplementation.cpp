#include "PhysicsHandler.hpp"

#include <ContentManagement/ContentManager.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include "BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"
#include "BulletCollision/Gimpact/btGImpactShape.h"

#include <glm/glm.hpp>


void obs::PhysicsHandler::SendRay( obs::RayResult& data )
{
	data = SendRay( data.m_from, data.m_dir, data.m_length );
}

obs::RayResult obs::PhysicsHandler::SendRay( glm::vec3 from, glm::vec3 dir, float length )
{
	btVector3 origin( from.x, from.y, from.z );
	btVector3 direction( dir.x, dir.y, dir.z );
	btVector3 end = origin + direction * length;

	RayResult result;
	result.m_from = from;
	result.m_dir = dir;
	result.m_length = length;

	if (m_dynamicsWorld)
	{
		
		// These two are run in the bullet demo for ray casting.
		// It's unclear wether these need to be run. 
		// Note that these ray-casts will use the bullet positions rather than the 
		// current position in the transformation components. 
		// Should this be a problem feel free to enable these or implement them as a option 
		// for the function call.
			//m_dynamicsWorld->updateAabbs();				
			//m_dynamicsWorld->computeOverlappingPairs();		
	
		btCollisionWorld::ClosestRayResultCallback closestResults( origin, end );
		closestResults.m_flags |= btTriangleRaycastCallback::kF_KeepUnflippedNormal;			
		closestResults.m_flags |= btTriangleRaycastCallback::kF_UseSubSimplexConvexCastRaytest;
		closestResults.m_flags |= btTriangleRaycastCallback::kF_FilterBackfaces;
		m_dynamicsWorld->rayTest( origin, end, closestResults );

		if (closestResults.hasHit())
		{
			result.m_distanceToHit = closestResults.m_closestHitFraction * length;
			result.m_shapeHitByRay = (btCollisionObject*)closestResults.m_collisionObject;
		}
	}

	return result;
}















