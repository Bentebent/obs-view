#ifndef PHYSICS_SHAPE_STRUCT_HPP
#define PHYSICS_SHAPE_STRUCT_HPP


class btCollisionShape;
class btRigidBody;
class btMotionState;

namespace obs
{
	enum PhysicsShapeType
	{
		Individual,
		ReferenceCounted
	};

	struct ShapeContainer
	{
		PhysicsShapeType		typeHint;
		btRigidBody*			body;
		btCollisionShape*		shape;
		btMotionState*			motionState;
	};

}

#endif