#include "RayResult.hpp"

#include <gfx/GFXInterface.hpp>
#include <bullet/btBulletDynamicsCommon.h>
#include <CoreFramework/Core.hpp>

obs::RayResult::RayResult()
{
	m_distanceToHit = NO_RAY_HIT;
	m_shapeHitByRay = nullptr;
}

obs::RayResult::RayResult( glm::vec3 origin, glm::vec3 direction, float maxLength )
{
	*this = RayResult();
	m_from = origin;
	m_dir = direction; 
	m_length = maxLength;
}

bool obs::RayResult::TestRay()
{
	obs::physicsHandler.SendRay( *this );
	return HasHit();
}

bool obs::RayResult::TestRay( glm::vec3 from, glm::vec3 dir, float length )
{
	*this = obs::RayResult( from, dir, length );
	obs::physicsHandler.SendRay( *this );
	return HasHit();
}

bool obs::RayResult::HasHit()
{
	if( m_distanceToHit != NO_RAY_HIT )
		return true;
	return false;
}

void obs::RayResult::DrawDebug()
{
	DrawDebug( m_from, m_dir, m_length );
}

void obs::RayResult::DrawDebug( glm::vec3 from, glm::vec3 dir, float length )
{
	if( m_distanceToHit > NO_RAY_HIT )
	{
		gfx::debug::DrawSphere( from + dir * m_distanceToHit, 0.5f, glm::vec4( 1, 1, 0, 1 ) );
		gfx::debug::DrawLine( from, from + dir * m_distanceToHit, glm::vec4( 0, 0, 1, 1 ) );
	}
	else
	{
		gfx::debug::DrawLine( from, from + dir * length, glm::vec4( 1, 0, 0, 1 ) );
	}		
}