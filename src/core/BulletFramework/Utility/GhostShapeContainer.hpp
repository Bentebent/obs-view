#ifndef PHYSICS_GHOST_SHAPE_STRUCT_HPP
#define PHYSICS_GHOST_SHAPE_STRUCT_HPP


class btCollisionShape;
class btGhostObject;
class btMotionState;

namespace obs
{
	struct GhostShapeContainer
	{
		btGhostObject*			body;
		btCollisionShape*		shape;
		btMotionState*			motionState;
	};
}

#endif