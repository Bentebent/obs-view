#ifndef PHYSICS_RAY_RESULT_HPP
#define PHYSICS_RAY_RESULT_HPP

#include <glm/glm.hpp>

#define NO_RAY_HIT -1.0f

class btCollisionObject;

namespace obs
{
	/*!
		Object used for ray casting with the physics handler.
		Note that the result of the test is based on the bullet environment which 
		is not necessarily the same as the data in the components. This will most likely
		only differ if objects has been moved manually after stepping the bullet simulation.
	*/

	class RayResult
	{
	public:

		/*!
			Will init members to a no-hit configuration.
		*/
		RayResult();

		/*!
			Will run the default constructor and after that save the supplied values.
			\param from is the origin of the ray. 
			\param dir is the normalized direction of the ray.
			\param length is the maxium hit distance of the ray.
		*/
		RayResult( glm::vec3 from, glm::vec3 dir, float length );

		/*!
			Will call the obs::physicsHandler with the internal values.
			\return Is true if ray hit something within the distance.
		*/
		bool TestRay();
		
		/*!
			Will call the corresponding constructor with the information and 
			after that call RayResult::TestRay().
			\return Is true if ray hit something within the distance.
		*/
		bool TestRay( glm::vec3 from, glm::vec3 dir, float length );

		/*!
			\return Is true if ray hit something within the distance.
		*/
		bool HasHit();

		/*!
			Will use the supplied information to call the member overloaded function.
			\return Is true if ray hit something within the distance. Used to be able to stack prototype with RayTest and Debug call.
		*/
		void DrawDebug();

		/*!
			Will call the gfx::debug interface. 
			If the ray hit an object, a blue line will be drawn from origin to the hit
			with a yellow sphere at the hit position.
			If no hit is recorded the line goes from origin to end in a red color.
		*/
		void DrawDebug( glm::vec3 from, glm::vec3 dir, float length );


		float m_distanceToHit;
		/*!
			The origin of the Ray.
		*/
		glm::vec3 m_from;

		/*!
			The normalized direction of the ray.
		*/
		glm::vec3 m_dir;

		/*!
			Maximum length of the ray, hits further than this will not be recorded.
		*/
		float m_length;

		/*!
			The collision shape that was hit by the ray.
		*/
		btCollisionObject* m_shapeHitByRay;
	};
}

#endif