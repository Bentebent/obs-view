#include "BulletDebugRenderInterface.hpp"

#include <gfx/GFXInterface.hpp>
#include <logger/Logger.hpp>

obs::BulletDebugRenderInterface::BulletDebugRenderInterface()
{
	m_debugLevel = DebugDrawModes::DBG_NoDebug;
}

void obs::BulletDebugRenderInterface::drawLine( const btVector3& from, const btVector3& to, const btVector3& color )
{
	glm::vec3 start, stop;
	for( int i = 0; i < 3; i++ )
		start[i] = from[i];
	for( int i = 0; i < 3; i++ )
		stop[i] = to[i];
	
	gfx::debug::DrawLine( start, stop, glm::vec4( color.x(), color.y(), color.z(), 1.0f ) );
}

void obs::BulletDebugRenderInterface::drawContactPoint( const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color )
{
	// left empty for now.
}

void obs::BulletDebugRenderInterface::drawAabb(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
	glm::vec3 start, stop;
	for( int i = 0; i < 3; i++ )
		start[i] = from[i];
	for( int i = 0; i < 3; i++ )
		stop[i] = to[i];

	gfx::debug::DrawAABB( start, stop, glm::vec4( color.x(), color.y(), color.z(), 1.0f ) ); 
}

void obs::BulletDebugRenderInterface::drawCapsule( btScalar radius, btScalar halfHeight, int upAxis, const btTransform &transform, const btVector3 &color )
{
	glm::vec3 a = glm::vec3( 0, -halfHeight, 0 );
	glm::vec3 b = glm::vec3( 0, halfHeight, 0 );
	glm::quat rot;
	btQuaternion rotation = transform.getRotation();
	for( int i = 0; i < 4; i++ )
		rot[i] = rotation[i];

	a = rot * a;
	b = rot * b;

	glm::vec4 colour = glm::vec4( color.x(), color.y(), color.z(), 1.0f );

	btVector3 pos = transform.getOrigin();
	glm::vec3 start;
	for( int i = 0; i < 3; i++ )
		start[i] = pos[i];

	gfx::debug::DrawSphere( start + a, radius, colour );
	gfx::debug::DrawSphere( start + b, radius, colour );


	glm::vec3 min = glm::vec3( -radius, -halfHeight, 0.0f );
	glm::vec3 max = glm::vec3( -radius, halfHeight, 0.0f );
	min = rot * min;
	max = rot * max;
	
	glm::vec3 axis = glm::normalize( max - min );	
	float numberOfLines = 6;
	for( int i = 0; i < numberOfLines; i++ )
	{
		glm::quat lineRotation = glm::angleAxis( (360.f / numberOfLines) * i, axis );
		glm::vec3 begin = lineRotation * min;
		glm::vec3 end = lineRotation * max;

		gfx::debug::DrawLine( start + begin, start + end, colour );
	}
}

void obs::BulletDebugRenderInterface::drawSphere( btScalar radius, const btTransform &transform, const btVector3 &color )
{
	btVector3 pos = transform.getOrigin();
	glm::vec3 start;
	for( int i = 0; i < 3; i++ )
		start[i] = pos[i];

	gfx::debug::DrawSphere( start, radius, glm::vec4( color.x(), color.y(), color.z(), 1.0f ) );
}

void obs::BulletDebugRenderInterface::drawSphere( const btVector3 &p, btScalar radius, const btVector3 &color )
{
	glm::vec3 start;
	for( int i = 0; i < 3; i++ )
		start[i] = p[i];

	gfx::debug::DrawSphere( start, radius, glm::vec4( color.x(), color.y(), color.z(), 1.0f ) );
}

void obs::BulletDebugRenderInterface::reportErrorWarning( const char* warningString )
{
	LOG_WARNING << "Bullet physics debug draw error; " << warningString << std::endl;
}

void obs::BulletDebugRenderInterface::draw3dText( const btVector3& location, const char* textString )
{
	// left empty for now.
}

void obs::BulletDebugRenderInterface::setDebugMode( int debugMode )
{
	m_debugLevel = (DebugDrawModes)debugMode;
}

int  obs::BulletDebugRenderInterface::getDebugMode() const 
{
	return m_debugLevel;
}