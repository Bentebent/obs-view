#ifndef PHYSICS_DEBUG_DRAW_INTERFACE_HPP
#define PHYSICS_DEBUG_DRAW_INTERFACE_HPP

#include <bullet/btBulletDynamicsCommon.h>



namespace obs
{
	
	class BulletDebugRenderInterface : public btIDebugDraw
	{
	public:
		BulletDebugRenderInterface();

		void drawLine( const btVector3& from, const btVector3& to, const btVector3& color ) override;
		void drawContactPoint( const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color ) override;
		void reportErrorWarning( const char* warningString ) override;
		void draw3dText( const btVector3& location, const char* textString ) override;
		void setDebugMode( int debugMode ) override;
		int getDebugMode() const override;

		virtual void drawAabb( const btVector3 &from, const btVector3 &to, const btVector3 &color ) override;
		virtual void drawCapsule( btScalar radius, btScalar halfHeight, int upAxis, const btTransform &transform, const btVector3 &color) override;
		virtual void drawSphere( btScalar radius, const btTransform &transform, const btVector3 &color );
		virtual void drawSphere( const btVector3 &p, btScalar radius, const btVector3 &color );

	private:

		DebugDrawModes m_debugLevel;
	};
}

#endif