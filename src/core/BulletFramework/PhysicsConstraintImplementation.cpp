#include "PhysicsHandler.hpp"

#include <ContentManagement/ContentManager.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionObject.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <ContentManagement/Loaders/PhysicsMeshLoader.hpp>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include <BulletFramework/Utility/BulletDebugRenderInterface.hpp>


void obs::PhysicsHandler::AddConstraint( btTypedConstraint* constraint )
{
	m_dynamicsWorld->addConstraint( constraint );
}