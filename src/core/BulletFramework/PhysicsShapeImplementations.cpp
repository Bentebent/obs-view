#include "PhysicsHandler.hpp"

#include <ContentManagement/ContentManager.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionObject.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <ContentManagement/Loaders/PhysicsMeshLoader.hpp>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>


int obs::PhysicsHandler::CreateShpereShape( float radius, float mass, int entity, bool ghostObject )
{
	obs::ShapeContainer sphereShape;
	std::memset( &sphereShape, 0, sizeof( sphereShape ) );
	
	sphereShape.typeHint = obs::PhysicsShapeType::Individual;
	sphereShape.shape = new btSphereShape( radius );
	sphereShape.motionState = new btDefaultMotionState( btTransform( btQuaternion(0,0,0,1), btVector3(0,0,0) ));

	btScalar objectMass = mass;
	btVector3 fallInertia(0,0,0);
	sphereShape.shape->calculateLocalInertia(objectMass,fallInertia);

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI( objectMass, sphereShape.motionState, sphereShape.shape, fallInertia );
    
	sphereShape.body = new btRigidBody(fallRigidBodyCI);

    m_dynamicsWorld->addRigidBody( sphereShape.body );

	sphereShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( sphereShape );
}

int obs::PhysicsHandler::CreateBoxShape( float width, float height, float length, float mass, int entity, bool ghostObject )
{
	obs::ShapeContainer boxShape;
	std::memset( &boxShape, 0, sizeof( boxShape ) );

	boxShape.typeHint = obs::PhysicsShapeType::Individual;
	
	boxShape.shape = new btBoxShape( btVector3( width, height, length ) * 0.5f );
	boxShape.motionState = new btDefaultMotionState( btTransform( btQuaternion(0,0,0,1), btVector3(0,0,0) ));

	btScalar objectMass = mass;
	btVector3 fallInertia(0,0,0);
	if( mass > 0 )
		boxShape.shape->calculateLocalInertia( objectMass, fallInertia );

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI( objectMass, boxShape.motionState, boxShape.shape, fallInertia );
	boxShape.body = new btRigidBody(fallRigidBodyCI);

    m_dynamicsWorld->addRigidBody( boxShape.body );

	boxShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( boxShape );
}

int obs::PhysicsHandler::CreateCylinderShape( float radius, float height, float mass, int entity, bool ghostObject )
{
	obs::ShapeContainer cylinderShape;
	std::memset( &cylinderShape, 0, sizeof( cylinderShape ) );

	cylinderShape.typeHint = obs::PhysicsShapeType::Individual;
	cylinderShape.shape = new btCapsuleShape( radius, height );
	cylinderShape.motionState = new btDefaultMotionState( btTransform( btQuaternion(0,0,0,1), btVector3(0,0,0) ));

	btScalar objectMass = mass;
	btVector3 fallInertia(0,0,0);
	if( mass > 0 )
		cylinderShape.shape->calculateLocalInertia( objectMass, fallInertia );

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI( objectMass, cylinderShape.motionState, cylinderShape.shape, fallInertia );
	cylinderShape.body = new btRigidBody(fallRigidBodyCI);

    m_dynamicsWorld->addRigidBody( cylinderShape.body );

	cylinderShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( cylinderShape );
}

int obs::PhysicsHandler::CreateCapsuleShape( float radius, float height, float mass, int entity, bool ghostObject )
{
	obs::ShapeContainer capsuleShape;
	std::memset( &capsuleShape, 0, sizeof( capsuleShape ) );

	capsuleShape.typeHint = obs::PhysicsShapeType::Individual;
	capsuleShape.shape = new btCapsuleShape( radius, height );
	capsuleShape.motionState = new btDefaultMotionState( btTransform( btQuaternion(0,0,0,1), btVector3(0,0,0) ));

	btScalar objectMass = mass;
	btVector3 fallInertia(0,0,0);
	if( mass > 0 )
		capsuleShape.shape->calculateLocalInertia( objectMass, fallInertia );

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI( objectMass, capsuleShape.motionState, capsuleShape.shape, fallInertia );
	capsuleShape.body = new btRigidBody(fallRigidBodyCI);

    m_dynamicsWorld->addRigidBody( capsuleShape.body );

	capsuleShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( capsuleShape );
}


int obs::PhysicsHandler::CreateMeshShape( std::string path, float scale, float mass, int entity )
{
	obs::ShapeContainer meshShape;
	std::memset( &meshShape, 0, sizeof( meshShape ) );

	meshShape.typeHint = obs::PhysicsShapeType::ReferenceCounted;

	obs::contentManager.Load( path, obs::ContentLoadPolicy::BLOCKING );
	obs::contentManager.WaitForOperations();

	obs::PhysicsMeshData* data = (obs::PhysicsMeshData*)obs::contentManager.GetContentData( path );
	
	meshShape.shape = data->shape;
	meshShape.motionState = new btDefaultMotionState( btTransform( btQuaternion( 0, 0, 0, 1), btVector3( 0, 0, 0 ) ));

	btVector3 localInertia( 0, 0, 0 );
	bool isDynamic = (mass != 0.f);
	if ( isDynamic )
	{
		meshShape.shape->calculateLocalInertia( mass, localInertia );
	}
	
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI( mass, meshShape.motionState, meshShape.shape, localInertia );	
	meshShape.body = new btRigidBody( groundRigidBodyCI );

	btGImpactMeshShape* trimesh = (btGImpactMeshShape*)data->shape;
	trimesh->setLocalScaling(btVector3( scale, scale, scale ));
	trimesh->updateBound();

	m_dynamicsWorld->addRigidBody( meshShape.body );

	meshShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( meshShape );
}

int obs::PhysicsHandler::CreatePlaneShape( glm::vec4 planeDefinition, int entity )
{
	obs::ShapeContainer planeShape;
	std::memset( &planeShape, 0, sizeof( planeShape ) );

	planeShape.typeHint = obs::PhysicsShapeType::Individual;
	planeShape.shape = new btStaticPlaneShape( btVector3( planeDefinition.x, planeDefinition.y, planeDefinition.z ), planeDefinition.w );
	planeShape.motionState = new btDefaultMotionState( btTransform( btQuaternion(0,0,0,1), btVector3(0,0,0) ));

	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI( 0, planeShape.motionState, planeShape.shape, btVector3( 0,0,0 ) );
	
	planeShape.body = new btRigidBody( groundRigidBodyCI );
	m_dynamicsWorld->addRigidBody( planeShape.body );

	planeShape.body->m_obs_parentEntity = entity;

	return m_shapes.push_back( planeShape );
}