#include "PhysicsHandler.hpp"

#include <ContentManagement/ContentManager.hpp>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionObject.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <ContentManagement/Loaders/PhysicsMeshLoader.hpp>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include <logger/Logger.hpp>

#include <CoreFramework/Core.hpp>


void obs::PhysicsHandler::HandleCollisionCallbacks()
{
	int numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; i++ )
	{
		btPersistentManifold* contactManifold =  m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal( i );
		btCollisionObject* obA = (btCollisionObject*)( contactManifold->getBody0() );
		btCollisionObject* obB = (btCollisionObject*)( contactManifold->getBody1() );

		obs::PhysicsComponent* pc = GET_DATA<obs::PhysicsComponent>( obA->m_obs_parentEntity );
		if( pc != nullptr )
		{
			pc->nrCollisions++;
		}

		pc = GET_DATA<obs::PhysicsComponent>( obB->m_obs_parentEntity );
		if( pc != nullptr )
		{
			pc->nrCollisions++;
		}
	}	
}






/*
	int numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; i++ )
	{
		btPersistentManifold* contactManifold =  m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal( i );
		
		btCollisionObject* obA = (btCollisionObject*)( contactManifold->getBody0() );
		btCollisionObject* obB = (btCollisionObject*)( contactManifold->getBody1() );

		int numContacts = contactManifold->getNumContacts();
		for (int j=0;j<numContacts;j++)
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance()<0.f)
			{
				const btVector3& ptA = pt.getPositionWorldOnA();
				const btVector3& ptB = pt.getPositionWorldOnB();
				const btVector3& normalOnB = pt.m_normalWorldOnB;

				//LOG_DEBUG << "collision at " << ptA.x() << " " << ptA.y() << " " << ptA.z() << std::endl;

			}
		}
	}
	*/



	//int size = m_ghosts.size();
	//for( int i = 0; i < size; i++ )
	//{
	//	btGhostObject* ghostObject = m_ghosts[i].body;
	//
	//
	//
	//	btManifoldArray   manifoldArray;
	//	//btBroadphasePairArray& pairArray = ghostObject->getOverlappingPairCache()->getOverlappingPairArray();
	//	btBroadphasePairArray& pairArray = m_broadphase->getOverlappingPairCache()->getOverlappingPairArray();
	//	int numPairs = pairArray.size();
	//
	//	for (int i=0;i<numPairs;i++)
	//	{
	//		manifoldArray.clear();
	//
	//		const btBroadphasePair& pair = pairArray[i];
	//
	//		//unless we manually perform collision detection on this pair, the contacts are in the dynamics world paircache:
	//		btBroadphasePair* collisionPair = m_dynamicsWorld->getPairCache()->findPair( pair.m_pProxy0, pair.m_pProxy1 );
	//		if( !collisionPair )
	//			continue;
	//
	//		if ( collisionPair->m_algorithm )
	//			collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);
	//
	//		for ( int j = 0; j < manifoldArray.size(); j++ )
	//		{
	//			btPersistentManifold* manifold = manifoldArray[j];
	//			btScalar directionSign = manifold->getBody0() == ghostObject ? btScalar(-1.0) : btScalar(1.0);
	//			for ( int p = 0; p < manifold->getNumContacts(); p++ )
	//			{
	//				const btManifoldPoint&pt = manifold->getContactPoint(p);
	//				if ( pt.getDistance()<0.f )
	//				{
	//					const btVector3& ptA = pt.getPositionWorldOnA();
	//					const btVector3& ptB = pt.getPositionWorldOnB();
	//					const btVector3& normalOnB = pt.m_normalWorldOnB;
	//	
	//					/// work here
	//
	//					int o = 0;
	//				}
	//			}
	//		}
	//	}
	//}