#include "CollisionTrackingSystem.hpp"

#include <CoreFramework/Core.hpp>

#include <CoreFramework/AllocatorInstances.hpp>

#include <gfx/GFXInterface.hpp>
#include <gfx/GFXInstanceData.hpp>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

#include <BulletFramework/PhysicsHandler.hpp>

#include <bullet/btBulletDynamicsCommon.h>


namespace obs
{
	CollisionTrackingSystem::CollisionTrackingSystem()
	{
		SetName("CollisionTracking");
		m_bucket = systemHandler.AddBucket( dataHandler.GetAspect< TransformationComponent, PhysicsComponent >());
	}

	CollisionTrackingSystem::~CollisionTrackingSystem()
	{
	}


	void CollisionTrackingSystem::Update(SystemHandlerTimeStruct time)
	{
		for (std::vector<int>::iterator it = m_bucket->begin(); it != m_bucket->end(); ++it)
		{
			obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( *it );
			pc->nrCollisions = 0;
		}

		obs::physicsHandler.HandleCollisionCallbacks();
	}
}