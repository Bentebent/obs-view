#include "PhysicsSystem.hpp"

#include <CoreFramework/Core.hpp>

#include <CoreFramework/AllocatorInstances.hpp>

#include <gfx/GFXInterface.hpp>
#include <gfx/GFXInstanceData.hpp>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

#include <BulletFramework/PhysicsHandler.hpp>

#include <bullet/btBulletDynamicsCommon.h>


namespace obs
{
	PhysicsSystem::PhysicsSystem()
	{
		SetName("Physics");
		m_bucket = systemHandler.AddBucket( dataHandler.GetAspect< TransformationComponent, PhysicsComponent >());
	}

	PhysicsSystem::~PhysicsSystem()
	{
	}


	void PhysicsSystem::Update(SystemHandlerTimeStruct time)
	{
		for (std::vector<int>::iterator it = m_bucket->begin(); it != m_bucket->end(); ++it)
		{
			obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( *it );
			if( pc->shapeIndex >= 0 )
			{
				obs::TransformationComponent* tc = GET_DATA< obs::TransformationComponent >( *it );
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );
				
				if( (body->getCollisionFlags() & btCollisionObject::CF_NO_CONTACT_RESPONSE) == 0 )
				{
					btTransform btTrans = body->getCenterOfMassTransform();
					btTrans.setOrigin( btVector3( tc->position[0], tc->position[1], tc->position[2]) );
					btTrans.setRotation( btQuaternion( tc->rotation[0], tc->rotation[1], tc->rotation[2], tc->rotation[3] ) );
					body->setCenterOfMassTransform( btTrans );
					body->getMotionState()->setWorldTransform( btTrans );
				}
			}
		}

		obs::physicsHandler.Step( time.delta );


		for (std::vector<int>::iterator it = m_bucket->begin(); it != m_bucket->end(); ++it)
		{
			obs::PhysicsComponent* pc = GET_DATA< obs::PhysicsComponent >( *it );
			if( pc->shapeIndex >= 0 )
			{
				obs::TransformationComponent* tc = GET_DATA< obs::TransformationComponent >( *it );
				btRigidBody* body = obs::physicsHandler.GetRigidBody( pc->shapeIndex );

				btTransform btTrans;
				body->getMotionState()->getWorldTransform( btTrans );

				btVector3 vec = btTrans.getOrigin();
				tc->position[0] = vec.getX();
				tc->position[1] = vec.getY();
				tc->position[2] = vec.getZ();

				btQuaternion quat = btTrans.getRotation();
				tc->rotation[0] = quat.getX();
				tc->rotation[1] = quat.getY();
				tc->rotation[2] = quat.getZ();
				tc->rotation[3] = quat.getW();
			}
		}
	}
}