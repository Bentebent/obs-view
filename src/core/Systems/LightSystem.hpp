#ifndef SRC_CORE_SYSTEMS_LIGHTSYSTEM_HPP
#define SRC_CORE_SYSTEMS_LIGHTSYSTEM_HPP

#include <CoreFramework/SystemHandler/BaseSystem.hpp>
#include <bitmask/dynamic_bitmask.hpp>
#include <bitmask/bitmask_system.hpp>

namespace obs
{
	class LightSystem : public BaseSystem
	{
	public:
		LightSystem();
		~LightSystem();

		void Update(SystemHandlerTimeStruct time) override;

	private:
		EntitieBucket* m_bucket;
		BitmaskSystem m_bitmaskSystem;
	};
}

#endif