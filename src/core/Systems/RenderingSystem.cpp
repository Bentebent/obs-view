#include "RenderingSystem.hpp"

#include <CoreFramework/Core.hpp>

#include <CoreFramework/AllocatorInstances.hpp>

#include <gfx/GFXInterface.hpp>
#include <gfx/GFXInstanceData.hpp>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

namespace obs
{
	RenderingSystem::RenderingSystem()
	{
		SetName("Rendering");
		m_bucket = systemHandler.AddBucket(dataHandler.GetAspect<RenderingComponent, TransformationComponent>());
	}

	RenderingSystem::~RenderingSystem()
	{

	}


	void RenderingSystem::Update(SystemHandlerTimeStruct time)
	{
		glm::quat rot;
		glm::mat4 rotationMatrix;
		glm::mat4 translationMatrix;
		glm::mat4 scaleMatrix;
		int foo;
		for (std::vector<int>::iterator it = m_bucket->begin(); it != m_bucket->end(); ++it)
		{
			RenderingComponent* rec = dataHandler.GetData<RenderingComponent>(*it);

			if (rec->renderMe)
			{
				TransformationComponent* tc = GET_DATA<TransformationComponent>(*it);

				rot = TransformationComponent::GetRotationQuat(tc->rotation);
				rotationMatrix = glm::toMat4(rot); 
				translationMatrix = glm::translate(glm::mat4(1.0f), TransformationComponent::GetPositionVec3(*tc));
				scaleMatrix = glm::scale(glm::vec3(tc->scale[0], tc->scale[1], tc->scale[2]));

				gfx::InstanceData* instanceData = POD_CAST_FRAME_ALLOC(gfx::InstanceData);
				instanceData->prevModelMatrix = glm::make_mat4(tc->prevModelMatrix);
				instanceData->modelMatrix = translationMatrix * rotationMatrix * scaleMatrix;

				memcpy(tc->prevModelMatrix, glm::value_ptr(instanceData->modelMatrix), sizeof(instanceData->modelMatrix));

				bool same = instanceData->modelMatrix == instanceData->prevModelMatrix;

				gfx::Execute(rec->bitmask, instanceData);
			}
		}
	}
}