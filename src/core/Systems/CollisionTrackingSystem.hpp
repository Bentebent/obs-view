#ifndef SRC_CORE_SYSTEMS_COLLISION_TRACKING_HPP
#define SRC_CORE_SYSTEMS_COLLISION_TRACKING_HPP

#include <CoreFramework/SystemHandler/BaseSystem.hpp>

namespace obs
{
	class CollisionTrackingSystem : public BaseSystem
	{
	public:
		CollisionTrackingSystem();
		~CollisionTrackingSystem() override;

		void Update( SystemHandlerTimeStruct time ) override;



	private:
		EntitieBucket* m_bucket;
	};

}

#endif