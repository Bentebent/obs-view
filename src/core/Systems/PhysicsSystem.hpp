#ifndef SRC_CORE_SYSTEMS_PHYSICSSYSTEM_HPP
#define SRC_CORE_SYSTEMS_PHYSICSSYSTEM_HPP

#include <CoreFramework/SystemHandler/BaseSystem.hpp>

namespace obs
{
	class PhysicsSystem : public BaseSystem
	{
	public:
		PhysicsSystem();
		~PhysicsSystem() override;

		void Update(SystemHandlerTimeStruct time) override;



	private:
		EntitieBucket* m_bucket;
	};

}

#endif