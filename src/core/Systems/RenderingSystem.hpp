#ifndef SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP
#define SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP

#include <CoreFramework/SystemHandler/BaseSystem.hpp>

namespace obs
{
	class RenderingSystem : public BaseSystem
	{
	public:
		RenderingSystem();
		~RenderingSystem() override;

		void Update(SystemHandlerTimeStruct time) override;



	private:
		EntitieBucket* m_bucket;
	};

}

#endif