#ifndef SYSTEMS_TEST_SYSTEM
#define SYSTEMS_TEST_SYSTEM

#include <CoreFramework/SystemHandler/BaseSystem.hpp>

namespace obs
{

	class TestSystem : public BaseSystem
	{
	private:





	public: 
		TestSystem();
		~TestSystem() override;

		void Update(  SystemHandlerTimeStruct time  ) override;


	
	private:

		EntitieBucket* m_bucket;

	};

}

#endif