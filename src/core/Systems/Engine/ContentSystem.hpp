#ifndef SYSTEMS_ENGINE_CONTENT_SYSTEM
#define SYSTEMS_ENGINE_CONTENT_SYSTEM

#include <CoreFramework/SystemHandler/BaseSystem.hpp>

namespace obs
{

	class ContentSystem : public BaseSystem
	{
	private:
		

	public: 
		ContentSystem();
		~ContentSystem() override;

		void Update(  SystemHandlerTimeStruct time  ) override;
	
	private:

	};

}

#endif