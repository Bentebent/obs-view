#include "ContentSystem.hpp"

#include <CoreFramework/Core.hpp>
#include <iostream>
#include <Components/TestComponent.hpp>
#include <ContentManagement/ContentManager.hpp>

obs::ContentSystem::ContentSystem()
{
	SetName( "Content" );
}

obs::ContentSystem::~ContentSystem()
{
}

void obs::ContentSystem::Update(  SystemHandlerTimeStruct time  )
{
	contentManager.HandleEvents();
}