#include "LightSystem.hpp"

#include <CoreFramework/Core.hpp>
#include <CoreFramework/AllocatorInstances.hpp>

#include <gfx/GFXInterface.hpp>
#include <gfx/GFXLightData.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>



namespace obs
{
	LightSystem::LightSystem()
	{
		SetName("Light");
		m_bitmaskSystem.Init();
		m_bucket = systemHandler.AddBucket(dataHandler.GetAspect<LightComponent, TransformationComponent>());
	}

	LightSystem::~LightSystem()
	{

	}

	void LightSystem::Update(SystemHandlerTimeStruct time)
	{
		glm::quat rot;
		glm::mat4 rotationMatrix;
		glm::mat4 translationMatrix;
		glm::mat4 scaleMatrix;

		for (std::vector<int>::iterator it = m_bucket->begin(); it != m_bucket->end(); ++it)
		{
			TransformationComponent* tc = GET_DATA<TransformationComponent>(*it);
			LightComponent* lc				= GET_DATA<LightComponent>(*it);

			gfx::LightData* lightData = POD_CAST_FRAME_ALLOC(gfx::LightData);
			lightData->position			= glm::vec3(0.0f);
			lightData->color			= glm::vec3(0.0f);
			lightData->spec_color		= glm::vec3(0.0f);
			lightData->orientation		= glm::vec3(0.0f);
			lightData->radius_length	= 0.0f;
			lightData->spot_penumbra	= 0.0f;
			lightData->spot_angle		= 0.0f;
			lightData->intensity		= lc->intensity;
			
			if (lightData->intensity <= 0.0f)
				continue;

			unsigned int lightType = m_bitmaskSystem.Decode(lc->bitmask, MaskType::MESH);

			switch (lightType)
			{
				case gfx::LIGHT_TYPES::POINT_SHADOW:
				case gfx::LIGHT_TYPES::POINT:
					lightData->color[0] = lc->color[0];
					lightData->color[1] = lc->color[1];
					lightData->color[2] = lc->color[2];

					lightData->spec_color[0] = lc->specularColor[0];
					lightData->spec_color[1] = lc->specularColor[1];
					lightData->spec_color[2] = lc->specularColor[2];

					lightData->position = TransformationComponent::GetPositionVec3(*tc);
					lightData->radius_length = tc->scale[0];
					break;

				case gfx::LIGHT_TYPES::SPOT_SHADOW:
				case gfx::LIGHT_TYPES::SPOT:
					lightData->color[0] = lc->color[0];
					lightData->color[1] = lc->color[1];
					lightData->color[2] = lc->color[2];

					lightData->spec_color[0] = lc->specularColor[0];
					lightData->spec_color[1] = lc->specularColor[1];
					lightData->spec_color[2] = lc->specularColor[2];

					lightData->spot_penumbra	= lc->lightSpecific.spotlight.penumbraAngle;
					lightData->spot_angle		= lc->lightSpecific.spotlight.angle;

					lightData->orientation = glm::vec3(
						TransformationComponent::GetRotationQuat(tc->rotation).x,
						TransformationComponent::GetRotationQuat(tc->rotation).y,
						TransformationComponent::GetRotationQuat(tc->rotation).z);

					lightData->position = TransformationComponent::GetPositionVec3(*tc);
					lightData->radius_length = tc->scale[0];
					break;

				case gfx::LIGHT_TYPES::DIR_SHADOW:
				case gfx::LIGHT_TYPES::DIR:
					lightData->color[0] = lc->color[0];
					lightData->color[1] = lc->color[1];
					lightData->color[2] = lc->color[2];

					lightData->spec_color[0] = lc->specularColor[0];
					lightData->spec_color[1] = lc->specularColor[1];
					lightData->spec_color[2] = lc->specularColor[2];

					lightData->orientation = glm::vec3(
						TransformationComponent::GetRotationQuat(tc->rotation).x,
						TransformationComponent::GetRotationQuat(tc->rotation).y,
						TransformationComponent::GetRotationQuat(tc->rotation).z);
					break;

				case gfx::LIGHT_TYPES::AMBIENT:
					lightData->color[0] = lc->color[0];
					lightData->color[1] = lc->color[1];
					lightData->color[2] = lc->color[2];
					break;
			}

			gfx::Execute(lc->bitmask, (void*)lightData);
		}
	}
}