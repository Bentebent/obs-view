
CreateLight = dofile( "scripts/objects/pointLight.lua" )
objects = {}


local spaceship
local plane

function Init()
	
	
	-- create ground test plane...
	plane = eli.CreateEntity()
	eli.create_transformationComponent( plane )
	eli.setScale( plane, 100,100,100 )
	eli.create_renderingComponent( plane )
	eli.initializeRenderingComponent( plane, "content/pbr_textures/gold_cloudy.mat", "content/plane.obj" )
	eli.create_physicsComponent( plane )
	eli.addTriangleShape( plane, "content/plane.obj.bullet", 100, 0 )
	eli.setStaticShapePosition( plane, 0, 0, 0 )
	
	objects[#objects + 1 ] = entity
	
	spaceship = eli.CreateEntity()
	eli.create_transformationComponent( spaceship )
	eli.setPosition( spaceship, 0, 10, 0 )
	eli.setRotation(spaceship, 0, math.sin(6), 0, math.cos(6))
	eli.setScale( spaceship, 4,4,4 )
	eli.create_renderingComponent( spaceship )
	eli.initializeRenderingComponent( spaceship, "content/pbr_textures/gold_cloudy.mat", "content/spaceship.obj" )
	
	-- Add a light
	objects[#objects + 1 ] = CreateLight( 0, 50, 0, 1, 1, 1, 700 )
	
	-- initialize camera
	local cameraIndex = eli.getActiveCamera()
	eli.setCameraPosition( cameraIndex, 55, 55, -55 )
	eli.setCameraLookAt( cameraIndex, 7, 0, 0 )
	
	
end

-- Update function
function Update( dt, realDt )
	-- TODO: Update stuff
end