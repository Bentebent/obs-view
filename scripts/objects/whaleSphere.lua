
flipper = false

return function( positionX, positionY, positionZ )


	
	entity = {}
	entity.id = eli.CreateEntity()
	
	eli.create_transformationComponent ( entity.id )
	
	eli.setPosition( entity.id, positionX, positionY, positionZ )
	eli.setScale( entity.id, 1,1,1 )
	
	
	eli.create_physicsComponent( entity.id )
	eli.create_renderingComponent( entity.id )
	
	if not flipper then
		eli.initializeRenderingComponent( entity.id, "content/pbr_textures/gold.mat", "content/whale.obj" )
		eli.addTriangleShape( entity.id, "content/whale.obj.bullet", 1, 10 )	
	else 
		eli.initializeRenderingComponent( entity.id, "content/pbr_textures/painted_metal.mat", "content/uv_sphere.obj" )
		eli.addSphereShape( entity.id, 1, 1 )
	end
		


	flipper = not flipper	
	return entity
	
end






