


return function( positionX, positionY, positionZ )
	
	entity = eli.CreateEntity()
	
	eli.create_transformationComponent ( entity )
	
	local scale = 10.0
	
	eli.setPosition( entity, positionX, positionY, positionZ )
	eli.setScale( entity, scale, scale, scale )
	
	eli.create_physicsComponent( entity )
	eli.create_renderingComponent( entity )
	
	eli.initializeRenderingComponent( entity, 
			"content/demoContent/stageA.mat", 
			"content/demoContent/stageA.obj" )

	eli.addTriangleShape( entity, "content/demoContent/stageA.obj.bullet", scale, 0 )
	
	eli.makeKinematic( entity )
	--eli.setStaticShapePosition( entity, positionX, positionY, positionZ )
	
	
	
	return entity
end






