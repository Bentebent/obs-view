


return function( positionX, positionY, positionZ )
	
	entity = eli.CreateEntity()
	
	eli.create_transformationComponent ( entity )
	
	local scale = 1.0
	
	eli.setPosition( entity, positionX, positionY, positionZ )
	eli.setScale( entity, scale, scale, scale )
	
	eli.create_physicsComponent( entity )
	eli.create_renderingComponent( entity )
	
	eli.initializeRenderingComponent( entity, 
			"content/demoContent/planeCube.mat", 
			"content/demoContent/planeCube.obj" )

	eli.addTriangleShape( entity, "content/demoContent/planeCube.obj.bullet", 1, 0 )
	eli.setStaticShapePosition( entity, positionX, positionY, positionZ )
	
	return entity
end






