


return function( positionX, positionY, positionZ )
	
	entity = eli.CreateEntity()
	
	eli.create_transformationComponent ( entity )
	
	eli.setPosition( entity, positionX, positionY, positionZ )
	eli.setScale( entity, 1,1,1 )
	
	eli.create_physicsComponent( entity )
	eli.create_renderingComponent( entity )
	
	eli.initializeRenderingComponent( entity, 
			"content/demoContent/planeCube.mat", 
			"content/uv_sphere.obj" )

	eli.addSphereShape( entity, 1, 100 )

	
	return entity
end






