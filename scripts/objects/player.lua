


return function( positionX, positionY, positionZ )
	
	entity = eli.CreateEntity()
	
	eli.create_transformationComponent ( entity )
	
	eli.setPosition( entity, positionX, positionY, positionZ )
	eli.setScale( entity, 1,1,1 )
	
	eli.create_renderingComponent( entity )
	eli.initializeRenderingComponent( entity, 
			"content/demoContent/yoloCubeMaterial.mat", 
			"content/demoContent/yoloCube.obj" )

	eli.create_physicsComponent( entity )
	eli.addCapsuleShape( entity, 0.5, 2, 1000 )
	
	eli.setAngularFactor( entity, 0, 0, 0 )
	eli.setAlwaysActive( entity, true )
	eli.setFrictionCoef( entity, 0.5 )
	
	--eli.setRenderMe( entity, false )

	-- create foot
	footEntity = eli.CreateEntity()
	eli.create_transformationComponent ( footEntity )
	eli.setPosition( 	footEntity, 5, 5, 5 )
	eli.setScale( 		footEntity, 1,1,1 )
	eli.create_physicsComponent( footEntity )
	eli.addSphereShape( footEntity, 0.4, 1 )
	eli.setAlwaysActive( footEntity, true )
	
	eli.makeGhost( footEntity )
	eli.addSocetConstraint( entity, footEntity, 0, -1.2, 0 )
	
	return entity, footEntity
end






