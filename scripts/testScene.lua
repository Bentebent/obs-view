


CreateObject = dofile( "scripts/objects/whaleSphere.lua" )
CreateLight = dofile( "scripts/objects/pointLight.lua" )
objects = {}


local moving = {}
local spaceship

function Init()
	
	
	-- create ground test plane...
	local entity = {}	
	entity.id = eli.CreateEntity()
	
	eli.create_transformationComponent( entity.id )
	eli.setScale( entity.id, 100,100,100 )
	
	eli.create_physicsComponent( entity.id )
	eli.addTriangleShape( entity.id, "content/plane.obj.bullet", 100, 0 )
	eli.setStaticShapePosition( entity.id, 0, 0, 0 )
	--eli.addPlaneShape(entity.id, 0, 1, 0, 0)
	
	eli.create_renderingComponent( entity.id )
	eli.initializeRenderingComponent( entity.id, "content/pbr_textures/gold_cloudy.mat", "content/plane.obj" )
	
	objects[#objects + 1 ] = entity
	
	for x=0,4 do
		for y=0,4 do
			local matviewid = eli.CreateEntity()
			eli.create_transformationComponent( matviewid )
			eli.setPosition( matviewid, 45+9 * x, 0, -80+9 * y )
			eli.setScale( matviewid, 3,3,3 )
			eli.create_renderingComponent( matviewid )
			eli.initializeRenderingComponent( matviewid, "content/pbr_textures/shades/material"..(5*y+x)..".mat", "content/obscure_material.obj" )
			objects[#objects + 1 ] = matviewid
		end
	end
	
	for i=1,100 do
	moving[i] = {}
	moving[i].id = eli.CreateEntity()
	eli.create_transformationComponent( moving[i].id )
	eli.setPosition( moving[i].id, 0, 0, 0 )
	eli.setScale( moving[i].id, 1,1,1 )
	eli.create_renderingComponent( moving[i].id )
	eli.initializeRenderingComponent( moving[i].id, "content/pbr_textures/gold.mat", "content/whale.obj" )
	objects[#objects + 1 ] = moving[i]
	end
	
	spaceship = eli.CreateEntity()
	eli.create_transformationComponent( spaceship )
	eli.setPosition( spaceship, 0, 10, 0 )
	eli.setRotation(spaceship, 0, math.sin(6), 0, math.cos(6))
	eli.setScale( spaceship, 4,4,4 )
	eli.create_renderingComponent( spaceship )
	eli.initializeRenderingComponent( spaceship, "content/pbr_textures/gold_cloudy.mat", "content/spaceship.obj" )
	
	objects[#objects + 1 ] = CreateLight( 50, 50, -50, 1, 1, 1, 700 )
	
	-- create falling objects...
	--for y = 0, 1, 1 do
	--	for x = 0, 100, 1 do
	--		objects[#objects + 1 ] = CreateObject( -6 + x * 0.1 + y * 8, x * 8 + 50, 0 )
	--	end
	--end
	
	-- initialize camera
	local cameraIndex = eli.getActiveCamera()
	eli.setCameraPosition( cameraIndex, 55, 55, -55 )
	eli.setCameraLookAt( cameraIndex, 7, 0, 0 )
	
	
end

local incr = 0
local p = 2
local q = 3
function Update( dt, realDt )
	incr = incr + 0.25 * dt
	eli.setPosition( spaceship, math.sin(incr)*75, 10, math.cos(incr)*75 )
	eli.setRotation( spaceship, 0, math.sin(incr/2 + math.pi/4), 0, math.cos(incr/2 + math.pi/4))
	
	for i=1,#moving do
		local offs = (i)*((math.pi * 2) / (#moving))
		local t = math.mod(incr+offs, 2*math.pi)
		local r = math.cos(q*t) + 2
		eli.setPosition( moving[i].id, 
		r * math.cos(p*t)*20, 
		20-math.sin(q*t)*10,
		r * math.sin(p*t)*20 )
	end
end